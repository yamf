
#include <QtTest/QtTest>

#include <QFile>
#include <QDebug>
#include <QGraphicsRectItem>

#include <model/project.h>
#include <model/scene.h>
#include <model/layer.h>
#include <model/frame.h>
#include <model/object.h>

/**
 * @brief Prueba unitaria de la clase YAMF::Model::Object.
*/
class TestObject: public QObject
{
	Q_OBJECT;
	private slots:
		void initTestCase();
		void cleanupTestCase();
		void setObjectName();
		void setItem();
		
	private:
		YAMF::Model::Project *project;
		YAMF::Model::Scene *scene;
		YAMF::Model::Layer *layer;
		YAMF::Model::Frame *frame;
		YAMF::Model::Object *object;
};

/**
 * Inicializa el proyecto, la escena, el layer, el frame y el objeto grafico que se va a probar
 * dejandole el nombre de la escena por defecto "Object 1".
 */
void TestObject::initTestCase()
{
	project = new YAMF::Model::Project();
	scene = project->createScene();
	layer = scene->createLayer();
	frame = layer->createFrame();
	object = new YAMF::Model::Object(new QGraphicsRectItem(QRect(0,0,100,100)), frame);
}

/**
 * Borra los elementos de prueba.
 */
void TestObject::cleanupTestCase()
{
	delete project;
}

/**
 * Prueba la funci�n setObjectName:
 * @li cambiando el nombre del objeto grafico que se esta probando por "test_object".
 * @li compara si se ha asignado bien el nombre.
 */
void TestObject::setObjectName()
{
	object->setObjectName("test_object");
	QCOMPARE(object->objectName(), QString("test_object") );
}

/**
 * Prueba la funci�n setItem:
 * @li Creando un item.
 * @li asignadole el item a el objeto.
 * @li comprobando que el item que tiene el objeto el mismo que se la acava de asignar.
 */
void TestObject::setItem()
{
	QGraphicsRectItem *newRect = new QGraphicsRectItem(QRect(10,10,10,10));
	object->setItem(newRect);
	QCOMPARE(object->item(), newRect );
}



QTEST_MAIN(TestObject);
#include "test_object.moc"


