
#include <QtTest/QtTest>

#include <QFile>
#include <QDebug>

#include <model/project.h>
#include <model/scene.h>
#include <model/layer.h>

/**
 * @brief Prueba unitaria de la clase YAMF::Model::Scene.
*/
class TestScene: public QObject
{
	Q_OBJECT
	private slots:
		void initTestCase();
		void cleanupTestCase();
		void setSceneName();
		void setLocked();
		
		void setVisible();
		void createLayer();
		void addLayer();
		void logicalIndexOf();
		void visualIndexOf();
		void moveLayer();
		void removeLayer();
		
	private:
		YAMF::Model::Project *project;
		YAMF::Model::Scene *scene;
};

/**
 * Inicializa el proyecto y la escena que se va a probar
 * dejandole el nombre de la escena por defecto "Scene 1".
 */
void TestScene::initTestCase()
{
	project = new YAMF::Model::Project();
	scene = new YAMF::Model::Scene(project);
}

/**
 * Borra los elementos de prueba.
 */
void TestScene::cleanupTestCase()
{
	delete project;
}

/**
 * Prueba la funci�n setSceneName:
 * @li cambiando el nombre de la escena que se esta probando por "test_scene".
 * @li compara si se ha asignado bien el nombre.
 */
void TestScene::setSceneName()
{
	scene->setSceneName("test_scene");
	QCOMPARE(scene->sceneName(), QString("test_scene") );
}

/**
 * Prueba la funci�n setLocked:
 * @li bloqueando la escena que se esta probando.
 * @li verificando que su estado de bloqueo sea true.
 */
void TestScene::setLocked()
{
	scene->setLocked(true);
	QCOMPARE(scene->isLocked(), true );
}

/**
 * Prueba la funci�n setVisible:
 * @li ocultando la escena que se esta probando.
 * @li verificando que su estado de visibilidad sea true.
 */
void TestScene::setVisible()
{
	scene->setVisible(false);
	QCOMPARE(scene->isVisible(), false );
}

/**
 * Prueba la funci�n createLayer:
 * @li usando createLayer para crear un layer en la posici�n 0 y nombre "test_layer".
 * @li verificando si la escena tiene un layer y el nombre del layer creado es "test_layer".
 */
void TestScene::createLayer()
{
	YAMF::Model::Layer *layer = scene->createLayer(0, "test_layer");
	QCOMPARE(scene->layers().count(), 1);
	QCOMPARE(layer->layerName(), QString("test_layer"));
}

/**
 * Prueba la funci�n addLayer:
 * @li creando un layer.
 * @li a�adiendolo a la escena.
 * @li verificando si la escena tiene dos layer.
 */
void TestScene::addLayer()
{
	YAMF::Model::Layer *layer = new YAMF::Model::Layer(scene);
	QCOMPARE(scene->layers().count(), 1);
	scene->addLayer(layer);
	QCOMPARE(scene->layers().count(), 2);
}


/**
 * Prueba la funci�n logicalIndexOf:
 * @li creando un layer.
 * @li verificando que su indice logico dentro de la escena que estamos probando sea -1(no esta dentro de la escena).
 * @li a�adiendo el layer a la escena.
 * @li comprobando que el indice logico del layer sea 2.
 */
void TestScene::logicalIndexOf()
{
	YAMF::Model::Layer *layer = new YAMF::Model::Layer(scene);
	QCOMPARE(scene->logicalIndexOf(layer), -1);
	scene->addLayer(layer);
	QCOMPARE(scene->logicalIndexOf(layer), 2);
}

/**
 * Prueba la funci�n visualIndexOf:
 * @li creando un layer.
 * @li verificando que su indice visual dentro de la escena que estamos probando sea -1(no esta dentro de la escena).
 * @li a�adiendo el layer a la scena.
 * @li comprobando que el indice logico del layer sea 3.
 */
void TestScene::visualIndexOf()
{
	YAMF::Model::Layer *layer = new YAMF::Model::Layer(scene);
	QCOMPARE(scene->visualIndexOf(layer), -1);
	scene->addLayer(layer);
	QCOMPARE(scene->visualIndexOf(layer), 3);
}

/**
 * Prueba la funci�n moveLayer:
 * @li obteniendo los layer de las posici�nes 0 y 1.
 * @li verificando que sus indices visuales sean 0 y 1 respectivamente
 * @li moviendo el layer de la posici�n 0 a la 1.
 * @li verificando que sus indices visuales sean 1 y 0 respectivamente
 */
void TestScene::moveLayer()
{
	YAMF::Model::Layer *layer1 = scene->layer(0);
	YAMF::Model::Layer *layer2 = scene->layer(1);
	
	QCOMPARE(scene->visualIndexOf(layer1), 0);
	QCOMPARE(scene->visualIndexOf(layer2), 1);
	
	scene->moveLayer(0, 1);
	
	QCOMPARE(scene->visualIndexOf(layer1), 1);
	QCOMPARE(scene->visualIndexOf(layer2), 0);
	
	
	QCOMPARE(scene->logicalIndexOf(layer1), 0);
	QCOMPARE(scene->logicalIndexOf(layer2), 1);
	
}

/**
 * Prueba la funci�n removeLayer:
 * @li verificando que el numero de layers dentro de la escena sea 4.
 * @li eliminado el layer de la posici�n 1.
 * @li verificando que el numero de layers dentro de la escena sea 3.
 */
void TestScene::removeLayer()
{
	QCOMPARE(scene->layers().count(), 4);
	QVERIFY(scene->removeLayer(1));
	QCOMPARE(scene->layers().count(), 3);
}


QTEST_MAIN(TestScene);
#include "test_scene.moc"


