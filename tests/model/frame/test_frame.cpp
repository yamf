
#include <QtTest/QtTest>

#include <QFile>
#include <QDebug>

#include <item/rect.h>

#include <model/project.h>
#include <model/scene.h>
#include <model/layer.h>
#include <model/frame.h>

/**
 * @brief Prueba unitaria de la clase YAMF::Model::Frame.
*/
class TestFrame: public QObject
{
	Q_OBJECT
	private slots:
		void initTestCase();
		void cleanupTestCase();
		void setFrameName();
		void setLocked();
		void setVisible();
		
		void addItem();
		void logicalIndexOf();
		void visualIndexOf();
		
		void moveItem();
		void removeObjectAt();
		
	private:
		YAMF::Model::Project *project;
		YAMF::Model::Scene *scene;
		YAMF::Model::Layer *layer;
		YAMF::Model::Frame *frame;
		
};

/**
 * Inicializa el proyecto, la escena, el layer y el frame que se va a probar
 * dejandole el nombre por defecto.
 */
void TestFrame::initTestCase()
{
	project = new YAMF::Model::Project();
	scene = project->createScene();
	layer = scene->createLayer();
	frame = layer->createFrame();
}

/**
 * Borra los elementos de prueba.
 */
void TestFrame::cleanupTestCase()
{
	delete project;
}

/**
 * Prueba la funci�n setFrameName:
 * @li cambiando el nombre de la escena que se esta probando por "test_frame".
 * @li compara si se ha asignado bien el nombre.
 */
void TestFrame::setFrameName()
{
	frame->setFrameName("test_frame");
	QCOMPARE(frame->frameName(), QString("test_frame") );
}

/**
 * Prueba la funci�n setLocked:
 * @li bloqueando el frame que se esta probando.
 * @li verificando que su estado de bloqueo sea true.
 */
void TestFrame::setLocked()
{
	frame->setLocked(true);
	QCOMPARE(frame->isLocked(), true );
}

/**
 * Prueba la funci�n setVisible:
 * @li ocultando el frame que se esta probando.
 * @li verificando que su estado de visibilidad sea true.
 */
void TestFrame::setVisible()
{
	frame->setVisible(true);
	QCOMPARE(frame->isVisible(), true );
}


/**
 * Prueba la funci�n createItem:
 * @li creando una rectangulo(QGraphicsRectItem).
 * @li verificamos que el frame no tenga graficos
 * @li a�adimos el rectangulo.
 * @li verficiamos que el frame contenga un grafico.
 */
void TestFrame::addItem()
{
	YAMF::Item::Rect *rect = new YAMF::Item::Rect(QRect(0,0,100,100));
	QCOMPARE(frame->graphics().count(), 0);
	frame->addItem(rect);
	QCOMPARE(frame->graphics().count(), 1);
}


/**
 * Prueba la funci�n logicalIndexOf:
 * @li creando un item.
 * @li verificando que su indice logico dentro del frame que estamos probando sea -1(no esta dentro del frame).
 * @li a�adiendo el item a el frame.
 * @li comprobando que el indice logico del frame sea 1.
 */
void TestFrame::logicalIndexOf()
{
	YAMF::Item::Rect *rect = new YAMF::Item::Rect(QRect(0,0,100,100));
	QCOMPARE(frame->logicalIndexOf(rect), -1);
	frame->addItem(rect);
	QCOMPARE(frame->logicalIndexOf(rect), 1);
}

/**
 * Prueba la funci�n visualIndexOf:
 * @li creando un item.
 * @li verificando que su indice visual dentro del frame que estamos probando sea -1(no esta dentro del frame).
 * @li a�adiendo el item a el frame.
 * @li comprobando que el indice logico del frame sea 2.
 */
void TestFrame::visualIndexOf()
{
	YAMF::Item::Rect *rect = new YAMF::Item::Rect(QRect(0,0,100,100));
	QCOMPARE(frame->logicalIndexOf(rect), -1);
	frame->addItem(rect);
	QCOMPARE(frame->logicalIndexOf(rect), 2);
}

/**
 * Prueba la funci�n moveItem:
 * @li obteniendo los graficos de las posici�nes 0 y 1.
 * @li verificando que sus indices visuales sean 0 y 1 respectivamente.
 * @li moviendo el frame de la posici�n 0 a la 1.
 * @li verificando que sus indices visuales sean 1 y 0 respectivamente.
 */
void TestFrame::moveItem()
{
	YAMF::Model::Object *graphic1 = frame->graphic(0);
	YAMF::Model::Object *graphic2 = frame->graphic(1);
	
	QCOMPARE(frame->visualIndexOf(graphic1), 0);
	QCOMPARE(frame->visualIndexOf(graphic2), 1);
	
	frame->moveItem(0, 1);
	
	QCOMPARE(frame->visualIndexOf(graphic1), 1);
	QCOMPARE(frame->visualIndexOf(graphic2), 0);
	
	
	QCOMPARE(frame->logicalIndexOf(graphic1), 0);
	QCOMPARE(frame->logicalIndexOf(graphic2), 1);
}

/**
 * Prueba la funci�n removeObjectAt:
 * @li verificando que el numero de graficos dentro de la escena sea 3.
 * @li eliminado el grafico de la posici�n 1.
 * @li verificando que el numero de frames dentro de la escena sea 2.
 */
void TestFrame::removeObjectAt()
{
	QCOMPARE(frame->graphics().count(), 3);
	frame->removeObjectAt(1);
	QCOMPARE(frame->graphics().count(), 2);
}


QTEST_MAIN(TestFrame);
#include "test_frame.moc"


