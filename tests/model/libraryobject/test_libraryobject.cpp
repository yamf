
#include <QtTest/QtTest>

#include <QFile>
#include <QDebug>

#include <model/libraryobject.h>

/**
 * @brief Prueba unitaria de la clase YAMF::Model::LibraryObject.
*/
class TestLibraryObject: public QObject
{
	Q_OBJECT
	private slots:
		void initTestCase();
		void cleanupTestCase();
		void setType();
		void setSymbolName();
		
	private:
		YAMF::Model::LibraryObject *libraryObject;
		
};

/**
 * Inicializa el objecto de la libreria que se va a probar.
 */
void TestLibraryObject::initTestCase()
{
	libraryObject = new YAMF::Model::LibraryObject(0);
}

/**
 * Borra los elementos de prueba.
 */
void TestLibraryObject::cleanupTestCase()
{
	delete libraryObject;
}

/**
 * Prueba la funci�n setType:
 * @li asigna el tipo item a el objeto.
 * @li verficia que el tipo de el objeto sea el mismo que se le acava de asignar.
 */
void TestLibraryObject::setType()
{
	libraryObject->setType( YAMF::Model::LibraryObject::Item );
	QVERIFY( libraryObject->type() == YAMF::Model::LibraryObject::Item);
}

/**
 * Prueba la funci�n setObjectName:
 * @li cambiando el nombre del simbolo que se esta probando por "test_symbol".
 * @li compara si se ha asignado bien el nombre.
 */
void TestLibraryObject::setSymbolName()
{
	libraryObject->setSymbolName( "test_symbol" );
	QCOMPARE( libraryObject->symbolName(), QString("test_symbol"));
}


QTEST_MAIN(TestLibraryObject);
#include "test_libraryobject.moc"


