
#include <QtTest/QtTest>

#include <QFile>
#include <QDebug>

#include <model/project.h>
#include <model/scene.h>
#include <model/layer.h>

/**
 * @brief Prueba unitaria de la clase YAMF::Model::Layer.
*/
class TestLayer: public QObject
{
	Q_OBJECT
	private slots:
		void initTestCase();
		void cleanupTestCase();
		void setLayerName();
		void setLocked();
		
		void setVisible();
		void createFrame();
		void addFrame();
		void logicalIndexOf();
		void visualIndexOf();
		void moveFrame();
		void removeFrame();
		
		void expandFrame();
		
	private:
		YAMF::Model::Project *project;
		YAMF::Model::Scene *scene;
		YAMF::Model::Layer *layer;
};

/**
 * Inicializa el proyecto, la escena y el layer que se va a probar
 * dejandole el nombre por defecto.
 */
void TestLayer::initTestCase()
{
	project = new YAMF::Model::Project();
	
	scene = project->createScene();
	
	layer = scene->createLayer();
}

/**
 * Borra los elementos de prueba.
 */
void TestLayer::cleanupTestCase()
{
	delete project;
}

/**
 * Prueba la funci�n setLayerName:
 * @li cambiando el nombre de la escena que se esta probando por "test_layer".
 * @li compara si se ha asignado bien el nombre.
 */
void TestLayer::setLayerName()
{
	layer->setLayerName("test_layer");
	QCOMPARE(layer->layerName(), QString("test_layer") );
}

/**
 * Prueba la funci�n setLocked:
 * @li bloqueando el layer que se esta probando.
 * @li verificando que su estado de bloqueo sea true.
 */
void TestLayer::setLocked()
{
	layer->setLocked(true);
	QCOMPARE(layer->isLocked(), true );
}

/**
 * Prueba la funci�n setVisible:
 * @li ocultando el layer que se esta probando.
 * @li verificando que su estado de visibilidad sea true.
 */
void TestLayer::setVisible()
{
	layer->setVisible(true);
	QCOMPARE(layer->isVisible(), true );
}

/**
 * Prueba la funci�n createFrame:
 * @li usando createFrane para crear un frame en la posici�n 0 y nombre "test_layer".
 * @li verificando si el layer tiene un frame y el nombre del frame creado es "test_layer".
 */
void TestLayer::createFrame()
{
	YAMF::Model::Frame *frame = layer->createFrame(-1, "test_frame");
	QCOMPARE(layer->frames().count(), 1);
	QCOMPARE(frame->frameName(), QString("test_frame"));
}

/**
 * Prueba la funci�n addFrame:
 * @li creando un frame.
 * @li a�adiendolo a el layer.
 * @li verificando si el layer tiene dos frames.
 */
void TestLayer::addFrame()
{
	YAMF::Model::Frame *frame = new YAMF::Model::Frame(layer);
	QCOMPARE(layer->frames().count(), 1);
	layer->addFrame(frame);
	QCOMPARE(layer->frames().count(), 2);
}


/**
 * Prueba la funci�n logicalIndexOf:
 * @li creando un frame.
 * @li verificando que su indice logico dentro del layer que estamos probando sea -1(no esta dentro del layer).
 * @li a�adiendo el frame a el layer.
 * @li comprobando que el indice logico del frame sea 2.
 */
void TestLayer::logicalIndexOf()
{
	YAMF::Model::Frame *frame = new YAMF::Model::Frame(layer);
	QCOMPARE(layer->logicalIndexOf(frame), -1);
	layer->addFrame(frame);
	QCOMPARE(layer->logicalIndexOf(frame), 2);
}

/**
 * Prueba la funci�n visualIndexOf:
 * @li creando un frame.
 * @li verificando que su indice visual dentro del layer que estamos probando sea -1(no esta dentro del layer).
 * @li a�adiendo el frame a el layer.
 * @li comprobando que el indice logico del frame sea 3.
 */
void TestLayer::visualIndexOf()
{
	YAMF::Model::Frame *frame = new YAMF::Model::Frame(layer);
	QCOMPARE(layer->visualIndexOf(frame), -1);
	layer->addFrame(frame);
	QCOMPARE(layer->visualIndexOf(frame), 3);
}

/**
 * Prueba la funci�n moveFrame:
 * @li obteniendo los frames de las posici�nes 0 y 1.
 * @li verificando que sus indices visuales sean 0 y 1 respectivamente.
 * @li moviendo el frame de la posici�n 0 a la 1.
 * @li verificando que sus indices visuales sean 1 y 0 respectivamente.
 */
void TestLayer::moveFrame()
{
	YAMF::Model::Frame *frame1 = layer->frame(0);
	YAMF::Model::Frame *frame2 = layer->frame(1);
	
	QCOMPARE(layer->visualIndexOf(frame1), 0);
	QCOMPARE(layer->visualIndexOf(frame2), 1);
	
	layer->moveFrame(0, 1);
	
	QCOMPARE(layer->visualIndexOf(frame1), 1);
	QCOMPARE(layer->visualIndexOf(frame2), 0);
	
	
	QCOMPARE(layer->logicalIndexOf(frame1), 0);
	QCOMPARE(layer->logicalIndexOf(frame2), 1);
}

/**
 * Prueba la funci�n removeFrame:
 * @li verificando que el numero de frames dentro de la escena sea 4.
 * @li eliminado el frames de la posici�n 0.
 * @li verificando que el numero de frames dentro de la escena sea 3.
 */
void TestLayer::removeFrame()
{
	YAMF::Model::Frame *frame1 = layer->frame(0);
	
	QCOMPARE(layer->frames().count(), 4);
	layer->removeFrame(frame1);
	QCOMPARE(layer->frames().count(), 3);
}

/**
 * Prueba la funci�n removeFrame:
 * @li verficando que el frame de la posici�n 1 exista y este dentro de el layer,
 * @li expandiendo el frame de la posici�n 1, 10 frames.
 * @li verficando que el numerdo de frames visualemente sea 13.
 * @li veficicando que el numero de clones del frame en la posici�n 1 sea 10.
 */
void TestLayer::expandFrame()
{
	QVERIFY(layer->frame(1));
	
	QVERIFY(layer->frame(1)->logicalIndex() >= 0);
	
	QCOMPARE(layer->frames().visualIndices().count(), 3);
	layer->expandFrame(1, 10);
	QCOMPARE(layer->frames().visualIndices().count(), 13);
	QCOMPARE(layer->clones( layer->frame(1) ), 10);
}


QTEST_MAIN(TestLayer);
#include "test_layer.moc"


