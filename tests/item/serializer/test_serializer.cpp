
#include <QtTest/QtTest>

#include <QDebug>
#include <QDomDocument>
#include <QXmlStreamWriter>
#include <QXmlStreamReader>
#include <QConicalGradient>
#include <QLinearGradient>
#include <QRadialGradient>
#include <QGradient>
#include <QFont>

#include <item/serializer.h>

#include <item/rect.h>

/**
 * @brief Prueba unitaria de la clase YAMF::Item::Serializer.
*/
class TestSerializer: public QObject
{
	Q_OBJECT
	private slots:
		void initTestCase();
		void cleanupTestCase();
		void properties();
		void gradient();
		void brush();
		void pen();
		void font();
};

void TestSerializer::initTestCase()
{
}

void TestSerializer::cleanupTestCase()
{
}


/**
 * Prueba la funci�n properties:
 * @li creando un item y asignadole una matrix de tranformacion escalada 0.5 en x y 0,5 en y y en la posicion 100,100.
 * @li serializando las propiedades de el item en una cadena de texto.
 * @li cargando las propiedades que contiene la cadena de texto en un nuevo item.
 * @li comparando los las propiedades de los dos itemes.
 */
void TestSerializer::properties()
{
	YAMF::Item::Rect *rect = new YAMF::Item::Rect(QRectF(100,100,100,100));
	QMatrix matrix;
	matrix.scale(0.5,0.5);
	
	rect->setMatrix(matrix);
	
	rect->setPos(100,100);
	
	QString strPropieties = ""; 
	
	QXmlStreamWriter writer(&strPropieties);
	
	YAMF::Item::Serializer::properties(rect, &writer);
	
	YAMF::Item::Rect *newRect = new YAMF::Item::Rect;
	
	YAMF::Item::Serializer::properties(rect, &writer);
	
	YAMF::Item::Serializer::loadProperties( newRect, strPropieties);
	
	QCOMPARE(newRect->pos(), rect->pos());
	QCOMPARE(newRect->zValue(), rect->zValue());
	QCOMPARE(newRect->isEnabled(), rect->isEnabled());
	QCOMPARE(newRect->flags(), rect->flags());
	
	QCOMPARE(newRect->matrix(), rect->matrix());
	
	QCOMPARE(newRect->matrix(), matrix);
}

/**
 * Prueba la funci�n gradient:
 * para cada un de los tres tipos de gradiente se realizan los siguientes pasos.
 * @li se crea el gradiente con los datos de creaci�n.
 * @li se serializa el gradiente en una cadena de texto.
 * @li se crea un nuevo gradiente y se asignan las propiedades que contiene la cadena de texto.
 * @li se compara si las propiedades son las mismas.
 */
void TestSerializer::gradient()
{
	
	{
		QString strGradient;
		QRadialGradient *gradientTest = new QRadialGradient(QPointF(100,100), 5, QPointF(200, 200));
		
		gradientTest->setColorAt(0, Qt::red);
		gradientTest->setColorAt(0.5, Qt::blue);
		gradientTest->setColorAt(1, Qt::green);
		
		QXmlStreamWriter writer(&strGradient);
	
		YAMF::Item::Serializer::gradient(gradientTest, &writer);
		
		QXmlStreamReader reader(strGradient);
		
		reader.readNext();
		if(reader.tokenType() == QXmlStreamReader::StartDocument)
		{
			if(!reader.atEnd())
			{
				reader.readNext();
			}
		}
		
		QRadialGradient *newGradient = static_cast<QRadialGradient* >(YAMF::Item::Serializer::loadGradient(&reader));
	
		QCOMPARE(gradientTest->stops(), newGradient->stops());
		QCOMPARE(gradientTest->focalPoint(), newGradient->focalPoint());
		QCOMPARE(gradientTest->center(), newGradient->center());
		QCOMPARE(gradientTest->spread(), newGradient->spread());
	}
	
	{
		QString strGradient;
		QLinearGradient *gradientTest = new QLinearGradient(QPointF(100,100), QPointF(200,200));
		gradientTest->setColorAt(0, Qt::red);
		gradientTest->setColorAt(0.5, Qt::blue);
		gradientTest->setColorAt(1, Qt::green);
		
		QXmlStreamWriter writer(&strGradient);
		
		YAMF::Item::Serializer::gradient(gradientTest, &writer);
		QXmlStreamReader reader(strGradient);
		reader.readNext();
		if(reader.tokenType() == QXmlStreamReader::StartDocument)
		{
			if(!reader.atEnd())
			{
				reader.readNext();
			}
		}
		
		QLinearGradient *newGradient = static_cast<QLinearGradient* >(YAMF::Item::Serializer::loadGradient(&reader));
		
		QCOMPARE(gradientTest->stops(), newGradient->stops());
		QCOMPARE(gradientTest->finalStop(), newGradient->finalStop());
		QCOMPARE(gradientTest->start(), newGradient->start());
		QCOMPARE(gradientTest->spread(), newGradient->spread());
	}
	
	{
		QString strGradient;
		QConicalGradient *gradientTest = new QConicalGradient(QPointF(100,100), 5);
		gradientTest->setColorAt(0, Qt::red);
		gradientTest->setColorAt(0.5, Qt::blue);
		gradientTest->setColorAt(1, Qt::green);
		
		QXmlStreamWriter writer(&strGradient);
		
		YAMF::Item::Serializer::gradient(gradientTest, &writer);
		
		QXmlStreamReader reader(strGradient);
		reader.readNext();
		if(reader.tokenType() == QXmlStreamReader::StartDocument)
		{
			if(!reader.atEnd())
			{
				reader.readNext();
			}
		}
		
		QConicalGradient *newGradient = static_cast<QConicalGradient* >(YAMF::Item::Serializer::loadGradient(&reader));
		
		QCOMPARE(gradientTest->stops() , newGradient->stops());
		QCOMPARE(gradientTest->angle(), newGradient->angle());
		QCOMPARE(gradientTest->center(), newGradient->center());
		QCOMPARE(gradientTest->spread(), newGradient->spread());
		
	}
}

/**
 * Prueba la funci�n brush:
 * @li creando un brocha con un color rojo 100, verde 100, azul 100, alfa 100 y un pattern Qt::Dense5Pattern.
 * @li asignando una matrix de transformacion scalada 0.5 en x y 0.5 en y.
 * @li se serializa la brocha en una cadena de texto.
 * @li se crea una nueva brocha y se le asignan las propiedades que contiene la cadena de texto.
 * @li se compara que las propiedades de las dos brochas sean iguales.
 */
void TestSerializer::brush()
{
	{
		QString strBrush = "";
		QColor color(100,100,100,100);
		QBrush brTest(color, Qt::Dense5Pattern);
		
		QMatrix matrix;
		matrix.scale(0.5,0.5);
		
		brTest.setMatrix(matrix);
		
		QXmlStreamWriter writer(&strBrush);
		
		YAMF::Item::Serializer::brush(&brTest, &writer);
		
		
		QXmlStreamReader reader(strBrush);
		reader.readNext();
		if(reader.tokenType() == QXmlStreamReader::StartDocument)
		{
			if(!reader.atEnd())
			{
				reader.readNext();
			}
		}
		
		QBrush newBrush;
		YAMF::Item::Serializer::loadBrush(newBrush, &reader);
		
		QCOMPARE(newBrush.color(), color);
		
		QCOMPARE(newBrush.style(), Qt::Dense5Pattern);
		QVERIFY(newBrush.matrix() == brTest.matrix());
	}
}

/**
 * Prueba la funci�n pen:
 * @li creando una linea de contorno de color rojo, ancho 10, de estilo punto-linea.
 * @li se serializa la linea de contorno en una cadena de texto.
 * @li se crea una nueva linea de contorno y se le asignan las propiedades que contiene la cadena de texto.
 * @li se compara que las propiedades de las dos lineas de contorno sean iguales.
 */
void TestSerializer::pen()
{
	{
		QString strPen = "";
		QPen penTest(Qt::red, 10, Qt::DotLine, Qt::SquareCap, Qt::RoundJoin);
		
		QXmlStreamWriter writer(&strPen);
		
		YAMF::Item::Serializer::pen(&penTest, &writer);
		
		QXmlStreamReader reader(strPen);
		reader.readNext();
		if(reader.tokenType() == QXmlStreamReader::StartDocument)
		{
			if(!reader.atEnd())
			{
				reader.readNext();
			}
		}
		
		QPen newPen;
		YAMF::Item::Serializer::loadPen(newPen, &reader);
		
		QCOMPARE(newPen.style(), Qt::DotLine);
		QCOMPARE(newPen.capStyle(), Qt::SquareCap);
		QCOMPARE(newPen.joinStyle(), Qt::RoundJoin);
		QCOMPARE(newPen.width(), 10);
		QVERIFY(penTest == newPen);
	}
}

/**
 * Prueba la funci�n font:
 * @li creando un estilo de letra de familia "Arial", tama�o 10 e italica.
 * @li se serializa el estilo de letra en una cadena de texto.
 * @li se crea un nuevo estilo de letra y se le asignan las propiedades que contiene la cadena de texto.
 * @li se compara que las propiedades de los dos estilos sean iguales.
 */
void TestSerializer::font()
{
	QFont fontTest( "Arial", 6, QFont::Normal, true);
	QString srtFont;
	QXmlStreamWriter writer(&srtFont);
	YAMF::Item::Serializer::font(fontTest, &writer);
	
	QFont newFont;
	YAMF::Item::Serializer::loadFont(newFont, srtFont);
	
	
	QCOMPARE(newFont.pointSize(), fontTest.pointSize());
	QCOMPARE(newFont.italic(), fontTest.italic());
	QCOMPARE(newFont.family(), fontTest.family());
}


QTEST_MAIN(TestSerializer);
#include "test_serializer.moc"
