
#include <QtTest/QtTest>

#include <QDebug>
#include <QDomDocument>
#include <QXmlStreamWriter>
#include <QXmlStreamReader>
#include <QConicalGradient>
#include <QLinearGradient>
#include <QRadialGradient>
#include <QGradient>
#include <QPainterPath>
#include <QFont>

#include <item/serializer.h>

#include <item/builder.h>
#include <item/rect.h>
#include <item/ellipse.h>
#include <item/line.h>
#include <item/path.h>
#include <item/group.h>
#include <item/text.h>
#include <item/polygon.h>

/**
 * @brief Prueba unitaria de la clase YAMF::Item::Builder.
*/
class TestBuilder: public QObject
{
	Q_OBJECT
	private slots:
		void initTestCase();
		void cleanupTestCase();
		void create();
};

void TestBuilder::initTestCase()
{
}

void TestBuilder::cleanupTestCase()
{
}

/**
 * Prueba si se construyen bien todos los items a partir de su representación XML.
 */
void TestBuilder::create()
{
	YAMF::Item::Builder builder;
	QString xml = "";
	{
		YAMF::Item::Rect *rect = new YAMF::Item::Rect(QRect(10,10,10,10));
		rect->setBrush(Qt::red);
		rect->setPen(QPen(QPen(Qt::red, 10,  Qt::DashDotDotLine) ));
		
		{
			QDomDocument doc;
			doc.appendChild(rect->toXml(doc));
			
			xml = doc.toString();
		}
		
		YAMF::Item::Rect *rect2 = static_cast<YAMF::Item::Rect *>(builder.create(xml));
		
		QCOMPARE(rect2->rect(),  rect->rect() );
		QCOMPARE(rect2->brush(),  rect->brush() );
		QCOMPARE(rect2->pen(),  rect->pen() );
		
		delete rect;
		delete rect2;
	}
	
	
	{
		YAMF::Item::Ellipse *ellipse = new YAMF::Item::Ellipse(QRect(10,10,10,10));
		
		QRadialGradient gradient(QPointF(100,100), 5, QPointF(200, 200));
		gradient.setColorAt(0, Qt::red);
		gradient.setColorAt(0.5, Qt::blue);
		gradient.setColorAt(1, Qt::green);
		
		ellipse->setBrush(QBrush(gradient));
		
		{
			QDomDocument doc;
			doc.appendChild(ellipse->toXml(doc));
			
			xml = doc.toString();
		}
		
		YAMF::Item::Ellipse *ellipse2 = static_cast<YAMF::Item::Ellipse *>(builder.create(xml));//Warning
		
		QCOMPARE(ellipse2->rect(),  ellipse->rect() );
		
		delete ellipse;
		delete ellipse2;
	}
	
	{
		YAMF::Item::Line *line = new YAMF::Item::Line();
		line->setPen(QPen(QPen(Qt::red, 10,  Qt::DashDotDotLine) ));
		line->setLine(QLineF(100,100,200,200));
		{
			QDomDocument doc;
			doc.appendChild(line->toXml(doc));
			
			xml = doc.toString();
		}
		
		YAMF::Item::Line *line2 = static_cast<YAMF::Item::Line *>(builder.create(xml));
		
		QCOMPARE(line2->line(),  line->line() );
		QCOMPARE(line2->pen(),  line->pen() );
		
		delete line;
		delete line2;
	}
	
	{
		YAMF::Item::Path *path = new YAMF::Item::Path();
		QPainterPath ppath;
		ppath.lineTo(100,100);
		path->setPath(ppath);
		
		{
			QDomDocument doc;
			doc.appendChild(path->toXml(doc));
			
			xml = doc.toString();
		}
		
		YAMF::Item::Path *path2 = static_cast<YAMF::Item::Path *>(builder.create(xml));
		
		QCOMPARE(path2->path(),  path->path() );
		
		delete path;
		delete path2;
	}
	
	
	{
		YAMF::Item::Group *group = new YAMF::Item::Group();
		
		YAMF::Item::Rect *rect = new YAMF::Item::Rect(QRect(10,10,10,10));
		rect->setMatrix(QMatrix().scale(2,2));
		group->addToGroup(rect);
		YAMF::Item::Ellipse *ellipse = new YAMF::Item::Ellipse(QRect(10,10,10,10));
		group->addToGroup(ellipse);
		
		group->setMatrix(QMatrix().scale(2,2));
		
		{
			QDomDocument doc;
			doc.appendChild(group->toXml(doc));
			
			xml = doc.toString();
		}
		
		YAMF::Item::Group *group2 = static_cast<YAMF::Item::Group *>(builder.create(xml));
		
		QCOMPARE(group2->transform(),  group->transform() );
		
		QCOMPARE(group2->children().size(), 2 );
		
		delete group;
		delete group2;
	}
	
	{
		YAMF::Item::Text *text = new YAMF::Item::Text();
		QFont serifFont("Times", 10, QFont::Bold);
		text->setFont(serifFont);
		
		{
			QDomDocument doc;
			doc.appendChild(text->toXml(doc));
			
			xml = doc.toString();
		}
		
		YAMF::Item::Text *text2 = static_cast<YAMF::Item::Text *>(builder.create(xml));
		QCOMPARE(text2->toPlainText(),  text->toPlainText() );
		
		
		QCOMPARE(text2->font(), text->font());
		
		delete text;
		delete text2;
	}
	
	{
		YAMF::Item::Polygon *polygon = new YAMF::Item::Polygon();
		polygon->setRect(QRect(0,0,100,100));
		polygon->setCorners(5);
		polygon->setStart(0.5);
		
		{
			QDomDocument doc;
			doc.appendChild(polygon->toXml(doc));
			
			xml = doc.toString();
		}
		
		YAMF::Item::Polygon *polygon2 = static_cast<YAMF::Item::Polygon *>(builder.create(xml));
		
		QCOMPARE(polygon2->rect(),  polygon->rect() );
		QCOMPARE(polygon2->corners(),  polygon->corners() );
		QCOMPARE(polygon2->start(),  polygon->start() );
		
		
		delete polygon;
		delete polygon2;
	}
}


QTEST_MAIN(TestBuilder);
#include "test_builder.moc"
