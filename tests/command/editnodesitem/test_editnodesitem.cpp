
#include <QtTest/QtTest>

#include <QDebug>

#include <model/command/editnodesitem.h>
#include <model/scene.h>
#include <model/layer.h>
#include <model/frame.h>
#include <model/object.h>

#include <item/path.h>


#include <QGraphicsPathItem>
#include <QGraphicsRectItem>


class TestEditNodesItem: public QObject
{
	Q_OBJECT
	private slots:
		void initTestCase();
		void cleanupTestCase();
		void execute();
		void unexecute();
		void toXml();
		
	private:
		YAMF::Command::EditNodesItem *cmd;
		QPainterPath newPath;
		QPainterPath oldPath;
		YAMF::Item::Path* item;
		YAMF::Model::Project *project;
};

void TestEditNodesItem::initTestCase()
{
	project = new YAMF::Model::Project;
	YAMF::Model::Scene *scene = project->createScene();
	YAMF::Model::Layer *layer = scene->createLayer();
	YAMF::Model::Frame *frame = layer->createFrame();
	
	QFont font;
	oldPath.addText(QPoint(0,0),font, QString("|"));
	newPath.addText(QPoint(0,0),font, QString("||"));
	
	item = new YAMF::Item::Path();
	item->setPath(newPath);
	
	YAMF::Model::Object *object = new YAMF::Model::Object(item, frame);
	frame->addObject(object);
	
	cmd = new YAMF::Command::EditNodesItem(item, oldPath, frame );
}

void TestEditNodesItem::cleanupTestCase()
{
	delete item;
	delete cmd;
}

void TestEditNodesItem::execute()
{
	cmd->redo();
	QCOMPARE(item->path(), newPath);
}

void TestEditNodesItem::unexecute()
{
	cmd->undo();
	QCOMPARE(item->path(), oldPath);
}

void TestEditNodesItem::toXml()
{
	cmd->redo();
	
	QString result = cmd->toXml().simplified();
	
	QString expected =  QString("<editnodesitem> <information oldpath=\"M 1.890625 -6.875  L 1.890625 2.125 1.140625 2.125 1.140625 -6.875 1.890625 -6.875 \" newpath=\"M 1.890625 -6.875  L 1.890625 2.125 1.140625 2.125 1.140625 -6.875 1.890625 -6.875 M 4.890625 -6.875  L 4.890625 2.125 4.140625 2.125 4.140625 -6.875 4.890625 -6.875 \" /> <location scene=\"0\" layer=\"0\" frame=\"0\" index=\"0\" /> </editnodesitem>").simplified();
	QCOMPARE(result, expected);
}

QTEST_MAIN(TestEditNodesItem)
#include "test_editnodesitem.moc"

