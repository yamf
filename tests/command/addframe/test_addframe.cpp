
#include <QtTest/QtTest>

#include <QFile>
#include <QDebug>

#include <model/command/insertframe.h>
#include <model/scene.h>
#include <model/layer.h>
#include <model/frame.h>
#include <yamf/common/inthash.h>


class TestAddFrame: public QObject
{
	Q_OBJECT
	private slots:
		void initTestCase();
		void cleanupTestCase();
		void execute();
		void unexecute();
		void toXml();
		
	private:
		YAMF::Model::Project *project;
		YAMF::Command::InsertFrame *cmd1;
		YAMF::Command::InsertFrame *cmd2;
		YAMF::Common::IntHash<YAMF::Model::Frame *> frames;
		YAMF::Model::Frame *frame1;
		YAMF::Model::Frame *frame2;
};


void TestAddFrame::initTestCase()
{
	project = new YAMF::Model::Project;
	YAMF::Model::Scene *scene = project->createScene();
	YAMF::Model::Layer *layer = scene->createLayer();
	
	frame1 = new YAMF::Model::Frame(layer);
	frame2 = new YAMF::Model::Frame(layer);
	
	cmd1 = new YAMF::Command::InsertFrame(frame1, &frames);
	cmd2 = new YAMF::Command::InsertFrame(frame2, &frames, 0);
}

void TestAddFrame::cleanupTestCase()
{
	delete project;
	delete cmd1;
	delete cmd2;
}

void TestAddFrame::execute()
{
	cmd1->redo();
	QCOMPARE(frames.count(), 1 );
	QVERIFY( frames[0] == frame1  );
	
	cmd2->redo();
	QCOMPARE(frames.count(), 2 );
	qDebug() << frames.visualValues();
}

void TestAddFrame::unexecute()
{
	cmd2->undo();
	cmd1->undo();
	QCOMPARE(frames.count(), 0 );
	
}

void TestAddFrame::toXml()
{
	qDebug() << cmd1->toXml();
	qDebug() << cmd2->toXml();
}


QTEST_MAIN(TestAddFrame);
#include "test_addframe.moc"

