
#include <QtTest/QtTest>

#include <QFile>
#include <QDebug>
#include <QGraphicsRectItem>

#include <model/command/addobject.h>
#include <model/command/removeobject.h>
#include <model/scene.h>
#include <model/layer.h>
#include <model/frame.h>
#include <model/object.h>
#include <yamf/common/inthash.h>


class TestRemoveObject: public QObject
{
	Q_OBJECT
	private slots:
		void execute();
		void unexecute();
};

void TestRemoveObject::execute()
{
	YAMF::Model::Project *project = new YAMF::Model::Project;
	YAMF::Model::Scene *scene = new YAMF::Model::Scene(project);
	YAMF::Model::Layer *layer = new YAMF::Model::Layer(scene);
	YAMF::Model::Frame *frame = new YAMF::Model::Frame(layer);
	YAMF::Model::Object *object = new YAMF::Model::Object(new QGraphicsRectItem(QRect(0,0,100,100)), frame);
	
	YAMF::Common::IntHash<YAMF::Model::Object *> objects;
	
	YAMF::Command::AddObject addObject(object, &objects);
	addObject.redo();
	
	YAMF::Command::RemoveObject cmd( object, &objects, frame);
	cmd.redo();

	QCOMPARE(objects.count(), 0 );
	
	delete object;
	delete frame;
	delete layer;
	delete scene;
	delete project;
}

void TestRemoveObject::unexecute()
{
	YAMF::Model::Project *project = new YAMF::Model::Project;
	YAMF::Model::Scene *scene = new YAMF::Model::Scene(project);
	YAMF::Model::Layer *layer = new YAMF::Model::Layer(scene);
	YAMF::Model::Frame *frame = new YAMF::Model::Frame(layer);
	YAMF::Model::Object *object = new YAMF::Model::Object(new QGraphicsRectItem(QRect(0,0,100,100)), frame);
	
	YAMF::Common::IntHash<YAMF::Model::Object *> objects;
	
	YAMF::Command::RemoveObject cmd(object, &objects, frame);
	cmd.redo();
	cmd.undo();

	QCOMPARE(objects.count(), 1 );
	QVERIFY( objects[0] == object  );
	
	delete object;
	delete frame;
	delete layer;
	delete scene;
	delete project;
}


QTEST_MAIN(TestRemoveObject)
#include "test_removeobject.moc"

