
#include <QtTest/QtTest>

#include <QFile>
#include <QDebug>

#include <model/command/insertscene.h>
#include <model/command/removescene.h>
#include <model/scene.h>
#include <yamf/common/inthash.h>

class TestRemoveScene: public QObject
{
	Q_OBJECT
	private slots:
		void execute();
		void unexecute();
};

void TestRemoveScene::execute()
{
	YAMF::Model::Project *project = new YAMF::Model::Project;
	YAMF::Model::Scene *scene = new YAMF::Model::Scene(project);
	
	YAMF::Common::IntHash<YAMF::Model::Scene *> scenes;
	YAMF::Command::InsertScene addScene(scene, &scenes);
	addScene.redo();
	
	YAMF::Command::RemoveScene cmd(scene, &scenes);
	cmd.redo();

	QCOMPARE(scenes.count(), 0 );
	
	delete scene;
	delete project;
	
}

void TestRemoveScene::unexecute()
{
	YAMF::Model::Project *project = new YAMF::Model::Project;
	YAMF::Model::Scene *scene = new YAMF::Model::Scene(project);
	
	YAMF::Common::IntHash<YAMF::Model::Scene *> scenes;
	
	YAMF::Command::InsertScene addScene(scene, &scenes);
	addScene.redo();
	
	YAMF::Command::RemoveScene cmd(scene, &scenes);
	cmd.redo();
	cmd.undo();
	
	QCOMPARE(scenes.count(), 1 );
	
	delete scene;
	delete project;
}


QTEST_MAIN(TestRemoveScene)
#include "test_removescene.moc"


