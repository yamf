
#include <QtTest/QtTest>

#include <QFile>
#include <QDebug>

#include <model/command/insertscene.h>
#include <model/scene.h>
#include <yamf/common/inthash.h>

class TestAddScene: public QObject
{
	Q_OBJECT
	private slots:
		void execute();
		void unexecute();
};

void TestAddScene::execute()
{
	YAMF::Model::Project *project = new YAMF::Model::Project;
	YAMF::Model::Scene *scene = new YAMF::Model::Scene(project);
	
	YAMF::Common::IntHash<YAMF::Model::Scene *> scenes;
	
	YAMF::Command::InsertScene cmd(scene, &scenes);
	cmd.redo();

	QCOMPARE(scenes.count(), 1 );
	QVERIFY( scenes[0] == scene  );
	
	delete scene;
	delete project;
	
}

void TestAddScene::unexecute()
{
	YAMF::Model::Project *project = new YAMF::Model::Project;
	YAMF::Model::Scene *scene = new YAMF::Model::Scene(project);
	
	YAMF::Common::IntHash<YAMF::Model::Scene *> scenes;
	
	YAMF::Command::InsertScene cmd(scene, &scenes);
	cmd.redo();
	cmd.undo();
	
	QCOMPARE(scenes.count(), 0 );
	
	delete scene;
	delete project;
}


QTEST_MAIN(TestAddScene)
#include "test_addscene.moc"


