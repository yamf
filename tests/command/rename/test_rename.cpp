
#include <QtTest/QtTest>

#include <QFile>
#include <QDebug>

#include <dgui/commandhistory.h>

#include <yamf/model/project.h>
#include <yamf/model/scene.h>
#include <yamf/model/command/renameframe.h>


class TestRename: public QObject
{
	Q_OBJECT
	private slots:
		void execute();
		void unexecute();
		
	private:
		YAMF::Model::Project *project;

};

void TestRename::execute()
{
	YAMF::Model::Project project;
	YAMF::Model::Scene scene(&project);
	
	scene.setSceneName("new name");
	QCOMPARE(scene.sceneName(), QString("new name") );
}

void TestRename::unexecute()
{
	YAMF::Model::Project project;
	YAMF::Model::Scene scene(&project);
	QString original = scene.sceneName();
	
	scene.setSceneName("new name");
	
	//project.commandManager()->history()->undo();

	QCOMPARE(scene.sceneName(), original );
}


QTEST_MAIN(TestRename)
#include "test_rename.moc"


