
#include <QtTest/QtTest>

#include <QFile>
#include <QDebug>

#include <model/command/bringtofrontitem.h>

#include <model/scene.h>
#include <model/layer.h>
#include <model/frame.h>
#include <model/object.h>
#include <item/rect.h>

#include <yamf/common/inthash.h>

#include <item/serializer.h>

#include <QGraphicsRectItem>

class TestBringToFrontItem: public QObject
{
	Q_OBJECT
	private slots:
		void initTestCase();
		void cleanupTestCase();
		void execute();
		void unexecute();
		void toXml();
		
	private:
		YAMF::Command::BringToFrontItem *cmd;
		QList<YAMF::Model::Object *> objects;
		QGraphicsScene *photogram;
		YAMF::Model::Project *project;
};

void TestBringToFrontItem::initTestCase()
{
	photogram = new QGraphicsScene;
	project = new YAMF::Model::Project;
	YAMF::Model::Scene *scene = project->createScene();
	YAMF::Model::Layer *layer = scene->createLayer();
	
	YAMF::Model::Frame *frame = layer->createFrame();
	
	YAMF::Model::Object *object = new YAMF::Model::Object(new YAMF::Item::Rect(QRect(0,0,1000,1000)), frame);
	frame->addObject(object);
	photogram->addItem(object->item());
	objects << object;
	
	object = new YAMF::Model::Object(new YAMF::Item::Rect(QRect(0,0,100,100)), frame);
	frame->addObject(object);
	photogram->addItem(object->item());
	objects << object;
	
	object = new YAMF::Model::Object(new YAMF::Item::Rect(QRect(0,0,10,10)), frame);
	frame->addObject(object);
	photogram->addItem(object->item());
	objects << object;
	
	double count = 0;
	foreach(YAMF::Model::Object *o, frame->graphics().values())
	{
		o->item()->setZValue(count);
		count++;
	}
	cmd = new YAMF::Command::BringToFrontItem(objects[0]);
}

void TestBringToFrontItem::cleanupTestCase()
{
	delete cmd;
	
	delete photogram;
	delete project;
}

void TestBringToFrontItem::execute()
{
	cmd->redo();
	QCOMPARE( objects[0]->item()->zValue(), 3.0 );
}

void TestBringToFrontItem::unexecute()
{
	cmd->undo();
	QCOMPARE( objects[0]->item()->zValue(), 0.0 );
}

void TestBringToFrontItem::toXml()
{
	cmd->redo();
	QString result = cmd->toXml().simplified();
	QString expected =  QString("<bringtofrontitem> <location scene=\"0\" layer=\"0\" frame=\"0\" index=\"0\" /> </bringtofrontitem>").simplified();
	QCOMPARE(result, expected);
}


QTEST_MAIN(TestBringToFrontItem)
#include "test_bringtofrontitem.moc"

