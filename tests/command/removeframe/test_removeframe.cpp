
#include <QtTest/QtTest>

#include <QFile>
#include <QDebug>

#include <model/command/insertframe.h>
#include <model/command/removeframe.h>

#include <model/scene.h>
#include <model/layer.h>
#include <model/frame.h>
#include <yamf/common/inthash.h>


class TestRemoveFrame: public QObject
{
	Q_OBJECT
	private slots:
		void initTestCase();
		void cleanupTestCase();
		void execute();
		void unexecute();
		void toXml();
		
		
	private:
		YAMF::Model::Project *project;
		YAMF::Model::Frame *frame;
		YAMF::Model::Frame *frame1;
		YAMF::Command::RemoveFrame *cmd;
		
		YAMF::Common::IntHash<YAMF::Model::Frame *> frames;
};

void TestRemoveFrame::initTestCase()
{
	project = new YAMF::Model::Project;
	YAMF::Model::Scene *scene = project->createScene();
	YAMF::Model::Layer *layer = scene->createLayer();
	
	
	frame = layer->createFrame();
	frame1 = layer->createFrame();
	
	frames = layer->frames();
	
	cmd = new YAMF::Command::RemoveFrame(frame, &frames);
}

void TestRemoveFrame::cleanupTestCase()
{
	delete project;
}

void TestRemoveFrame::execute()
{
	
	QCOMPARE(frames.count(), 2 );
	cmd->redo();
	QCOMPARE(frames.count(), 1 );
}

void TestRemoveFrame::unexecute()
{
	QCOMPARE(frames.count(), 1 );
	cmd->undo();
	QCOMPARE(frames.count(), 2);
}

void TestRemoveFrame::toXml()
{
	qDebug() << cmd->toXml();
}


QTEST_MAIN(TestRemoveFrame);
#include "test_removeframe.moc"

