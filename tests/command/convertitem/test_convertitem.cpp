
#include <QtTest/QtTest>

#include <QDebug>

#include <model/command/convertitem.h>

#include <model/project.h>
#include <model/scene.h>
#include <model/layer.h>
#include <model/frame.h>
#include <model/object.h>

#include <item/rect.h>

#include <yamf/common/inthash.h>

#include <QGraphicsPathItem>
#include <QGraphicsRectItem>


class TestConvertItem: public QObject
{
	Q_OBJECT
	private slots:
		void initTestCase();
		void cleanupTestCase();
		void execute();
		void unexecute();
		void toXml();
		
	private:
		YAMF::Model::Project *project;
		YAMF::Model::Object *object;
		YAMF::Command::ConvertItem *cmd;
};

void TestConvertItem::initTestCase()
{
	project = new YAMF::Model::Project;
	YAMF::Model::Scene *scene = project->createScene();
	YAMF::Model::Layer *layer = scene->createLayer();
	YAMF::Model::Frame *frame = layer->createFrame();
	
	object = new YAMF::Model::Object(new YAMF::Item::Rect(QRect(0,0,100,100)), frame);
	frame->addObject(object);
	
	cmd = new YAMF::Command::ConvertItem(object->item(), QGraphicsPathItem::Type, frame);
}

void TestConvertItem::cleanupTestCase()
{
	delete project;
	delete cmd;
}

void TestConvertItem::execute()
{
	cmd->redo();
	QVERIFY( object->item()->type() == QGraphicsPathItem::Type );
}

void TestConvertItem::unexecute()
{
	cmd->undo();
	QVERIFY( object->item()->type() == QGraphicsRectItem::Type );
}

void TestConvertItem::toXml()
{
	cmd->redo();
	QString result = cmd->toXml().simplified();
	QString expected =  QString("<command name=\"convertitem\"><location scene=\"0\" layer=\"0\" frame=\"0\" object=\"0\"></location><body><information fromType=\"3\" toType=\"2\"/></body></command>").simplified();
	
	
	
	QCOMPARE(result, expected);
}


QTEST_MAIN(TestConvertItem);

#include "test_convertitem.moc"

