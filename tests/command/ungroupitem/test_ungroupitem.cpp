
#include <QtTest/QtTest>

#include <QFile>
#include <QDebug>

#include <model/command/groupitem.h>
#include <model/command/ungroupitem.h>
#include <model/scene.h>
#include <model/layer.h>
#include <model/frame.h>
#include <model/object.h>
#include <yamf/common/inthash.h>
#include <item/group.h>

#include <QGraphicsPathItem>
#include <QGraphicsRectItem>



class TestUngroupItem: public QObject
{
	Q_OBJECT
	private slots:
		void initTestCase();
		void cleanupTestCase();
		void execute();
		void unexecute();
		
	private:
		YAMF::Model::Project *project;
		YAMF::Model::Scene *scene;
		YAMF::Model::Layer *layer;
		YAMF::Model::Frame *frame;
		QList<QGraphicsItem *> items;
		YAMF::Command::UngroupItem *cmd;
};

void TestUngroupItem::initTestCase()
{
	project = new YAMF::Model::Project();
	scene = new YAMF::Model::Scene(project);
	layer = new YAMF::Model::Layer(scene);
	frame = new YAMF::Model::Frame(layer);
	items << new QGraphicsRectItem(QRect(0,0,100,100));
	items << new QGraphicsRectItem(QRect(0,0,10,10));
	
	foreach(QGraphicsItem *item, items)
	{
		frame->addItem(item);
	}
	
	YAMF::Command::GroupItem cmdGroup(items, frame);
	cmdGroup.redo();
	
	YAMF::Item::Group *group = dynamic_cast<YAMF::Item::Group *>(frame->item(2));
	
	cmd = new YAMF::Command::UngroupItem( group, frame);
	
}

void TestUngroupItem::cleanupTestCase()
{
	delete frame;
	delete layer;
	delete scene;
	delete project;
	delete cmd;
}

void TestUngroupItem::execute()
{
	QCOMPARE( frame->graphics().count(), 1 );
	cmd->redo();
	QCOMPARE( frame->graphics().count(), 2 );
}

void TestUngroupItem::unexecute()
{
	cmd->undo();
	QCOMPARE( frame->graphics().count(), 1 );
}


QTEST_MAIN(TestUngroupItem)
#include "test_ungroupitem.moc"

