
#include <QtTest/QtTest>

#include <QFile>
#include <QDebug>

#include <model/command/sendbackwardsitem.h>

#include <model/scene.h>
#include <model/layer.h>
#include <model/frame.h>
#include <model/object.h>
#include <item/rect.h>

#include <yamf/common/inthash.h>

#include <item/serializer.h>

#include <QGraphicsRectItem>

class TestSendBackwardsItem: public QObject
{
	Q_OBJECT
	private slots:
		void initTestCase();
		void cleanupTestCase();
		void execute();
		void unexecute();
		void toXml();
		
	private:
		YAMF::Command::SendBackwardsItem *cmd;
		QList<YAMF::Model::Object *> objects;
		QGraphicsScene *photogram;
		YAMF::Model::Project *project;
};

void TestSendBackwardsItem::initTestCase()
{
	photogram = new QGraphicsScene;
	project = new YAMF::Model::Project;
	YAMF::Model::Scene *scene = project->createScene();
	YAMF::Model::Layer *layer = scene->createLayer();
	
	YAMF::Model::Frame *frame = layer->createFrame();
	
	YAMF::Model::Object *object = new YAMF::Model::Object(new YAMF::Item::Rect(QRect(0,0,1000,1000)), frame);
	frame->addObject(object);
	photogram->addItem(object->item());
	objects << object;
	
	object = new YAMF::Model::Object(new YAMF::Item::Rect(QRect(0,0,100,100)), frame);
	frame->addObject(object);
	photogram->addItem(object->item());
	objects << object;
	
	object = new YAMF::Model::Object(new YAMF::Item::Rect(QRect(0,0,10,10)), frame);
	frame->addObject(object);
	photogram->addItem(object->item());
	objects << object;
	
	double count = 0;
	foreach(YAMF::Model::Object *o, frame->graphics().values())
	{
		o->item()->setZValue(count);
		count++;
	}
	cmd = new YAMF::Command::SendBackwardsItem(objects[2]);
}

void TestSendBackwardsItem::cleanupTestCase()
{
	delete cmd;
	
	delete photogram;
	delete project;
}

void TestSendBackwardsItem::execute()
{
	cmd->redo();
	QCOMPARE( objects[2]->item()->zValue(), 1.0 );
}

void TestSendBackwardsItem::unexecute()
{
	cmd->undo();
	QCOMPARE( objects[2]->item()->zValue(), 2.0 );
}

void TestSendBackwardsItem::toXml()
{
	cmd->redo();
	QString result = cmd->toXml().simplified();
	QString expected =  QString("<sendbackwardsitem> <location scene=\"0\" layer=\"0\" frame=\"0\" index=\"2\" /> </sendbackwardsitem>").simplified();
	QCOMPARE(result, expected);
}


QTEST_MAIN(TestSendBackwardsItem)
#include "test_sendbackwardsitem.moc"

