
#include <QtTest/QtTest>

#include <QFile>
#include <QDebug>

#include <model/command/modifyitem.h>
#include <model/scene.h>
#include <model/layer.h>
#include <model/frame.h>
#include <model/object.h>
#include <yamf/common/inthash.h>

#include <item/serializer.h>

#include <QGraphicsPathItem>
#include <QGraphicsRectItem>


class TestModifyItem: public QObject
{
	Q_OBJECT
	private slots:
		void initTestCase();
		void cleanupTestCase();
		void execute();
		void unexecute();
		
	private:
		YAMF::Model::Project *project;
		YAMF::Model::Scene *scene;
		YAMF::Model::Layer *layer;
		YAMF::Model::Frame *frame;
		YAMF::Model::Object *object;
		YAMF::Command::ModifyItem *cmd;
};

void TestModifyItem::initTestCase()
{
	project = new YAMF::Model::Project();
	scene = new YAMF::Model::Scene(project);
	layer = new YAMF::Model::Layer(scene);
	frame = new YAMF::Model::Frame(layer);
	object = new YAMF::Model::Object(new QGraphicsRectItem(QRect(0,0,100,100)), frame);
	frame->addObject(object);
	QDomDocument doc;
	
	doc.appendChild(YAMF::Item::Serializer::properties(object->item(), doc));
	
	cmd = new YAMF::Command::ModifyItem(object->item(), doc.toString(), frame);
	
	object->item()->setPos(10,0);
}

void TestModifyItem::cleanupTestCase()
{
	delete frame;
	delete layer;
	delete scene;
	delete project;
	delete cmd;
}

void TestModifyItem::execute()
{
	cmd->redo();
	QCOMPARE( object->item()->pos(), QPointF(0,0) );
}

void TestModifyItem::unexecute()
{
	cmd->undo();
	QCOMPARE( object->item()->pos(), QPointF(0,0) );
}


QTEST_MAIN(TestModifyItem)
#include "test_modifyitem.moc"

