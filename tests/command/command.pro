TEMPLATE = subdirs

CONFIG += warn_on

SUBDIRS += rename \
           addscene \
	   addlayer \
	   addframe \
	   addobject \
	   expandframe \
	   convertitem \
	   ungroupitem \
	   groupitem \
	   modifyitem \
	   editnodesitem \
	   bringforwardsitem \
	   bringtofrontitem \
	   sendbackwardsitem \
	   sendtobackitem \
	   removescene \
	   removelayer \
	   removeframe \
	   removeobject


