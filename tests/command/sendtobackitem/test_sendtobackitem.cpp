
#include <QtTest/QtTest>

#include <QFile>
#include <QDebug>

#include <model/command/sendtobackitem.h>

#include <model/scene.h>
#include <model/layer.h>
#include <model/frame.h>
#include <model/object.h>
#include <item/rect.h>

#include <yamf/common/inthash.h>

#include <item/serializer.h>

#include <QGraphicsRectItem>

class TestSendToBackItem: public QObject
{
	Q_OBJECT
	private slots:
		void initTestCase();
		void cleanupTestCase();
		void execute();
		void unexecute();
		void toXml();
		
	private:
		YAMF::Command::SendToBackItem *cmd;
		QList<YAMF::Model::Object *> objects;
		QGraphicsScene *photogram;
		YAMF::Model::Project *project;
};

void TestSendToBackItem::initTestCase()
{
	photogram = new QGraphicsScene;
	project = new YAMF::Model::Project;
	YAMF::Model::Scene *scene = project->createScene();
	YAMF::Model::Layer *layer = scene->createLayer();
	
	YAMF::Model::Frame *frame = layer->createFrame();
	
	YAMF::Model::Object *object = new YAMF::Model::Object(new YAMF::Item::Rect(QRect(0,0,1000,1000)), frame);
	frame->addObject(object);
	photogram->addItem(object->item());
	objects << object;
	
	object = new YAMF::Model::Object(new YAMF::Item::Rect(QRect(0,0,100,100)), frame);
	frame->addObject(object);
	photogram->addItem(object->item());
	objects << object;
	
	object = new YAMF::Model::Object(new YAMF::Item::Rect(QRect(0,0,10,10)), frame);
	frame->addObject(object);
	photogram->addItem(object->item());
	objects << object;
	
	double count = 0;
	foreach(YAMF::Model::Object *o, frame->graphics().values())
	{
		o->item()->setZValue(count);
		count++;
	}
	cmd = new YAMF::Command::SendToBackItem(objects[2]);
}

void TestSendToBackItem::cleanupTestCase()
{
	delete cmd;
	
	delete photogram;
	delete project;
}

void TestSendToBackItem::execute()
{
	cmd->redo();
	QCOMPARE( objects[2]->item()->zValue(), -1.0 );
}

void TestSendToBackItem::unexecute()
{
	cmd->undo();
	QCOMPARE( objects[2]->item()->zValue(), 2.0 );
}

void TestSendToBackItem::toXml()
{
	cmd->redo();
	QString result = cmd->toXml().simplified();
	QString expected =  QString("<sendtobackitem> <location scene=\"0\" layer=\"0\" frame=\"0\" index=\"2\" /> </sendtobackitem>").simplified();
	QCOMPARE(result, expected);
}


QTEST_MAIN(TestSendToBackItem)
#include "test_sendtobackitem.moc"

