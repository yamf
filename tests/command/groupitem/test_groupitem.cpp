
#include <QtTest/QtTest>

#include <QDebug>

#include <model/command/groupitem.h>
#include <model/scene.h>
#include <model/layer.h>
#include <model/frame.h>
#include <model/object.h>
#include <yamf/common/inthash.h>


#include <QGraphicsPathItem>
#include <QGraphicsRectItem>


class TestGroupItem: public QObject
{
	Q_OBJECT
	private slots:
		void initTestCase();
		void cleanupTestCase();
		void execute();
		void unexecute();
		void toXml();
		
	private:
		YAMF::Model::Project *project;
		YAMF::Model::Frame *frame;
		QList<QGraphicsItem *> items;
		YAMF::Command::GroupItem *cmd;
};

void TestGroupItem::initTestCase()
{
	project = new YAMF::Model::Project;
	YAMF::Model::Scene *scene = project->createScene();
	YAMF::Model::Layer *layer = scene->createLayer();
	frame = layer->createFrame();
	
	items << new QGraphicsRectItem(QRect(0,0,100,100));
	items << new QGraphicsRectItem(QRect(0,0,10,10));
	
	foreach(QGraphicsItem *item, items)
	{
		frame->addItem(item);
	}
	
	cmd = new YAMF::Command::GroupItem(items, frame);
	
}

void TestGroupItem::cleanupTestCase()
{
	delete project;
	delete cmd;
}

void TestGroupItem::execute()
{
	QCOMPARE( frame->graphics().count(), 2 );
	cmd->redo();
	QCOMPARE( frame->graphics().count(), 1 );
}

void TestGroupItem::unexecute()
{
	cmd->undo();
	QCOMPARE( frame->graphics().count(), 2 );
}

void TestGroupItem::toXml()
{
	qDebug() << cmd->toXml();
}


QTEST_MAIN(TestGroupItem)
#include "test_groupitem.moc"

