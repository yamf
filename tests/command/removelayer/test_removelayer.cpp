
#include <QtTest/QtTest>

#include <QFile>
#include <QDebug>

#include <model/command/removelayer.h>
#include <model/command/insertlayer.h>

#include <model/scene.h>
#include <model/layer.h>
#include <yamf/common/inthash.h>


class TestRemoveLayer: public QObject
{
	Q_OBJECT
	private slots:
		void execute();
		void unexecute();
};

void TestRemoveLayer::execute()
{
	YAMF::Model::Project *project = new YAMF::Model::Project;
	YAMF::Model::Scene *scene = new YAMF::Model::Scene(project);
	YAMF::Model::Layer *layer = new YAMF::Model::Layer(scene);
	
	YAMF::Common::IntHash<YAMF::Model::Layer *> layers;
	
	YAMF::Command::InsertLayer addLayer(layer, &layers);
	addLayer.redo();
	
	YAMF::Command::RemoveLayer cmd(layer, &layers);
	cmd.redo();

	QCOMPARE(layers.count(), 0 );
	
	delete layer;
	delete scene;
	delete project;
	
}

void TestRemoveLayer::unexecute()
{
	YAMF::Model::Project *project = new YAMF::Model::Project;
	YAMF::Model::Scene *scene = new YAMF::Model::Scene(project);
	YAMF::Model::Layer *layer = new YAMF::Model::Layer(scene);
	
	YAMF::Common::IntHash<YAMF::Model::Layer *> layers;
	
	
	YAMF::Command::InsertLayer addLayer(layer, &layers);
	addLayer.redo();
	
	YAMF::Command::RemoveLayer cmd(layer, &layers);
	cmd.redo();
	cmd.undo();

	QCOMPARE(layers.count(), 1);
	
	delete layer;
	delete scene;
	delete project;
}


QTEST_MAIN(TestRemoveLayer)
#include "test_removelayer.moc"


