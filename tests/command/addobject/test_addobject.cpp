
#include <QtTest/QtTest>

#include <QFile>
#include <QDebug>

#include <yamf/item/rect.h>

#include <yamf/model/command/addobject.h>
#include <yamf/model/scene.h>
#include <yamf/model/layer.h>
#include <yamf/model/frame.h>
#include <yamf/model/object.h>
#include <yamf/common/inthash.h>


class TestAddObject: public QObject
{
	Q_OBJECT
	private slots:
		void execute();
		void unexecute();
		void toXml();
};

void TestAddObject::execute()
{
	YAMF::Model::Project *project = new YAMF::Model::Project;
	YAMF::Model::Scene *scene = new YAMF::Model::Scene(project);
	YAMF::Model::Layer *layer = new YAMF::Model::Layer(scene);
	YAMF::Model::Frame *frame = new YAMF::Model::Frame(layer);
	YAMF::Model::Object *object = new YAMF::Model::Object(new QGraphicsRectItem(QRect(0,0,100,100)), frame);
	
	YAMF::Common::IntHash<YAMF::Model::Object *> objects;
	
	YAMF::Command::AddObject cmd(object, &objects);
	cmd.redo();

	QCOMPARE(objects.count(), 1 );
	QVERIFY( objects[0] == object  );
	
	delete object;
	delete frame;
	delete layer;
	delete scene;
	delete project;
}

void TestAddObject::unexecute()
{
	YAMF::Model::Project *project = new YAMF::Model::Project;
	YAMF::Model::Scene *scene = new YAMF::Model::Scene(project);
	YAMF::Model::Layer *layer = new YAMF::Model::Layer(scene);
	YAMF::Model::Frame *frame = new YAMF::Model::Frame(layer);
	YAMF::Model::Object *object = new YAMF::Model::Object(new QGraphicsRectItem(QRect(0,0,100,100)), frame);
	
	YAMF::Common::IntHash<YAMF::Model::Object *> objects;
	
	YAMF::Command::AddObject cmd(object, &objects);
	cmd.redo();
	cmd.undo();

	QCOMPARE(objects.count(), 0 );
	
	delete object;
	delete frame;
	delete layer;
	delete scene;
	delete project;
}

void TestAddObject::toXml()
{
	YAMF::Model::Project *project = new YAMF::Model::Project;
	YAMF::Model::Scene *scene = project->createScene();
	YAMF::Model::Layer *layer = scene->createLayer();
	YAMF::Model::Frame *frame = layer->createFrame();
	
	YAMF::Model::Object *object = new YAMF::Model::Object(new YAMF::Item::Rect(QRect(0,0,100,100)), frame);
 	YAMF::Common::IntHash<YAMF::Model::Object *> objects = frame->graphics();
	YAMF::Command::AddObject cmd(object, &objects);
	cmd.redo();
	QString result = cmd.toXml().simplified();
	QString expected =  QString("<command name=\"addobject\"> <location scene=\"0\" layer=\"0\" frame=\"0\" index=\"0\" /> <body> <object> <rect width=\"100\" x=\"0\" y=\"0\" height=\"100\" > <properties flags=\"0\" enabled=\"1\" pos=\"(0,0)\" transform=\"matrix(1,0,0,1,0,0)\" /> <brush alpha=\"255\" style=\"0\" color=\"#000000\" transform=\"matrix(1,0,0,1,0,0)\" /> <pen width=\"0\" joinStyle=\"64\" alpha=\"255\" style=\"1\" capStyle=\"16\" color=\"#000000\" miterLimit=\"2\" > <brush alpha=\"255\" style=\"1\" color=\"#000000\" transform=\"matrix(1,0,0,1,0,0)\" /> </pen> </rect> </object> </body> </command>").simplified();
	QCOMPARE(result, expected);
	delete project;
}


QTEST_MAIN(TestAddObject)
#include "test_addobject.moc"

