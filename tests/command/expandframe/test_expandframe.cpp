
#include <QtTest/QtTest>

#include <QFile>
#include <QDebug>

#include <model/command/expandframe.h>
#include <model/scene.h>
#include <model/layer.h>
#include <model/frame.h>
#include <yamf/common/inthash.h>


class TestExpandFrame: public QObject
{
	Q_OBJECT
	private slots:
		void initTestCase();
		void cleanupTestCase();
		
		void execute();
		void unexecute();
		void toXml();
		
	private:
		YAMF::Model::Project *project;
		YAMF::Command::ExpandFrame *cmd;
		YAMF::Common::IntHash<YAMF::Model::Frame *> frames;
};

void TestExpandFrame::initTestCase()
{
	project = new YAMF::Model::Project;
	
	YAMF::Model::Scene *scene = project->createScene();
	YAMF::Model::Layer *layer = scene->createLayer();
	YAMF::Model::Frame *frame = layer->createFrame();
	frames = layer->frames();
	
	cmd = new YAMF::Command::ExpandFrame(frame, &frames, 10);
}

void TestExpandFrame::cleanupTestCase()
{
	delete cmd;
	delete project;
}

void TestExpandFrame::execute()
{
	cmd->redo();
	
	QCOMPARE(frames.visualIndices().count(), 11);
	
	QCOMPARE(frames.count(), 1 );
}

void TestExpandFrame::unexecute()
{
	cmd->undo();
	QCOMPARE(frames.visualIndices().count(), 1);
	QCOMPARE(frames.count(), 1 );
}

void TestExpandFrame::toXml()
{
	QString result = cmd->toXml().simplified();
	QString expected =  QString("<expandframe> <information size=\"10\" name=\"Frame 1\" /> <location scene=\"0\" layer=\"0\" index=\"0\" /> </expandframe>").simplified();
	
	QCOMPARE(result, expected);
}


QTEST_MAIN(TestExpandFrame)
#include "test_expandframe.moc"

