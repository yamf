
#include <QtTest/QtTest>

#include <QFile>
#include <QDebug>

#include <model/command/insertlayer.h>
#include <model/scene.h>
#include <model/layer.h>
#include <yamf/common/inthash.h>


class TestAddLayer: public QObject
{
	Q_OBJECT
	private slots:
		void execute();
		void unexecute();
};

void TestAddLayer::execute()
{
	YAMF::Model::Project *project = new YAMF::Model::Project;
	YAMF::Model::Scene *scene = new YAMF::Model::Scene(project);
	YAMF::Model::Layer *layer = new YAMF::Model::Layer(scene);
	
	YAMF::Common::IntHash<YAMF::Model::Layer *> layers;
	
	YAMF::Command::InsertLayer cmd(layer, &layers);
	cmd.redo();

	QCOMPARE(layers.count(), 1 );
	QVERIFY( layers[0] == layer  );
	
	delete layer;
	delete scene;
	delete project;
	
}

void TestAddLayer::unexecute()
{
	YAMF::Model::Project *project = new YAMF::Model::Project;
	YAMF::Model::Scene *scene = new YAMF::Model::Scene(project);
	YAMF::Model::Layer *layer = new YAMF::Model::Layer(scene);
	
	YAMF::Common::IntHash<YAMF::Model::Layer *> layers;
	
	YAMF::Command::InsertLayer cmd(layer, &layers);
	cmd.redo();
	cmd.undo();

	QCOMPARE(layers.count(), 0);
	
	delete layer;
	delete scene;
	delete project;
}


QTEST_MAIN(TestAddLayer)
#include "test_addlayer.moc"


