
#include <QtTest/QtTest>

#include <QDebug>

#include <yamf/common/inthash.h>


class TestIntHash: public QObject
{
	Q_OBJECT
	private slots:
		void initTestCase();
		
		void add();
		void remove();
		
		void removeVisual();
		
		void insert();
		
		void moveVisual();
		
		void removeVisual2();
		
		void containsVisual();
		
		void visualIndex();
		void logicalIndex();
		
		void visualValue();
		void takeVisual();
		
		void clear();
		
		
	private:
		YAMF::Common::IntHash<double> hash;
};

void TestIntHash::initTestCase()
{
	QVERIFY(hash.count() == 0);
}

void TestIntHash::add()
{
	hash.add(0.0);
	hash.add(1.0);
	hash.add(2.0);
	hash.add(3.0);
	
	QVERIFY(hash.count() == 4);
	
// 	qDebug() << hash.visualValues();
}

void TestIntHash::remove()
{
	hash.remove(0.0);
	QVERIFY(hash.count() == 3);
}

void TestIntHash::removeVisual()
{
	hash.removeVisual(0);
	
	QVERIFY(hash.count() == 2);
}

void TestIntHash::insert()
{
	hash.insert(1, 4.0);
	
	QVERIFY(hash.visualValues() == (QList<double>() << 2 << 4 << 3));
	
	hash.insert(1, 4.0);
	QVERIFY(hash.visualValues() == (QList<double>() << 2 << 4 << 4 << 3));
	
	hash.insert(1, 5.0);
	QVERIFY(hash.visualValues() == (QList<double>() << 2 << 5 << 4 << 4 << 3));
	
}

void TestIntHash::moveVisual()
{
	hash.moveVisual(1,2);
	QVERIFY(hash.visualValues() == (QList<double>() << 2 << 4 << 4 << 5 << 3));
}

void TestIntHash::removeVisual2()
{
	hash.removeVisual(1);
	QVERIFY(hash.visualValues() == (QList<double>() << 2 << 4 << 5 << 3));
}

void TestIntHash::containsVisual()
{
	QVERIFY(hash.containsVisual(1));
}

void TestIntHash::visualIndex()
{
	QCOMPARE(hash.visualIndex(2.0), 0);
	QCOMPARE(hash.visualIndex(4.0), 1);
	
	hash.add(6.0);
	QCOMPARE(hash.visualIndex(6.0), 4);
}

void TestIntHash::logicalIndex()
{
	QCOMPARE(hash.logicalIndex(2.0), 2);
}

void TestIntHash::visualValue()
{
	QVERIFY(hash.visualValue(0) == 2.0);
	QVERIFY(hash.visualValue(1) == 4.0);
	QVERIFY(hash.visualValue(2) == 5.0);
	QVERIFY(hash.visualValue(3) == 3.0);
	QVERIFY(hash.visualValue(4) == 6.0);
	
}

void TestIntHash::takeVisual()
{
	
}
	
void TestIntHash::clear()
{
	
}

QTEST_MAIN(TestIntHash);
#include "test_inthash.moc"

