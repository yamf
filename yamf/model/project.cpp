/***************************************************************************
 *   Copyright (C) 2005 by David Cuadrado                                  *
 *   krawek@gmail.com                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "project.h"

#include <QDir>

#include <dcore/debug.h>
#include <dgui/commandhistory.h>

#include "scene.h"
#include "layer.h"
#include "audiolayer.h"
#include "frame.h"

#include "model/library.h"
#include "model/libraryobject.h"
#include "model/command/insertscene.h"
#include "model/command/removescene.h"
#include "model/command/movescene.h"

#include "model/command/manager.h"

#include "model/libraryitem.h"

#include "item/builder.h"

#include <QGraphicsView>
#include <QGraphicsSvgItem>
#include <QSvgRenderer>

namespace YAMF {
namespace Model {

struct Project::Private
{
	Scenes scenes;
	QString name;
	QString dataDir;
	
	int sceneCounter;
	Model::Library *library;
	bool isOpen;
	QString author;
	QString description;
	
	Command::Manager *commandManager;
};

/**
 * Constructor por defecto
 */
Project::Project(QObject *parent) : QObject(parent), d(new Private)
{
	D_INIT;
	
	d->commandManager = new Command::Manager(this);
	
	d->sceneCounter = 0;
	d->isOpen = false;
	d->library = new Model::Library(this, "library");
}


/**
 * Destructor por defecto
 */
Project::~Project()
{
	D_END;
	
	deleteDataDir();
	
	d->scenes.clear(true);
	delete d->library;
	
	delete d;
}

Command::Manager *Project::commandManager() const
{
	return d->commandManager;
}

/**
 * Esta funcion carga la libreria local
 */
void Project::loadLibrary(const QString &filename)
{
	if ( QFile::exists(filename) )
	{
		d->library->load(filename);
	}
	else
	{
		dFatal("library") << "Cannot open library from: " << filename;
	}
}

/**
 * Cierra el proyecto
 */
void Project::clear()
{
	d->scenes.clear(true);
	d->sceneCounter = 0;
	
	d->commandManager->clear();
	
	delete d->library;
	d->library = new Library(this, "library");
	
	deleteDataDir();
}

/**
 * Pone un nombre al proyecto
 */
void Project::setProjectName(const QString &name)
{
	d->name = name;
}

/**
 * Pone el autor del proyecto
 */
void Project::setAuthor(const QString &author)
{
	d->author = author;
}

/**
 * Pone la descripcion del proyecto
 */
void Project::setDescription(const QString& description)
{
	d->description = description;
}

/**
 * @if spanish
 * Retorna el nombre del proyecto
 * @endif
 * 
 * @if english
 * Returns project name
 * @endif
 */
QString Project::projectName() const
{
	return d->name;
}

/**
 * retorna el autor del proyecto
 * @return 
 */
QString Project::author() const
{
	return d->author;
}

/**
 * retorna la descripcion del proyecto
 * @return 
 */
QString Project::description() const
{
	return d->description;
}

// bool Project::removeSceneAt(int position)
// {
// 	D_FUNCINFO;
// 	Scene *toRemove = scene(position);
// 	
// 	if ( toRemove )
// 	{
// 		d->scenes.removeVisual(position);
// 		
// 		delete toRemove;
// 		toRemove = 0;
// 		
// 		d->sceneCounter--;
// 		
// 		return true;
// 	}
// 	
// 	return false;
// }

// void Project::insertScene(int visualIndex, Scene *scene)
// {
// 	d->scenes.insert(visualIndex, scene);
// }

bool Project::moveScene(int position, int newPosition)
{
	if ( position < 0 || position >= d->scenes.count() || newPosition < 0 || newPosition >= d->scenes.count() )
	{
		dWarning() << "Failed moving scene!";
		return false;
	}
	
	Command::MoveScene *cmd = new Command::MoveScene(position, newPosition, &d->scenes);
	commandManager()->addCommand(cmd);
	
	return true;
}

Scene *Project::scene(int visualIndex) const
{
	D_FUNCINFOX("project")<< visualIndex;
	if( !d->scenes.containsVisual(visualIndex) )
	{
		D_FUNCINFO << " FATAL ERROR: index out of bound " << visualIndex;
		D_FUNCINFO << d->scenes.count();
		return 0;
	}
	return d->scenes.visualValue(visualIndex);
}

int Project::visualIndexOf(Scene *scene) const
{
	return d->scenes.visualIndex(scene);
}

int Project::logicalIndexOf(Scene *scene) const
{
	return d->scenes.logicalIndex(scene);
}

void Project::fromXml(const QString &xml )
{
	QDomDocument document;
	
	if (! document.setContent(xml) )
	{
		return;
	}
	
	QDomElement root = document.documentElement();
	
	if( root.tagName() == "project" )
	{
		setProjectName( root.attribute( "name", projectName() ) );
	}
	else
	{
		qWarning("Cannot load project");
		return;
	}
	
	QDomNode n = root.firstChild();
	
	while( !n.isNull() )
	{
		QDomElement e = n.toElement();
		
		if(!e.isNull())
		{
			if ( e.tagName() == "meta")
			{
				QDomNode n1 = e.firstChild();
				while( !n1.isNull() )
				{
					QDomElement e1 = n1.toElement();
					if ( e1.tagName() == "author")
					{
						if(e1.firstChild().isText())
						{
							setAuthor(e1.text());
						}
					}
					else if ( e1.tagName() == "description")
					{
						if(e1.firstChild().isText())
						{
							setDescription( e1.text());
						}
					}
					n1 = n1.nextSibling();
				}
			}
		}
		
		n = n.nextSibling();
	}
}

QDomElement Project::toXml(QDomDocument &doc) const
{
	QDomElement project = doc.createElement("project");
	project.setAttribute("name", d->name);
	
	
	QDomElement meta = doc.createElement("meta");
	
	QDomElement author = doc.createElement("author");
	author.appendChild(doc.createTextNode(d->author));
	
	QDomElement description = doc.createElement("description");
	description.appendChild(doc.createTextNode(d->description));
	
	
	meta.appendChild(author);
	meta.appendChild(description);
	project.appendChild(meta);
	
	return project;
}

Scenes Project::scenes() const
{
	return d->scenes;
}

/**
 * @~spanish
 * Crea y añade una escena en la posicion indicada. Si @par logicalIndex es -1 la escena sera añadida al final.
 * 
 * @param logicalIndex 
 * @param name 
 * @return 
 */
Model::Scene *Project::createScene(int visualIndex, const QString &name)
{
	QString sceneName = name;
	if( sceneName.isEmpty() )
	{
		sceneName = tr("Scene %1").arg(d->sceneCounter+1);
	}
	
	Scene *scene = new Scene(this, sceneName);
	
	insertScene(scene, visualIndex);
// 	scene->setSceneName(sceneName);
	
	return scene;
}

void Project::insertScene(Scene *scene, int visualIndex)
{
	Command::InsertScene *cmd = new Command::InsertScene(scene, &d->scenes, visualIndex);
	d->commandManager->addCommand(cmd);
	
	d->sceneCounter++;
}

void Project::addScene(Scene *scene)
{
	Command::InsertScene *cmd = new Command::InsertScene(scene, &d->scenes);
	d->commandManager->addCommand(cmd);
	
	d->sceneCounter++;
}

void Project::removeScene(Scene *scene)
{
	Command::RemoveScene *cmd = new Command::RemoveScene(scene, &d->scenes);
	d->commandManager->addCommand(cmd);
}

Model::Library *Project::library() const
{
	return d->library;
}

void Project::setOpen(bool open)
{
	d->isOpen = open;
}

bool Project::isOpen()
{
	return d->isOpen;
}

bool Project::deleteDataDir()
{
	if(QFile::exists( dataDir() ) && !d->name.isEmpty() )
	{
		QDir dir(dataDir() );
		
		if( dir.exists("audio") && dir.exists("video") && dir.exists("images") || dir.exists("project") )
		{
			dDebug("project") << "Removing " << dir.absolutePath() << "...";
			
			dir.remove("project");
			dir.remove("library");
			
			foreach(QString scene, dir.entryList(QStringList() << "scene*", QDir::Files ))
			{
				dir.remove(scene);
			}
			
			foreach( QString subdir, QStringList() << "audio" << "video" << "images" << "svg" )
			{
				if( dir.exists(subdir) )
				{
					dir.cd(subdir);
					foreach(QString file, dir.entryList() )
					{
						QString absolute = dir.absolutePath() + "/" + file;
						
						if( !file.startsWith(".") )
						{
							QFileInfo finfo(absolute);
							
							if( finfo.isFile() )
							{
								QFile::remove(absolute);
							}
						}
					}
					dir.cdUp();
					dir.rmdir(subdir);
				}
			}
			
			if( ! dir.rmdir(dir.absolutePath()) )
			{
				dError("project") << "Cannot remove project data directory!";
			}
		}
		return true;
	}
	
	return false;
}

QString Project::dataDir() const
{
	return CACHE_DIR + "/"+ d->name;
}

}
}

