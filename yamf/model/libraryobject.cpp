/***************************************************************************
 *   Copyright (C) 2006 by David Cuadrado                                  *
 *   krawek@gmail.com                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "libraryobject.h"

#include "item/builder.h"
#include "item/pixmap.h"
#include "yamf/model/libraryitem.h"
#include "yamf/model/project.h"

#include "yamf/item/serializer.h"

#include "model/command/manager.h"
#include "model/command/renamelibraryobject.h"

#include <QGraphicsSvgItem>
#include <QSvgRenderer>
#include <QTemporaryFile>
#include <QGraphicsSvgItem>
#include <QXmlStreamReader>
#include <QDir>

#include <dmedia/player.h>

#include <dcore/algorithm.h>
#include <dcore/debug.h>


namespace YAMF {
namespace Model {

struct LibraryObject::Private
{
	Private(Project *project) : project(project) {}
	int type;
	QString symbolName;
	QString dataDir;
	
	QList<LibraryItem *> symbols;
	
	Project *const project;
};

LibraryObject::LibraryObject(Project *project) : QObject(project), d(new Private(project))
{
}


LibraryObject::~LibraryObject()
{
	delete d;
}

Project *LibraryObject::project() const
{
	return d->project;
}

void LibraryObject::setType(int type)
{
	d->type = type;
}

int LibraryObject::type() const
{
	return d->type;
}

void LibraryObject::setDataDir(const QString &path)
{
	d->dataDir = path;
}

QString LibraryObject::dataDir() const
{
	return d->dataDir;
}


void LibraryObject::setSymbolName(const QString &name)
{
	QString str = name;
	str.replace(QDir::separator(), "-");
	
	Command::RenameLibraryObject *cmd = new  Command::RenameLibraryObject(this, str, &d->symbolName);
	project()->commandManager()->addCommand(cmd);
}

QString LibraryObject::symbolName() const
{
	return d->symbolName;
}

void LibraryObject::addSymbolItem(LibraryItem *item)
{
	d->symbols << item;
}

void LibraryObject::removeSymbolItem(LibraryItem *item)
{
	d->symbols.removeAll(item);
}

int LibraryObject::indexOf(LibraryItem *item) const
{
	return d->symbols.indexOf(item);
}

LibraryItem *LibraryObject::item(int pos) const
{
	return d->symbols[pos];
}

void LibraryObject::updateSymbols()
{
	foreach(LibraryItem *libitem, d->symbols)
	{
		libitem->setSymbolName(d->symbolName);
		QGraphicsItem *item = libitem->item();
		if( item )
		{
			libitem->setMatrix(item->matrix());
		}
	}
}

void LibraryObject::loadData(const QString &data)
{
	Q_UNUSED(data);
}

QDomElement LibraryObject::dataXml(QDomDocument &doc) const
{
	return QDomElement();
}

void LibraryObject::fromXml(const QString &xml )
{
	D_FUNCINFO;
	
	QDomDocument document;
	
	if(! document.setContent(xml) )
	{
		return;
	}
	
	QDomElement objectTag = document.documentElement();
	
	if( objectTag.tagName() == "object" )
	{
		setSymbolName(objectTag.attribute("id"));
		
		if( d->symbolName.isEmpty() ) return;
		
		d->type = objectTag.attribute("type").toInt();
		
		QDomElement objectData = objectTag.firstChild().toElement();
		if( !objectTag.isNull())
		{
			QString data;
			{
				QTextStream ts(&data);
				ts << objectData;
			}
			
			this->loadData(data);
		}
	}
}

QDomElement LibraryObject::toXml(QDomDocument &doc) const
{
	QDomElement object = doc.createElement("object");
	object.setAttribute("id", d->symbolName);
	object.setAttribute("type", d->type);
	
	object.appendChild(this->dataXml(doc));
	
	return object;
}

// LibraryItemObject

struct LibraryItemObject::Private
{
	QGraphicsItem *item;
};

LibraryItemObject::LibraryItemObject(QGraphicsItem *item, Project *parent) : LibraryObject(parent), d(new Private)
{
	setType(LibraryObject::Item);
	d->item = item;
}

LibraryItemObject::~LibraryItemObject()
{
	delete d;
}

QGraphicsItem *LibraryItemObject::item() const
{
	return d->item;
}

void LibraryItemObject::loadData(const QString &data)
{
	Item::Builder factory;
	d->item = factory.create(data);
}

QDomElement LibraryItemObject::dataXml(QDomDocument &doc) const
{
	if( d->item )
	{
		if( Common::AbstractSerializable *serializable = dynamic_cast<Common::AbstractSerializable *>(d->item) )
		{
			return serializable->toXml(doc);
		}
	}
	
	return QDomElement();
}

// LibraryURLObject

struct LibraryURLObject::Private
{
	Private(const QUrl &url) : url(url) {}
	QUrl url;
};

LibraryURLObject::LibraryURLObject(Project *parent) : LibraryObject(parent), d(new Private(QUrl()))
{
}

LibraryURLObject::LibraryURLObject(const QUrl &url, Project *parent) : LibraryObject(parent), d(new Private(url))
{
}

LibraryURLObject::~LibraryURLObject()
{
	QString file = d->url.toLocalFile();
	
	if( QFile::exists(file) )
	{
		QFile::remove(file);
	}
	delete d;
}

void LibraryURLObject::setUrl(const QUrl &url)
{
	d->url = url;
}

QUrl LibraryURLObject::url() const
{
	return d->url;
}

void LibraryURLObject::loadData(const QString &data)
{
	QString destination = QDir::tempPath()+"/yamf_file."+QString::number(DCore::Algorithm::random());
	
	QXmlStreamReader reader(data);
	
	while(!reader.atEnd() )
	{
		bool finish = false;
		switch(reader.readNext())
		{
			case QXmlStreamReader::StartElement:
			{
				if( reader.name().toString() == "url" )
				{
					QString att = reader.attributes().value("path").toString();
					if( att.isEmpty() ) continue;
					
					QString path = dataDir()+"/"+ att;
					
					if( QFile::exists(path) )
					{
						if( QFile::copy(path, destination) )
						{
							setUrl(QUrl::fromLocalFile(destination));
							
							
							load(&reader);
							finish = true;
						}
					}
				}
				else if( reader.name().toString() == "file" )
				{
					if( reader.readNext() == QXmlStreamReader::Characters )
					{
						if( reader.isCDATA() )
						{
							QFile f(destination);
							
							if( f.open(QIODevice::WriteOnly) )
							{
								dfDebug << "Writing to " << destination;
								
								setUrl(QUrl::fromLocalFile(destination));
								f.write(QByteArray::fromBase64(reader.text().toString().toLocal8Bit()));
								f.close();
								
								load(&reader);
								finish = true;
							}
						}
					}
				}
			}
			break;
			default: break;
		}
		
		if( finish ) break;
	}
}

QDomElement LibraryURLObject::dataXml(QDomDocument &doc) const
{
	if( d->url.isValid() )
	{
		QDomElement data = doc.createElement("data");
		
		if( !this->dataDir().isEmpty() )
		{
			QDomElement uri = doc.createElement("url");
			QString current = d->url.toLocalFile();
			
			{
				QString destination = saveData();
				uri.setAttribute("path", destination);
			}
			
			data.appendChild(uri);
		}
		else
		{
			data.appendChild(fileData(doc));
		}
		
		
		return data;
	}
	
	return QDomElement();
}

void LibraryURLObject::load(QXmlStreamReader *reader)
{
	Q_UNUSED(reader);
}

QDomElement LibraryURLObject::fileData(QDomDocument &doc) const
{
	QDomElement fileData = doc.createElement("file");
	
	QFile f(d->url.toLocalFile());
	
	if( f.open(QIODevice::ReadOnly) )
	{
		fileData.appendChild(doc.createCDATASection( f.readAll().toBase64() ));
	}
	
	return fileData;
}

///////////////////////

struct LibraryURLItemObject::Private
{
	Private() : item(0) {}
	
	QGraphicsItem *item;
};

LibraryURLItemObject::LibraryURLItemObject(const QUrl &url, Project *parent) : LibraryURLObject(url, parent),d(new Private)
{
}

LibraryURLItemObject::LibraryURLItemObject(Project *parent) : LibraryURLObject(parent),d(new Private)
{
}

LibraryURLItemObject::~LibraryURLItemObject()
{
	delete d;
}

void LibraryURLItemObject::setItem(QGraphicsItem *item)
{
	d->item = item;
}

QGraphicsItem *LibraryURLItemObject::item() const
{
	return d->item;
}

QDomElement LibraryURLItemObject::dataXml(QDomDocument &doc) const
{
	QDomElement data = LibraryURLObject::dataXml(doc);
	
	if( d->item )
	{
		data.appendChild(YAMF::Item::Serializer::properties(d->item, doc));
	}
	
	return data;
}

void LibraryURLItemObject::load(QXmlStreamReader *reader)
{
	createItem();
	
	while(!reader->atEnd())
	{
		if( reader->readNext() == QXmlStreamReader::StartElement )
		{
			if( reader->name().toString() == "properties" )
			{
				YAMF::Item::Serializer::loadProperties(d->item, reader);
				break;
			}
		}
	}
	
// 	QDomNode node = data.firstChild();
// 	
// 	while(!node.isNull())
// 	{
// 		QDomElement element = node.toElement();
// 		if( element.tagName() == "properties" )
// 		{
// 			YAMF::Item::Serializer::loadProperties(d->item, element);
// 			break;
// 		}
// 		
// 		node = node.nextSibling();
// 	}
}


//////////////////////

// LibrarySvgObject

struct LibrarySvgObject::Private
{
	Private() {};
};

LibrarySvgObject::LibrarySvgObject(const QUrl &url, Project *parent) : LibraryURLItemObject(parent), d(new Private)
{
	setType(LibraryObject::Svg);
	
	QString file = url.toLocalFile();
	QString destination = QDir::tempPath()+"/yamf_svg_file."+QString::number(DCore::Algorithm::random());
	
	if( QFile::exists(file) )
	{
		if( QFile::copy(file, destination) )
		{
			setUrl(QUrl::fromLocalFile(destination));
			createItem();
		}
	}
}

LibrarySvgObject::~LibrarySvgObject()
{
	delete d;
}

QString LibrarySvgObject::saveData() const
{
	QString current = url().toLocalFile();
	QDir destDir(dataDir());
	
	if( !destDir.exists("svg") )
	{
		destDir.mkdir("svg");
	}
	
	destDir.cd("svg");
	
	QString destination = destDir.absoluteFilePath(symbolName()+".svg");
	
	D_SHOW_VAR(destination);
	
	if( !QFile::copy(current, destination) )
	{
		return current;
	}
	
	QFileInfo finfo(destination);
	return "svg/"+finfo.fileName();
}

void LibrarySvgObject::createItem()
{
	setItem(new QGraphicsSvgItem( url().toLocalFile() ));
}

// LibraryBitmapObject

struct LibraryBitmapObject::Private
{
};

LibraryBitmapObject::LibraryBitmapObject(const QUrl &url, Project *parent) : LibraryURLItemObject(parent), d(new Private)
{
	setType(LibraryObject::Image);
	
	QString file = url.toLocalFile();
	QString destination = QDir::tempPath()+"/yamf_bitmap_file."+QString::number(DCore::Algorithm::random());
	
	if( QFile::exists(file) )
	{
		if( QFile::copy(file, destination) )
		{
			setUrl(QUrl::fromLocalFile(destination));
			createItem();
		}
	}
}

LibraryBitmapObject::~LibraryBitmapObject()
{
	delete d;
}

QString LibraryBitmapObject::saveData() const
{
	QString current = url().toLocalFile();
	
	QDir destDir(dataDir());
	
	if( !destDir.exists("images") )
	{
		destDir.mkdir("images");
	}
	
	destDir.cd("images");
	
	QString destination = destDir.absoluteFilePath(symbolName());
	if(!QFile::copy(current, destination))
	{
		return current;
	}
	
	QFileInfo finfo(destination);
	return "images/"+finfo.fileName();
}

void LibraryBitmapObject::createItem()
{
	setItem(new QGraphicsPixmapItem( QPixmap(url().toLocalFile()) ));
}

// LibrarySoundObject

struct LibrarySoundObject::Private
{
};

LibrarySoundObject::LibrarySoundObject(const QUrl &url, Project *parent) : LibraryURLObject(parent), d(new Private)
{
	setType(LibraryObject::Sound);
	
	QString file = url.toLocalFile();
	QString destination = QDir::tempPath()+"/yamf_audio_file."+QString::number(DCore::Algorithm::random());
	
	if( QFile::exists(file) )
	{
		if( QFile::copy(file, destination) )
		{
			setUrl(QUrl::fromLocalFile(destination));
		}
	}
}

LibrarySoundObject::~LibrarySoundObject()
{
	delete d;
}

QString LibrarySoundObject::saveData() const
{
	QString current = url().toLocalFile();
	
	QDir destDir(dataDir());
	
	if( !destDir.exists("audio") )
	{
		destDir.mkdir("audio");
	}
	
	destDir.cd("audio");
	
	QString destination = destDir.absoluteFilePath(symbolName());
	if(!QFile::copy(current, destination))
	{
		return current;
	}
	
	QFileInfo finfo(destination);
	return "audio/"+finfo.fileName();
}

LibraryObject *LibraryObjectFactory::create(int id, Project *parent)
{
	switch(id)
	{
		case LibraryObject::Image:
		{
			return new LibraryBitmapObject(QUrl(), parent);
		}
		break;
		case LibraryObject::Svg:
		{
			return new LibrarySvgObject(QUrl(), parent);
		}
		break;
		case LibraryObject::Text:
		{
		}
		break;
		case LibraryObject::Item:
		{
			return new LibraryItemObject(0, parent);
		}
		break;
		case LibraryObject::Sound:
		{
			return new LibrarySoundObject(QUrl(), parent);
		}
		break;
	}
	
	return 0;
}




}
}


