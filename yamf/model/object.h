/***************************************************************************
 *   Copyright (C) 2006 by David Cuadrado                                  *
 *   krawek@gmail.com                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef YAMFMODELOBJECT_H
#define YAMFMODELOBJECT_H

#include <QObject>
#include <yamf/common/abstractserializable.h>
#include <yamf/model/frame.h>
#include <yamf/model/location.h>

#include <yamf/common/yamf_exports.h>

class QGraphicsItem;

namespace YAMF {

namespace Item {
	class Tweener;
}

namespace Effect {
	class Filter;
}

namespace Model {


/**
 * @ingroup model
 * @author David Cuadrado \<krawek@gmail.com\>
*/
class YAMF_EXPORT Object : public QObject, public Common::AbstractSerializable
{
	public:
		enum Transformations{ ScaleX = 1, ScaleY, Rotate, TranslateX, TranslateY };
		
		Object(QGraphicsItem *item, Frame *parent);
		Object(Frame *parent);
		~Object();
		
		void setItem(QGraphicsItem *item);
		QGraphicsItem *item() const;
		
		void setObjectName(const QString &name);
		QString objectName() const;
		
		void setTweener(Item::Tweener *tweener);
		void deleteTweener();
		Item::Tweener *tweener() const;
		
		void setTweeningEnabled(bool e);
		bool tweeningEnabled() const;
		
		void setFilter(YAMF::Effect::Filter *filter);
		void removeFilter() const;
		
		Frame *frame() const;
		Layer *layer() const;
		Scene *scene() const;
		
		int logicalIndex() const;
		int visualIndex() const;
		
		Location location() const;
		
	public:
		virtual void fromXml(const QString &xml );
		virtual QDomElement toXml(QDomDocument &doc)  const;
		
	private:
		struct Private;
		Private *const d;
		
};
}
}

#endif

