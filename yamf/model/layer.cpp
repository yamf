/***************************************************************************
 *   Copyright (C) 2005 by David Cuadrado                                  *
 *   krawek@gmail.com                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "layer.h"
#include "scene.h"

#include <dcore/debug.h>

#include "model/command/manager.h"
#include "model/command/insertframe.h"
#include "model/command/removeframe.h"
#include "model/command/locklayer.h"
#include "model/command/changelayervisibility.h"
#include "model/command/moveframe.h"
#include "model/command/renamelayer.h"
#include "model/command/expandframe.h"

namespace YAMF {
namespace Model {

struct Layer::Private
{
	Frames frames;
	bool isVisible;
	QString name;
	int framesCount;
	bool isLocked;
	
	Type type;
};


/**
 * Constructor por defecto
 */
Layer::Layer(Scene *parent, const QString& name) : QObject(parent), d(new Private)
{
	d->type = Vectorial;
	
	d->isVisible = true;
	d->name = name;
	d->framesCount = 0;
	d->isLocked = false;
}

/**
 * Destructor
 */
Layer::~Layer()
{
	d->frames.clear(true);
	
	delete d;
}

/**
 * Pone la lista de frames, esta funcion reemplaza los frames anteriores
 */
void Layer::setFrames(const Frames &frames)
{
	d->frames = frames;
	d->framesCount = frames.count();
}

/**
 * Retorna los frames del layer.
 */
Frames Layer::frames()
{
	return d->frames;
}

/**
 * Pone el nombre del layer.
 */
void Layer::setLayerName(const QString &name)
{
	if(name != d->name)
	{
		Command::RenameLayer *cmd = new Command::RenameLayer(this, name, &d->name);
		project()->commandManager()->addCommand(cmd);
	}
}

/**
 * Retorna el nombre del layer.
 */
QString Layer::layerName() const
{
	return d->name;
}

/**
 * Bloquea el layer.
 */
void Layer::setLocked(bool isLocked)
{
	if(isLocked != d->isLocked)
	{
		Command::LockLayer *command = new Command::LockLayer(this, isLocked, &d->isLocked);
		project()->commandManager()->addCommand(command);
	}
// 	d->isLocked = isLocked;
}

/**
 * Returna verdadero cuando el layer esta bloqueado.
 */
bool Layer::isLocked() const
{
	return d->isLocked;
}

/**
 * Pone la visibilidad del layer.
 */
void Layer::setVisible(bool isVisible)
{
	if(isVisible != d->isVisible)
	{
		Command::ChangeLayerVisibility *command = new Command::ChangeLayerVisibility(this, isVisible, &d->isVisible);
		project()->commandManager()->addCommand(command);
	}
	else
	{
		if( d->isVisible )
			dWarning() << "The layer " << visualIndex() << " is visible";
		else
			dWarning() << "The layer " << visualIndex() << " is not visible";
	}
	
// 	d->isVisible = isVisible;
// 	emit visibilityChanged(isVisible);
}

/**
 * Retorna verdadero si el layer es visible.
 */
bool Layer::isVisible() const
{
	return d->isVisible;
}

/**
 * Crea un marco y la inserta, en la posición @p visualIndex y con el nombre name;
 * por defecto añadirá el frame al final de la capa.
 */
Model::Frame *Layer::createFrame(int visualIndex, const QString &name)
{
	QString frameName = name;
	if( frameName.isEmpty() )
	{
		frameName = tr("Frame %1").arg(d->framesCount+1);
	}
	
	Model::Frame *frame = new Model::Frame(this, frameName);
// 	frame->setFrameName(frameName);
	
	insertFrame(visualIndex, frame);
	
	return frame;
}

/**
 * Inserte el marco @p frame en la posición @p visualIndex a la capa.
 */
void Layer::insertFrame(int visualIndex, Model::Frame *frame)
{
	Command::InsertFrame *cmd = new Command::InsertFrame(frame, &d->frames, visualIndex);
	project()->commandManager()->addCommand(cmd);
	
	d->framesCount++;
}

/**
 * Añade el marco @p frame a la capa.
 */
void Layer::addFrame(Model::Frame *frame)
{
	Command::InsertFrame *cmd = new Command::InsertFrame(frame, &d->frames);
	project()->commandManager()->addCommand(cmd);
	
	d->framesCount++;
}

/**
 * Remueve el marco @p frame de la capa.
 */
void Layer::removeFrame(Model::Frame *frame)
{
	Command::RemoveFrame *cmd = new Command::RemoveFrame(frame, &d->frames);
	project()->commandManager()->addCommand(cmd);
}

/**
 * Cambia el tipo de capa a type.
 */
void Layer::setType(Type type)
{
	d->type = type;
}

/**
 * Retorna el tipo de capa.
 */
Layer::Type Layer::type() const
{
	return d->type;
}

Model::Location Layer::location() const
{
	Model::Location loc;
	
	loc.setValue(Location::Scene, scene()->visualIndex());
	loc.setValue(Location::Layer, visualIndex());
	
	return loc;
}


/**
 * Remueve el marco que esta en la posición @p visualIndex.
 */
bool Layer::removeFrame(int visualIndex)
{
	Frame *toRemove = frame(visualIndex);
	if ( toRemove )
	{
		d->frames.removeVisual(visualIndex);
		
		return true;
	}
	
	return false;
}

/**
 * Mueve el marco de la posición @p from a la posición @p to.
 */
bool Layer::moveFrame(int from, int to)
{
	int count = d->frames.visualCount();
	if ( from < 0 || from >= count || to < 0 || to >= count )
	{
		return false;
	}
	
	Command::MoveFrame *cmd = new Command::MoveFrame(this, from, to, &d->frames);
	
	project()->commandManager()->addCommand(cmd);
	
	return true;
}

/**
 * Expande el marco de la posición @p logicalIndex a un tamaño @p size.
 */
bool Layer::expandFrame(int visualIndex, int size)
{
	if(!d->frames.containsVisual(visualIndex)) return false;
	
	
	Frame *toExpand = d->frames.visualValue(visualIndex);
	
	if(toExpand)
	{
		Command::ExpandFrame *cmd = new Command::ExpandFrame(toExpand, &d->frames, size);
		project()->commandManager()->addCommand(cmd);
		return true;
	}
	return false;
}

/**
 * Retorna el frame que esta en la posición @p logicalIndex.
 */
Frame *Layer::frame(int visualIndex) const
{
	if ( !d->frames.containsVisual(visualIndex) )
	{
		D_FUNCINFO << " FATAL ERROR: index out of bound " << "layer " << this->visualIndex() << " frame " << visualIndex;
		return 0;
	}
	
	return d->frames.visualValue(visualIndex);
}



/**
 * Retorna la escena a la cual pertenece capa.
 */
Scene *Layer::scene() const
{
	return static_cast<Scene *>(parent());
}


/**
 * Retorna proyecto al cual pertenece capa.
 */
Project *Layer::project() const
{
	return scene()->project();
}

/**
 * Retorna la posición logica del marco @p frame.
 */
int Layer::logicalIndexOf(Frame *frame) const
{
	return d->frames.logicalIndex(frame);
}

/**
 * Retorna la posición visual del marco @p frame.
 */
int Layer::visualIndexOf(Frame *frame) const
{
	return d->frames.visualIndex(frame);
}

/**
 * Retorna la posición logica de la capa.
 */
int Layer::logicalIndex() const
{
	return scene()->logicalIndexOf(const_cast<Layer *>(this));
}

/**
 * Retorna la posición visual de la capa.
 */
int Layer::visualIndex() const
{
	return scene()->visualIndexOf(const_cast<Layer *>(this));
}

int Layer::clones(Frame *frame) const
{
	return d->frames.clones(frame);
}

/**
 * @~spanish
 * Construye la capa a partir del xml.
 */
void Layer::fromXml(const QString &xml )
{
	QDomDocument document;
	
	if (! document.setContent(xml) )
	{
		return;
	}
	
	QDomElement root = document.documentElement();
	
	setLayerName( root.attribute( "name", layerName() ) );
	
	QDomNode n = root.firstChild();
	
	while( !n.isNull() )
	{
		QDomElement e = n.toElement();
		
		if(!e.isNull())
		{
			if ( e.tagName() == "frame" )
			{
				int vindex = e.attribute("index", "-1").toInt();
				QString name = e.attribute("name", QString());
				Frame *frame = createFrame(vindex, name);
				
				if ( frame )
				{
					QString newDoc;
					
					{
						QTextStream ts(&newDoc);
						ts << n;
					}
					frame->fromXml( newDoc );
					
					int clones = e.attribute("clones", "-1").toInt();
					
					if( clones > 0 )
					{
						this->expandFrame(vindex, clones);
					}
				}
			}
		}
		
		n = n.nextSibling();
	}
}

/**
 * @~spanish
 * Convierte la capa a un documento xml.
 */
QDomElement Layer::toXml(QDomDocument &doc) const
{
	QDomElement root = doc.createElement("layer");
	root.setAttribute("name", d->name );
	root.setAttribute("type", d->type );
	doc.appendChild(root);
	
	foreach(Frame *frame, d->frames.values() )
	{
		root.appendChild( frame->toXml(doc) );
	}
	
	return root;
}


}
}

