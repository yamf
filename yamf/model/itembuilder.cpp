/***************************************************************************
 *   Copyright (C) 2007 David Cuadrado                                     *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/

#include "itembuilder.h"

#include "library.h"
#include "libraryitem.h"
#include "libraryobject.h"

namespace YAMF {

namespace Model {

struct ItemBuilder::Private
{
	Private() : library(0) {}
	const Model::Library *library;
};

ItemBuilder::ItemBuilder()
 : YAMF::Item::Builder(), d(new Private)
{
}


ItemBuilder::~ItemBuilder()
{
	delete d;
}

void ItemBuilder::setLibrary(const Model::Library *library)
{
	d->library = library;
}

bool ItemBuilder::endTag(const QString& qname)
{
	if( qname == "symbol" )
	{
		if(addingToGroup())
		{
			addCurrentToGroup();
		}
		
		pop();
	}
	
    return Builder::endTag(qname);
}

bool ItemBuilder::startTag(const QString& qname, const QXmlAttributes& atts)
{
	if ( qname == "symbol" )
	{
		if( addingToGroup() )
		{
			LibraryItem *item = qgraphicsitem_cast<LibraryItem *>(createItem( qname ));
			QString id = atts.value("id");
			item->setSymbolName(id);
			
			if( d->library )
				item->setObject(d->library->findObject(id));
			
			push(item);
		}
		else
		{
			if( !item())
				setItem(createItem(qname));
			
			QString id = atts.value("id");
			
			qgraphicsitem_cast<LibraryItem *>(item())->setSymbolName(id);
			
			if( d->library )
				qgraphicsitem_cast<LibraryItem *>(item())->setObject(d->library->findObject(id));
			
			push(item());
		}
		
		return true;
	}
	
    return Builder::startTag(qname, atts);
}

void ItemBuilder::text(const QString& ch)
{
    Item::Builder::text(ch);
}

QGraphicsItem* ItemBuilder::createItem(const QString& root)
{
	if( root == "symbol")
	{
		return new LibraryItem;
	}
	
    return Builder::createItem(root);
}



}
}


