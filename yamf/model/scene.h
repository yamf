/***************************************************************************
 *   Copyright (C) 2005 by David Cuadrado                                  *
 *   krawek@gmail.com                                          	   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef YAMF_MODELSCENE_H
#define YAMF_MODELSCENE_H

#include <yamf/common/abstractserializable.h>
#include <yamf/common/inthash.h>
#include <yamf/common/yamf_exports.h>
#include <yamf/model/project.h>
#include <yamf/model/location.h>

#include <QDomDocument>
#include <QDomElement>
#include <QGraphicsScene>
#include <QMap>

class QGraphicsItem;
class QPainter;
class QStyleOptionGraphicsItem;


namespace YAMF {
namespace Model {

class Layer;
class AudioLayer;
class Object;

typedef Common::IntHash<Layer *> Layers;
typedef QList<AudioLayer *> AudioLayers;

/**
 * @ingroup model
 * @brief Esta clase representa una escena.
 * @author David Cuadrado \<krawek@gmail.com\>
*/

class YAMF_EXPORT Scene : public QObject, public Common::AbstractSerializable
{
	Q_OBJECT
	public:
		Scene(Project *parent, const QString &name = tr("Scene"));
		~Scene();
		
		void setSceneName(const QString &name);
		QString sceneName() const;
		
		void setLocked(bool isLocked);
		bool isLocked() const;
		
		void setVisible(bool isVisible);
		bool isVisible() const;
		
		Layers layers() const;
		AudioLayers audioLayers() const;
		
		AudioLayer *createAudioLayer(int visualIndex = -1, const QString &name = QString());
		Layer *layer(int visualIndex) const;
		
		Model::Layer *createLayer(int visualIndex = -1, const QString &name = QString());
		void insertLayer(int visualIndex, Model::Layer *layer);
		void addLayer(Model::Layer *layer);
		
		void removeLayer(Model::Layer *layer);
		bool removeLayer(int visualIndex);
		bool moveLayer(int from, int to);
		
		
		int logicalIndex() const;
		int visualIndex() const;
		
		int visualIndexOf(Layer *layer) const;
		int logicalIndexOf(Layer *layer) const;
		
		Project *project() const;
		
		void addTweeningObject(Object *object);
		void removeTweeningObject(Object *object);
		
		void setAlwaysVisible(Object *object, bool visible = true);
		QList<Object *> alwaysVisible() const;
		
		QList<Object *> tweeningObjects() const;
		bool containsTweeningObject(Object *object) const;
		Model::Object *tweeningObject(QGraphicsItem *item) const;
		
		Location location() const;
		
	public:
		virtual void fromXml(const QString &xml );
		virtual QDomElement toXml(QDomDocument &doc) const;
		
	private:
		struct Private;
		Private *const d;
};

}
}

#endif

