/***************************************************************************
 *   Copyright (C) 2006 by David Cuadrado                                  *
 *   krawek@gmail.com                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "object.h"

#include <QMatrix>
#include <QGraphicsItem>
#include <QTimer>

#include <model/frame.h>
#include <model/scene.h>

#include <item/tweener.h>

#include "yamf/model/libraryitem.h"
#include "yamf/model/library.h"
#include "yamf/model/layer.h"
#include "yamf/model/itembuilder.h"

#include "yamf/drawing/photogram.h"

#include "yamf/effect/filter.h"
#include "yamf/effect/factory.h"

#include <dcore/debug.h>

namespace YAMF {
namespace Model {

struct Object::Private
{
	Private() : item(0), tweener(0), frame(0) {}
	
	QString name;
	QGraphicsItem *item;
	
	Item::Tweener *tweener;
	
	Frame *frame;
	
	void initItemData();
};

void Object::Private::initItemData()
{
	if(! this->item->data(ScaleX).isValid())
	{
		this->item->setData(ScaleX, 1.0);
	}
	if(! this->item->data(ScaleY).isValid())
	{
		this->item->setData(ScaleY, 1.0);
	}
	if(! this->item->data(Rotate).isValid())
	{
		this->item->setData(Rotate, 0.0);
	}
	if(! this->item->data(TranslateX).isValid())
	{
		this->item->setData(TranslateX, 0.0);
	}
	if(! this->item->data(TranslateY).isValid())
	{
		this->item->setData(TranslateY, 0.0);
	}
}


Object::Object(QGraphicsItem *item, Frame *parent)
	: QObject(parent), d(new Private)
{
	D_INIT;
	
	d->item = item;
	d->tweener = 0;
	d->frame = parent;
	
	d->initItemData();
}

Object::Object(Frame *parent) : QObject(parent), d(new Private)
{
	d->item = 0;
	d->tweener = 0;
	d->frame = parent;
}

Object::~Object()
{
	if(d->tweener && d->frame->scene())
	{
		d->frame->scene()->removeTweeningObject(this);
	}
	delete d;
}

void Object::setItem(QGraphicsItem *item)
{
	d->item = item;
	d->initItemData();
}

QGraphicsItem *Object::item() const
{
	return d->item;
}

void Object::setObjectName(const QString &name)
{
	d->name = name;
}

QString Object::objectName() const
{
	return d->name;
}

void Object::setTweener(Item::Tweener *tweener)
{
	if( d->tweener == tweener )
	{
		return;
	}
	
	d->tweener = tweener;
	
	if( d->tweener )
	{
		d->tweener->setItem(d->item);
		d->frame->scene()->addTweeningObject(this);
	}
	else
	{
		d->frame->scene()->removeTweeningObject(this);
	}
}

void Object::deleteTweener()
{
	Item::Tweener *tweener = d->tweener;
	
	if( tweener )
	{
		setTweener(0);
		delete tweener;
	}
}

Item::Tweener *Object::tweener() const
{
	return d->tweener;
}

void Object::setTweeningEnabled(bool e)
{
	if(e)
	{
		d->frame->scene()->addTweeningObject(this);
	}
	else
	{
		d->frame->scene()->removeTweeningObject(this);
	}
}

bool Object::tweeningEnabled() const
{
	return d->frame->scene()->containsTweeningObject(const_cast<Model::Object *>(this));
}

void Object::setFilter(YAMF::Effect::Filter *filter)
{
	if( !d->item ) return;
	
	QGraphicsScene *scene = d->item->scene();
	
	if( scene )
	{
		scene->removeItem(d->item);
	}
	
	
	QPointF pos = d->item->pos();
	
	filter->setTarget(d->item);
	d->item = filter;
	
	d->item->setPos(pos);
	
// 	if( d->tweener )
// 	{
// 		d->tweener->setItem(d->item);
// 	}
	
	if( scene )
	{
		scene->addItem(d->item);
	}
}

void Object::removeFilter() const
{
	QGraphicsScene *scene = d->item->scene();
	
	
	if( YAMF::Effect::Filter *filter = dynamic_cast<YAMF::Effect::Filter *>(d->item) )
	{
		if( scene )
		{
			scene->removeItem(filter);
		}
		
		d->item = filter->item();
		
		if( scene )
		{
			scene->addItem(d->item);
		}
	}
}

Frame *Object::frame() const
{
	return d->frame;
}

Layer *Object::layer() const
{
	return d->frame->layer();
}

Scene *Object::scene() const
{
	return d->frame->scene();
}

int Object::logicalIndex() const
{
	return d->frame->logicalIndexOf(const_cast<Object *>(this));
}

int Object::visualIndex() const
{
	return d->frame->visualIndexOf(const_cast<Object *>(this));
}

Model::Location Object::location() const
{
	Model::Location loc;
	loc.setValue(Location::Scene, scene()->visualIndex() );
	loc.setValue(Location::Layer, layer()->visualIndex() );
	loc.setValue(Location::Frame, frame()->visualIndex() );
	loc.setValue(Location::Object, this->visualIndex() );
	
	return loc;
}

void Object::fromXml(const QString &xml )
{
	QDomDocument doc;
	if( ! doc.setContent(xml) ) { qWarning("Invalid Object document"); return; }
	
	QDomNode n2 = doc.documentElement();
	
	setObjectName(doc.documentElement().attribute("name"));
	
	while(!n2.isNull())
	{
		QDomElement e2 = n2.toElement();
		
		QString itemxml;
		QTextStream ts(&itemxml);
		ts << doc.documentElement().firstChild();
		
		QDomNode ne = e2.firstChild();
		
		while( !ne.isNull() )
		{
			QDomElement ce = ne.toElement();
			
			if( ce.tagName() == "symbol" )
			{
				LibraryItem *libraryItem = new LibraryItem;
				libraryItem->fromXml(itemxml);
				libraryItem->setObject(frame()->project()->library()->findObject(libraryItem->symbolName()));
				
				setItem(libraryItem);
			}
			else if( ce.tagName() == "tweening")
			{
				Item::Tweener *tweener = new Item::Tweener(0, this);
				
				QString newDoc;
		
				{
					QTextStream ts(&newDoc);
					ts << ne;
				}
			
				tweener->fromXml(newDoc);
				this->setTweener(tweener);
			}
			else if( ce.tagName() == "filter" )
			{
				Effect::Filter *filter = Effect::Factory::create(ce.attribute("name"));
				filter->fromXml(itemxml);
				
				setItem(filter);
			}
			else
			{
				Item::Builder builder;
				setItem(builder.create(itemxml));
			}
			
			ne = ne.nextSibling();
		}
		n2 = n2.nextSibling();
	}
}

QDomElement Object::toXml(QDomDocument &doc) const
{
	QDomElement object = doc.createElement("object");
	object.setAttribute("name", d->name);
	
	if ( Common::AbstractSerializable *is = dynamic_cast<Common::AbstractSerializable *>(d->item) )
	{
		object.appendChild(is->toXml(doc));
	}
	
	if(d->tweener)
	{
		object.appendChild(d->tweener->toXml(doc));
	}
	
	return object;
}

}
}
