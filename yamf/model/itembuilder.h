/***************************************************************************
 *   Copyright (C) 2007 David Cuadrado                                     *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/

 

#ifndef YAMF_MODELITEMBUILDER_H
#define YAMF_MODELITEMBUILDER_H

#include <yamf/item/builder.h>

namespace YAMF {

namespace Model {

class Library;

/**
 * @ingroup model
 * @author David Cuadrado <krawek@gmail.com>
*/
class YAMF_EXPORT ItemBuilder : public YAMF::Item::Builder
{
	public:
		ItemBuilder();
		~ItemBuilder();
		
		void setLibrary(const Library *library);
		
		virtual bool endTag(const QString& qname);
		virtual bool startTag(const QString& qname, const QXmlAttributes& atts);
		virtual void text(const QString& ch);
		
	protected:
		QGraphicsItem* createItem(const QString& root);
		
	private:
		struct Private;
		Private *const d;
};

}

}

#endif
