/***************************************************************************
 *   Copyright (C) 2007 David Cuadrado                                     *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/

#include "location.h"

#include <QXmlStreamWriter>
#include <QHash>

namespace YAMF {
namespace Model {

struct Location::Private
{
	QHash<Part, QVariant> values;
};

Location::Location() : d(new Private)
{
}


Location::~Location()
{
	delete d;
}

void Location::setValue(Part part, const QVariant &value)
{
	d->values[part] = value;
}

QVariant Location::value(Part part) const
{
	return d->values.value(part);
}

void Location::toXml(QXmlStreamWriter *writer) const
{
	writer->writeStartElement("location");
	
	if( d->values.contains(Scene) )
	{
		writer->writeAttribute("scene", d->values.value(Scene).toString() );
		if( d->values.contains(Layer) )
		{
			writer->writeAttribute("layer", d->values.value(Layer).toString() );
			if( d->values.contains(Frame) )
			{
				writer->writeAttribute("frame", d->values.value(Frame).toString());
				if( d->values.contains(Object) )
				{
					writer->writeAttribute("object", d->values.value(Object).toString());
				}
			}
		}
	}
	
	if( d->values.contains(Symbol) )
	{
		writer->writeAttribute("symbol", d->values.value(Symbol).toString());
	}
	
	writer->writeEndElement();
}

void Location::fromXml(const QString &xml)
{
	QXmlStreamReader reader(xml);
	
	while(!reader.atEnd())
	{
		switch(reader.readNext())
		{
			case QXmlStreamReader::StartElement:
			{
				QString tag = reader.name().toString();
				
				if( tag == "location" )
				{
					QXmlStreamAttributes atts = reader.attributes();
					
					foreach(QXmlStreamAttribute att, atts)
					{
						if( att.name().toString() == "scene" )
						{
							setValue(Scene, att.value().toString().toInt());
						}
						else if( att.name().toString() == "layer" )
						{
							setValue(Layer, att.value().toString().toInt());
						}
						else if( att.name().toString() == "frame" )
						{
							setValue(Frame, att.value().toString().toInt());
						}
						else if( att.name().toString() == "object" )
						{
							setValue(Object, att.value().toString().toInt());
						}
						else if( att.name().toString() == "symbol" )
						{
							setValue(Symbol, att.value().toString());
						}
					}
				}
			}
			break;
			default: break;
		}
	}
}

}

}
