/***************************************************************************
 *   Copyright (C) 2007 David Cuadrado                                     *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/

#ifndef YAMF_MODELLIBRARYITEM_H
#define YAMF_MODELLIBRARYITEM_H

#include <yamf/item/proxy.h>
#include <yamf/common/abstractserializable.h>

#include <yamf/common/yamf_exports.h>
#include <yamf/model/location.h>

namespace YAMF {
namespace Model {

class LibraryObject;

/**
 * @ingroup model
 * @author David Cuadrado \<krawek@gmail.com\>
*/
class YAMF_EXPORT LibraryItem : public Item::Proxy, public Common::AbstractSerializable
{
	public:
		LibraryItem();
		LibraryItem(LibraryObject *object);
		~LibraryItem();
		
		QDomElement toXml(QDomDocument &doc) const;
		void fromXml(const QString &xml);
		
		void setSymbolName(const QString &name);
		QString symbolName() const;
		
		void setObject(LibraryObject *object);
		LibraryObject *libraryObject() const;
		
	private:
		struct Private;
		Private *const d;

};

}
}

#endif
