/***************************************************************************
 *   Copyright (C) 2005 by David Cuadrado                                  *
 *   krawek@gmail.com                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef YAMFMODELLAYER_H
#define YAMFMODELLAYER_H


#include <QDomDocument>
#include <QDomElement>

#include <yamf/common/inthash.h>
#include <yamf/common/yamf_exports.h>
#include <yamf/common/abstractserializable.h>


#include <yamf/model/frame.h>
#include <yamf/model/location.h>

namespace YAMF {
namespace Model {

typedef Common::IntHash<Frame *> Frames;

class Scene;
class Project;

/**
 * @ingroup model
 * @brief Esta clase representa un layer, los layers estan contenidos en una escena y contienen Frame's
 * @author David Cuadrado \<krawek@gmail.com\>
*/

class YAMF_EXPORT Layer : public QObject, public Common::AbstractSerializable
{
	Q_OBJECT
	public:
		enum Type
		{
			Vectorial = 0x0,
			Audio = 0x04
		};
		
		Layer(Scene *parent, const QString& name = tr("Layer"));
		~Layer();
		
		void setFrames(const Frames &frames);
		Frames frames();
		
		void setLayerName(const QString &name);
		QString layerName() const;
		
		void setLocked(bool isLocked);
		bool isLocked() const;
		
		void setVisible(bool isVisible);
		bool isVisible() const;
		
		Model::Frame *createFrame(int visualIndex = -1, const QString &name = QString());
		void insertFrame(int visualIndex, Model::Frame *frame);
		void addFrame(Model::Frame *frame);
		void removeFrame(Model::Frame *frame);
		
		
		void setType(Type type);
		Type type() const;
		
		Location location() const;
		
	public:
		bool removeFrame(int visualIndex);
		bool moveFrame(int from, int to);
		bool expandFrame(int visualIndex, int size);
		
		Frame *frame(int visualIndex) const;
		Scene *scene() const;
		Project *project() const;
		
		int logicalIndexOf(Frame *frame) const;
		int visualIndexOf(Frame *frame) const;
		int logicalIndex() const;
		int visualIndex() const;
		
		int clones(Frame *frame) const;
		
	public:
		virtual void fromXml(const QString &xml );
		virtual QDomElement toXml(QDomDocument &doc) const;
		
	private:
		struct Private;
		Private *const d;
};

}
}

#endif
