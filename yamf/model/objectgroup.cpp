/***************************************************************************
 *   Copyright (C) 2007 David Cuadrado                                     *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/

#include "objectgroup.h"

#include <item/group.h>

namespace YAMF {

namespace Model {

struct ObjectGroup::Private
{
	Item::Group *group;
	QList<Object *> children;
};

ObjectGroup::ObjectGroup(YAMF::Model::Frame *frame) : Object(frame), d(new Private)
{
	d->group = new Item::Group;
	setItem(d->group);
}


ObjectGroup::~ObjectGroup()
{
	delete d;
}

QList<Object *> ObjectGroup::children() const
{
	return d->children;
}

void ObjectGroup::addToGroup(Object *object)
{
	object->frame()->detach(object);
	d->children << object;
	
	if( object->item() )
	{
		object->item()->setSelected(false);
	}
	
	d->group->addToGroup(object->item());
}

void ObjectGroup::removeFromGroup(Object *object)
{
	d->children.removeAll(object);
	d->group->removeFromGroup(object->item());
	
	object->frame()->attach(object);
}

}

}
