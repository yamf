/***************************************************************************
 *   Copyright (C) 2005 by David Cuadrado                                  *
 *   krawek@gmail.com                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "frame.h"
#include "layer.h"

#include <dcore/debug.h>

#include <QGraphicsItem>

#include "object.h"
#include "scene.h"
#include "item/builder.h"
#include "model/command/manager.h"
#include "model/command/addobject.h"
#include "model/command/removeobject.h"
#include "model/command/lockframe.h"
#include "model/command/changeframevisibility.h"
#include "model/command/renameframe.h"

#include "model/library.h"
#include "model/object.h"
#include "model/objectgroup.h"

#include "item/group.h"
#include "item/tweener.h"

#define INVALID_Z -10000


namespace YAMF {
namespace Model {

struct Frame::Private
{
	Private() : objectCount(0)
	{
	}
	
	QString name;
	bool isLocked;
	bool isVisible;
	GraphicObjects graphics;
	int repeat;
	
	int objectCount;
	
	ObjectGroup *createObjectGroup(Frame *frame , QGraphicsItem *item)
	{
		ObjectGroup *oGroup = new ObjectGroup(frame);
		if(Item::Group *group = static_cast<Item::Group * >(item))
		{
			foreach(QGraphicsItem *child, group->childs())
			{
				if(child->type() == Item::Group::Type)
				{
					ObjectGroup *ocGroup = createObjectGroup(frame, child);
					oGroup->addToGroup(ocGroup);
					
				}
				else
				{
					Object *object = new Object(child, frame);
					oGroup->addToGroup(object);
				}
			}
		}
		return oGroup;
	}
	
	void fixZValue( Object *object )
	{
		if( QGraphicsItem *item = object->item() )
		{
			if( item->zValue() == INVALID_Z )
			{
				double zMax = 0;
				foreach(Object *graphic, graphics.values() )
				{
					if( zMax < graphic->item()->zValue() )
					{
						zMax = graphic->item()->zValue();
					}
				}
				item->setZValue(zMax+0.1);
			}
		}
	}
	
	
};

/**
 * @~spanish
 * Constructor por defecto
 */
Frame::Frame(Layer *parent, const QString& name ) : QObject(parent), d(new Private)
{
	d->name = name;
	d->isLocked = false;
	d->isVisible = true;
	d->repeat = 1;
}

/**
 * @~spanish
 * Destructor
 */
Frame::~Frame()
{
	d->graphics.clear(true);
	delete d;
}

/**
 * @~spanish
 * Limpia el marco.
 */
void Frame::clean()
{
	d->graphics.clear(true);
}

/**
 * @~spanish
 * Pone el nombre del frame
 */
void Frame::setFrameName(const QString &name)
{
	if(name != d->name)
	{
		Command::RenameFrame *cmd = new Command::RenameFrame(this, name, &d->name);
		project()->commandManager()->addCommand(cmd);
	}
}

/**
 * @~spanish
 * Retorna el nombre del frame
 */
QString Frame::frameName() const
{
	return d->name;
}

/**
 * @~spanish
 * Si isLocked es verdadero bloquea el frame, de lo contrario lo desbloquea.
 */
void Frame::setLocked(bool isLocked)
{
	if(isLocked != d->isLocked)
	{
		Command::LockFrame *command = new Command::LockFrame(this, isLocked, &d->isLocked);
		project()->commandManager()->addCommand(command);
	}
}


/**
 * @~spanish
 * Returna verdadero cuando el frame esta bloqueado
 */
bool Frame::isLocked() const
{
	return d->isLocked;
}

/**
 * @~spanish
 * Cambia la visibilidad del marco.
 * Si @p isVisible es verdadero permite la visualización del marco de lo contrario lo oculta.
 */
void Frame::setVisible(bool isVisible)
{
	if(d->isVisible != isVisible)
	{
		Command::ChangeFrameVisibility *command = new Command::ChangeFrameVisibility(this, isVisible, &d->isVisible);
		project()->commandManager()->addCommand(command);
	}
}

/**
 * @~spanish
 * Retorna verdadero si el marco es visible, de lo contrario retorna falso.
 */
bool Frame::isVisible() const
{
	return d->isVisible;
}

/**
 * @~spanish
 * Añade un objeto grafico al marco.
 */
void Frame::addObject(Object *object)
{
	Command::Manager *commandManager = project()->commandManager();
	
	object->setParent(this);
	
	d->fixZValue(object);
	
	if( object->objectName().isEmpty() )
	{
		object->setObjectName(tr("Object %1").arg(d->objectCount++));
	}
	
	Command::AddObject *command = new Command::AddObject(object, &d->graphics);
	commandManager->addCommand(command);
}

/**
 * @~spanish
 * Remueve el objeto grafico  @p object del marco.
 */
void Frame::removeObject(Object *object)
{
	Command::RemoveObject *cmd = new Command::RemoveObject(object, &d->graphics, this);
	project()->commandManager()->addCommand(cmd);
}

/**
 * @~spanish
 * Remueve los objetos graficos  @p objects del marco.
 */
void Frame::removeObjects(const QList<Object *> &objects)
{
	Command::RemoveObject *cmd = new Command::RemoveObject(objects, &d->graphics, this);
	project()->commandManager()->addCommand(cmd);
		
	foreach(Object *object, objects)
	{
		this->scene()->removeTweeningObject(object);
	}
}

/**
 * @~spanish
 * Remueve los objetos graficos que contiene el ítem @p item del marco.
 */
void Frame::removeItem(QGraphicsItem *item)
{
	int index = visualIndexOf(item);
	if( index >= 0 )
	{
		removeObject(graphic(index));
	}
}

/**
 * @~spanish
 * Remueve los objetos graficos que contiene los ítems @p items del marco.
 */
void Frame::removeItems(const QList<QGraphicsItem *> &items)
{
	QList<Object *> objects;
	foreach(QGraphicsItem *item, items)
	{
		int index = visualIndexOf(item);
		if( index >= 0 )
		{
			objects << graphic(index);
		}
	}
	
	if(!objects.isEmpty())
		removeObjects(objects);
}

/**
 * @~spanish
 * Añade el item grafico @p item al marco, retornando el objeto grafico que lo contiene.
 */
Object *Frame::addItem(QGraphicsItem *item)
{
	Object *object = new Object(item, this);
	object->setObjectName(tr("Object %1").arg(d->objectCount++));
	item->setZValue(INVALID_Z);
	
	addObject(object);
	
	return object;
}

/**
 * @~spanish
 * Reemplaza el item que contien el objeto grafico de la posición @p logicalIndex por el ítem @p item..
 */
void Frame::replaceItem(int logicalIndex, QGraphicsItem *item)
{
	Object *toReplace = this->graphic(logicalIndex);
	
	if ( toReplace )
	{
		toReplace->setItem( item );
	}
}

/**
 * @~spanish
 * Remueve el objeto grafico que esta en la posición @p logicalIndex del marco.
 */
void Frame::removeObjectAt(int logicalIndex)
{
	if ( !d->graphics.contains(logicalIndex) )
	{
		return;
	}
	
	Object *object = graphic(logicalIndex);
	removeObject(object);
}

void Frame::detach(Model::Object *object)
{
	d->graphics.remove(object);
		
	QGraphicsItem *item = object->item();
		
	if ( item )
	{
		QGraphicsScene *scene = item->scene();
			
		if(scene)
		{
			scene->removeItem(item);
		}
	}
}

void Frame::attach(Model::Object *object)
{
	d->graphics.add(object);
}

/**
 * @~spanish
 * Cambia la posición del objeto grafico de la posición @p currentPosition a la posición @p newPosition.
 */
bool Frame::moveItem(int currentPosition, int newPosition)
{
	D_FUNCINFO << "current "<< currentPosition << " new "  << newPosition;
	if(currentPosition == newPosition || currentPosition < 0 || currentPosition >= d->graphics.count() || newPosition < 0 || newPosition >= d->graphics.count())
	{
		return false;
	}
	
	if(currentPosition < newPosition)
	{
		for( int i = currentPosition; i < newPosition; i++)
		{
			double tmp = d->graphics.visualValue(i)->item()->zValue();
			d->graphics.visualValue(i)->item()->setZValue(d->graphics.visualValue(i+1)->item()->zValue());
			d->graphics.visualValue(i+1)->item()->setZValue(tmp);
			d->graphics.moveVisual(i, i+1);
		}
	}
	else
	{
		for( int i = currentPosition; i > newPosition; i--)
		{
			double tmp = d->graphics.visualValue(i)->item()->zValue();
			d->graphics.visualValue(i)->item()->setZValue(d->graphics.visualValue(i-1)->item()->zValue());
			d->graphics.visualValue(i-1)->item()->setZValue(tmp);
			d->graphics.moveVisual(i, i-1);
		}
	}
	return true;
}



/**
 * @~spanish
 * Retorna los objetos graficos que contiene el marco.
 */
GraphicObjects Frame::graphics() const
{
	return d->graphics;
}

/**
 * @~spanish
 * Retorna el objeto grafico de la posición @p visualIndex.
 */
Object *Frame::graphic(int visualIndex) const
{
	if ( !d->graphics.containsVisual(visualIndex) )
	{
		D_FUNCINFO << " FATAL ERROR: index out of bound " << visualIndex << " FRAME: " << this->visualIndex() << " LAYER: " << layer()->visualIndex();
		return 0;
	}
	
	return d->graphics.visualValue(visualIndex);
}

/**
 * @~spanish
 * Retorna el objecto grafico que contiene al item @p item.
 */
Object *Frame::graphic(QGraphicsItem *item) const
{
	if( item )
	{
		foreach(Object *object, d->graphics.values() )
		{
			if (object->item() == item )
			{
				return object;
			}
		}
	}
	return 0;
}

Object *Frame::findGraphic(QGraphicsItem *item) const
{
	Object *object = graphic(item);
	
	if( ! object )
	{
		object = scene()->tweeningObject(item);
	}
	
	return object;
}

/**
 * @~spanish
 * Retorna el item que contiene el objeto grafico de la posición @p visualIndex.
 */
QGraphicsItem *Frame::item(int visualIndex) const
{
	Object *object = graphic(visualIndex);
	
	if ( object )
	{
		return object->item();
	}
	
	return 0;
}

/**
 * @~spanish
 * Obtiene la posición visual del objeto grafico @p object.
 */
int Frame::visualIndexOf(Object *object) const
{
	return d->graphics.visualIndex(object);
}

/**
 * @~spanish
 * Obtiene la posición logica del objeto grafico @p object.
 */
int Frame::logicalIndexOf(Object *object) const
{
	return d->graphics.logicalIndex(object);
}

/**
 * @~spanish
 * Obtiene la posición visual del objeto grafico que contiene al ítem @p item.
 */
int Frame::visualIndexOf(QGraphicsItem *item) const
{
	foreach(Object *object, d->graphics.values() )
	{
		if (object->item() == item )
		{
			return d->graphics.visualIndex(object);
		}
	}
	return -1;
}

/**
 * @~spanish
 * Obtiene la posición logica del objeto grafico que contiene al ítem @p item.
 */
int Frame::logicalIndexOf(QGraphicsItem *item) const
{
	foreach(Object *object, d->graphics.values())
	{
		if (object->item() == item )
		{
			return d->graphics.logicalIndex(object);
		}
	}
	
	return -1;
}


/**
 * @~spanish
 * Retorna la posición visual del marco.
 */
int Frame::visualIndex() const
{
	return layer()->visualIndexOf(const_cast<Frame *>(this));
}


/**
 * @~spanish
 * Retorna la posición logica del marco.
 */
int Frame::logicalIndex() const
{
	return layer()->logicalIndexOf(const_cast<Frame *>(this));
}

Model::Location Frame::location() const
{
	Model::Location loc;
	loc.setValue(Location::Scene, scene()->visualIndex() );
	loc.setValue(Location::Layer, layer()->visualIndex() );
	loc.setValue(Location::Frame, this->visualIndex() );
	
	return loc;
}


/**
 * @~spanish
 * Retorna la capa en la que esta el marco.
 */
Layer *Frame::layer() const
{
	return static_cast<Layer *>(parent());
}

/**
 * @~spanish
 * Retorna la escena en la que esta el marco.
 */
Scene *Frame::scene() const
{
	return layer()->scene();
}


/**
 * @~spanish
 * Retorna el proyecto en el que esta el marco.
*/
Project *Frame::project() const
{
	return layer()->project();
}


/**
 * @~spanish
 * Construye el marco a partir del xml.
 */
void Frame::fromXml(const QString &xml )
{
	QDomDocument document;
	
	if (! document.setContent(xml) )
	{
		return;
	}
	
	QDomElement root = document.documentElement();
	
	setFrameName( root.attribute( "name", frameName() ) );
	
	QDomNode n = root.firstChild();
	
	while( !n.isNull() )
	{
		QDomElement e = n.toElement();
		
		if(!e.isNull())
		{
			if( e.tagName() == "object" )
			{
				QString objectXml;
				QTextStream ts(&objectXml);
				ts << e;
				
				Object *last = new Object(this);
				last->fromXml(objectXml);
				
				if( last->item() )
				{
					if(last->item()->type() == Item::Group::Type)
					{
						ObjectGroup *oGroup = d->createObjectGroup(this, last->item());
						addObject(oGroup);
					}
					else
					{
						addObject(last);
					}
				}
				else
				{
					delete last;
				}
			}
		}
		
		n = n.nextSibling();
	}
}

/**
 * @~spanish
 * Convierte el marco a un documento xml.
 */
QDomElement Frame::toXml(QDomDocument &doc) const
{
	QDomElement root = doc.createElement("frame");
	root.setAttribute("name", d->name );
	root.setAttribute("index", visualIndex());
	int nclones = layer()->clones(const_cast<Frame *>(this));
	if(nclones > 0)
	{
		root.setAttribute("clones", nclones);
	}
	
	doc.appendChild(root);
	
	foreach(Object *object, d->graphics.visualValues() )
	{
		if( dynamic_cast<Common::AbstractSerializable *>(object->item()) )
		{
			root.appendChild( object->toXml(doc) );
		}
	}
	
	return root;
}

}
}
