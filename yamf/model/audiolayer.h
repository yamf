/***************************************************************************
 *   Copyright (C) 2007 by David Cuadrado                                  *
 *   krawek@gmail.com                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef YAMFMODELAUDIOLAYER_H
#define YAMFMODELAUDIOLAYER_H

#include <yamf/model/layer.h>
#include <yamf/common/yamf_exports.h>

namespace YAMF {
namespace Model {
class LibrarySoundObject;

/**
 * @ingroup model
 * @author David Cuadrado <krawek@gmail.com>
*/
class YAMF_EXPORT AudioLayer : public Model::Layer
{
	public:
		AudioLayer(Scene *parent, const QString& name = tr("Audio layer"));
		~AudioLayer();
		
		void setStartFrame(int frame);
		int startFrame() const;
		
		void fromFile(const QString& fileName);
		
		void fromSymbol(const QString &symbolName);
		void fromSymbol(LibrarySoundObject *object);
		QString filePath() const;
		
		void play();
		void stop();
		void setVolume(int percent);
		
		void fromXml(const QString &xml );
		QDomElement toXml(QDomDocument &doc) const;
		
	private:
		struct Private;
		Private *const d;

};

}
}
#endif
