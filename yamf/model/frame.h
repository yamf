/***************************************************************************
 *   Copyright (C) 2005 by David Cuadrado                                  *
 *   krawek@gmail.com                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef YAMFMODELFRAME_H
#define YAMFMODELFRAME_H

#include <QGraphicsScene>

#include <QDomDocument>
#include <QDomElement>

#include <yamf/common/inthash.h>
#include <yamf/common/abstractserializable.h>
#include <yamf/common/yamf_exports.h>

#include <yamf/model/location.h>


namespace YAMF {
namespace Model {

class Frame;
class Layer;
class Object;
class Project;
class Scene;

typedef Common::IntHash<Object *> GraphicObjects;

/**
 * @ingroup model
 * @~spanish
 * @brief Esta clase representa un marco o frame de la animación.
 * @author David Cuadrado \<krawek@gmail.com\>
*/
class YAMF_EXPORT Frame : public QObject, public YAMF::Common::AbstractSerializable
{
	public:
		Frame(Layer *parent, const QString& name = tr("Frame"));
		~Frame();
		void clean();
		
		void setFrameName(const QString &name);
		QString frameName() const;
		
		void setLocked(bool isLocked);
		bool isLocked() const;
		
		void setVisible(bool isVisible);
		bool isVisible() const;
		
		void addObject(Object *object);
		Object *addItem(QGraphicsItem *item);
		
		void removeObject(Object *object);
		void removeObjects(const QList<Object *> &objects);
		void removeItem(QGraphicsItem *item);
		void removeItems(const QList<QGraphicsItem *> &items);
		void removeObjectAt(int logicalIndex);
		
		void detach(Model::Object *object);
		void attach(Model::Object *object);
		
		void replaceItem(int logicalIndex, QGraphicsItem *item);
		bool moveItem(int currentPosition, int newPosition);
		
		GraphicObjects graphics() const;
		Object *graphic(int visualIndex) const;
		Object *graphic(QGraphicsItem *item) const;
		Object *findGraphic(QGraphicsItem *item) const;
		
		QGraphicsItem *item(int visualIndex) const;
		
		Layer *layer() const;
		Scene *scene() const;
		Project *project() const;
		
		int visualIndexOf(Object *object) const;
		int logicalIndexOf(Object *object) const;
		
		int visualIndexOf(QGraphicsItem *item) const;
		int logicalIndexOf(QGraphicsItem *item) const;
		
		int visualIndex() const;
		int logicalIndex() const;
		
		Location location() const;
		
	public:
		virtual void fromXml(const QString &xml );
		virtual QDomElement toXml(QDomDocument &doc) const;
		
	private:
		struct Private;
		Private *const d;
};

}
}

#endif
