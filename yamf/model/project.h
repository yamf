/***************************************************************************
 *   Copyright (C) 2005 by David Cuadrado                                  *
 *   krawek@gmail.com                                          	   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef YAMFMODELPROJECT_H
#define YAMFMODELPROJECT_H

#include <yamf/common/abstractserializable.h>
#include <yamf/common/inthash.h>
#include <yamf/common/yamf_exports.h>

#include <QObject>

class QGraphicsItem;



namespace YAMF {
namespace Command { class Manager; }

namespace Model {

class Scene;
class Layer;
class Frame;
class Library;

typedef Common::IntHash<Scene *> Scenes;


/**
 * @ingroup model
 * Esta clase maneja el proyecto, además contiene las diferentes escenas que componen todo el proyecto.
 * 
 * @brief Project manager
 * @author David Cuadrado \<krawek@gmail.com\>
*/

class YAMF_EXPORT Project : public QObject, public Common::AbstractSerializable
{
	Q_OBJECT;
	public:
		Project(QObject *parent = 0);
		~Project();
		
		
		Command::Manager *commandManager() const;
		
		void setProjectName(const QString &name);
		void setAuthor(const QString &author);
		void setDescription(const QString& description);
		
		QString projectName() const;
		QString author() const;
		QString description() const;
		
		bool deleteDataDir();
		QString dataDir() const;
		
		Scene *scene(int visualIndex) const;
		
		int visualIndexOf(Scene *scene) const;
		int logicalIndexOf(Scene *scene) const;
		
		Scenes scenes() const;
		
		Model::Scene *createScene(int visualIndex = -1, const QString &name = QString());
		
		void insertScene(Scene *scene, int visualIndex);
		void addScene(Scene *scene);
		void removeScene(Scene *scene);
		bool moveScene(int position, int newPosition);
		
		void clear();
		void loadLibrary(const QString &filename);
		
		Model::Library *library() const;
		
		virtual void fromXml(const QString &xml );
		virtual QDomElement toXml(QDomDocument &doc) const;
		
		void setOpen(bool open);
		bool isOpen();
		
	private:
		struct Private;
		Private *const d;
};

}
}

#endif
