/***************************************************************************
 *   Copyright (C) 2007 Jorge Cuadrado                                     *
 *   kuadrosxx@gmail.com                                                    *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/
 
#include "changetextwidth.h"


#include <model/scene.h>
#include <model/layer.h>
#include <model/frame.h>
#include <model/object.h>

#include <QXmlStreamWriter>
#include <QGraphicsTextItem>
namespace YAMF {

namespace Command {

struct ChangeTextWidth::Private
{
	Model::Object *object;
	QGraphicsTextItem *item;
	double newWidth;
	double oldWidth;
};

ChangeTextWidth::ChangeTextWidth(Model::Frame *frame, QGraphicsTextItem *item, double oldWidth): Command::Base(), d(new Private)
{
	setText(QObject::tr("Changed text width"));
	d->object = frame->graphic(item);
	d->item = item;
	d->oldWidth = oldWidth;
	d->newWidth = item->textWidth();
}


ChangeTextWidth::~ChangeTextWidth()
{
	delete d;
}


QString ChangeTextWidth::name() const
{
	return "changetextwidth";
}

Model::Location ChangeTextWidth::location() const
{
	Model::Location location;
	location.setValue(Model::Location::Object, d->object->visualIndex());
	location.setValue(Model::Location::Scene, d->object->scene()->visualIndex());
	location.setValue(Model::Location::Layer, d->object->layer()->visualIndex());
	location.setValue(Model::Location::Frame, d->object->frame()->visualIndex());
	return location;
}

void ChangeTextWidth::execute()
{
	d->item->setTextWidth(d->newWidth);
}

void ChangeTextWidth::unexecute()
{
	d->item->setTextWidth(d->oldWidth);
}

void ChangeTextWidth::writeCommandBody(QXmlStreamWriter *writer) const
{
	writer->writeEmptyElement("information");
	writer->writeAttribute("oldwidth", QString::number(d->oldWidth));
	writer->writeAttribute("newwidth", QString::number(d->newWidth));
}


}

}
