/***************************************************************************
 *   Copyright (C) 2007 David Cuadrado                                     *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/

#include "insertlayer.h"

#include <model/scene.h>
#include <model/layer.h>
#include <model/project.h>

#include <dcore/debug.h>

#include <QXmlStreamWriter>

namespace YAMF {

namespace Command {

struct InsertLayer::Private
{
	Private() : layers(0), layer(0), index(-1) {}
	
	Common::IntHash<Model::Layer *> *layers;
	Model::Layer *layer;
	
	int index;
};

/**
 * @~spanish
 * Construye un comando para insertar la capa @p layer en la coleccion de capas @p layers.
 */
InsertLayer::InsertLayer(Model::Layer *layer, Common::IntHash<Model::Layer *> *layers)
 : YAMF::Command::Base(), d(new Private)
{
	setText(QObject::tr("Insert layer: ")+layer->layerName());
	
	d->layers = layers;
	d->layer = layer;
}

/**
 * @~spanish
 * Construye un comando para insertar la capa @p layer en la colección de capas @p layers y la posición @p visualIndex.
 */
InsertLayer::InsertLayer(Model::Layer *layer, Common::IntHash<Model::Layer *> *layers, int visualIndex) : YAMF::Command::Base(), d(new Private)
{
	setText(QObject::tr("Insert layer: ")+layer->layerName());
	
	if( visualIndex < 0 )
	{
		d->index = layers->count();
	}
	else 
	{
		d->index = visualIndex;
	}
	
	d->layers = layers;
	d->layer = layer;
}

/**
 * Destructor
 */
InsertLayer::~InsertLayer()
{
	delete d;
}

QString InsertLayer::name() const
{
	return "insertlayer";
}

Model::Location InsertLayer::location() const
{
	Model::Location location;
	location.setValue(Model::Location::Layer, d->index);
	location.setValue(Model::Location::Scene, d->layer->scene()->visualIndex());
	
	return location;
}

/**
 * @~spanish
 * Ejecuta el comando.
 */
void InsertLayer::execute()
{
	if( d->index < 0 )
	{
		d->layers->add(d->layer);
	}
	else
	{
		d->layers->insert(d->index, d->layer);
	}
	d->index = d->layers->visualIndex(d->layer);
}

/**
 * @~spanish
 * Deshace la ejecución del comando  el comando.
 */
void InsertLayer::unexecute()
{
	d->index = d->layers->visualIndex(d->layer);
	d->layers->remove(d->layer);
}

/**
 * @~spanish
 */
void InsertLayer::writeCommandBody(QXmlStreamWriter *writer) const
{
	QString xml;
	
	{
		QDomDocument doc;
		doc.appendChild(d->layer->toXml(doc));
		xml = doc.toString();
	}
	
	QXmlStreamReader reader(xml);
	
	while(!reader.atEnd())
	{
		if( reader.readNext() != QXmlStreamReader::StartDocument)
			writer->writeCurrentToken(reader);
	}
}

}

}
