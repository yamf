/***************************************************************************
 *   Copyright (C) 2007 David Cuadrado                                     *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/

#include "removeobject.h"


#include <model/scene.h>
#include <model/layer.h>
#include <model/frame.h>
#include <model/object.h>

#include <dcore/debug.h>

#include <QGraphicsScene>
#include <QGraphicsItem>

#include <QXmlStreamWriter>

namespace YAMF {

namespace Command {

struct RemoveObject::Private
{
	Private() 
	{
	}
	Common::IntHash<Model::Object *> *objects;
	QList<Model::Object *> toRemove;
	Model::Frame *frame;
	
	QString indices;
	
	void saveIndices()
	{
		QList<Model::Object *>::iterator it = toRemove.begin();
		while(it != toRemove.end())
		{
			if(it != toRemove.end()-1)
				indices += QString("%1,").arg((*it)->visualIndex());
			else
				indices += QString("%1").arg((*it)->visualIndex());
			++it;
		}
	};
	
};

/**
 * @~spanish
 * Construye un comando para remover la lista de objetos graficos @p toRemove de la colección que los contiene @p objects.
 */
RemoveObject::RemoveObject(const QList<Model::Object *> &toRemove, Common::IntHash<Model::Object *> *objects, Model::Frame *frame) : YAMF::Command::Base(), d(new Private)
{
	setText(QObject::tr("Remove %1 objects").arg(toRemove.count()));
	
	d->frame = frame;
	
	d->objects = objects;
	d->toRemove = toRemove;
	d->saveIndices();
}

/**
 * Construye un comando para remover el objeto grafico @p object de la colección que lo contiene @p objects.
 */
RemoveObject::RemoveObject(Model::Object *object, Common::IntHash<Model::Object *> *objects, Model::Frame *frame)
 : YAMF::Command::Base(), d(new Private)
{
	setText(QObject::tr("Remove object"));
	
	d->frame = frame;
	d->objects = objects;
	d->toRemove << object;
	
	d->saveIndices();
}

/**
 * Destructor
 */
RemoveObject::~RemoveObject()
{
	delete d;
}

QString RemoveObject::name() const
{
	return "removeobject";
}

Model::Location RemoveObject::location() const
{
	Model::Location location;
	
	location.setValue(Model::Location::Frame, d->frame->visualIndex());
	location.setValue(Model::Location::Object, d->toRemove.first()->visualIndex());
	location.setValue(Model::Location::Scene, d->frame->scene()->visualIndex());
	location.setValue(Model::Location::Layer, d->frame->layer()->visualIndex());
	
	return location;
}


/**
 * @~spanish
 * Ejecuta el comando.
 */
void RemoveObject::execute()
{
	foreach(Model::Object *object, d->toRemove)
	{
		object->setTweeningEnabled(false);
		d->objects->remove(object);
		
		QGraphicsItem *item = object->item();
		
		if ( item )
		{
			QGraphicsScene *scene = item->scene();
			
			if(scene)
			{
				scene->removeItem(item);
			}
		}
	}
}

/**
 * @~spanish
 * Deshace la ejecución del comando  el comando.
 */
void RemoveObject::unexecute()
{
	foreach(Model::Object *object, d->toRemove)
	{
		d->objects->add(object);
		object->setTweeningEnabled(true);
	}
	d->saveIndices();
}

/**
 * @~spanish
 */
void RemoveObject::writeCommandBody(QXmlStreamWriter *writer) const
{
	writer->writeEmptyElement("information");
	writer->writeAttribute("indices", d->indices);
}

}

}
