/***************************************************************************
 *   Copyright (C) 2007 David Cuadrado                                     *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/

#include "removescene.h"

#include <model/scene.h>
#include <model/project.h>

namespace YAMF {

namespace Command {

struct RemoveScene::Private
{
	Common::IntHash<Model::Scene *> *scenes;
	Model::Scene *scene;
	
	int index;
};

/**
 * @~spanish
 * Construye un comando para remover el objeto grafico @p object de la colección que lo contiene @p objects.
 */
RemoveScene::RemoveScene(Model::Scene *scene, Common::IntHash<Model::Scene *> *scenes)
 : YAMF::Command::Base(), d(new Private)
{
	setText(QObject::tr("Remove scene: ")+scene->sceneName());
	
	d->scenes = scenes;
	d->scene = scene;
	
	d->index = d->scene->visualIndex();
}

/**
 * Destructor
 */
RemoveScene::~RemoveScene()
{
	delete d;
}

QString RemoveScene::name() const
{
	return "removescene";
}

Model::Location RemoveScene::location() const
{
	Model::Location location;
	location.setValue(Model::Location::Scene, d->index);
	
	return location;
}

/**
 * @~spanish
 * Ejecuta el comando.
 */
void RemoveScene::execute()
{
	d->scenes->remove(d->scene);
}

/**
 * @~spanish
 * Deshace la ejecución del comando  el comando.
 */
void RemoveScene::unexecute()
{
	d->scenes->insert(d->index, d->scene);
}

/**
 * @~spanish
 */
void RemoveScene::writeCommandBody(QXmlStreamWriter *writer) const
{
	
}

}

}
