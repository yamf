/***************************************************************************
 *   Copyright (C) 2007 David Cuadrado                                     *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/


#include "modifysymbol.h"

#include <QXmlStreamWriter>

#include "yamf/model/libraryitem.h"
#include "yamf/model/libraryobject.h"
#include "yamf/item/serializer.h"

namespace YAMF {
namespace Command {

struct ModifySymbol::Private
{
	Model::LibraryItem *item;
	QMatrix original, newMatrix;
};

/**
 * @~spanish
 * Construye un comando para modificar las propiedades del objeto de la biblioteca @p item.
 */
ModifySymbol::ModifySymbol(Model::LibraryItem *item, QUndoCommand *parent)
 : YAMF::Command::Base(parent), d(new Private)
{
	setText(QObject::tr("Modify symbol:")+item->symbolName());
	
	d->item = item;
	d->original = d->item->item()->matrix();
	d->newMatrix = d->item->matrix();
}

/**
 * Destructor
 */
ModifySymbol::~ModifySymbol()
{
	delete d;
}

QString ModifySymbol::name() const
{
	return "modifysymbol";
}

Model::Location ModifySymbol::location() const
{
	Model::Location location;
	
	location.setValue(Model::Location::Symbol, d->item->symbolName());
	
	return location;
}

/**
 * @~spanish
 * Ejecuta el comando.
 */
void ModifySymbol::execute()
{
	QGraphicsItem *real = d->item->item();
	real->setMatrix(d->item->matrix());
	d->item->libraryObject()->updateSymbols();
}

/**
 * @~spanish
 * Deshace la ejecución del comando  el comando.
 */
void ModifySymbol::unexecute()
{
	QGraphicsItem *real = d->item->item();
	real->setMatrix(d->original);
	d->item->libraryObject()->updateSymbols();
}

/**
 * @~spanish
 */
void ModifySymbol::writeCommandBody(QXmlStreamWriter *writer) const
{
	writer->writeStartElement("object");
	writer->writeAttribute("id", d->item->symbolName() );
	writer->writeAttribute("index", QString::number(d->item->libraryObject()->indexOf(d->item)) );
	
	YAMF::Item::Serializer::properties(d->item, writer);
	
	writer->writeEndElement();
}


}

}
