/***************************************************************************
 *   Copyright (C) 2007 David Cuadrado                                     *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/

#ifndef YAMF_COMMANDREMOVEOBJECT_H
#define YAMF_COMMANDREMOVEOBJECT_H

#include <yamf/model/command/base.h>
#include <yamf/common/inthash.h>
#include <yamf/common/yamf_exports.h>

namespace YAMF {

namespace Model {
	class Object;
	class Frame;
}

namespace Command {

/**
 * @ingroup model
 * @~spanish
 * @brief Esta clase provee de un comando para remover un objeto grafico de la colección que lo contiene.
 * @author David Cuadrado <krawek@gmail.com>
*/
class YAMF_EXPORT RemoveObject : public YAMF::Command::Base
{
	public:
		RemoveObject(const QList<Model::Object *> &toRemove, Common::IntHash<Model::Object *> *objects, Model::Frame *frame);
		RemoveObject(Model::Object *object, Common::IntHash<Model::Object *> *objects, Model::Frame *frame);
		~RemoveObject();
		
		QString name() const;
		Model::Location location() const;
		
	protected:
		virtual void execute();
		virtual void unexecute();
		
		void writeCommandBody(QXmlStreamWriter *writer) const;
		
	private:
		struct Private;
		Private *const d;
};

}

}

#endif
