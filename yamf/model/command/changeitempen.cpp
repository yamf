/***************************************************************************
 *   Copyright (C) 2007 David Cuadrado                                     *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/

 

#include "changeitempen.h"
#include "model/object.h"

#include <QAbstractGraphicsShapeItem>

#include "item/serializer.h"
#include "item/proxy.h"

namespace YAMF {

namespace Command {

struct ChangeItemPen::Private {
	Model::Frame *frame;
	QGraphicsItem *item;
	
	QPen newPen;
	QPen oldPen;
	
};

ChangeItemPen::ChangeItemPen(Model::Frame *frame, QGraphicsItem *item, const QPen &newPen)
 : YAMF::Command::Base(), d(new Private)
{
	setText(QObject::tr("Changed item pen"));
	d->frame = frame;
	d->item = item;
	d->newPen = newPen;
	
	
	QGraphicsItem *target = item;
	while( Item::Proxy *proxy = qgraphicsitem_cast<Item::Proxy *>(target ) )
	{
		target = proxy->item();
	}
	
	if( QAbstractGraphicsShapeItem *shape = qgraphicsitem_cast<QAbstractGraphicsShapeItem *>(target) )
	{
		d->oldPen = shape->pen();
	}
}


ChangeItemPen::~ChangeItemPen()
{
	delete d;
}

QString ChangeItemPen::name() const
{
	return "changeitempen";
}

Model::Location ChangeItemPen::location() const
{
	Model::Location loc;
	
	Model::Object *object = d->frame->graphic(d->item);
	
	if( !object )
	{
		return loc;
	}
	
	return object->location();
}

void ChangeItemPen::execute()
{
	QGraphicsItem *item = d->item;
	while( Item::Proxy *proxy = qgraphicsitem_cast<Item::Proxy *>(item ) )
	{
		item = proxy->item();
	}
	
	if( QAbstractGraphicsShapeItem *shape = qgraphicsitem_cast<QAbstractGraphicsShapeItem *>(item) )
	{
		shape->setPen(d->newPen);
	}
}

void ChangeItemPen::unexecute()
{
	QGraphicsItem *item = d->item;
	while( Item::Proxy *proxy = qgraphicsitem_cast<Item::Proxy *>(item ) )
	{
		item = proxy->item();
	}
	
	if( QAbstractGraphicsShapeItem *shape = qgraphicsitem_cast<QAbstractGraphicsShapeItem *>(item) )
	{
		shape->setPen(d->oldPen);
	}
}

void ChangeItemPen::writeCommandBody(QXmlStreamWriter *writer) const
{
	Item::Serializer::pen(&d->newPen, writer);
}

}

}
