/***************************************************************************
 *   Copyright (C) 2007 David Cuadrado                                     *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/

#include "modifyitem.h"

#include <model/scene.h>
#include <model/layer.h>
#include <model/frame.h>
#include <model/object.h>

#include <item/serializer.h>

#include <QXmlStreamWriter>

namespace YAMF {

namespace Command {

struct ModifyItem::Private
{
	Model::Frame *frame;
	Model::Object *object;
	
	QString oldProperties;
	QString newProperties;
};

/**
 * @~spanish
 * Construye un comando para modificar las propiedades del elemento grafico @p item.
 */
ModifyItem::ModifyItem(QGraphicsItem *item, const QString &properties, Model::Frame *frame, QUndoCommand *parent)
 : YAMF::Command::Base(parent), d(new Private)
{
	setText(QObject::tr("Modify item"));
	d->frame = frame;
	
	int index = frame->visualIndexOf(item);
	d->object = frame->graphic(index);
	
	d->oldProperties = properties;
	
	QDomDocument doc;
	doc.appendChild(Item::Serializer::properties(item, doc));
	d->newProperties = doc.toString();
}

/**
 * Destructor
 */
ModifyItem::~ModifyItem()
{
	delete d;
}

QString ModifyItem::name() const
{
	return "modifyitem";
}

Model::Location ModifyItem::location() const
{
	Model::Location location;
	
	location.setValue(Model::Location::Object, d->object->visualIndex());
	location.setValue(Model::Location::Scene, d->object->scene()->visualIndex());
	location.setValue(Model::Location::Layer, d->object->layer()->visualIndex());
	location.setValue(Model::Location::Frame, d->object->frame()->visualIndex());
	
	return location;
}

/**
 * @~spanish
 * Ejecuta el comando.
 */
void ModifyItem::execute()
{
	QDomDocument doc;
	doc.setContent(d->newProperties);
	Item::Serializer::loadProperties(d->object->item(), doc.documentElement());
}

/**
 * @~spanish
 * Deshace la ejecución del comando  el comando.
 */
void ModifyItem::unexecute()
{
	QDomDocument doc;
	doc.setContent(d->oldProperties);
	Item::Serializer::loadProperties(d->object->item(), doc.documentElement());
}

/**
 * @~spanish
  */
void ModifyItem::writeCommandBody(QXmlStreamWriter *writer) const
{
	writer->writeEmptyElement("information");
	writer->writeAttribute("oldproperties", d->oldProperties );
	writer->writeAttribute("newproperties", d->newProperties );
}


}

}
