/***************************************************************************
 *   Copyright (C) 2007 David Cuadrado                                     *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/

 

#ifndef YAMF_COMMANDINSERTFRAME_H
#define YAMF_COMMANDINSERTFRAME_H

#include <yamf/model/command/base.h>
#include <yamf/common/inthash.h>
#include <yamf/common/yamf_exports.h>

namespace YAMF {

namespace Model {
	class Frame;
}

namespace Command {

/**
 * @ingroup model
 * @~spanish
 * @brief Esta clase provee de un comando para insertar un marco en una coleccion de marcos.
 * @author David Cuadrado <krawek@gmail.com>
*/
class YAMF_EXPORT InsertFrame : public YAMF::Command::Base
{
	public:
		InsertFrame(Model::Frame *frame, Common::IntHash<Model::Frame *> *frames);
		InsertFrame(Model::Frame *frame, Common::IntHash<Model::Frame *> *frames, int visualIndex);
		
		~InsertFrame();
		
		QString name() const;
		Model::Location location() const;
		
	protected:
		virtual void execute();
		virtual void unexecute();
		
		void writeCommandBody(QXmlStreamWriter *writer) const;
		
	private:
		struct Private;
		Private *const d;
};

}

}

#endif
