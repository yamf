/***************************************************************************
 *   Copyright (C) 2007 Jorge Cuadrado                                     *
 *   kuadrosxx@gmail.com                                                   *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/
 
#include "sendbackwardsitem.h"

#include <model/scene.h>
#include <model/layer.h>
#include <model/frame.h>
#include <model/object.h>

#include <dgraphics/algorithm.h>
#include <dcore/debug.h>

#include <QGraphicsItem>

namespace YAMF {

namespace Command {

struct SendBackwardsItem::Private
{
	Private() : object(0), before(0) {}
	Model::Object *object;
	QGraphicsItem  *before;
	QList<QGraphicsItem *> items;
	double z;
	bool valid;
};

/**
 * @~spanish
 * Construye un comando para enviar atras una unidad dentro de su fotograma el objeto grafico @p item.
 */
SendBackwardsItem::SendBackwardsItem(Model::Object *object)
 : Command::Base(), d(new Private)
{
	setText(QObject::tr("Send to backwards"));
	
	
	d->object = object;
	
	if(d->object->item()->scene())
	{
		
		d->items = d->object->item()->scene()->items();
		d->z = d->object->item()->zValue();
	
		DGraphics::Algorithm::sort(d->items, DGraphics::Algorithm::ZAxis );
	
		int index = d->items.indexOf(d->object->item());
	
		if(index > 0)
		{
			d->before = d->items[index-1];
		}
		d->valid = true;
	}
	else
	{
		d->valid = false;
	}
}

/**
 * Destructor
 */
SendBackwardsItem::~SendBackwardsItem()
{
	delete d;
}

QString SendBackwardsItem::name() const
{
	return "sendbackwardsitem";
}

Model::Location SendBackwardsItem::location() const
{
	Model::Location location;
	
	location.setValue(Model::Location::Object, d->object->visualIndex());
	location.setValue(Model::Location::Scene, d->object->scene()->visualIndex());
	location.setValue(Model::Location::Layer, d->object->layer()->visualIndex());
	location.setValue(Model::Location::Frame, d->object->frame()->visualIndex());
	
	return location;
}

/**
 * @~spanish
 * Ejecuta el comando.
 */
void SendBackwardsItem::execute()
{
	if(d->valid)
	{
		if(d->before)
		{
			d->object->item()->setZValue(d->before->zValue());
			d->before->setZValue(d->z);
		}
	}
}

/**
 * @~spanish
 * Deshace la ejecución del comando  el comando.
 */
void SendBackwardsItem::unexecute()
{
	if(d->valid)
	{
		if(d->before)
		{
			d->before->setZValue(d->object->item()->zValue());
			d->object->item()->setZValue(d->z);
		}
	}
}

/**
* @~spanish
*/
void SendBackwardsItem::writeCommandBody(QXmlStreamWriter *writer) const
{
	
}

}

}

