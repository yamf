/***************************************************************************
 *   Copyright (C) 2007 Jorge Cuadrado                                     *
 *   kuadrosxx@gmail.com                                                    *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/
 
#include "bringtofrontitem.h"

#include <model/scene.h>
#include <model/layer.h>
#include <model/frame.h>
#include <model/object.h>

#include <dgraphics/algorithm.h>

#include <QGraphicsItem>


namespace YAMF {
namespace Command {

struct BringToFrontItem::Private
{
	Model::Object *object;
	QList<QGraphicsItem *> items;
	double z;
	bool valid;
};

/**
 * Construye un comando para traer al frente dentro de su fotograma el objeto grafico @p item.
 */
BringToFrontItem::BringToFrontItem(Model::Object *object)
 : Command::Base(), d(new Private)
{
	setText(QObject::tr("Bring to front"));
	d->object = object;
	
	if(d->object->item()->scene())
	{
		d->z = d->object->item()->zValue();
		d->items = d->object->item()->scene()->items();
		
		DGraphics::Algorithm::sort(d->items, DGraphics::Algorithm::ZAxis );
		d->valid = true;
	}
	else
	{
		d->valid = false;
	}
	
}

/**
 * Destructor
 */
BringToFrontItem::~BringToFrontItem()
{
	delete d;
}

QString BringToFrontItem::name() const
{
	return "bringtofrontitem";
}

Model::Location BringToFrontItem::location() const
{
	Model::Location location;
	
	location.setValue(Model::Location::Object, d->object->visualIndex());
	location.setValue(Model::Location::Scene, d->object->scene()->visualIndex());
	location.setValue(Model::Location::Layer, d->object->layer()->visualIndex());
	location.setValue(Model::Location::Frame, d->object->frame()->visualIndex());
	
	return location;
}

/**
 * @~spanish
 * Ejecuta el comando.
 */
void BringToFrontItem::execute()
{
	if(d->valid)
	{
		d->object->item()->setZValue(d->items.last()->zValue()+1);
	}
}

/**
 * @~spanish
 * Deshace la ejecución del comando  el comando.
 */
void BringToFrontItem::unexecute()
{
	if(d->valid)
	{
		d->object->item()->setZValue(d->z);
	}
}

/**
 * @~spanish
 */
void BringToFrontItem::writeCommandBody(QXmlStreamWriter *writer) const
{
	
}

}
}
