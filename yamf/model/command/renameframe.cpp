/***************************************************************************
 *   Copyright (C) 2007 David Cuadrado                                     *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/

#include "renameframe.h"

#include <model/frame.h>
#include <model/layer.h>
#include <model/scene.h>
#include <model/project.h>

#include <dcore/debug.h>

#include <QXmlStreamWriter>

namespace YAMF {

namespace Command {

struct RenameFrame::Private
{
	QString newName;
	QString oldName;
	
	QString *string;
	Model::Frame *frame;
};

/**
 * @~spanish
 * Construye un comando para cambiar el nombre del marco @p frame.
 */
RenameFrame::RenameFrame(Model::Frame *frame, const QString &newName, QString *string)
 : YAMF::Command::Base(), d(new Private)
{
	d->string = string;
	d->newName = newName;
	d->oldName = *string;
	
	setText(QObject::tr("RenameFrame: %1 => %2").arg(d->oldName).arg(d->newName));
	
	
	d->frame = frame;
}

/**
 * Destructor
 */
RenameFrame::~RenameFrame()
{
	delete d;
}

QString RenameFrame::name() const
{
	return "renameframe";
}

Model::Location RenameFrame::location() const
{
	Model::Location location;
	location.setValue(Model::Location::Frame, d->frame->visualIndex());
	location.setValue(Model::Location::Scene, d->frame->layer()->scene()->visualIndex());
	location.setValue(Model::Location::Layer, d->frame->layer()->visualIndex());
	
	return location;
}

/**
 * @~spanish
 * Ejecuta el comando.
 */
void RenameFrame::execute()
{
	*d->string = d->newName;
}

/**
 * @~spanish
 * Deshace la ejecución del comando  el comando.
 */
void RenameFrame::unexecute()
{
	*d->string = d->oldName;
}

/**
 * @~spanish
 */
void RenameFrame::writeCommandBody(QXmlStreamWriter *writer) const
{
	writer->writeEmptyElement("information");
	writer->writeAttribute("newname", d->newName);
	writer->writeAttribute("oldname", d->oldName);
}

}

}
