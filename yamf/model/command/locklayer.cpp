/***************************************************************************
 *   Copyright (C) 2007 Jorge Cuadrado                                     *
 *   kuadrosxx@gmail.com                                                   *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/

#include "locklayer.h"

#include <model/scene.h>
#include <model/layer.h>
#include <model/project.h>

#include <QXmlStreamWriter>

namespace YAMF {

namespace Command {

struct LockLayer::Private
{
	bool *lock;
	bool newLock;
	Model::Layer *layer;
	
	int index;
	int scene;
};

/**
 * @~spanish
 * Construye un comando para cambiar el estado de bloqueo de la capa @p layer el estado @p newLock.
 */
LockLayer::LockLayer(Model::Layer *layer , bool &newLock, bool *lock)
 : YAMF::Command::Base(), d(new Private)
{
	if(newLock)
	{
		setText(QObject::tr("Lock layer: ")+layer->layerName());
	}
	else
	{
		setText(QObject::tr("Unlock layer: ")+layer->layerName());
	}
	d->layer = layer;
	d->newLock = newLock;
	d->lock = lock;
	
	d->index = d->layer->visualIndex();
	d->scene = d->layer->scene()->visualIndex();
}

/**
 * Destructor
 */
LockLayer::~LockLayer()
{
	delete d;
}

QString LockLayer::name() const
{
	return "locklayer";
}

Model::Location LockLayer::location() const
{
	Model::Location location;
	location.setValue(Model::Location::Scene, d->layer->scene()->visualIndex());
	location.setValue(Model::Location::Layer, d->layer->visualIndex());
	
	return location;
}

/**
 * @~spanish
 * Ejecuta el comando.
 */
void LockLayer::execute()
{
	*d->lock = d->newLock;
}

/**
 * @~spanish
 * Deshace la ejecución del comando  el comando.
 */
void LockLayer::unexecute()
{
	*d->lock = !d->newLock;
}

/**
 * @~spanish
 */
void LockLayer::writeCommandBody(QXmlStreamWriter *writer) const
{
	writer->writeEmptyElement("information");
	writer->writeAttribute("locked", QString::number(d->newLock));
}

}

}
