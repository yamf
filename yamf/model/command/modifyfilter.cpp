/***************************************************************************
 *   Copyright (C) 2007 David Cuadrado                                     *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/


#include "modifyfilter.h"

#include <QXmlStreamWriter>

#include <yamf/model/object.h>
#include <yamf/effect/filter.h>

namespace YAMF {
namespace Command {

struct ModifyFilter::Private
{
	Model::Object *object;
	QString property, value;
	QString oldValue;
};

ModifyFilter::ModifyFilter(Model::Object *object, const QString &property, const QString &value)
 : YAMF::Command::Base(), d(new Private)
{
	setText(QObject::tr("Modify filter"));
	
	d->object = object;
	
	d->property = property;
	d->value = value;
}


ModifyFilter::~ModifyFilter()
{
	delete d;
}


QString ModifyFilter::name() const
{
	return "modifyfilter";
}

void ModifyFilter::execute()
{
	if( YAMF::Effect::Filter *filter = dynamic_cast<YAMF::Effect::Filter *>(d->object->item()) )
	{
		d->oldValue = filter->property(d->property);
		
		filter->setProperty(d->property, d->value);
	}
}

void ModifyFilter::unexecute()
{
	if( YAMF::Effect::Filter *filter = dynamic_cast<YAMF::Effect::Filter *>(d->object->item()) )
	{
		filter->setProperty(d->property, d->oldValue);
	}
}

void ModifyFilter::writeCommandBody(QXmlStreamWriter* writer) const
{
	writer->writeStartElement("properties");
	
	writer->writeStartElement(d->property);
	writer->writeAttribute("value", d->value);
	writer->writeEndElement();
	
	writer->writeEndElement();
}

YAMF::Model::Location ModifyFilter::location() const
{
	return d->object->location();
}

}

}
