/***************************************************************************
 *   Copyright (C) 2007 David Cuadrado                                     *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/

#include "addlibraryobject.h"
#include "yamf/model/libraryobject.h"

#include <QXmlStreamReader>

namespace YAMF {

namespace Command {

struct AddLibraryObject::Private
{
	Private(YAMF::Model::LibraryObject *object, QHash<QString, YAMF::Model::LibraryObject *> *objects) : object(object), objects(objects) {}
	
	YAMF::Model::LibraryObject *object;
	QHash<QString, YAMF::Model::LibraryObject *> *objects;
};

/**
 * @~spanish
 * Construye un comando para añadir un objeto @p object a la biblioteca de objetos.
 */
AddLibraryObject::AddLibraryObject(YAMF::Model::LibraryObject *object, QHash<QString, YAMF::Model::LibraryObject *> *objects)
 : YAMF::Command::Base(), d(new Private(object, objects))
{
	setText(QObject::tr("Add library object: ")+d->object->symbolName());
}


/**
 * Destructor
 */
AddLibraryObject::~AddLibraryObject()
{
	delete d;
}

QString AddLibraryObject::name() const
{
	return "addlibraryobject";
}

Model::Location AddLibraryObject::location() const
{
	Model::Location location;
	
	return location;
}

/**
 * @~spanish
 * Ejecuta el comando.
 */
void AddLibraryObject::execute()
{
	d->objects->insert(d->object->symbolName(), d->object);
}

/**
 * @~spanish
 * Deshace la ejecución del comando  el comando.
 */
void AddLibraryObject::unexecute()
{
	d->objects->remove(d->object->symbolName());
}

/**
 * @~spanish
 */
void AddLibraryObject::writeCommandBody(QXmlStreamWriter *writer) const
{
	QString xml;
	
	{
		QDomDocument doc;
		doc.appendChild(d->object->toXml(doc));
		xml = doc.toString();
	}
	
	QXmlStreamReader reader(xml);
	
	while(!reader.atEnd())
	{
		if( reader.readNext() != QXmlStreamReader::StartDocument)
			writer->writeCurrentToken(reader);
	}
}

}

}
