/***************************************************************************
 *   Copyright (C) 2007 Jorge Cuadrado                                     *
 *   kuadrosxx@gmail.com                                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/

#include "moveframe.h"

#include <model/layer.h>
#include <model/frame.h>
#include <model/project.h>
#include <model/scene.h>

#include <dcore/debug.h>

#include <QXmlStreamWriter>

namespace YAMF {

namespace Command {

struct MoveFrame::Private
{
	Private() : frames(0), layer(0), from(-1), to(-1) {}
	
	Common::IntHash<Model::Frame *> *frames;
	Model::Layer *layer;
	
	int from;
	int to;
};

/**
 * @~spanish
 * Construye un comando para cambiar la posicion del marco que esta en la posición @p from a la posición @p to dentro de la colección que la contiene @p frames.
 */
MoveFrame::MoveFrame(Model::Layer *layer, int from, int to, Common::IntHash<Model::Frame *> *frames)
 : YAMF::Command::Base(), d(new Private)
{
	setText(QObject::tr("Move frame: from %1 to %2").arg(from).arg(to) );
	
	d->frames = frames;
	d->layer = layer;
	d->from = frames->visualValue(from)->visualIndex();
	d->to = frames->visualValue(to)->visualIndex();
}

/**
 * Destructor
 */
MoveFrame::~MoveFrame()
{
	delete d;
}

QString MoveFrame::name() const
{
	return "moveframe";
}

Model::Location MoveFrame::location() const
{
	Model::Location location;
	
	location.setValue(Model::Location::Frame, d->from);
	location.setValue(Model::Location::Scene, d->layer->scene()->visualIndex());
	location.setValue(Model::Location::Layer, d->layer->visualIndex());
	
	return location;
}

/**
 * @~spanish
 * Ejecuta el comando.
 */
void MoveFrame::execute()
{
	d->frames->moveVisual(d->from, d->to);
}

/**
 * @~spanish
 * Deshace la ejecución del comando  el comando.
 */
void MoveFrame::unexecute()
{
	d->frames->moveVisual(d->to, d->from);
}

/**
 * @~spanish
 */
void MoveFrame::writeCommandBody(QXmlStreamWriter *writer) const
{
	writer->writeEmptyElement("information");
	
	writer->writeAttribute("from", QString::number(d->from));
	writer->writeAttribute("to", QString::number(d->to));
}

}

}
