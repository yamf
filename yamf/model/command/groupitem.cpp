/***************************************************************************
 *   Copyright (C) 2007 David Cuadrado                                     *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/
 
#include "groupitem.h"

#include <model/scene.h>
#include <model/layer.h>
#include <model/frame.h>
#include <model/object.h>
#include <model/objectgroup.h>

#include <item/group.h>

#include <QGraphicsItem>
#include <QXmlStreamWriter>

namespace YAMF {

namespace Command {

struct GroupItem::Private
{
	Model::Frame *frame;
	
	QList<Model::Object *> objects;
	Model::ObjectGroup *group;
	int index;
};

/**
 * @~spanish
 * Construye un comando para agrupar la lista de graficos @p items.
 */
GroupItem::GroupItem(const QList<QGraphicsItem *> &items, Model::Frame *frame) : Base(), d(new Private)
{
	setText(QObject::tr("Group items"));
	
	d->group = new Model::ObjectGroup(frame);
	
	
	d->frame = frame;
	
	foreach(QGraphicsItem *item, items)
	{
		Model::Object *object = frame->findGraphic(item);
		if( object )
		{
			d->objects << object;
		}
	}
	
	d->index = -1;
}

/**
 * Destructor
 */
GroupItem::~GroupItem()
{
	delete d;
}

QString GroupItem::name() const
{
	return "groupitem";
}

Model::Location GroupItem::location() const
{
	Model::Location location;
	
	location.setValue(Model::Location::Object, d->index);
	location.setValue(Model::Location::Scene, d->frame->scene()->visualIndex());
	location.setValue(Model::Location::Layer, d->frame->layer()->visualIndex());
	location.setValue(Model::Location::Frame, d->frame->visualIndex());
	
	return location;
}

/**
 * @~spanish
 * Ejecuta el comando.
 */
void GroupItem::execute()
{
	d->group->setTweeningEnabled(true);
	foreach(Model::Object *object, d->objects)
	{
		object->setTweeningEnabled(false);
		d->group->addToGroup(object);
	}
	
	d->group->item()->setSelected(true);
	
	d->frame->attach(d->group);
	d->index = d->group->visualIndex();
}

/**
 * @~spanish
 * Deshace la ejecución del comando  el comando.
 */
void GroupItem::unexecute()
{
	d->frame->detach(d->group);
	d->group->setTweeningEnabled(false);
	
	foreach(Model::Object *object, d->objects)
	{
		object->setTweeningEnabled(true);
		d->group->removeFromGroup(object);
	}
}

/**
 * @~spanish
 */
void GroupItem::writeCommandBody(QXmlStreamWriter *writer) const
{
	writer->writeEmptyElement("information");
	
	QString indices;
	
	QList<Model::Object *>::iterator it = d->objects.begin();
	
	while(it != d->objects.end())
	{
		if(it != d->objects.end()-1)
			indices += QString("%1,").arg((*it)->visualIndex());
		else
			indices += QString("%1").arg((*it)->visualIndex());
		++it;
	}
	
	writer->writeAttribute("items", indices);
}


}

}
