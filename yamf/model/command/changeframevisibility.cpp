/***************************************************************************
 *   Copyright (C) 2007 Jorge Cuadrado                                     *
 *   kuadrosxx@gmail.com                                                   *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/

#include "changeframevisibility.h"

#include <model/scene.h>
#include <model/layer.h>
#include <model/frame.h>
#include <model/project.h>

#include <QXmlStreamReader>

namespace YAMF {

namespace Command {

struct ChangeFrameVisibility::Private
{
	bool *isVisible;
	bool newVisibility;
	Model::Frame *frame;
	
	int index;
	int scene;
	int layer;
};

/**
 * @~spanish
 * Construye un comando para cambiar la visibilidad del marco @p frame, si @p newVisibility es verdadero cambia el marco a visible y es falso cambia el marco a no visible.
 */
ChangeFrameVisibility::ChangeFrameVisibility(Model::Frame *frame , bool &newVisibility, bool *isVisible)
 : YAMF::Command::Base(), d(new Private)
{
	if(newVisibility)
	{
		setText(QObject::tr("Show frame: ")+frame->frameName());
	}
	else
	{
		setText(QObject::tr("Hide frame: ")+frame->frameName());
	}
	d->frame = frame;
	d->newVisibility = newVisibility;
	d->isVisible = isVisible;
	
	d->index = d->frame->visualIndex();
	
	d->scene = d->frame->layer()->scene()->visualIndex();
	d->layer = d->frame->layer()->visualIndex();
}

/**
 * Destructor
 */
ChangeFrameVisibility::~ChangeFrameVisibility()
{
	delete d;
}

QString ChangeFrameVisibility::name() const
{
	return "changeframevisibility";
}

Model::Location ChangeFrameVisibility::location() const
{
	Model::Location location;
	location.setValue(Model::Location::Frame, d->frame->visualIndex());
	location.setValue(Model::Location::Scene, d->frame->layer()->scene()->visualIndex());
	location.setValue(Model::Location::Layer, d->frame->layer()->visualIndex());
	
	return location;
}

/**
 * @~spanish
 * Ejecuta el comando.
 */
void ChangeFrameVisibility::execute()
{
	*d->isVisible = d->newVisibility;
}

/**
 * @~spanish
 * Deshace la ejecución del comando  el comando.
 */
void ChangeFrameVisibility::unexecute()
{
	*d->isVisible = !d->newVisibility;
}

/**
 * @~spanish
 */
void ChangeFrameVisibility::writeCommandBody(QXmlStreamWriter *writer) const
{
	writer->
writeEmptyElement("information");
	writer->writeAttribute("visible", QString::number(d->newVisibility));
}


}

}
