/***************************************************************************
 *   Copyright (C) 2007 David Cuadrado                                     *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/

 

#ifndef YAMF_COMMANDBASE_H
#define YAMF_COMMANDBASE_H

#include <QUndoCommand>
#include <QVariant>

#include <yamf/model/location.h>
#include <yamf/common/yamf_exports.h>

class QXmlStreamWriter;

namespace YAMF {

namespace Command {

class Observer;

/**
 * @ingroup model
 * @author David Cuadrado <krawek@gmail.com>
*/
class YAMF_EXPORT Base : public QUndoCommand
{
	public:
		Base(Observer *observer = 0);
		Base(QUndoCommand *parent, Observer *observer = 0);
		~Base();
		
		void setObserver(Observer *observer);
		Observer *observer() const;
		
		void undo();
		void redo();
		
		virtual QString name() const = 0;
		virtual YAMF::Model::Location location() const = 0;
		
		QString toXml() const;
		
	protected:
		void writeCommand(QXmlStreamWriter *writer) const;
		virtual void writeCommandBody(QXmlStreamWriter *writer) const;
		
	protected:
		virtual void execute() = 0;
		virtual void unexecute() = 0;
		
	private:
		struct Private;
		Private *const d;
};

}

}

Q_DECLARE_METATYPE(YAMF::Command::Base *);
Q_DECLARE_METATYPE(const YAMF::Command::Base *);

#endif
