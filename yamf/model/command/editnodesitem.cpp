/***************************************************************************
 *   Copyright (C) 2007 Jorge Cuadrado                                     *
 *   kuadrosxx@gmail.com                                                   *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/

#include "editnodesitem.h"

#include <dgraphics/pathhelper.h>

#include <model/scene.h>
#include <model/layer.h>
#include <model/frame.h>
#include <model/object.h>

#include <item/converter.h>

#include <QGraphicsItem>
#include <QXmlStreamWriter>


namespace YAMF {

namespace Command {

struct EditNodesItem::Private
{
	QGraphicsPathItem *item;
	QPainterPath newPath, oldPath;
	Model::Frame *frame;
};

/**
 * @~spanish
 * Construye un comando para modificar la figura que tienen un grafico @p item.
 */
EditNodesItem::EditNodesItem(QGraphicsPathItem* item, const QPainterPath& path, Model::Frame *frame): Command::Base(), d(new Private)
{
	setText(QObject::tr("Edit nodes item"));
	
	d->item = item;
	d->frame = frame;
	d->oldPath = path;
	d->newPath = d->item->path();
}

/**
 * Destructor
 */
EditNodesItem::~EditNodesItem()
{
	delete d;
}

QString EditNodesItem::name() const
{
	return "editnodesitem";
}

YAMF::Model::Location EditNodesItem::location() const
{
	Model::Location location;
	
	location.setValue(Model::Location::Object, d->frame->visualIndexOf(d->item));
	location.setValue(Model::Location::Scene, d->frame->scene()->visualIndex());
	location.setValue(Model::Location::Layer, d->frame->layer()->visualIndex());
	location.setValue(Model::Location::Frame, d->frame->visualIndex());
	
	return location;
}

/**
 * @~spanish
 * Ejecuta el comando.
 */
void EditNodesItem::execute()
{
	d->item->setPath(d->newPath);
}

/**
 * @~spanish
 * Deshace la ejecución del comando  el comando.
 */
void EditNodesItem::unexecute()
{
	d->item->setPath(d->oldPath);
}

/**
 * @~spanish
 */
void EditNodesItem::writeCommandBody(QXmlStreamWriter *writer) const
{
	writer->writeEmptyElement("information");
	writer->writeAttribute("oldpath", DGraphics::PathHelper::toString(d->oldPath) );
	writer->writeAttribute("newpath", DGraphics::PathHelper::toString(d->newPath) );
}

}

}
