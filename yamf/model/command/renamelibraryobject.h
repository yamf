/***************************************************************************
 *   Copyright (C) 2007 Jorge Cuadrado                                     *
 *   kuadrosxx@gmail.com                                                   *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/

#ifndef YAMF_COMMANDRENAMELIBRARYOBJECT_H
#define YAMF_COMMANDRENAMELIBRARYOBJECT_H

#include <yamf/model/command/base.h>
#include "yamf/model/libraryobject.h"

namespace YAMF {

namespace Model {
class LibraryObject;
}

namespace Command {

/**
 * @author Jorge Cuadrado <kuadrosxx@gmail.com>
*/
class YAMF_EXPORT RenameLibraryObject : public YAMF::Command::Base
{
	public:
		RenameLibraryObject(YAMF::Model::LibraryObject *symbol, const QString &newName, QString* name);
		
		~RenameLibraryObject();
		
		virtual QString name() const;
		virtual void execute();
		virtual void unexecute();
		virtual void writeCommandBody(QXmlStreamWriter* writer) const;
		virtual YAMF::Model::Location location() const;
		
	private:
		struct Private;
		Private *const d;
};

}

}

#endif
