/***************************************************************************
 *   Copyright (C) 2007 Jorge Cuadrado                                     *
 *   kuadrosxx@gmail.com                                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/

#include "movescene.h"

#include <model/scene.h>
#include <model/scene.h>
#include <model/project.h>

#include <dcore/debug.h>

#include <QXmlStreamWriter>

namespace YAMF {

namespace Command {

struct MoveScene::Private
{
	Private() : scenes(0), from(-1), to(-1) {}
	
	Common::IntHash<Model::Scene *> *scenes;
	
	int from;
	int to;
};

/**
 * @~spanish
 * Construye un comando para cambiar la posicion de la escena que esta en la posición @p from a la posición @p to dentro de la colección que la contiene @p escena.
 */
MoveScene::MoveScene(int from, int to, Common::IntHash<Model::Scene *> *scenes)
 : YAMF::Command::Base(), d(new Private)
{
	setText(QObject::tr("Move scene: from %1 to %2").arg(from).arg(to) );
	
	d->scenes = scenes;
	d->from = from;
	d->to = to;
}

/**
 * Destructor
 */
MoveScene::~MoveScene()
{
	delete d;
}

QString MoveScene::name() const
{
	return "movescene";
}

Model::Location MoveScene::location() const
{
	Model::Location location;
	location.setValue(Model::Location::Scene, d->from);
	
	return location;
}

/**
 * @~spanish
 * Ejecuta el comando.
 */
void MoveScene::execute()
{
	d->scenes->moveVisual(d->from, d->to);
}

/**
 * @~spanish
 * Deshace la ejecución del comando  el comando.
 */
void MoveScene::unexecute()
{
	d->scenes->moveVisual(d->to, d->from);
}

/**
 * @~spanish
 */
void MoveScene::writeCommandBody(QXmlStreamWriter *writer) const
{
	writer->writeEmptyElement("information");
	
	writer->writeAttribute("from", QString::number(d->from));
	writer->writeAttribute("to", QString::number(d->to));
}

}

}
