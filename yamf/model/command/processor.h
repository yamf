/***************************************************************************
 *   Copyright (C) 2007 David Cuadrado                                     *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/

 

#ifndef YAMF_COMMANDPROCESSOR_H
#define YAMF_COMMANDPROCESSOR_H

#include <QString>

#include <yamf/common/yamf_exports.h>

namespace YAMF {

namespace Model {
class Project;
}

namespace Command {

/**
 * @~english
 * A command processor. This class receives and executes a command XML 
 * @author David Cuadrado <krawek@gmail.com>
*/

class YAMF_EXPORT Processor
{
	public:
		Processor(YAMF::Model::Project *project);
		virtual ~Processor();
		
		void setProject(YAMF::Model::Project *project);
		YAMF::Model::Project *project() const;
		
		bool execute(const QString &xml);
		
	protected:
		virtual bool aboutToExecute(const QString &pkgname);
		
	private:
		struct Private;
		Private *const d;
};

}

}

#endif
