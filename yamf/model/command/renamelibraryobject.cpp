/***************************************************************************
 *   Copyright (C) 2007 Jorge Cuadrado                                     *
 *   kuadrosxx@gmail.com                                                   *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/
 
#include "renamelibraryobject.h"

#include <QXmlStreamWriter>

namespace YAMF {

namespace Command {

struct RenameLibraryObject::Private
{
	YAMF::Model::LibraryObject *object;
	QString oldName;
	QString newName;
	
	QString *string;
};

RenameLibraryObject::RenameLibraryObject(YAMF::Model::LibraryObject *object, const QString &newName, QString* name)
 : YAMF::Command::Base(), d(new Private)
{
	d->string = name;
	d->object = object;
	d->oldName = *name;
	d->newName = newName;
}


RenameLibraryObject::~RenameLibraryObject()
{
	delete d;
}


QString RenameLibraryObject::name() const
{
	return "renamelibraryobject";
}

void RenameLibraryObject::execute()
{
	*d->string = d->newName;
	d->object->updateSymbols();
}

void RenameLibraryObject::unexecute()
{
	*d->string = d->oldName;
	d->object->updateSymbols();
}

void RenameLibraryObject::writeCommandBody(QXmlStreamWriter* writer) const
{
	writer->writeEmptyElement("information");
	writer->writeAttribute("newname", d->newName);
	writer->writeAttribute("oldname", d->oldName);
}

YAMF::Model::Location RenameLibraryObject::location() const
{
	Model::Location location;
	return location;
}

}

}
