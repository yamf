/***************************************************************************
 *   Copyright (C) 2007 David Cuadrado                                     *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/

#include "insertscene.h"

#include <model/scene.h>
#include <model/project.h>

#include <dcore/debug.h>

#include <QXmlStreamWriter>

namespace YAMF {

namespace Command {

struct InsertScene::Private
{
	Private() : scenes(0), scene(0), index(-1) {}
	Common::IntHash<Model::Scene *> *scenes;
	Model::Scene *scene;
	
	int index;
};

/**
 * @~spanish
 * Construye un comando para insertar la escena @p scene en la coleccion de escenas @p scenes.
 */
InsertScene::InsertScene(Model::Scene *scene, Common::IntHash<Model::Scene *> *scenes)
 : YAMF::Command::Base(), d(new Private)
{
	setText(QObject::tr("Insert scene: ")+scene->sceneName());
	
	d->scenes = scenes;
	d->scene = scene;
}

/**
 * @~spanish
 * Construye un comando para insertar la escena @p scene en la colección de escenas @p scenes y la posición @p visualIndex.
 */
InsertScene::InsertScene(Model::Scene *scene, Common::IntHash<Model::Scene *> *scenes, int visualIndex) : YAMF::Command::Base(), d(new Private)
{
	setText(QObject::tr("Insert scene: ")+scene->sceneName());
	
	if( visualIndex < 0 )
	{
		d->index = scenes->count();
	}
	else
	{
		d->index = visualIndex;
	}
	
	d->scenes = scenes;
	d->scene = scene;
}

/**
 * Destructor
 */
InsertScene::~InsertScene()
{
	delete d;
}

QString InsertScene::name() const
{
	return "insertscene";
}

Model::Location InsertScene::location() const
{
	Model::Location location;
	location.setValue(Model::Location::Scene, d->index);
	
	return location;
}

/**
 * @~spanish
 * Ejecuta el comando.
 */
void InsertScene::execute()
{
	D_FUNCINFO;
	
	if( d->index < 0 )
	{
		d->scenes->add(d->scene);
	}
	else
	{
		d->scenes->insert(d->index, d->scene);
	}
	
	d->index = d->scenes->visualIndex(d->scene);
	
	dfDebug << d->index;
}

/**
 * @~spanish
 * Deshace la ejecución del comando  el comando.
 */
void InsertScene::unexecute()
{
	d->index = d->scenes->visualIndex(d->scene);
	d->scenes->remove(d->scene);
}

/**
 * @~spanish
 */
void InsertScene::writeCommandBody(QXmlStreamWriter *writer) const
{
	QString xml;
	
	{
		QDomDocument doc;
		doc.appendChild(d->scene->toXml(doc));
		xml = doc.toString();
	}
	
	QXmlStreamReader reader(xml);
	
	while(!reader.atEnd())
	{
		if( reader.readNext() != QXmlStreamReader::StartDocument)
			writer->writeCurrentToken(reader);
	}
}


}

}
