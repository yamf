/***************************************************************************
 *   Copyright (C) 2007 David Cuadrado                                     *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/

#include "ungroupitem.h"

#include <model/scene.h>
#include <model/layer.h>
#include <model/frame.h>
#include <model/object.h>
#include <model/objectgroup.h>

#include <item/group.h>

#include <QXmlStreamWriter>

namespace YAMF {

namespace Command {
struct UngroupItem::Private
{
	Model::Frame *frame;
	QList<Model::Object *> objects;
	Model::ObjectGroup *group;
	int index;
};

/**
 * @~spanish
 * Construye un comando para deshacer el grupo de objetos graficos @p group.
 */
UngroupItem::UngroupItem(Item::Group *group, Model::Frame *frame) : Base(), d(new Private)
{
	setText(QObject::tr("Ungroup items"));
	d->group = dynamic_cast<Model::ObjectGroup *>(frame->findGraphic(group));
	
	if( d->group )
	{
		d->objects = d->group->children();
		d->index = d->group->visualIndex();
	}
	d->frame = frame;
}

/**
 * Destructor
 */
UngroupItem::~UngroupItem()
{
	delete d;
}

QString UngroupItem::name() const
{
	return "ungroupitem";
}

Model::Location UngroupItem::location() const
{
	Model::Location location;
	
	location.setValue(Model::Location::Object, d->index);
	location.setValue(Model::Location::Scene, d->frame->scene()->visualIndex());
	location.setValue(Model::Location::Layer, d->frame->layer()->visualIndex());
	location.setValue(Model::Location::Frame, d->frame->visualIndex());
	
	return location;
}

/**
 * @~spanish
 * Ejecuta el comando.
 */
void UngroupItem::execute()
{
	if( !d->objects.isEmpty() )
	{
		d->frame->detach(d->group);
		d->group->setTweeningEnabled(false);
		
		foreach(Model::Object *object, d->objects)
		{
			d->group->removeFromGroup(object);
			object->setTweeningEnabled(true);
		}
	}
}

/**
 * @~spanish
 * Deshace la ejecución del comando  el comando.
 */
void UngroupItem::unexecute()
{
	if( !d->objects.isEmpty() )
	{
		Item::Tweener *tweener = d->objects.first()->tweener();
		d->group->setTweeningEnabled(true);
		
		foreach(Model::Object *object, d->objects)
		{
			object->setTweeningEnabled(false);
			d->group->addToGroup(object);
		}
		
		d->frame->attach(d->group);
	}
}

/**
 * @~spanish
 */
void UngroupItem::writeCommandBody(QXmlStreamWriter *writer) const
{
	writer->writeEmptyElement("information");
	
	QString indices;
	
	
	QList<Model::Object *>::iterator it = d->objects.begin();
	
	while(it != d->objects.end())
	{
		if(it != d->objects.end()-1)
			indices += QString("%1,").arg((*it)->visualIndex());
		else
			indices += QString("%1").arg((*it)->visualIndex());
		++it;
	}
	
	writer->writeAttribute("items", indices);
}


}

}
