/***************************************************************************
 *   Copyright (C) 2007 David Cuadrado                                     *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/

#include "manager.h"

#include <dgui/commandhistory.h>
#include <QUndoStack>
#include <model/command/base.h>

#include <dcore/debug.h>

namespace YAMF {
namespace Command {

struct Manager::Private
{
	Private() : /*history(0), */stack(0), observer(0)
	{
	}
	
	~Private()
	{
	}
	
// 	DGui::CommandHistory *history;
	QUndoStack *stack;
	Command::Observer *observer;
};

Manager::Manager(QObject *parent)
	: QObject(parent), d(new Private)
{
	d->stack = new QUndoStack(this);
	
	connect(d->stack, SIGNAL(indexChanged(int)), this, SLOT(onIndexChanged(int)));
	connect(d->stack, SIGNAL(cleanChanged(bool)), this, SIGNAL(cleanChanged(bool)));
}


Manager::~Manager()
{
	delete d;
}

void Manager::removeLastCommand()
{
	int ul = d->stack->undoLimit();
	
	D_SHOW_VAR(d->stack->count());
	d->stack->setUndoLimit(d->stack->count()-1);
	d->stack->setUndoLimit(ul);
	
	D_SHOW_VAR(d->stack->count());
}

void Manager::setObserver(Command::Observer *observer)
{
	d->observer = observer;
}

Command::Observer *Manager::observer() const
{
	return d->observer;
}

void Manager::addCommand(Command::Base *command)
{
	if( d->observer )
	{
		if( !d->observer->aboutToRedo(command) )
		{
			d->stack->push(command);
			d->observer->executed(command);
		}
	}
	else
	{
		d->stack->push(command);
	}
	
	command->setObserver(d->observer);
}

QUndoStack *Manager::stack() const
{
	return d->stack;
}

void Manager::clear()
{
// 	d->stack->blockSignals(true);
	d->stack->clear();
// 	d->stack->blockSignals(false);
	d->stack->setClean();
}

void Manager::onIndexChanged(int)
{
	emit historyChanged();
}

}

}
