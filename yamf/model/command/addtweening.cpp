/***************************************************************************
 *   Copyright (C) 2007 David Cuadrado                                     *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/

#include "addtweening.h"

#include <QXmlStreamReader>

#include "yamf/model/object.h"
#include "yamf/item/tweener.h"

namespace YAMF {
namespace Command {

struct AddTweening::Private
{
	Model::Object *object;
	Item::Tweener *tweener;
};

AddTweening::AddTweening(Model::Object *object, Item::Tweener *tweener)
 : YAMF::Command::Base(), d(new Private)
{
	setText(QObject::tr("Tweening: ")+object->objectName());
	
	d->object = object;
	d->tweener = tweener;
}


AddTweening::~AddTweening()
{
	delete d;
}

QString AddTweening::name() const
{
	return "addtweening";
}

Model::Location AddTweening::location() const
{
	return d->object->location();
}

void AddTweening::execute()
{
	d->object->setTweener(d->tweener);
}

void AddTweening::unexecute()
{
	d->object->setTweener(0);
}


void AddTweening::writeCommandBody(QXmlStreamWriter *writer) const
{
	QString xml;
	
	{
		QDomDocument doc;
		doc.appendChild(d->tweener->toXml(doc));
		xml = doc.toString();
	}
	
	QXmlStreamReader reader(xml);
	
	while(!reader.atEnd())
	{
		if( reader.readNext() != QXmlStreamReader::StartDocument)
			writer->writeCurrentToken(reader);
	}
}


}

}
