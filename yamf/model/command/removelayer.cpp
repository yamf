/***************************************************************************
 *   Copyright (C) 2007 David Cuadrado                                     *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/

#include "removelayer.h"
#include <model/scene.h>
#include <model/layer.h>
#include <model/project.h>

namespace YAMF {

namespace Command {

struct RemoveLayer::Private
{
	Common::IntHash<Model::Layer *> *layers;
	Model::Layer *layer;
	
	int index;
	int scene;
};

/**
 * @~spanish
 * Construye un comando para remover la capa @p layer de la colección que lo contiene @p layers.
 */
RemoveLayer::RemoveLayer(Model::Layer *layer, Common::IntHash<Model::Layer *> *layers)
 : YAMF::Command::Base(), d(new Private)
{
	setText(QObject::tr("Remove layer: ")+layer->layerName());
	
	d->layers = layers;
	d->layer = layer;
	
	d->index = d->layer->visualIndex();
	d->scene = d->layer->scene()->visualIndex();
}

/**
 * Destructor
 */
RemoveLayer::~RemoveLayer()
{
	delete d;
}

QString RemoveLayer::name() const
{
	return "removelayer";
}

Model::Location RemoveLayer::location() const
{
	Model::Location location;
	
	location.setValue(Model::Location::Layer, d->index);
	location.setValue(Model::Location::Scene, d->layer->scene()->visualIndex());
	
	return location;
}



/**
 * @~spanish
 * Ejecuta el comando.
 */
void RemoveLayer::execute()
{
	d->layers->remove(d->layer);
}

/**
 * @~spanish
 * Deshace la ejecución del comando  el comando.
 */
void RemoveLayer::unexecute()
{
	d->layers->insert(d->index, d->layer);
	d->index = d->layer->visualIndex();
}

/**
 * @~spanish
 */
void RemoveLayer::writeCommandBody(QXmlStreamWriter *writer) const
{
	
}

}

}
