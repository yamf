/***************************************************************************
 *   Copyright (C) 2007 Jorge Cuadrado                                     *
 *   kuadrosxx@gmail.com                                                   *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/

#include "expandframe.h"

#include <model/frame.h>
#include <model/layer.h>
#include <model/scene.h>

#include <dcore/debug.h>

#include <QXmlStreamWriter>

namespace YAMF {
namespace Command {

struct ExpandFrame::Private {
	YAMF::Model::Frame *frame;
	YAMF::Common::IntHash<YAMF::Model::Frame *> *frames;
	int size;
};

ExpandFrame::ExpandFrame(YAMF::Model::Frame *frame, YAMF::Common::IntHash<YAMF::Model::Frame *> *frames,  int size) : YAMF::Command::Base(), d(new Private)
{
	d->frames = frames;
	d->frame = frame;
	d->size = size;
}


ExpandFrame::~ExpandFrame()
{
	delete d;
}

QString ExpandFrame::name() const
{
	return "expandframe";
}

Model::Location ExpandFrame::location() const
{
	Model::Location location;
	
	location.setValue(Model::Location::Frame, d->frame->visualIndex());
	location.setValue(Model::Location::Scene, d->frame->scene()->visualIndex());
	location.setValue(Model::Location::Layer, d->frame->layer()->visualIndex());
	
	return location;
}

void ExpandFrame::execute()
{
	int vindex = d->frame->visualIndex();
	for(int i = 0; i < d->size; i++)
	{
		d->frames->insert(vindex, d->frame);
	}
}

void ExpandFrame::unexecute()
{
	int vindex = d->frame->visualIndex();
	for(int i = 0; i < d->size; i++)
	{
		d->frames->removeVisual(vindex);
	}
}

void ExpandFrame::writeCommandBody(QXmlStreamWriter *writer) const
{
	writer->writeEmptyElement("information");
	writer->writeAttribute("size", QString::number(d->size));
	writer->writeAttribute("name", d->frame->frameName());
}

}
}
