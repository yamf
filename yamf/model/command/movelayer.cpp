/***************************************************************************
 *   Copyright (C) 2007 Jorge Cuadrado                                     *
 *   kuadrosxx@gmail.com                                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/

#include "movelayer.h"

#include <model/scene.h>
#include <model/layer.h>
#include <model/project.h>

#include <dcore/debug.h>

#include <QXmlStreamWriter>

namespace YAMF {

namespace Command {

struct MoveLayer::Private
{
	Private() : layers(0), scene(0), from(-1), to(-1) {}
	
	Common::IntHash<Model::Layer *> *layers;
	Model::Scene *scene;
	
	int from;
	int to;
};

/**
 * @~spanish
 * Construye un comando para cambiar la posicion de la capa que esta en la posición @p from a la posición @p to dentro de la colección que la contiene @p layers.
 */
MoveLayer::MoveLayer(Model::Scene *scene, int from, int to, Common::IntHash<Model::Layer *> *layers)
 : YAMF::Command::Base(), d(new Private)
{
	setText(QObject::tr("Move layer: from %1 to %2").arg(from).arg(to) );
	
	d->layers = layers;
	d->scene = scene;
	d->from = from;
	d->to = to;
}

/**
 * Destructor
 */
MoveLayer::~MoveLayer()
{
	delete d;
}

QString MoveLayer::name() const
{
	return "movelayer";
}

Model::Location MoveLayer::location() const
{
	Model::Location location;
	
	location.setValue(Model::Location::Layer, d->from);
	location.setValue(Model::Location::Scene, d->scene->visualIndex());
	
	return location;
}

/**
 * @~spanish
 * Ejecuta el comando.
 */
void MoveLayer::execute()
{
	d->layers->moveVisual(d->from, d->to);
}

/**
 * @~spanish
 * Deshace la ejecución del comando  el comando.
 */
void MoveLayer::unexecute()
{
	d->layers->moveVisual(d->to, d->from);
}

/**
 * @~spanish
 */
void MoveLayer::writeCommandBody(QXmlStreamWriter *writer) const
{
	writer->writeEmptyElement("information");
	
	writer->writeAttribute("from", QString::number(d->from));
	writer->writeAttribute("to", QString::number(d->to));
}

}

}
