/***************************************************************************
 *   Copyright (C) 2007 Jorge Cuadrado                                     *
 *   kuadrosxx@gmail.com                                                   *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/

#include "lockframe.h"

#include <model/scene.h>
#include <model/layer.h>
#include <model/frame.h>
#include <model/project.h>

#include <QXmlStreamWriter>

namespace YAMF {

namespace Command {

struct LockFrame::Private
{
	bool *lock;
	bool newLock;
	Model::Frame *frame;
	
	int index;
	int scene;
	int layer;
};

/**
 * @~spanish
 * Construye un comando para cambiar el estado de bloqueo del marco @p a el estado @p newLock.
 */
LockFrame::LockFrame(Model::Frame *frame , bool &newLock, bool *lock)
 : YAMF::Command::Base(), d(new Private)
{
	if(newLock)
	{
		setText(QObject::tr("Lock frame: ")+frame->frameName());
	}
	else
	{
		setText(QObject::tr("Unlock frame: ")+frame->frameName());
	}
	d->frame = frame;
	d->newLock = newLock;
	d->lock = lock;
	
	d->index = d->frame->visualIndex();
	
	d->scene = d->frame->layer()->scene()->visualIndex();
	d->layer = d->frame->layer()->visualIndex();
}

/**
 * Destructor
 */
LockFrame::~LockFrame()
{
	delete d;
}

QString LockFrame::name() const
{
	return "lockframe";
}

Model::Location LockFrame::location() const
{
	Model::Location location;
	location.setValue(Model::Location::Frame, d->frame->visualIndex());
	location.setValue(Model::Location::Scene, d->frame->layer()->scene()->visualIndex());
	location.setValue(Model::Location::Layer, d->frame->layer()->visualIndex());
	
	return location;
}

/**
 * @~spanish
 * Ejecuta el comando.
 */
void LockFrame::execute()
{
	*d->lock = d->newLock;
}

/**
 * @~spanish
 * Deshace la ejecución del comando  el comando.
 */
void LockFrame::unexecute()
{
	*d->lock = !d->newLock;
}

/**
 * @~spanish
 */
void LockFrame::writeCommandBody(QXmlStreamWriter *writer) const
{
	writer->
writeEmptyElement("information");
	writer->writeAttribute("locked", QString::number(d->newLock));
}


}

}
