/***************************************************************************
 *   Copyright (C) 2007 Jorge Cuadrado                                     *
 *   kuadrosxx@gmail.com                                                   *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/

#include "changelayervisibility.h"

#include <model/scene.h>
#include <model/layer.h>
#include <model/project.h>

#include <QXmlStreamWriter>

namespace YAMF {

namespace Command {

struct ChangeLayerVisibility::Private
{
	bool *isVisible;
	bool newVisibility;
	Model::Layer *layer;
	
	int index;
	int scene;
};

/**
 * @~spanish
 * Construye un comando para cambiar la visibilidad de la capa @p layer, si @p newVisibility es verdadero cambia la capa a visible y es falso cambia la capa a no visible.
 */
ChangeLayerVisibility::ChangeLayerVisibility(Model::Layer *layer , bool &newVisibility, bool *isVisible)
 : YAMF::Command::Base(), d(new Private)
{
	if(newVisibility)
	{
		setText(QObject::tr("Show layer: ")+layer->layerName());
	}
	else
	{
		setText(QObject::tr("Hide layer: ")+layer->layerName());
	}
	
	d->layer = layer;
	d->newVisibility = newVisibility;
	d->isVisible = isVisible;
	d->index = d->layer->visualIndex();
	d->scene = d->layer->scene()->visualIndex();
}

/**
 * Destructor
 */
ChangeLayerVisibility::~ChangeLayerVisibility()
{
	delete d;
}

QString ChangeLayerVisibility::name() const
{
	return "changelayervisibility";
}

Model::Location ChangeLayerVisibility::location() const
{
	Model::Location location;
	location.setValue(Model::Location::Scene, d->layer->scene()->visualIndex());
	location.setValue(Model::Location::Layer, d->layer->visualIndex());
	
	return location;
}

/**
 * @~spanish
 * Ejecuta el comando.
 */
void ChangeLayerVisibility::execute()
{
	*d->isVisible = d->newVisibility;
}

/**
 * @~spanish
 * Deshace la ejecución del comando  el comando.
 */
void ChangeLayerVisibility::unexecute()
{
	*d->isVisible = !d->newVisibility;
}

/**
 * @~spanish
 */
void ChangeLayerVisibility::writeCommandBody(QXmlStreamWriter *writer) const
{
	writer->writeEmptyElement("information");
	writer->writeAttribute("visible", QString::number(d->newVisibility));
}

}

}
