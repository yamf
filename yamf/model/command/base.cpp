/***************************************************************************
 *   Copyright (C) 2007 David Cuadrado                                     *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/

#include "base.h"
#include "observer.h"

#include <QXmlStreamWriter>

namespace YAMF {

namespace Command {

struct Base::Private
{
	Observer *observer;
};

Base::Base(Observer *observer)
 : QUndoCommand(), d( new Private )
{
	d->observer = observer;
}

Base::Base(QUndoCommand *parent, Observer *observer)
 : QUndoCommand(parent), d( new Private )
{
	d->observer = observer;
}

Base::~Base()
{
	delete d;
}

void Base::setObserver(Observer *observer)
{
	d->observer = observer;
}

Observer *Base::observer() const
{
	return d->observer;
}


void Base::redo()
{
	if( d->observer )
	{
		if( ! d->observer->aboutToRedo(this) )
		{
			execute();
			d->observer->executed(this);
		}
	}
	else
		execute();
}

void Base::undo()
{
	if( d->observer )
	{
		if( ! d->observer->aboutToUndo(this) )
		{
			unexecute();
			d->observer->unexecuted(this);
		}
	}
	else
		unexecute();
}

// QString Base::name() const
// {
// 	return "base";
// }

QString Base::toXml() const
{
	QString xml;
	QXmlStreamWriter writer(&xml);
	writeCommand(&writer);
	
	return xml;
}

void Base::writeCommand(QXmlStreamWriter *writer) const
{
	writer->writeStartElement("command");
	writer->writeAttribute("name", name());
	
	location().toXml(writer);
	
	writer->writeStartElement("body");
	
	writeCommandBody(writer);
	
	writer->writeEndElement();
	
	writer->writeEndElement();
}

void Base::writeCommandBody(QXmlStreamWriter *writer) const
{
	Q_UNUSED(writer);
}

}

}

