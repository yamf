/***************************************************************************
 *   Copyright (C) 2007 Jorge Cuadrado                                     *
 *   kuadrosxx@gmail.com                                                   *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/

#include "convertitem.h"

#include <model/scene.h>
#include <model/layer.h>
#include <model/frame.h>
#include <model/object.h>

#include <item/converter.h>
#include <item/proxy.h>
#include <item/path.h>

#include <QGraphicsItem>
#include <QXmlStreamWriter>

namespace YAMF {

namespace Command {

struct ConvertItem::Private
{
	QGraphicsItem *item, *newItem;
	Model::Frame *frame;
	int index;
};

/**
 * @~spanish
 * Construye un comando para convertir el item @item a el tipo @p toType otro dentro del marco @p frame.
 */
ConvertItem::ConvertItem(QGraphicsItem* item, int toType, Model::Frame *frame) : Command::Base(), d(new Private)
{
	setText(QObject::tr("Convert item"));
	
	d->item = item;
	d->frame = frame;
	if(item->type() == Item::Proxy::Type)
	{
		QGraphicsItem *data = qgraphicsitem_cast<Item::Proxy*>(item)->item();
		
		if(data->type() != Item::Path::Type)
		{
			while( data->type() == Item::Proxy::Type )
			{
				data = qgraphicsitem_cast<Item::Proxy*>(data)->item();
			}
			
			data = Item::Converter::convertTo(data, toType);
			qgraphicsitem_cast<Item::Proxy*>(item)->setItem(data);
		}
		
		d->newItem = item;
	}
	else
	{
		d->newItem = Item::Converter::convertTo(item, toType);
	}
	
	d->index = d->frame->visualIndexOf(item);
	
}

/**
 * Destructor
 */
ConvertItem::~ConvertItem()
{
	delete d;
}

QString ConvertItem::name() const
{
	return "convertitem";
}

YAMF::Model::Location ConvertItem::location() const
{
	Model::Location location;
	location.setValue(Model::Location::Object, d->index);
	location.setValue(Model::Location::Frame, d->frame->visualIndex());
	location.setValue(Model::Location::Scene, d->frame->scene()->visualIndex());
	location.setValue(Model::Location::Layer, d->frame->layer()->visualIndex());
	
	return location;
}

/**
 * @~spanish
 * Ejecuta el comando.
 */
void ConvertItem::execute()
{
	if( d->index > -1 && d->newItem )
		d->frame->graphic(d->index)->setItem(d->newItem);
	
	if(d->item->scene())
	{
		d->item->scene()->removeItem(d->item);
	}
	
	d->index = d->frame->visualIndexOf(d->newItem);
}

/**
 * @~spanish
 * Deshace la ejecución del comando  el comando.
 */
void ConvertItem::unexecute()
{
	if( d->index > -1 && d->newItem )
		d->frame->graphic(d->index)->setItem(d->item);
	
	
	if(d->newItem->scene())
	{
		d->newItem->scene()->removeItem(d->newItem);
	}
	
	d->index = d->frame->visualIndexOf(d->item);
}

/**
 * @~spanish
 * retorna verdadero si se convirtio en item exitosamente, de lo contrario retorna falso.
 */
bool ConvertItem::isValid() const
{
	if(d->newItem)
	{
		return true;
	}
	return false;
}

/**
 * @~spanish
 */
void ConvertItem::writeCommandBody(QXmlStreamWriter *writer) const
{
	writer->writeEmptyElement("information");
	writer->writeAttribute("fromtype", QString::number(d->item->type()));
	writer->writeAttribute("totype", QString::number(d->newItem->type()));
}


}

}
