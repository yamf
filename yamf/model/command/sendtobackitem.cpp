/***************************************************************************
 *   Copyright (C) 2007 Jorge Cuadrado                                     *
 *   kuadrosxx@gmail.com                                                   *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/

#include "sendtobackitem.h"

#include <model/scene.h>
#include <model/layer.h>
#include <model/frame.h>
#include <model/object.h>

#include <dgraphics/algorithm.h>

#include <QGraphicsItem>

#include <dcore/debug.h>

namespace YAMF {
namespace Command {

struct SendToBackItem::Private {
	Model::Object *object;
	QList<QGraphicsItem *> items;
	double z;
	bool valid;
};

/**
 * @~spanish
 * Construye un comando para enviar atras una unidad dentro de su fotograma el objeto grafico @p item.
 */
SendToBackItem::SendToBackItem(Model::Object *object)
 : Command::Base(), d(new Private)
{
	setText(QObject::tr("Send to back"));
	
	d->object = object;
	if(d->object->item()->scene())
	{
		d->items = d->object->item()->scene()->items();
		d->z = d->object->item()->zValue();
		DGraphics::Algorithm::sort(d->items, DGraphics::Algorithm::ZAxis );
		d->valid = true;
	}
	else
	{
		d->valid = false;
	}
}

/**
 * Destructor
 */
SendToBackItem::~SendToBackItem()
{
	delete d;
}

QString SendToBackItem::name() const
{
	return "sendtobackitem";
}

Model::Location SendToBackItem::location() const
{
	Model::Location location;
	
	location.setValue(Model::Location::Object, d->object->visualIndex());
	location.setValue(Model::Location::Scene, d->object->scene()->visualIndex());
	location.setValue(Model::Location::Layer, d->object->layer()->visualIndex());
	location.setValue(Model::Location::Frame, d->object->frame()->visualIndex());
	
	return location;
}

/**
 * @~spanish
 * Ejecuta el comando.
 */
void SendToBackItem::execute()
{
	if(d->valid)
	{
		d->object->item()->setZValue(d->items.first()->zValue()-1);
	}
}

/**
 * @~spanish
 * Deshace la ejecución del comando  el comando.
 */
void SendToBackItem::unexecute()
{
	if(d->valid)
	{
		d->object->item()->setZValue(d->z);
	}
}

/**
* @~spanish
*/
void SendToBackItem::writeCommandBody(QXmlStreamWriter *writer) const
{
	
}

}

}
