/***************************************************************************
 *   Copyright (C) 2007 David Cuadrado                                     *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/

#include "processor.h"

#include <QDomDocument>

#include <dgraphics/pathhelper.h>
#include <dcore/debug.h>

#include "yamf/model/project.h"
#include "yamf/model/scene.h"
#include "yamf/model/frame.h"
#include "yamf/model/layer.h"
#include "yamf/model/object.h"
#include "yamf/model/location.h"

#include "yamf/item/private/svg2qt.h"
#include "yamf/item/serializer.h"
#include "yamf/item/tweener.h"
#include "yamf/item/group.h"
#include "yamf/item/text.h"

#include "yamf/effect/factory.h"
#include "yamf/effect/filter.h"

#include "yamf/model/command/additemfilter.h"
#include "yamf/model/command/modifyitem.h"
#include "yamf/model/command/convertitem.h"
#include "yamf/model/command/editnodesitem.h"
#include "yamf/model/command/bringforwardsitem.h"
#include "yamf/model/command/bringtofrontitem.h"
#include "yamf/model/command/sendbackwardsitem.h"
#include "yamf/model/command/sendtobackitem.h"
#include "yamf/model/command/groupitem.h"
#include "yamf/model/command/ungroupitem.h"
#include "yamf/model/command/addtweening.h"
#include "yamf/model/command/modifysymbol.h"
#include "yamf/model/command/changetext.h"
#include "yamf/model/command/changetextwidth.h"
#include "yamf/model/command/changetextfont.h"
#include "yamf/model/command/changeitembrush.h"
#include "yamf/model/command/changeitempen.h"

#include "yamf/model/command/manager.h"

#include "yamf/model/libraryobject.h"
#include "yamf/model/libraryitem.h"
#include "yamf/model/library.h"


#include <QXmlStreamReader>

namespace YAMF {
namespace Command {

struct Processor::Private
{
	Private(YAMF::Model::Project *project) : project(project) {}
	
	
	YAMF::Model::Project *project;
	
	struct ProjectLocation {
		ProjectLocation() : scene(-1), layer(-1), frame(-1), object(-1) {}
		int scene, layer, frame, object;
	};
	
	ProjectLocation location(const QDomElement &e) const;
};

Processor::Private::ProjectLocation Processor::Private::location(const QDomElement &e) const
{
	ProjectLocation loc;
	
	if( e.tagName() == "location" )
	{
		bool ok = false;
		
		int v = e.attribute("scene").toInt(&ok);
		
		if( ok )
		{
			loc.scene = v;
		}
		
		ok = false;
		
		v = e.attribute("layer").toInt(&ok);
		
		if( ok )
		{
			loc.layer = v;
		}
		
		ok = false;
		
		v = e.attribute("frame").toInt(&ok);
		
		if( ok )
		{
			loc.frame = v;
		}
		
		ok = false;
		
		v = e.attribute("object").toInt(&ok);
		
		if( ok )
		{
			loc.object = v;
		}
		
	}
	
	return loc;
}

Processor::Processor(YAMF::Model::Project *project) : d(new Private(project))
{
}


Processor::~Processor()
{
	delete d;
}

void Processor::setProject(YAMF::Model::Project *project)
{
	d->project = project;
	
}

YAMF::Model::Project *Processor::project() const
{
	return d->project;
}

bool Processor::execute(const QString &xml)
{
	dfDebug << "Executing command: " << xml.left(100) << "...";
	
	QString command;
	Model::Location location;
	QString body;
	
	QXmlStreamReader reader(xml);
	QXmlStreamWriter writer(&body);
	
	bool readBody = false;
	
	while( !reader.atEnd() )
	{
		bool finish = false;
		
		switch(reader.readNext())
		{
			case QXmlStreamReader::StartElement:
			{
				QString tag = reader.name().toString();
				if( tag == "command" )
				{
					command = reader.attributes().value("name").toString();
				}
				else if( tag == "location" )
				{
					foreach(QXmlStreamAttribute att, reader.attributes())
					{
						if( att.name().toString() == "scene" )
						{
							location.setValue(Model::Location::Scene, att.value().toString().toInt());
						}
						else if( att.name().toString() == "layer")
						{
							location.setValue(Model::Location::Layer, att.value().toString().toInt());
						}
						else if( att.name().toString() == "frame")
						{
							location.setValue(Model::Location::Frame, att.value().toString().toInt());
						}
						else if( att.name().toString() == "object")
						{
							location.setValue(Model::Location::Object, att.value().toString().toInt());
						}
						else if( att.name().toString() == "symbol")
						{
							location.setValue(Model::Location::Symbol, att.value().toString());
						}
					}
				}
				else if( tag == "body" )
				{
					while( !reader.atEnd() )
					{
						reader.readNext();
						if(reader.tokenType() == QXmlStreamReader::EndElement)
						{
							if(reader.name().toString() == "body")
							{
								finish = true;
								break;
							}
						}
						writer.writeCurrentToken(reader);
					}
					readBody = true;
					finish = true;
				}
			}
			break;
			
			default:
			break;
		}
		
		if( finish ) break;
	}
	
	bool ret = false;
	
	if( !this->aboutToExecute(command) )
	{
		return false;
	}
	
	Model::Scene *scene = d->project->scene(location.value(Model::Location::Scene).toInt());
	Model::Layer *layer = 0;
	Model::Frame *frame = 0;
	Model::Object *object = 0;
	
	if(scene)
	{
		layer = scene->layer(location.value(Model::Location::Layer).toInt());
		if( layer )
		{
			frame = layer->frame(location.value(Model::Location::Frame).toInt());
			if(frame)
			{
				object = frame->graphic(location.value(Model::Location::Object).toInt());
			}
		}
	}
	
	if( command == "insertscene" )
	{
		Model::Scene *scene = d->project->createScene(location.value(Model::Location::Scene).toInt());
		scene->fromXml(body);
		ret = true;
	}
	else if( command == "removescene" )
	{
		d->project->removeScene(scene);
		ret = true;
	}
	else if( command == "insertlayer" )
	{
		D_SHOW_VAR(location.value(Model::Location::Scene).toInt());
		Q_CHECK_PTR(scene);
		
		Model::Layer *layer = scene->createLayer(location.value(Model::Location::Layer).toInt());
		layer->fromXml(body);
		
		ret = true;
	}
	else if( command == "removelayer" )
	{
		scene->removeLayer(layer);
		ret = true;
	}
	else if( command == "insertframe" )
	{
		Model::Frame *frame = layer->createFrame(location.value(Model::Location::Frame).toInt());
		
		frame->fromXml(body);
		
		ret = true;
	}
	else if( command == "removeframe" )
	{
		layer->removeFrame(frame);
		ret = true;
	}
	else if( command == "addobject" )
	{
		Model::Object *object = new Model::Object(frame);
		object->fromXml(body);
		frame->addObject(object);
		ret = true;
	}
	else if( command == "removeobject" )
	{
		QXmlStreamReader reader(body);
		QList<Model::Object *> objects;
		
		while(!reader.atEnd())
		{
			if( reader.readNext() == QXmlStreamReader::StartElement )
			{
				if( reader.name().toString() == "information" )
				{
					QString str = reader.attributes().value("indices").toString();
					QString::const_iterator istr = str.constBegin();
					
					QList<int> pts = Item::Private::Svg2Qt::parseIntList( istr );
					
					foreach(int vi, pts)
					{
						Model::Object *obj = frame->graphic(vi);
						Q_CHECK_PTR(obj);
						
						if( obj )
						{
							objects << obj;
						}
					}
				}
			}
		}
		
		if( !objects.isEmpty() )
		{
			frame->removeObjects(objects);
			ret = true;
		}
	}
	else if( command == "modifyitem" )
	{
		QXmlStreamReader reader(body);
		
		while(!reader.atEnd())
		{
			if( reader.readNext() == QXmlStreamReader::StartElement )
			{
				if( reader.name().toString() == "information" )
				{
					QString newProperties = reader.attributes().value("newproperties").toString();
					
					QString oldProperties = reader.attributes().value("oldproperties").toString();
					
					QDomDocument doc;
					doc.setContent(newProperties);
					Item::Serializer::loadProperties(object->item(), doc.documentElement());
					
					Command::ModifyItem *cmd = new Command::ModifyItem(object->item(), oldProperties, frame);
					d->project->commandManager()->addCommand(cmd);
					ret = true;
				}
			}
		}
	}
	else if( command == "convertitem" )
	{
		QXmlStreamReader reader(body);
		
		while(!reader.atEnd())
		{
			if( reader.readNext() == QXmlStreamReader::StartElement )
			{
				
// 				int fromType = reader.attributes().value("fromtype").toString().toInt();
				int toType = reader.attributes().value("totype").toString().toInt();
				
				Command::ConvertItem *cmd = new Command::ConvertItem(object->item(), toType, frame);
				
				d->project->commandManager()->addCommand(cmd);
				
				ret = true;
			}
		}
	}
	else if ( command == "editnodesitem" )
	{
		QXmlStreamReader reader(body);
// 		<information oldpath="" newpath=""></information>
		while(!reader.atEnd())
		{
			if( reader.readNext() == QXmlStreamReader::StartElement )
			{
				if( QGraphicsPathItem *ipath = dynamic_cast<QGraphicsPathItem *>(object->item()) )
				{
					QString oldPath = reader.attributes().value("oldpath").toString();
					QString newPath = reader.attributes().value("newpath").toString();
					
					QPainterPath opath, npath;
					
					opath = DGraphics::PathHelper::fromString(oldPath);
					npath = DGraphics::PathHelper::fromString(newPath);
					
					ipath->setPath(npath);
					Command::EditNodesItem *cmd = new Command::EditNodesItem(ipath, opath, frame);
					
					d->project->commandManager()->addCommand(cmd);
					
					ret = true;
				}
			}
		}
	}
	else if ( command == "bringforwardsitem" )
	{
		Command::BringForwardsItem *cmd = new Command::BringForwardsItem(object);
		d->project->commandManager()->addCommand(cmd);
		ret = true;
	}
	else if ( command == "bringtofrontitem" )
	{
		Command::BringForwardsItem *cmd = new Command::BringForwardsItem(object);
		d->project->commandManager()->addCommand(cmd);
		ret = true;
	}
	else if ( command == "sendbackwardsitem" )
	{
		Command::SendBackwardsItem *cmd = new Command::SendBackwardsItem(object);
		d->project->commandManager()->addCommand(cmd);
		ret = true;
	}
	else if ( command == "sendtobackitem" )
	{
		Command::SendToBackItem *cmd = new Command::SendToBackItem(object);
		d->project->commandManager()->addCommand(cmd);
		ret = true;
	}
	else if ( command == "changeframevisibility" )
	{
		QXmlStreamReader reader(body);
		while(!reader.atEnd())
		{
			if( reader.readNext() == QXmlStreamReader::StartElement )
			{
				if( reader.name().toString() == "information" )
				{
					bool visible = bool(reader.attributes().value("visible").toString().toInt());
					D_SHOW_VAR(visible);
					frame->setVisible(visible);
					ret = true;
				}
			}
		}
	}
	else if ( command == "changelayervisibility" )
	{
		QXmlStreamReader reader(body);
		while(!reader.atEnd())
		{
			if( reader.readNext() == QXmlStreamReader::StartElement )
			{
				if( reader.name().toString() == "information" )
				{
					bool visible = bool(reader.attributes().value("visible").toString().toInt());
					layer->setVisible(visible);
					ret = true;
				}
			}
		}
	}
	else if ( command == "lockframe" )
	{
		QXmlStreamReader reader(body);
		while(!reader.atEnd())
		{
			if( reader.readNext() == QXmlStreamReader::StartElement )
			{
				if( reader.name().toString() == "information" )
				{
					bool locked = bool(reader.attributes().value("locked").toString().toInt());
					frame->setLocked(locked);
					ret = true;
				}
			}
		}
	}
	else if ( command == "locklayer" )
	{
		QXmlStreamReader reader(body);
		while(!reader.atEnd())
		{
			if( reader.readNext() == QXmlStreamReader::StartElement )
			{
				if( reader.name().toString() == "information" )
				{
					bool locked = bool(reader.attributes().value("locked").toString().toInt());
					layer->setLocked(locked);
					ret = true;
				}
			}
		}
	}
	else if ( command == "moveframe")
	{
		QXmlStreamReader reader(body);
		while(!reader.atEnd())
		{
			if( reader.readNext() == QXmlStreamReader::StartElement )
			{
				if( reader.name().toString() == "information" )
				{
					int from = reader.attributes().value("from").toString().toInt();
					int to = reader.attributes().value("to").toString().toInt();
					layer->moveFrame(from, to);
					ret = true;
				}
			}
		}
	}
	else if ( command == "movelayer")
	{
		QXmlStreamReader reader(body);
		while(!reader.atEnd())
		{
			if( reader.readNext() == QXmlStreamReader::StartElement )
			{
				if( reader.name().toString() == "information" )
				{
					int from = reader.attributes().value("from").toString().toInt();
					int to = reader.attributes().value("to").toString().toInt();
					scene->moveLayer(from, to);
					ret = true;
				}
			}
		}
	}
	else if ( command == "movescene")
	{
		
		QXmlStreamReader reader(body);
		while(!reader.atEnd())
		{
			if( reader.readNext() == QXmlStreamReader::StartElement )
			{
				if( reader.name().toString() == "information" )
				{
					int from = reader.attributes().value("from").toString().toInt();
					int to = reader.attributes().value("to").toString().toInt();
					d->project->moveScene(from, to);
					ret = true;
				}
			}
		}
	}
	else if ( command == "renameframe")
	{
		QXmlStreamReader reader(body);
		while(!reader.atEnd())
		{
			if( reader.readNext() == QXmlStreamReader::StartElement )
			{
				if( reader.name().toString() == "information" )
				{
					QString newname = reader.attributes().value("newname").toString();
					frame->setFrameName(newname);
					ret = true;
				}
			}
		}
	}
	else if ( command == "renamelayer")
	{
		QXmlStreamReader reader(body);
		while(!reader.atEnd())
		{
			if( reader.readNext() == QXmlStreamReader::StartElement )
			{
				if( reader.name().toString() == "information" )
				{
					QString newname = reader.attributes().value("newname").toString();
					layer->setLayerName(newname);
					ret = true;
				}
			}
		}
	}
	else if ( command == "renamescene")
	{
		QXmlStreamReader reader(body);
		while(!reader.atEnd())
		{
			if( reader.readNext() == QXmlStreamReader::StartElement )
			{
				if( reader.name().toString() == "information" )
				{
					QString newname = reader.attributes().value("newname").toString();
					scene->setSceneName(newname);
					ret = true;
				}
			}
		}
	}
	else if ( command == "groupitem")
	{
		QXmlStreamReader reader(body);
		QList<QGraphicsItem *> items;
		while(!reader.atEnd())
		{
			if( reader.readNext() == QXmlStreamReader::StartElement )
			{
				if( reader.name().toString() == "information" )
				{
					QString str = reader.attributes().value("items").toString();
					QString::const_iterator istr = str.constBegin();
					
					QList<int> pts = Item::Private::Svg2Qt::parseIntList( istr );
					
					foreach(int vi, pts)
					{
						Model::Object *obj = frame->graphic(vi);
						
						if(obj)
						{
							Q_CHECK_PTR(obj);
							items << obj->item();
						}
					}
				}
			}
		}
		
		if( !items.isEmpty() )
		{
			Command::GroupItem *cmd = new Command::GroupItem(items, frame);
			d->project->commandManager()->addCommand(cmd);
			ret = true;
		}
	}
	else if ( command == "ungroupitem")
	{
		if( Item::Group *group = static_cast<Item::Group *>(object->item()))
		{
			Command::UngroupItem *cmd = new Command::UngroupItem(group, frame);
			d->project->commandManager()->addCommand(cmd);
			ret = true;
		}
	}
	else if ( command == "expandframe")
	{
		QXmlStreamReader reader(body);
		while(!reader.atEnd())
		{
			if( reader.readNext() == QXmlStreamReader::StartElement )
			{
				if( reader.name().toString() == "information" )
				{
					int size = reader.attributes().value("size").toString().toInt();
					layer->expandFrame( frame->visualIndex() , size );
				}
			}
		}
		ret = true;
	}
	else if( command == "addtweening" )
	{
		Item::Tweener *tweener = new Item::Tweener(0);
		tweener->fromXml(body);
		
		AddTweening *cmd = new AddTweening(object, tweener);
		d->project->commandManager()->addCommand(cmd);
		
		ret = true;
	}
	else if( command == "additemfilter" )
	{
		QString filterName;
		{
			QXmlStreamReader reader(body);
			
			while( ! reader.atEnd() )
			{
				if( reader.readNext() == QXmlStreamReader::StartElement )
				{
					if( reader.name().toString() == "filter" )
					{
						filterName = reader.attributes().value("name").toString();
						break;
					}
				}
			}
		}
		
		Q_ASSERT(!filterName.isEmpty());
		
		if( !filterName.isEmpty() )
		{
			Effect::Filter *filter = Effect::Factory::create(filterName);
			
			Q_ASSERT(filter);
			if( filter )
			{
				AddItemFilter *cmd = new AddItemFilter(object, filter);
				d->project->commandManager()->addCommand(cmd);
				
				ret = true;
			}
		}
	}
	else if( command == "modifyfilter" )
	{
		if( Effect::Filter *filter = dynamic_cast<Effect::Filter*>(object->item()) )
		{
			filter->loadProperties(body);
			ret = true;
		}
	}
	else if(command == "addlibraryobject" )
	{
		int type = -1;
		{
			QXmlStreamReader reader(body);
			
			while(!reader.atEnd())
			{
				if( reader.readNext() == QXmlStreamReader::StartElement )
				{
					if( reader.name().toString() == "object" )
					{
						type = reader.attributes().value("type").toString().toInt();
						break;
					}
				}
			}
		}
		
		if( type != -1 )
		{
			Model::LibraryObject *object = Model::LibraryObjectFactory::create(type, d->project);
			object->fromXml(body);
			
			if( object )
			{
				d->project->library()->addObject(object);
				ret = true;
			}
		}
	}
	else if(command == "removelibraryobject" )
	{
		QString symbolId;
		{
			QXmlStreamReader reader(body);
			
			while(!reader.atEnd())
			{
				if( reader.readNext() == QXmlStreamReader::StartElement )
				{
					if( reader.name().toString() == "object" )
					{
						symbolId = reader.attributes().value("id").toString();
						break;
					}
				}
			}
		}
		
		if( !symbolId.isEmpty() )
		{
			d->project->library()->removeObject(symbolId);
			ret = true;
		}
	}
	else if(command == "modifysymbol" )
	{
		QString symbolId;
		int index = -1;
		QString properties;
		
		{
			QXmlStreamReader reader(body);
			
			while(!reader.atEnd())
			{
				if( reader.readNext() == QXmlStreamReader::StartElement )
				{
					QString tag = reader.name().toString();
					
					if( tag == "object" )
					{
						symbolId = reader.attributes().value("id").toString();
						index = reader.attributes().value("index").toString().toInt();
					}
					else if( tag == "properties" )
					{
						QXmlStreamWriter writer(&properties);
						writer.writeCurrentToken(reader);
					}
				}
			}
		}
		
		if( !symbolId.isEmpty() && index > -1)
		{
			Model::LibraryObject *obj = d->project->library()->findObject(symbolId);
			if( obj )
			{
				Model::LibraryItem *item = obj->item(index);
				YAMF::Item::Serializer::loadProperties(item, properties);
				
				ModifySymbol *cmd = new ModifySymbol(item);
				d->project->commandManager()->addCommand(cmd);
				
				ret = true;
			}
		}
	}
	else if(command == "renamelibraryobject" )
	{
		QXmlStreamReader reader(body);
		while(!reader.atEnd())
		{
			if( reader.readNext() == QXmlStreamReader::StartElement )
			{
				if( reader.name().toString() == "information" )
				{
					QString newname = reader.attributes().value("newname").toString();
					QString oldname = reader.attributes().value("oldname").toString();
					
					Model::LibraryObject *obj = d->project->library()->findObject(oldname);
					if( obj )
					{
						obj->setSymbolName(newname);
						ret = true;
					}
				}
			}
		}
	}
	else if( command == "changetext")
	{
		QXmlStreamReader reader(body);
		while(!reader.atEnd())
		{
			if( reader.readNext() == QXmlStreamReader::StartElement )
			{
				if( reader.name().toString() == "information" )
				{
					QString newtext = reader.attributes().value("newtext").toString();
					QString oldtext = reader.attributes().value("oldtext").toString();
					
					if(Item::Text *text = static_cast<Item::Text *>(object->item()))
					{
						text->setPlainText(newtext);
						ChangeText *cmd = new ChangeText(frame, text, oldtext);
						d->project->commandManager()->addCommand(cmd);
						ret = true;
					}
				}
			}
		}
	}
	else if( command == "changetextwidth")
	{
		QXmlStreamReader reader(body);
		while(!reader.atEnd())
		{
			if( reader.readNext() == QXmlStreamReader::StartElement )
			{
				if( reader.name().toString() == "information" )
				{
					double newwidth = reader.attributes().value("newwidth").toString().toDouble();
					double oldwidth = reader.attributes().value("oldwidth").toString().toDouble();
					
					if(Item::Text *text = static_cast<Item::Text *>(object->item()))
					{
						text->setTextWidth(newwidth);
						ChangeTextWidth *cmd = new ChangeTextWidth(frame, text, oldwidth);
						d->project->commandManager()->addCommand(cmd);
						ret = true;
					}
				}
			}
		}
	}
	else if( command == "changetextfont")
	{
		QXmlStreamReader reader(body);
		
		bool readNewFont = false;
		bool readOldFont = false;
		
		QFont newFont;
		QFont oldFont;
		
		
		while(!reader.atEnd())
		{
			if( reader.readNext() == QXmlStreamReader::StartElement )
			{
				if( reader.name().toString() == "newfont" )
				{
					readNewFont = true;
				}
				else if( reader.name().toString() == "oldfont" )
				{
					readOldFont = true;
				}
				else if(reader.name().toString() == "font")
				{
					if(readNewFont)
					{
						readNewFont = false;
						Item::Serializer::loadFont(newFont, &reader);
					}
					else if(readOldFont)
					{
						readOldFont = false;
						Item::Serializer::loadFont(oldFont, &reader);
					}
				}
			}
		}
		
		if(Item::Text *text = static_cast<Item::Text *>(object->item()) )
		{
			text->setFont(newFont);
			ChangeTextFont *cmd = new ChangeTextFont(frame, text, oldFont);
			d->project->commandManager()->addCommand(cmd);
			ret = true;
		}
	}
	else if( command == "changeitembrush" )
	{
		QBrush brush;
		Item::Serializer::loadBrush( brush, body);
		
		ChangeItemBrush *cmd = new ChangeItemBrush(frame, object->item(), brush);
		d->project->commandManager()->addCommand(cmd);
		ret = true;
	}
	else if( command == "changeitempen" )
	{
		QPen pen;
		Item::Serializer::loadPen( pen, body);
		
		ChangeItemPen *cmd = new ChangeItemPen(frame, object->item(), pen);
		d->project->commandManager()->addCommand(cmd);
		ret = true;
	}
	else
	{
		dWarning() << "Unhandled command: " << command;
	}
	
	D_SHOW_VAR(ret); 
	
	return ret;
}

/**
 * @~spanish
 * Esta funcion es llamada antes de ejecutar un comando, el comando es ejecutado si esta funcion retorna verdadero.
 * La implementacion predeterminada retorna verdadero.
 */
bool Processor::aboutToExecute(const QString &pkgname)
{
	Q_UNUSED(pkgname);
	return true;
}

}
}
