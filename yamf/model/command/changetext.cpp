/***************************************************************************
 *   Copyright (C) 2007 Jorge Cuadrado                                     *
 *   kuadrosxx@gmail.com                                                    *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/

#include "changetext.h"

#include <model/scene.h>
#include <model/layer.h>
#include <model/frame.h>
#include <model/object.h>

#include <QXmlStreamWriter>
#include <QGraphicsTextItem>

namespace YAMF {

namespace Command {

struct ChangeText::Private
{
	Model::Object *object;
	QGraphicsTextItem *item;
	QString newText;
	QString oldText;
};

ChangeText::ChangeText(Model::Frame *frame, QGraphicsTextItem *item, const QString & oldText) : d(new Private)
{
	d->object = frame->graphic(item);
	d->item = item;
	d->oldText = oldText;
	d->newText = item->toPlainText();
}


ChangeText::~ChangeText()
{
	delete d;
}


QString ChangeText::name() const
{
	return "changetext";
}

Model::Location ChangeText::location() const
{
	Model::Location location;
	location.setValue(Model::Location::Object, d->object->visualIndex());
	location.setValue(Model::Location::Scene, d->object->scene()->visualIndex());
	location.setValue(Model::Location::Layer, d->object->layer()->visualIndex());
	location.setValue(Model::Location::Frame, d->object->frame()->visualIndex());
	return location;
}

void ChangeText::execute()
{
	d->item->setPlainText(d->newText);
}

void ChangeText::unexecute()
{
	d->item->setPlainText(d->oldText);
}

void ChangeText::writeCommandBody(QXmlStreamWriter *writer) const
{
	writer->writeEmptyElement("information");
	writer->writeAttribute("oldtext", d->oldText );
	writer->writeAttribute("newtext", d->newText );
}



}

}
