/***************************************************************************
 *   Copyright (C) 2007 David Cuadrado                                     *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/

#include "removeframe.h"

#include <model/scene.h>
#include <model/layer.h>
#include <model/frame.h>
#include <model/project.h>

#include <dcore/debug.h>

namespace YAMF {

namespace Command {

struct RemoveFrame::Private
{
	Common::IntHash<Model::Frame *> *frames;
	Model::Frame *frame;
	
	int index;
	int scene;
	int layer;
};

/**
 * @~spanish
 * Construye un comando para remover el marco @p frame de la colección que lo contiene @p frames.
 */
RemoveFrame::RemoveFrame(Model::Frame *frame, Common::IntHash<Model::Frame *> *frames)
 : YAMF::Command::Base(), d(new Private)
{
	setText(QObject::tr("Remove frame: ")+frame->frameName());
	
	d->frames = frames;
	d->frame = frame;
	
	d->index = d->frame->visualIndex();
	
	d->scene = d->frame->layer()->scene()->visualIndex();
	d->layer = d->frame->layer()->visualIndex();
}

/**
 * Destructor
 */
RemoveFrame::~RemoveFrame()
{
	delete d;
}

QString RemoveFrame::name() const
{
	return "removeframe";
}

Model::Location RemoveFrame::location() const
{
	Model::Location location;
	
	location.setValue(Model::Location::Frame, d->index);
	location.setValue(Model::Location::Scene, d->frame->layer()->scene()->visualIndex());
	location.setValue(Model::Location::Layer, d->frame->layer()->visualIndex());
	
	return location;
}

/**
 * @~spanish
 * Ejecuta el comando.
 */
void RemoveFrame::execute()
{
	d->frames->removeVisual(d->frame->visualIndex());
}

/**
 * @~spanish
 * Deshace la ejecución del comando  el comando.
 */
void RemoveFrame::unexecute()
{
	d->frames->insert(d->index, d->frame);
	d->index = d->frame->visualIndex();
}

/**
 * @~spanish
 */
void RemoveFrame::writeCommandBody(QXmlStreamWriter *writer) const
{
	
}


}

}
