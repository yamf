/***************************************************************************
 *   Copyright (C) 2007 David Cuadrado                                     *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/

#include "changeitembrush.h"
#include "model/object.h"

#include <QAbstractGraphicsShapeItem>

#include "item/serializer.h"
#include "item/proxy.h"

namespace YAMF {
namespace Command {

struct ChangeItemBrush::Private {
	Model::Frame *frame;
	QGraphicsItem *item;
	
	QBrush newBrush;
	QBrush oldBrush;
};

ChangeItemBrush::ChangeItemBrush(Model::Frame *frame, QGraphicsItem *item, const QBrush &newBrush)
 : YAMF::Command::Base(), d(new Private)
{
	setText(QObject::tr("Changed item brush"));
	d->frame = frame;
	d->item = item;
	d->newBrush = newBrush;
	
	
	QGraphicsItem *target = item;
	while( Item::Proxy *proxy = qgraphicsitem_cast<Item::Proxy *>(target ) )
	{
		target = proxy->item();
	}
	
	if( QAbstractGraphicsShapeItem *shape = qgraphicsitem_cast<QAbstractGraphicsShapeItem *>(target) )
	{
		d->oldBrush = shape->brush();
	}
}


ChangeItemBrush::~ChangeItemBrush()
{
	delete d;
}

QString ChangeItemBrush::name() const
{
	return "changeitembrush";
}

Model::Location ChangeItemBrush::location() const
{
	Model::Location loc;
	
	Model::Object *object = d->frame->graphic(d->item);
	
	if( !object )
	{
		return loc;
	}
	
	return object->location();
}

void ChangeItemBrush::execute()
{
	QGraphicsItem *item = d->item;
	while( Item::Proxy *proxy = qgraphicsitem_cast<Item::Proxy *>(item ) )
	{
		item = proxy->item();
	}
	
	if( QAbstractGraphicsShapeItem *shape = qgraphicsitem_cast<QAbstractGraphicsShapeItem *>(item) )
	{
		shape->setBrush(d->newBrush);
	}
}

void ChangeItemBrush::unexecute()
{
	QGraphicsItem *item = d->item;
	while( Item::Proxy *proxy = qgraphicsitem_cast<Item::Proxy *>(item ) )
	{
		item = proxy->item();
	}
	
	if( QAbstractGraphicsShapeItem *shape = qgraphicsitem_cast<QAbstractGraphicsShapeItem *>(item) )
	{
		shape->setBrush(d->oldBrush);
	}
}

void ChangeItemBrush::writeCommandBody(QXmlStreamWriter *writer) const
{
	Item::Serializer::brush(&d->newBrush, writer);
}

}
}


