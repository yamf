/***************************************************************************
 *   Copyright (C) 2007 David Cuadrado                                     *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/

#include "removelibraryobject.h"
#include "yamf/model/libraryobject.h"

#include <QXmlStreamWriter>

namespace YAMF {

namespace Command {

struct RemoveLibraryObject::Private
{
	Private(YAMF::Model::LibraryObject *object, QHash<QString, YAMF::Model::LibraryObject *> *objects) : object(object), objects(objects) {}
	
	YAMF::Model::LibraryObject *object;
	QHash<QString, YAMF::Model::LibraryObject *> *objects;
};

/**
 * @~spanish
 * Construye un comando para remover el objeto @p object de la libreria de la colección que lo contiene @p objects.
 */
RemoveLibraryObject::RemoveLibraryObject(YAMF::Model::LibraryObject *object, QHash<QString, YAMF::Model::LibraryObject *> *objects)
 : YAMF::Command::Base(), d(new Private(object, objects))
{
	setText(QObject::tr("Remove library object: ")+d->object->symbolName());
}

/**
 * Destructor
 */
RemoveLibraryObject::~RemoveLibraryObject()
{
	delete d;
}

QString RemoveLibraryObject::name() const
{
	return "removelibraryobject";
}

Model::Location RemoveLibraryObject::location() const
{
	Model::Location location;
	
	return location;
}

/**
 * @~spanish
 * Ejecuta el comando.
 */
void RemoveLibraryObject::execute()
{
	d->objects->remove(d->object->symbolName());
}

/**
 * @~spanish
 * Deshace la ejecución del comando  el comando.
 */
void RemoveLibraryObject::unexecute()
{
	d->objects->insert(d->object->symbolName(), d->object);
}

/**
 * @~spanish
 */
void RemoveLibraryObject::writeCommandBody(QXmlStreamWriter *writer) const
{
	writer->writeStartElement("object");
	writer->writeAttribute("id", d->object->symbolName() );
	writer->writeEndElement();
}

}

}
