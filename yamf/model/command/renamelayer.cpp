/***************************************************************************
 *   Copyright (C) 2007 David Cuadrado                                     *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/

#include "renamelayer.h"

#include <model/layer.h>
#include <model/scene.h>
#include <model/project.h>

#include <dcore/debug.h>

#include <QXmlStreamWriter>

namespace YAMF {

namespace Command {

struct RenameLayer::Private
{
	QString newName;
	QString oldName;
	
	QString *string;
	Model::Layer *layer;
};

/**
 * @~spanish
 * Construye un comando para cambiar el nombre de la capa @p layer a @p newName.
 */
RenameLayer::RenameLayer(Model::Layer *layer, const QString &newName, QString *string)
 : YAMF::Command::Base(), d(new Private)
{
	d->string = string;
	d->newName = newName;
	d->oldName = *string;
	
	setText(QObject::tr("RenameLayer: %1 => %2").arg(d->oldName).arg(d->newName));
	
	
	d->layer = layer;
}

/**
 * Destructor
 */
RenameLayer::~RenameLayer()
{
	delete d;
}

QString RenameLayer::name() const
{
	return "renamelayer";
}

Model::Location RenameLayer::location() const
{
	Model::Location location;
	location.setValue(Model::Location::Scene, d->layer->scene()->visualIndex());
	location.setValue(Model::Location::Layer, d->layer->visualIndex());
	
	return location;
}

/**
 * @~spanish
 * Ejecuta el comando.
 */
void RenameLayer::execute()
{
	*d->string = d->newName;
}

/**
 * @~spanish
 * Deshace la ejecución del comando  el comando.
 */
void RenameLayer::unexecute()
{
	*d->string = d->oldName;
}

/**
 * @~spanish
 */
void RenameLayer::writeCommandBody(QXmlStreamWriter *writer) const
{
	writer->writeEmptyElement("information");
	writer->writeAttribute("newname", d->newName);
	writer->writeAttribute("oldname", d->oldName);
}


}

}
