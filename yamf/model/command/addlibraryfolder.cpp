/***************************************************************************
 *   Copyright (C) 2007 David Cuadrado                                     *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/

#include "addlibraryfolder.h"

#include <yamf/model/libraryfolder.h>

namespace YAMF {
namespace Command {

struct AddLibraryFolder::Private
{
	YAMF::Model::LibraryFolder *folder;
	QList<YAMF::Model::LibraryFolder *> *folders;
};

AddLibraryFolder::AddLibraryFolder(YAMF::Model::LibraryFolder *folder, QList<YAMF::Model::LibraryFolder *> *folders)
 : YAMF::Command::Base(), d(new Private)
{
	d->folder = folder;
	d->folders = folders;
}


AddLibraryFolder::~AddLibraryFolder()
{
	delete d;
}

QString AddLibraryFolder::name() const
{
	return "addlibraryfolder";
}

YAMF::Model::Location AddLibraryFolder::location() const
{
	Model::Location location;
	
	return location;
}

void AddLibraryFolder::execute()
{
	d->folders->append(d->folder);
}

void AddLibraryFolder::unexecute()
{
	d->folders->removeAll(d->folder);
}

void AddLibraryFolder::writeCommand(QXmlStreamWriter* writer) const
{
	Base::writeCommand(writer);
}

}

}
