/***************************************************************************
 *   Copyright (C) 2007 David Cuadrado                                     *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/

#include "additemfilter.h"

#include <QXmlStreamWriter>

#include "yamf/model/object.h"
#include "yamf/effect/filter.h"

#include <dcore/debug.h>

namespace YAMF {

namespace Command {

struct AddItemFilter::Private
{
	YAMF::Model::Object *object;
	YAMF::Effect::Filter *filter;
};

/**
 * @~spanish
 * Construye el comando con el objeto @p object al cual se le va a asignar el filtro @p filter.
 */
AddItemFilter::AddItemFilter(YAMF::Model::Object *object, YAMF::Effect::Filter *filter)
 : YAMF::Command::Base(), d(new Private)
{
	d->object = object;
	d->filter = filter;
}


/**
 * @~spanish
 * Destructor.
 */
AddItemFilter::~AddItemFilter()
{
	delete d;
}

/**
 * @~spanish
 * Returna el nombre del filtro
 * @return 
 */
QString AddItemFilter::name() const
{
	return "additemfilter";
}

/**
 * @~spanish
 * Retorna la ubicación del objecto al que se le va aplicar el filtro
 * @return 
 */
Model::Location AddItemFilter::location() const
{
	return d->object->location();
}

/**
 * @~spanish
 */
void AddItemFilter::writeCommandBody(QXmlStreamWriter *writer) const
{
	QString xml;
	
	{
		QDomDocument doc;
		doc.appendChild(d->filter->toXml(doc));
		xml = doc.toString();
	}
	
	QXmlStreamReader reader(xml);
	
	while(!reader.atEnd())
	{
		reader.readNext();
		if( reader.tokenType() != QXmlStreamReader::StartDocument && reader.tokenType() != QXmlStreamReader::Invalid )
			writer->writeCurrentToken(reader);
		
		if( reader.hasError())
		{
			dfDebug << reader.errorString();
		}
	}
}

/**
 * @~spanish
 * Ejecuta el comando.
 */
void AddItemFilter::execute()
{
	d->object->setFilter(d->filter);
}

/**
 * @~spanish
 * Deshace la ejecución del comando  el comando.
 */
void AddItemFilter::unexecute()
{
	d->object->removeFilter();
}


}

}
