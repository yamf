/***************************************************************************
 *   Copyright (C) 2007 David Cuadrado                                     *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/

#include "insertframe.h"

#include <model/scene.h>
#include <model/layer.h>
#include <model/frame.h>
#include <model/project.h>

#include <dcore/debug.h>

#include <QXmlStreamWriter>
#include <QDebug>

namespace YAMF {

namespace Command {

struct InsertFrame::Private
{
	Private() : frames(0), frame(0), index(-1) {}
	
	Common::IntHash<Model::Frame *> *frames;
	Model::Frame *frame;
	
	int index;
};

/**
 * @~spanish
 * Construye un comando para insertar el marco @p frame en la coleccion de marcos @p frames.
 */
InsertFrame::InsertFrame(Model::Frame *frame, Common::IntHash<Model::Frame *> *frames)
 : YAMF::Command::Base(), d(new Private)
{
	setText(QObject::tr("Insert frame: ")+frame->frameName());
	
	d->frames = frames;
	d->frame = frame;
	
	d->index = frames->count();
}

/**
 * @~spanish
 * Construye un comando para insertar el marco @p frame en la colección de marcos @p frames y la posición @p visualIndex.
 */
InsertFrame::InsertFrame(Model::Frame *frame, Common::IntHash<Model::Frame *> *frames, int visualIndex)
 : YAMF::Command::Base(), d(new Private)
{
	setText(QObject::tr("Insert frame: ")+frame->frameName());
	
	if( visualIndex < 0 )
	{
		d->index = frames->count();
	}
	else
	{
		d->index = visualIndex;
	}
	
	d->frames = frames;
	d->frame = frame;
}

/**
 * Destructor
 */
InsertFrame::~InsertFrame()
{
	delete d;
}

QString InsertFrame::name() const
{
	return "insertframe";
}

Model::Location InsertFrame::location() const
{
	Model::Location location;
	
	location.setValue(Model::Location::Frame, d->index);
	location.setValue(Model::Location::Scene, d->frame->layer()->scene()->visualIndex());
	location.setValue(Model::Location::Layer, d->frame->layer()->visualIndex());
	
	return location;
}


/**
 * @~spanish
 * Ejecuta el comando.
 */
void InsertFrame::execute()
{
	if( d->index < 0 )
	{
		d->frames->add(d->frame);
	}
	else
	{
		
		d->frames->insert(d->index, d->frame);
		
	}
	
	d->index = d->frames->visualIndex(d->frame);
}

/**
 * @~spanish
 * Deshace la ejecución del comando  el comando.
 */
void InsertFrame::unexecute()
{
// 	d->index = d->frames->visualIndex(d->frame);
// 	dfDebug << d->frames->visualValues().contains(d->frame);
	d->frames->remove(d->frame);
}


 /**
 * @~spanish
 * Retorna una cadena de texto con la representación del comando en formato xml:
 * \<insertframe\>
  *   \<location index=frameIndex layer=layerIndex scene=sceneIndex /\>
  * \</insertframe\>
  */
void InsertFrame::writeCommandBody(QXmlStreamWriter *writer) const
{
	QString xml;
	
	{
		QDomDocument doc;
		doc.appendChild(d->frame->toXml(doc));
		xml = doc.toString();
	}
	
	QXmlStreamReader reader(xml);
	
	while(!reader.atEnd())
	{
		if( reader.readNext() != QXmlStreamReader::StartDocument)
			writer->writeCurrentToken(reader);
	}
}

}

}
