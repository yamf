/***************************************************************************
 *   Copyright (C) 2007 Jorge Cuadrado                                     *
 *   kuadrosxx@gmail.com                                                    *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/
 
#include "changetextfont.h"

#include <model/scene.h>
#include <model/layer.h>
#include <model/frame.h>
#include <model/object.h>

#include <item/serializer.h>

#include <QXmlStreamWriter>
#include <QGraphicsTextItem>
#include <QFont>

namespace YAMF {
namespace Command {

struct ChangeTextFont::Private
{
	Model::Object *object;
	QGraphicsTextItem *item;
	QFont newFont;
	QFont oldFont;
};

ChangeTextFont::ChangeTextFont(Model::Frame *frame, QGraphicsTextItem *item, const QFont & oldFont) : d(new Private)
{
	d->object = frame->graphic(item);
	d->item = item;
	d->oldFont = oldFont;
	d->newFont = item->font();
}


ChangeTextFont::~ChangeTextFont()
{
	delete d;
}


QString ChangeTextFont::name() const
{
	return "changetextfont";
}

Model::Location ChangeTextFont::location() const
{
	Model::Location location;
	location.setValue(Model::Location::Object, d->object->visualIndex());
	location.setValue(Model::Location::Scene, d->object->scene()->visualIndex());
	location.setValue(Model::Location::Layer, d->object->layer()->visualIndex());
	location.setValue(Model::Location::Frame, d->object->frame()->visualIndex());
	return location;
}

void ChangeTextFont::execute()
{
	d->item->setFont(d->newFont);
	d->item->setPlainText(d->item->toPlainText());
}

void ChangeTextFont::unexecute()
{
	d->item->setFont(d->oldFont);
	d->item->setPlainText(d->item->toPlainText());
}

void ChangeTextFont::writeCommandBody(QXmlStreamWriter *writer) const
{
	writer->writeStartElement("information");
	writer->writeStartElement("oldfont");
	Item::Serializer::font(d->oldFont, writer);
	writer->writeEndElement();
	
	writer->writeStartElement("newfont");
	Item::Serializer::font(d->newFont, writer);
	writer->writeEndElement();
	writer->writeEndElement();
}

}
}
