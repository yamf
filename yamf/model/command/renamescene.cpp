/***************************************************************************
 *   Copyright (C) 2007 David Cuadrado                                     *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/

#include "renamescene.h"

#include <model/scene.h>
#include <model/project.h>

#include <dcore/debug.h>

#include <QXmlStreamWriter>

namespace YAMF {

namespace Command {

struct RenameScene::Private
{
	QString newName;
	QString oldName;
	
	QString *string;
	Model::Scene *scene;
};

/**
 * @~spanish
 * Construye un comando para cambiar el nombre de la escena @p scene a @p newName.
 */
RenameScene::RenameScene(Model::Scene *scene, const QString &newName, QString *string)
 : YAMF::Command::Base(), d(new Private)
{
	d->string = string;
	d->newName = newName;
	d->oldName = *string;
	
	setText(QObject::tr("RenameScene: %1 => %2").arg(d->oldName).arg(d->newName));
	
	
	d->scene = scene;
}

/**
 * Destructor
 */
RenameScene::~RenameScene()
{
	delete d;
}

QString RenameScene::name() const
{
	return "renamescene";
}

Model::Location RenameScene::location() const
{
	Model::Location location;
	location.setValue(Model::Location::Scene, d->scene->visualIndex());
	
	return location;
}

/**
 * @~spanish
 * Ejecuta el comando.
 */
void RenameScene::execute()
{
	*d->string = d->newName;
}

/**
 * @~spanish
 * Deshace la ejecución del comando  el comando.
 */
void RenameScene::unexecute()
{
	*d->string = d->oldName;
}

/**
 * @~spanish
 */
void RenameScene::writeCommandBody(QXmlStreamWriter *writer) const
{
	writer->writeEmptyElement("information");
	writer->writeAttribute("newname", d->newName);
	writer->writeAttribute("oldname", d->oldName);
}

}

}
