/***************************************************************************
 *   Copyright (C) 2007 Jorge Cuadrado                                     *
 *   kuadrosxx@gmail.com                                                   *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/

#include "bringforwardsitem.h"

#include <model/scene.h>
#include <model/layer.h>
#include <model/frame.h>
#include <model/object.h>

#include <dgraphics/algorithm.h>
#include <dcore/debug.h>
#include <QGraphicsItem>

namespace YAMF {

namespace Command {

struct BringForwardsItem::Private
{
	Private() : object(0), after(0) {}
	
	Model::Object *object;
	QGraphicsItem *after;
	double z;
	int index;
	bool valid;
};

/**
 * @~spanish
 * Construye un comando para traer una unidad adelante dentro de su fotograma el objeto grafico @p item.
 */
BringForwardsItem::BringForwardsItem(Model::Object *object)
 : Command::Base(), d(new Private)
{
	setText(QObject::tr("Bring forwards"));
	
	d->object = object;
	d->index = object->visualIndex();
	
	d->z = object->item()->zValue();
	
	if(d->object->item()->scene())
	{
		QList<QGraphicsItem *> items = d->object->item()->scene()->items();
		
		DGraphics::Algorithm::sort(items, DGraphics::Algorithm::ZAxis );
		
		int index = items.indexOf(d->object->item());
		
		if(index < items.count()-1 )
		{
			d->after = items[index+1];
		}
		d->valid = true;
	}
	else
	{
		d->valid = false;
	}
}


/**
 * Destructor
 */
BringForwardsItem::~BringForwardsItem()
{
	delete d;
}

QString BringForwardsItem::name() const
{
	return "bringforwardsitem";
}

Model::Location BringForwardsItem::location() const
{
	Model::Location location;
	
	location.setValue(Model::Location::Object, d->object->visualIndex());
	location.setValue(Model::Location::Scene, d->object->scene()->visualIndex());
	location.setValue(Model::Location::Layer, d->object->layer()->visualIndex());
	location.setValue(Model::Location::Frame, d->object->frame()->visualIndex());
	
	return location;
}

/**
 * @~spanish
 * Ejecuta el comando.
 */
void BringForwardsItem::execute()
{
	if(d->valid)
	{
		if(d->after)
		{
			d->object->item()->setZValue(d->after->zValue());
			d->after->setZValue(d->z);
		}
	}
}

/**
 * @~spanish
 * Deshace la ejecución del comando  el comando.
 */
void BringForwardsItem::unexecute()
{
	if(d->valid)
	{
		if(d->after)
		{
			d->after->setZValue(d->object->item()->zValue());
			d->object->item()->setZValue(d->z);
		}
	}
}

/**
 * @~spanish
 */
void BringForwardsItem::writeCommandBody(QXmlStreamWriter *writer) const
{
	
}

}

}
