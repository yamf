/***************************************************************************
 *   Copyright (C) 2006 by David Cuadrado                                  *
 *   krawek@gmail.com                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef YAMFMODELLIBRARY_H
#define YAMFMODELLIBRARY_H

#include <QObject>

#include <yamf/model/libraryfolder.h>
#include <yamf/common/yamf_exports.h>

namespace YAMF {
namespace Model {

class Project;

/**
 * @ingroup model
 * @author David Cuadrado <krawek@gmail.com>
*/

class YAMF_EXPORT Library : public LibraryFolder
{
	Q_OBJECT;
	
	public:
		enum Class
		{
			Symbol = 0x01,
			Folder
		};
		
		Library(Model::Project *const project, const QString &id);
		~Library();
		
		QString dataPath() const;
		void save(const QString &path);
		void load(const QString &path);
		
	public:
		virtual void fromXml(const QString &xml );
		virtual QDomElement toXml(QDomDocument &doc) const;
		
	private:
		struct Private;
		Private *const d;
};

}
}

#endif
