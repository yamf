/***************************************************************************
 *   Copyright (C) 2006 by David Cuadrado                                  *
 *   krawek@gmail.com                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef KTLIBRARYOBJECT_H
#define KTLIBRARYOBJECT_H

#include <QVariant>
#include <QGraphicsItem>

#include <yamf/common/abstractserializable.h>
#include <yamf/common/yamf_exports.h>

class QGraphicsSvgItem;
class QXmlStreamReader;

namespace YAMF {
namespace Model {

class Project;
class LibraryItem;

/**
 * @ingroup model
 * @author David Cuadrado <krawek@gmail.com>
*/
class YAMF_EXPORT LibraryObject : public QObject, public Common::AbstractSerializable
{
	public:
		enum Type {
			Item = 0x01,
			Image,
			Sound,
			Svg,
			Text
		};
		
		LibraryObject(Project *parent);
		~LibraryObject();
		
		Project *project() const;
		
		void setType(int type);
		int type() const;
		
		void setDataDir(const QString &path);
		QString dataDir() const;
		
		void setSymbolName(const QString &name);
		QString symbolName() const;
		
		// Used to keep references to items using the object
		void addSymbolItem(LibraryItem *item);
		void removeSymbolItem(LibraryItem *item);
		int indexOf(LibraryItem *item) const;
		LibraryItem *item(int pos) const;
		void updateSymbols();
		
		virtual void loadData(const QString &data);
		virtual QDomElement dataXml(QDomDocument &doc) const;
		
	public:
		virtual void fromXml(const QString &xml );
		virtual QDomElement toXml(QDomDocument &doc) const;
		
	private:
		struct Private;
		Private *const d;
		
};

class YAMF_EXPORT LibraryItemObject : public LibraryObject
{
	public:
		LibraryItemObject(QGraphicsItem *item, Project *parent);
		~LibraryItemObject();
		
		QGraphicsItem *item() const;
		
		virtual void loadData(const QString &data);
		virtual QDomElement dataXml(QDomDocument &doc) const;
		
	private:
		struct Private;
		Private *const d;
};

class YAMF_EXPORT LibraryURLObject : public LibraryObject
{
	protected:
		LibraryURLObject(Project *parent);
		LibraryURLObject(const QUrl &url, Project *parent);
		
	public:
		~LibraryURLObject();
		
		void setUrl(const QUrl &url);
		QUrl url() const;
		
		virtual void loadData(const QString &data);
		virtual QDomElement dataXml(QDomDocument &doc) const;
		
		virtual QString saveData() const = 0;
		virtual void load(QXmlStreamReader *reader);
		
	protected:
		QDomElement fileData(QDomDocument &doc) const;
		
	private:
		struct Private;
		Private *const d;
};

class YAMF_EXPORT LibraryURLItemObject : public LibraryURLObject
{
	protected:
		LibraryURLItemObject(const QUrl &url, Project *parent);
		LibraryURLItemObject(Project *parent);
		
	public:
		~LibraryURLItemObject();
		
		void setItem(QGraphicsItem *item);
		QGraphicsItem *item() const;
		
		virtual QDomElement dataXml(QDomDocument &doc) const;
		
		void load(QXmlStreamReader *reader);
		
	protected:
		virtual void createItem() = 0;
		
	private:
		struct Private;
		Private *const d;
};

class YAMF_EXPORT LibrarySvgObject : public LibraryURLItemObject
{
	public:
		LibrarySvgObject(const QUrl &url, Project *parent);
		~LibrarySvgObject();
		
		virtual QString saveData() const;
		
	protected:
		void createItem();
		
	private:
		struct Private;
		Private *const d;
};

class YAMF_EXPORT LibraryBitmapObject : public LibraryURLItemObject
{
	public:
		LibraryBitmapObject(const QUrl &url, Project *parent);
		~LibraryBitmapObject();
		
		virtual QString saveData() const;
		
	protected:
		void createItem();
		
	private:
		struct Private;
		Private *const d;
};

class YAMF_EXPORT LibrarySoundObject : public LibraryURLObject
{
	public:
		LibrarySoundObject(const QUrl &url, Project *parent);
		~LibrarySoundObject();
		
		virtual QString saveData() const;
		
	private:
		struct Private;
		Private *const d;
};


class YAMF_EXPORT LibraryObjectFactory
{
	public:
		static LibraryObject *create(int id, Project *parent);
};

}
}

#endif


