/***************************************************************************
 *   Copyright (C) 2006 by David Cuadrado                                  *
 *   krawek@gmail.com                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "library.h"

#include "model/project.h"
#include <dcore/debug.h>

#include <QFileInfo>
#include <QDir>

namespace YAMF {
namespace Model {

struct Library::Private
{
	QString dataPath;
};


Library::Library(Model::Project *const project, const QString &id) : LibraryFolder(id, project), d(new Private)
{
}


Library::~Library()
{
	delete d;
}

QString Library::dataPath() const
{
	return d->dataPath;
}

void Library::save(const QString &path)
{
	QFileInfo pathInfo(path);
	
	d->dataPath = pathInfo.absoluteDir().path();
	
	QFile lbr(path);
	
	if ( lbr.open(QIODevice::WriteOnly | QIODevice::Text))
	{
		QTextStream ts(&lbr);
		
		QDomDocument doc;
		doc.appendChild(this->toXml(doc));
		
		ts << doc.toString();
		
		lbr.close();
	}
	else
	{
		dWarning() << "Cannot open file: " << lbr.fileName();
	}
	
	d->dataPath = QString();
}

void Library::load(const QString &path)
{
	QFileInfo pathInfo(path);
	
	d->dataPath = pathInfo.absoluteDir().path();
	D_SHOW_VAR(d->dataPath);
	
	QFile lbr(path);
	
	if ( lbr.open(QIODevice::ReadOnly | QIODevice::Text))
	{
		this->fromXml(QString::fromLocal8Bit(lbr.readAll()));
		
		lbr.close();
	}
	else
	{
		dWarning() << "Cannot open file: " << lbr.fileName();
	}
}

void Library::fromXml(const QString &xml )
{
	QDomDocument document;
	
	if(! document.setContent(xml) )
	{
		return;
	}
	
	QDomElement root = document.documentElement();
	QDomNode n = root.firstChild();
	
	while( !n.isNull() )
	{
		QDomElement e = n.toElement();
		
		if(!e.isNull())
		{
			if( e.tagName() == "library" )
			{
				
			}
			else if( e.tagName() == "folder" )
			{
				QString doc;
				{
					QTextStream ts(&doc);
					ts << n;
				}
				
				LibraryFolder::fromXml(doc);
			}
		}
		
		n = n.nextSibling();
	}
}


QDomElement Library::toXml(QDomDocument &doc) const
{
	QDomElement root = doc.createElement("library");
	
	root.appendChild(LibraryFolder::toXml(doc));
	
	return root;
}

}
}
