
SOURCES += command/insertscene.cpp \
command/insertlayer.cpp \
command/insertframe.cpp \
command/addobject.cpp \
command/base.cpp \
command/bringforwardsitem.cpp \
command/bringtofrontitem.cpp \
command/groupitem.cpp \
command/manager.cpp \
command/modifyitem.cpp \
command/modifysymbol.cpp \
command/observer.cpp \
command/renamescene.cpp \
command/renamelayer.cpp \
command/renameframe.cpp \
command/removeobject.cpp \
command/removelayer.cpp \
command/removeframe.cpp \
command/removescene.cpp \
command/locklayer.cpp \
command/changelayervisibility.cpp \
command/lockframe.cpp \
command/changeframevisibility.cpp \
command/movelayer.cpp \
command/moveframe.cpp \
command/movescene.cpp \
command/sendbackwardsitem.cpp \
command/sendtobackitem.cpp \
command/ungroupitem.cpp \
command/convertitem.cpp \
command/editnodesitem.cpp \
command/addlibraryobject.cpp \
command/additemfilter.cpp \
command/expandframe.cpp \
command/processor.cpp \
command/removelibraryobject.cpp \
command/addtweening.cpp \
command/addlibraryfolder.cpp \
command/changetext.cpp \
command/changetextwidth.cpp \
command/changetextfont.cpp \
command/changeitembrush.cpp \
command/changeitempen.cpp \
command/modifyfilter.cpp \
command/renamelibraryobject.cpp
HEADERS += command/insertscene.h \
command/insertlayer.h \
command/insertframe.h \
command/addobject.h \
command/base.h \
command/bringforwardsitem.h \
command/bringtofrontitem.h \
command/groupitem.h \
command/manager.h \
command/modifyitem.h \
command/modifysymbol.h \
command/observer.h \
command/renamescene.h \
command/renamelayer.h \
command/renameframe.h \
command/removescene.h \
command/removelayer.h \
command/removeframe.h \
command/removeobject.h \
command/locklayer.h \
command/changelayervisibility.h \
command/lockframe.h \
command/changeframevisibility.h \
command/movelayer.h \
command/moveframe.h \
command/movescene.h \
command/sendbackwardsitem.h \
command/sendtobackitem.h \
command/ungroupitem.h \
command/convertitem.h \
command/editnodesitem.h \
command/addlibraryobject.h \
command/additemfilter.h \
command/expandframe.h \
command/processor.h \
command/removelibraryobject.h \
command/addtweening.h \
command/addlibraryfolder.h \
command/changetext.h \
command/changetextwidth.h \
command/changetextfont.h \
command/changeitembrush.h \
command/changeitempen.h \
command/modifyfilter.h \
command/renamelibraryobject.h
INSTALLS += commands

commands.path = /include/yamf/model/command
commands.files = command/*.h



