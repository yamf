/***************************************************************************
 *   Copyright (C) 2006 by David Cuadrado                                  *
 *   krawek@gmail.com                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "libraryfolder.h"
#include "libraryobject.h"

#include <dcore/debug.h>

#include "yamf/item/builder.h"
#include "yamf/model/project.h"

#include "yamf/model/command/addlibraryobject.h"
#include "yamf/model/command/addlibraryfolder.h"
#include "yamf/model/command/removelibraryobject.h"
#include "yamf/model/command/manager.h"
#include "yamf/model/library.h"

namespace YAMF {
namespace Model {

struct LibraryFolder::Private
{
	QString id;
	Folders folders;
	LibraryObjects objects;
	
	Model::Project *project;
};

LibraryFolder::LibraryFolder(const QString &id, Model::Project *const project, QObject *parent) : QObject(parent), d(new Private)
{
	d->id = id;
	d->project = project;
}


LibraryFolder::~LibraryFolder()
{
	qDeleteAll(d->objects);
	qDeleteAll(d->folders);
	delete d;
}

bool LibraryFolder::addItemSymbol(QGraphicsItem *item, const QString &id)
{
// 	LibraryItemObject *object = new LibraryItemObject(item, this);
// 	addObject(object, id);
	
	if( YAMF::Common::AbstractSerializable *ob = dynamic_cast<YAMF::Common::AbstractSerializable *>(item))
	{
		QDomDocument doc;
		doc.appendChild(ob->toXml(doc));
		return createItemSymbol(doc.toString(), id);
	}
	
	return false;
}

bool LibraryFolder::createItemSymbol(const QString &xml, const QString &id)
{
	LibraryItemObject *object = new LibraryItemObject(0, this->project());
	object->setSymbolName(id);
	object->loadData(xml.toLocal8Bit());
	
	return addObject(object);
}

bool LibraryFolder::createSoundSymbol(const QUrl &url, const QString &id)
{
	LibrarySoundObject *object = new LibrarySoundObject(url, this->project());
	object->setSymbolName(id);
	
	return addObject(object);
}

bool LibraryFolder::createBitmapSymbol(const QUrl &url, const QString &id)
{
	LibraryBitmapObject *object = new LibraryBitmapObject(url, this->project());
	object->setSymbolName(id);
	
	return addObject(object);
}

bool LibraryFolder::createSvgSymbol(const QUrl &url, const QString &id)
{
	LibrarySvgObject *object = new LibrarySvgObject(url, this->project());
	object->setSymbolName(id);
	
	return addObject(object);
}

bool LibraryFolder::addObject(LibraryObject *object)
{
	QString id = object->symbolName();
	if( ! d->project->library()->findObject(id) )
	{
		Command::AddLibraryObject *cmd = new Command::AddLibraryObject(object, &d->objects);
		d->project->commandManager()->addCommand(cmd);
		
		return true;
	}
	else
	{
		dWarning() << "There is a object with the same id: " << id;
	}
	
	return false;
}

bool LibraryFolder::removeObject(const QString &id)
{
	foreach ( QString oid, d->objects.keys())
	{
		if ( oid == id )
		{
			Command::RemoveLibraryObject *cmd = new Command::RemoveLibraryObject(d->objects.value(oid), &d->objects);
			d->project->commandManager()->addCommand(cmd);
			
			return true;
		}
	}
	
	foreach ( LibraryFolder *folder, d->folders )
	{
		if( folder->removeObject(id) )
		{
			return true;
		}
	}
	
	return false;
}

void LibraryFolder::addFolder(LibraryFolder *folder)
{
	Command::AddLibraryFolder *cmd = new Command::AddLibraryFolder(folder, &d->folders);
	d->project->commandManager()->addCommand(cmd);
}

bool LibraryFolder::moveObject(const QString &id, LibraryFolder *folder)
{
	if ( d->objects.contains(id) )
	{
		LibraryObject *object = d->objects[id];
		removeObject( id );
		
		object->setSymbolName(id);
		
		folder->addObject( object );
		
		return true;
	}
	
	return false;
}

void LibraryFolder::setId(const QString &id)
{
	d->id = id;
}

QString LibraryFolder::id() const
{
	return d->id;
}

LibraryObject *LibraryFolder::findObject(const QString &id) const
{
	foreach ( QString oid, d->objects.keys())
	{
		if ( oid == id )
		{
			return d->objects[oid];
		}
	}
	
	foreach ( LibraryFolder *folder, d->folders )
	{
		LibraryObject *object = folder->findObject(id);
		
		if ( object )
		{
			return object;
		}
	}
	
	dDebug() << "Folder " << d->id << ": Cannot find symbol with id: " << id;
	
	return 0;
}

void LibraryFolder::renameObject(const QString &id, const QString &newId)
{
	if( d->objects.contains(id) )
	{
		LibraryObject *object = d->objects[id];
		object->setSymbolName(newId);
		
		d->objects.remove(id);
		
		d->objects.insert(newId, object);
	}
}

int LibraryFolder::objectsCount() const
{
	return d->objects.count();
}

int LibraryFolder::foldersCount() const
{
	return d->folders.count();
}

Model::Project *LibraryFolder::project() const
{
	return d->project;
}

Folders LibraryFolder::folders() const
{
	return d->folders;
}

LibraryObjects LibraryFolder::objects() const
{
	return d->objects;
}

void LibraryFolder::fromXml(const QString &xml )
{
	QDomDocument document;
	
	if(! document.setContent(xml) )
	{
		return;
	}
	
	QDomElement root = document.documentElement();
	QDomNode n = root.firstChild();
	
	while( !n.isNull() )
	{
		QDomElement e = n.toElement();
		
		if(!e.isNull())
		{
			if( e.tagName() == "object" )
			{
				int id = e.attribute("type").toInt();
				
				QDomDocument objectDocument;
				objectDocument.appendChild(objectDocument.importNode(n, true ));
				
				LibraryObject *object = LibraryObjectFactory::create(id, this->project());
				
				object->setDataDir( project()->library()->dataPath() );
				object->fromXml(objectDocument.toString(0));
				object->setDataDir(QString());
				
				addObject(object);
				
				QDomElement objectData = objectDocument.documentElement().firstChild().toElement();
				
				QString data;
				if( !objectData.isNull())
				{
					QTextStream ts(&data);
					ts << objectData;
				}
				
// 				Model::ProjectLoader::createSymbol(LibraryObject::Type(object->type()), object->symbolName(), data.toLocal8Bit(), d->project); FIXME
			}
			else if( e.tagName() == "folder" )
			{
				QDomDocument folderDocument;
				folderDocument.appendChild(folderDocument.importNode(n, true ));
				
				LibraryFolder *folder = new LibraryFolder( e.attribute("id"), d->project, this);
				folder->fromXml(folderDocument.toString(0));
				
				addFolder(folder);
			}
		}
		
		n = n.nextSibling();
	}
}

QDomElement LibraryFolder::toXml(QDomDocument &doc) const
{
	QDomElement folder = doc.createElement("folder");
	folder.setAttribute("id", d->id);
	
	foreach( LibraryObject *object, d->objects.values())
	{
		object->setDataDir( project()->library()->dataPath() );
		folder.appendChild(object->toXml(doc));
		object->setDataDir( QString() );
	}
	
	foreach( LibraryFolder *folderObject, d->folders)
	{
		folder.appendChild(folderObject->toXml(doc));
	}
	
	return folder;
}

}
}

