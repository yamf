/***************************************************************************
 *   Copyright (C) 2007 by David Cuadrado                                  *
 *   krawek@gmail.com                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "audiolayer.h"

#include <QFileInfo>


#include <common/player.h>

#include "library.h"
#include "project.h"
#include "libraryobject.h"


namespace YAMF {
namespace Model {
struct AudioLayer::Private
{
	Private() : startFrame(0) {}
	
	QString filePath, symbolName;
	int playerId;
	
	int startFrame;
};

AudioLayer::AudioLayer(Scene *parent, const QString& name)
 : Layer(parent, name), d(new Private)
{
	setType(Audio);
}

AudioLayer::~AudioLayer()
{
	delete d;
}

void AudioLayer::setStartFrame(int frame)
{
	d->startFrame = frame;
}

int AudioLayer::startFrame() const
{
	return d->startFrame;
}

void AudioLayer::fromFile(const QString& fileName)
{
	d->filePath = fileName;
	d->playerId = Common::Player::self()->load(d->filePath);
}

void AudioLayer::fromSymbol(const QString &symbolName)
{
	Library *library = project()->library();
	
	if( LibrarySoundObject *object = dynamic_cast<LibrarySoundObject *>(library->findObject(symbolName)) )
	{
		fromSymbol(object);
	}
}

void AudioLayer::fromSymbol(Model::LibrarySoundObject *object)
{
	d->symbolName = object->symbolName();
	d->filePath = object->url().toLocalFile();
	
	d->playerId = Common::Player::self()->load(d->filePath);
}

QString AudioLayer::filePath() const
{
	return d->filePath;
}

void AudioLayer::play()
{
	Common::Player::self()->setCurrentPlayer(d->playerId);
	Common::Player::self()->play();
}

void AudioLayer::stop()
{
	Common::Player::self()->setCurrentPlayer(d->playerId);
	Common::Player::self()->stop();
}

void AudioLayer::setVolume(int percent)
{
	Common::Player::self()->setCurrentPlayer(d->playerId);
	Common::Player::self()->setVolume(percent);
}

void AudioLayer::fromXml(const QString &xml )
{
	QDomDocument document;
	
	if (! document.setContent(xml) )
	{
		return;
	}
	
	QDomElement root = document.documentElement();
	setLayerName( root.attribute( "name", layerName() ) );
	
	fromSymbol(root.attribute("symbol"));
}

QDomElement AudioLayer::toXml(QDomDocument &doc) const
{
	QDomElement root = Layer::toXml(doc);
	
	QDomElement audio = doc.createElement("audio");
	audio.setAttribute("symbol", d->symbolName);
	
	root.appendChild(audio);
	
	return root;
}

}
}

