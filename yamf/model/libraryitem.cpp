/***************************************************************************
 *   Copyright (C) 2007 David Cuadrado                                     *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/

#include "libraryitem.h"

#include "item/serializer.h"

#include <QGraphicsPixmapItem>
#include <QGraphicsTextItem>
#include <QGraphicsSvgItem>

#include "yamf/model/libraryobject.h"
#include "yamf/model/project.h"
#include "yamf/model/command/manager.h"

#include <dcore/debug.h>

namespace YAMF {
namespace Model {

struct LibraryItem::Private
{
	Private() : object(0)  {}
	
	QString symbolName;
	LibraryObject *object;
};

LibraryItem::LibraryItem() : Item::Proxy(), d(new Private)
{
}

LibraryItem::LibraryItem(LibraryObject *object) : Item::Proxy(), d(new Private)
{
	setObject(object);
}

LibraryItem::~LibraryItem()
{
	d->object->removeSymbolItem(this);
	delete d;
}

QDomElement LibraryItem::toXml(QDomDocument &doc) const
{
	QDomElement library = doc.createElement("symbol");
	library.setAttribute("id", d->symbolName);
	library.appendChild( Item::Serializer::properties( this, doc));
	
	return library;
}

void LibraryItem::fromXml(const QString &xml)
{
	QDomDocument doc;
	doc.setContent(xml);
	
	QDomElement library = doc.documentElement();
	
	d->symbolName = library.attribute("id");
	
	QDomElement properties = library.firstChild().toElement();
	Item::Serializer::loadProperties(this, properties);	
}

void LibraryItem::setObject(LibraryObject *object)
{
	if( !object)
	{
		dWarning("library") << "Setting null library object";
		return;
	}
	
	if( d->object )
	{
		d->object->removeSymbolItem(this);
	}
	
	d->object = object;
	d->symbolName = object->symbolName();
	switch(object->type())
	{
		case YAMF::Model::LibraryObject::Item:
		{
			if( YAMF::Model::LibraryItemObject *sObject = dynamic_cast<YAMF::Model::LibraryItemObject *>(object) )
			{
				setItem(sObject->item());
			}
		}
		break;
		case YAMF::Model::LibraryObject::Svg:
		{
			if( YAMF::Model::LibrarySvgObject *sObject = dynamic_cast<YAMF::Model::LibrarySvgObject *>(object) )
			{
				setItem(sObject->item());
			}
		}
		break;
		case YAMF::Model::LibraryObject::Image:
		{
			if( YAMF::Model::LibraryBitmapObject *sObject = dynamic_cast<YAMF::Model::LibraryBitmapObject *>(object) )
			{
				setItem(sObject->item());
			}
		}
		break;
		default:
		{
		}
		break;
	}
	
	d->object->addSymbolItem(this);
}

LibraryObject *LibraryItem::libraryObject() const
{
	return d->object;
}

void LibraryItem::setSymbolName(const QString &name)
{
	d->symbolName = name;
}

QString LibraryItem::symbolName() const
{
	return d->symbolName;
}

}
}


