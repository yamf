/***************************************************************************
 *   Copyright (C) 2006 by David Cuadrado                                  *
 *   krawek@gmail.com                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef YAMFMODELLIBRARYFOLDER_H
#define YAMFMODELLIBRARYFOLDER_H

#include <QObject>
#include <QHash>
#include <QByteArray>
#include <QList>
#include <yamf/common/abstractserializable.h>
#include <yamf/model/libraryobject.h>
#include <yamf/common/yamf_exports.h>


namespace YAMF {

namespace Model {

class Project;
class LibraryFolder;
class LibraryObject;



typedef QList<LibraryFolder *> Folders;
typedef QHash<QString, LibraryObject *> LibraryObjects;



/**
 * @ingroup model
 * @author David Cuadrado \<krawek@gmail.com\>
*/
class YAMF_EXPORT LibraryFolder : public QObject, public Common::AbstractSerializable
{
	Q_OBJECT;
	
	public:
		LibraryFolder(const QString &id, Model::Project *const project, QObject *parent = 0);
		~LibraryFolder();
		
		void setId(const QString &id);
		QString id() const;
		
		bool addItemSymbol(QGraphicsItem *symbol, const QString &id);
		bool createItemSymbol(const QString &xml, const QString &id);
		bool createSoundSymbol(const QUrl &url, const QString &id);
		bool createBitmapSymbol(const QUrl &url, const QString &id);
		bool createSvgSymbol(const QUrl &url, const QString &id);
		
		bool addObject(LibraryObject *object);
		bool removeObject(const QString &id);
		
		virtual void addFolder(LibraryFolder *folder);
		
		bool moveObject(const QString &id, LibraryFolder *folder);
		
		LibraryObject *findObject(const QString &id) const;
		void renameObject(const QString &id, const QString &newId);
		
		Folders folders() const;
		LibraryObjects objects() const;
		
		int objectsCount() const;
		int foldersCount() const;
		
		Model::Project *project() const;
		
	public:
		virtual void fromXml(const QString &xml );
		virtual QDomElement toXml(QDomDocument &doc) const;
		
	private:
		struct Private;
		Private *const d;
};
}
}

#endif




