/***************************************************************************
 *   Copyright (C) 2005 by David Cuadrado                                  *
 *   krawek@gmail.com                                          	   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "scene.h"
#include <dcore/debug.h>

#include <QDir>
#include <QGraphicsItem>
#include <QSet>

#include "object.h"
#include "layer.h"

#include "audiolayer.h"

#include "item/group.h"
#include "item/builder.h"

#include "model/command/manager.h"
#include "model/command/insertlayer.h"
#include "model/command/removelayer.h"
#include "model/command/movelayer.h"

#include "model/command/renamescene.h"

#include "yamf/model/audiolayer.h"

namespace YAMF {
namespace Model {

struct Scene::Private
{
	Layers layers;
	
 	AudioLayers audioLayers;
	
	QString name;
	bool isLocked;
	int layerCount;
	bool isVisible;
	
	QSet<Object *> tweeningObjects;
	QSet<Object *> alwaysVisible;
};


/**
 * Constructor por defecto
 */
Scene::Scene(Project *parent, const QString &name) : QObject(parent), d(new Private)
{
	d->name = name;
	d->isLocked = false;
	d->layerCount = 0;
	d->isVisible = true;
}

/**
 * Destructor
 */
Scene::~Scene()
{
	D_END;
	
	d->layers.clear(true);
	
	delete d;
}

/**
 * Pone un nombre a la escena
 */
void Scene::setSceneName(const QString &name)
{
	if(name != d->name)
	{
		Command::RenameScene *cmd = new Command::RenameScene(this, name, &d->name);
		project()->commandManager()->addCommand(cmd);
	}
}

/**
 * retorna el nombre de la escena
 */
QString Scene::sceneName() const
{
	return d->name;
}

/**
 * Bloquea la escena
 */
void Scene::setLocked(bool isLocked)
{
	d->isLocked = isLocked;
}

/**
 * Returna verdadero cuando la escena esta bloqueada
 */
bool Scene::isLocked() const
{
	return d->isLocked;
}

/**
 * @~spanish
 * Cambia la visibilidad de la escena.
 * Si @p isVisible es verdadero permite la visualización del marco de lo contrario lo oculta.
 */
void Scene::setVisible(bool isVisible)
{
	d->isVisible = isVisible;
}

/**
 * @~spanish
 * Retorna verdadero si la escena es visible, de lo contrario retorna falso.
 */
bool Scene::isVisible() const
{
	return d->isVisible;
}

/**
 * Retorna la lista de layers
 */
Layers Scene::layers() const
{
	return d->layers;
}

/**
 * @~spanish
 * Retorna los layers de audio que tiene la escena.
 */
AudioLayers Scene::audioLayers() const
{
	return d->audioLayers;
}

/**
 * @~spanish
 * Crea una capa y la inserta, en la posición @p visualIndex y con el nombre name;
 * por defecto añadirá la capa al final de la escena.
 */
AudioLayer *Scene::createAudioLayer(int position, const QString &name)
{
	D_FUNCINFO << position;
	
	AudioLayer *layer = new AudioLayer(this, name);
	
	insertLayer(position, layer);
	layer->setLayerName(tr("Sound layer %1").arg(d->layerCount+1));
	
	return layer;
}

/**
 * @~spanish
 * Remueve la capa que esta en la posición @p visualIndex.
 * FIXME
 */
bool Scene::removeLayer( int visualIndex)
{
	Layer *layer = this->layer(visualIndex);
	if ( layer )
	{
		d->layers.removeAll(visualIndex);
		delete layer;
		return true;
	}
	
	return false;
}


/**
 * Retorna el layer que se encuentra en la posición indicada
 * @param position 
 * @return 
 */
Layer *Scene::layer(int visualIndex) const
{
	if ( !d->layers.containsVisual(visualIndex) )
	{
		D_FUNCINFO << " FATAL ERROR: index out of bound " << visualIndex;
		return 0;
	}
	
	return d->layers.visualValue(visualIndex);
}

/**
 * @~spanish
 * Crea una capa de audio y la inserta, en la posición @p visualIndex y con el nombre name;
 * por defecto añadirá la capa de audio al final de la escena.
 */
Model::Layer *Scene::createLayer(int visualIndex, const QString &name)
{
	QString layerName = name;
	if( layerName.isEmpty() )
	{
		layerName = tr("Layer %1").arg(d->layerCount+1);
	}
	
	Model::Layer *layer = new Model::Layer(this, layerName);
	
	insertLayer(visualIndex, layer);
// 	layer->setLayerName(layerName);
	
	return layer;
}

/**
 * @~spanish
 * Inserte la capa @p layer en la posición @p visualIndex a la escena.
 */
void Scene::insertLayer(int visualIndex, Model::Layer *layer)
{
	Command::InsertLayer *cmd = new Command::InsertLayer(layer, &d->layers, visualIndex);
	project()->commandManager()->addCommand(cmd);
	
	d->layerCount++;
}

/**
 * @~spanish
 * Añade la capa @p layer a la escena.
 */
void Scene::addLayer(Model::Layer *layer)
{
	Command::InsertLayer *cmd = new Command::InsertLayer(layer, &d->layers);
	project()->commandManager()->addCommand(cmd);
	
	d->layerCount++;
}

/**
 * @~spanish
 * Remueve la capa @p layer de la escena.
 */
void Scene::removeLayer(Model::Layer *layer)
{
	Command::RemoveLayer *cmd = new Command::RemoveLayer(layer, &d->layers);
	project()->commandManager()->addCommand(cmd);
}


/**
 * Mueve el layer a la posicion indicada
 */
bool Scene::moveLayer(int from, int to)
{
	if ( from < 0 || from >= d->layers.count() || to < 0 || to >= d->layers.count() )
	{
		return false;
	}
	
	Command::MoveLayer *cmd = new Command::MoveLayer(this, from, to, &d->layers);
	project()->commandManager()->addCommand(cmd);
	
	return true;
}

/**
 * Retorna la posición logica de la escena.
 */
int Scene::logicalIndex() const
{
	if ( Project *project = dynamic_cast<Project *>(parent()) )
	{
		return project->logicalIndexOf(const_cast<Scene *>(this) );
	}
	
	return -1;
}

/**
 * Retorna la posición visual de la escena.
 */
int Scene::visualIndex() const
{
	if ( Project *project = dynamic_cast<Project *>(parent()) )
	{
		return project->visualIndexOf(const_cast<Scene *>(this) );
	}
	
	return -1;
}

/**
 * Retorna la posición visual de la capa @p layer.
 */
int Scene::visualIndexOf(Layer *layer) const
{
	return d->layers.visualIndex(layer);
}

/**
 * Retorna la posición logica de la capa @p layer.
 */
int Scene::logicalIndexOf(Layer *layer) const
{
	return d->layers.logicalIndex(layer);
}

/**
 * @~spanish
 * Retorna el proyecto en el que esta la escena.
 */
Project *Scene::project() const
{
	return static_cast<Project *>(parent());
}

void Scene::addTweeningObject(Object *object)
{
	d->tweeningObjects << object;
}

void Scene::removeTweeningObject(Object *object)
{
	d->tweeningObjects.remove(object);
}

void Scene::setAlwaysVisible(Object *object, bool visible)
{
	if( visible)
	{
		d->alwaysVisible << object;
	}
	else
	{
		d->alwaysVisible.remove(object);
	}
}

QList<Object *> Scene::alwaysVisible() const
{
	return d->alwaysVisible.toList();
}

QList<Object *> Scene::tweeningObjects() const
{
	return d->tweeningObjects.toList();
}

bool Scene::containsTweeningObject(Object *object) const
{
	return d->tweeningObjects.contains(object);
}

Model::Object *Scene::tweeningObject(QGraphicsItem *item) const
{
	Model::Object *ret = 0;
	foreach(Model::Object *obj, d->tweeningObjects )
	{
		if( obj->item() == item )
		{
			ret = obj;
			break;
		}
	}
	
	return ret;
}

Model::Location Scene::location() const
{
	Model::Location loc;
	loc.setValue(Location::Scene, visualIndex());
	
	return loc;
}

/**
 * @~spanish
 * Construye la escena a partir del xml.
*/
void Scene::fromXml(const QString &xml )
{
	QDomDocument document;
	
	if (! document.setContent(xml) )
	{
		return;
	}
	
	QDomElement root = document.documentElement();
	QDomNode n = root.firstChild();
	setSceneName( root.attribute( "name", sceneName()) );
	
	while( !n.isNull() )
	{
		QDomElement e = n.toElement();
		
		if(!e.isNull())
		{
			if ( e.tagName() == "layer" )
			{
				int pos = d->layers.count();
				
				{
					QString newDoc;
					
					{
						QTextStream ts(&newDoc);
						ts << n;
					}
					
					QDomDocument doc;
					if( !doc.setContent(newDoc) ) return;
					
					QDomElement root = doc.documentElement();
					QString name = root.attribute("name");
					int type = root.attribute("type").toInt();
					
					
					Layer *layer = 0;
					
					switch(type)
					{
						case Layer::Audio:
						{
							layer = createAudioLayer(-1, name);
						}
						break;
						default:
						{
							layer = createLayer(-1, name);
						}
						break;
					}
					
					layer->fromXml( newDoc );
				}
			}
		}
		
		n = n.nextSibling();
	}
}

/**
 * @~spanish
 * Convierte la escena a un documento xml.
 */
QDomElement Scene::toXml(QDomDocument &doc) const
{
	QDomElement root = doc.createElement("scene");
	root.setAttribute("name", d->name );
	
	foreach(Layer *layer, d->layers.visualValues())
	{
		root.appendChild( layer->toXml(doc) );
	}
	
	foreach(AudioLayer *sound, d->audioLayers)
	{
		root.appendChild(sound->toXml(doc));
	}
	
	
	return root;
}


}
}
