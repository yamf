
DEFINES += _BUILD_YAMF_ 
TEMPLATE = lib

CONFIG += dll \
warn_on


SOURCES += frame.cpp \
layer.cpp \
object.cpp \
project.cpp \
scene.cpp \
library.cpp \
libraryfolder.cpp \
libraryobject.cpp \
audiolayer.cpp \
objectgroup.cpp \
libraryitem.cpp \
itembuilder.cpp  \
location.cpp

HEADERS += frame.h \
layer.h \
object.h \
project.h \
scene.h \
library.h \
libraryfolder.h \
libraryobject.h \
audiolayer.h \
objectgroup.h \
libraryitem.h \
itembuilder.h  \
location.h

INCLUDEPATH += ..
include(../../config.pri)


TARGET = yamf_model
LIBS += -L$$DESTDIR -lyamf_item -lyamf_effect -lyamf_common


INSTALLS += target headers
target.path = /lib/

headers.path = /include/yamf/model
headers.files = *.h

include(commands.pri)

