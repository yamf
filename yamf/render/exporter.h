/***************************************************************************
 *   Copyright (C) 2007 David Cuadrado                                     *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/

#ifndef YAMF_RENDEREXPORTER_H
#define YAMF_RENDEREXPORTER_H

#include <QObject>
#include <QSize>
#include <yamf/common/yamf_exports.h>
#include <dgraphics/moviegeneratorinterface.h>

namespace YAMF {

namespace Model {
class Scene;
}

namespace Render {

/**
 * @author David Cuadrado <krawek@gmail.com>
*/
class YAMF_EXPORT Exporter : public QObject
{
	Q_OBJECT;
	public:
		Exporter(QObject *parent = 0);
		~Exporter();
		
		void generate(const QList<YAMF::Model::Scene *> &scenes, const QString &fileName, DGraphics::MovieGeneratorInterface::Format format, const QSize &size, int fps = 24);
		
	protected:
		virtual DGraphics::MovieGeneratorInterface *createGenerator(DGraphics::MovieGeneratorInterface::Format format, const QSize &size, int fps) = 0;
		
	private:
		struct Private;
		Private *const d;
};

}

}

#endif
