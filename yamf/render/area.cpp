/***************************************************************************
 *   Copyright (C) 2007 David Cuadrado                                     *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/

#include "area.h"

#include "yamf/render/renderer.h"
#include "yamf/drawing/photogram.h"
#include "yamf/common/abstractserializable.h"

// DLib
#include <dcore/debug.h>

// Qt
#include <QTimeLine>
#include <QTimer>
#include <QResizeEvent>
#include <QStyleOptionGraphicsItem>

namespace YAMF {

namespace Render {

struct Area::Private
{
	Private() : renderer(new Renderer), fps(24), lastScaleX(1.0), lastScaleY(1.0) {}
	~Private() { delete renderer; delete timeLine; }
	
	Renderer *renderer;
	QTimeLine *timeLine;
	
	QList<YAMF::Model::Scene *> scenes;
	QList<YAMF::Model::Scene *>::iterator currentScene;
	
	int fps;
	
	double lastScaleX;
	double lastScaleY;
	
	QTimer scaleTimer;
};

Area::Area(QWidget *parent)
 : QGraphicsView(parent), d(new Private)
{
	d->currentScene = d->scenes.end();
	
	d->timeLine = new QTimeLine;
	d->timeLine->setCurveShape(QTimeLine::LinearCurve);
	
	connect(d->timeLine, SIGNAL(frameChanged(int)), this, SLOT(setFrame(int)));
	connect(d->timeLine, SIGNAL(finished()), this, SLOT(nextScene()));
	
	setScene(d->renderer->photogram());
	
	QRectF rect(0, 0, this->width(), this->height());
	scale(rect);
	
	
	setInteractive(false);
	
	setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	
	d->scaleTimer.setSingleShot(true);
	connect(&d->scaleTimer, SIGNAL(timeout()), this, SLOT(scale()));
}


Area::~Area()
{
	delete d;
}

void Area::setScenes(const QList<YAMF::Model::Scene *> &scenes)
{
	if( scenes.isEmpty() ) return;
	
	d->scenes = scenes;
	
	d->currentScene = d->scenes.begin();
	d->renderer->setScene(d->scenes.first());
	setFps(d->fps);
}

void Area::start()
{
	if( d->scenes.isEmpty() ) return;
	
	d->timeLine->start();
}

void Area::stop()
{
	d->timeLine->stop();
}

int Area::fps() const
{
	return d->fps;
}

void Area::setFps(int fps)
{
	d->fps = fps;
	
	
	int total = d->renderer->totalPhotograms();
	d->timeLine->setFrameRange(0, total);
	d->timeLine->setDuration((1000/fps) * total);
}

void Area::setFrame(int frame)
{
	d->renderer->setCurrentPhotogram(frame);
}

void Area::nextScene()
{
	if( d->scenes.isEmpty() ) return;
	
	d->currentScene++;
	
	if( d->currentScene >= d->scenes.end() )
	{
		d->currentScene = d->scenes.begin();
		d->renderer->setScene(*d->currentScene);
		setFps(d->fps);
		
		emit finished();
	}
	else
	{
		d->renderer->setScene(*d->currentScene);
		setFps(d->fps);
		d->timeLine->start();
	}
}

void Area::scale()
{
	scale(QRectF(QPointF(0,0), size()));
}

void Area::scale(const QRectF &rect, Qt::AspectRatioMode aspectRatio )
{
	QRectF sceneRect = d->renderer->photogram()->sceneRect();
	
	if( sceneRect.width() == 0 || sceneRect.height() == 0 ) return;
	
	double sx = 1, sy = 1;
		
	sx = static_cast<double>(rect.width()) / static_cast<double>(sceneRect.width());
	sy = static_cast<double>(rect.height()) / static_cast<double>(sceneRect.height());
	
	if(sx > 0 && sy > 0)
	{
		setUpdatesEnabled(false);
		{
			double restoreScaleX = 1.0/d->lastScaleX;
			double restoreScaleY = 1.0/d->lastScaleY;
			QGraphicsView::scale(restoreScaleX, restoreScaleY);
		}
		
		switch(aspectRatio)
		{
			case Qt::IgnoreAspectRatio:
			{
				QGraphicsView::scale(sx, sy);
			}
			break;
			case Qt::KeepAspectRatio:
			{
				float factor = qMin(sx, sy);
				QGraphicsView::scale(factor, factor);
			}
			break;
			case Qt::KeepAspectRatioByExpanding:
			{
				float factor = qMax(sx, sy);
				QGraphicsView::scale(factor, factor);
			}
			break;
		}
		
		d->lastScaleX = sx;
		d->lastScaleY = sy;
		
		setUpdatesEnabled(true);
	}
}

void Area::resizeEvent( QResizeEvent * event )
{
	if( d->scaleTimer.isActive() )
	{
		d->scaleTimer.stop();
	}
	
	d->scaleTimer.start(0);
	QGraphicsView::resizeEvent(event);
}

void Area::hideEvent(QHideEvent *event)
{
	stop();
	QGraphicsView::hideEvent(event);
}

void Area::drawItems(QPainter *painter, int numItems, QGraphicsItem *items[], const QStyleOptionGraphicsItem options[])
{
	QList<QGraphicsItem *> list;
	for(int i = 0; i < numItems; i++)
	{
		if(dynamic_cast<Common::AbstractSerializable *>(items[i]))
		{
			list << items[i];
		}
	}
	
	QGraphicsItem **drawitems = new QGraphicsItem *[list.size()];
	
	int count = 0;
	foreach(QGraphicsItem *item, list)
	{
		drawitems[count] = item;
		count++;
	}
	
	QGraphicsView::drawItems(painter, list.size(), drawitems, options);
	delete[] drawitems;
}

}

}
