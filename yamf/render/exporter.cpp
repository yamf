/***************************************************************************
 *   Copyright (C) 2007 David Cuadrado                                     *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/


#include "exporter.h"
#include "renderer.h"

#include <QPainter>

namespace YAMF {
namespace Render {

struct Exporter::Private
{
	Private() : renderer(new Renderer), generator(0) {}
	~Private() { delete renderer; }
	Renderer *renderer;
	
	DGraphics::MovieGeneratorInterface *generator;
};

Exporter::Exporter(QObject *parent)
 : QObject(parent), d( new Private )
{
}


Exporter::~Exporter()
{
	delete d;
}

void Exporter::generate(const QList<YAMF::Model::Scene *> &scenes, const QString &fileName, DGraphics::MovieGeneratorInterface::Format format, const QSize &size, int fps)
{
	DGraphics::MovieGeneratorInterface *generator = createGenerator(format, size, fps);
	
	if( QPaintDevice *pdev = dynamic_cast<QPaintDevice *>(generator) )
	{
		QPainter painter(pdev);
		painter.setRenderHint(QPainter::Antialiasing);
		
		foreach(YAMF::Model::Scene *scene, scenes )
		{
			if( scene )
			{
				d->renderer->setScene(scene);
				
				while(d->renderer->nextPhotogram())
				{
					d->renderer->render(&painter);
					generator->nextFrame();
					generator->reset();
				}
			}
		}
		
		generator->saveMovie(fileName);
		
		d->renderer->reset();
	}
	
	delete generator;
}

}
}
