/***************************************************************************
 *   Copyright (C) 2007 David Cuadrado                                     *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/

#ifndef YAMF_RENDERAREA_H
#define YAMF_RENDERAREA_H

#include <yamf/common/yamf_exports.h>
#include <QGraphicsView>
#include <QGraphicsItem>


namespace YAMF {

namespace Model {
class Scene;
}

namespace Render {

/**
 * @author David Cuadrado <krawek@gmail.com>
*/
class YAMF_EXPORT Area : public QGraphicsView
{
	Q_OBJECT;
	
	public:
		Area(QWidget *parent = 0);
		~Area();
		
		void setScenes(const QList<YAMF::Model::Scene *> &scenes);
		int fps() const;
		
		void scale(const QRectF &rect, Qt::AspectRatioMode aspectRatio = Qt::IgnoreAspectRatio);
		
	protected:
		void resizeEvent(QResizeEvent *event);
		void hideEvent(QHideEvent *event);
		
		void drawItems(QPainter *painter, int numItems, QGraphicsItem *items[], const QStyleOptionGraphicsItem options[]);
		
	public slots:
		void start();
		void stop();
		
		void setFps(int fps);
		void setFrame(int frame);
		void nextScene();
		void scale();
		
	signals:
		void finished();
		
	private:
		struct Private;
		Private *const d;
};

}

}

#endif
