/***************************************************************************
 *   Copyright (C) 2007 by David Cuadrado                                  *
 *   krawek@gmail.com                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "renderer.h"
#include "model/scene.h"
#include "model/layer.h"
#include "model/frame.h"
#include "model/object.h"


#include "drawing/photogram.h"

#include <QPainter>


namespace YAMF {
namespace Render {

struct Renderer::Private
{
	Drawing::Photogram *photogram;
	int totalPhotograms;
	int currentPhotogram;
	
	Private() : photogram(0), totalPhotograms(-1), currentPhotogram(0) {}
	
	~Private()
	{
		delete photogram;
	}
	
	int calculateTotalPhotograms(Model::Scene *scene);
};


int Renderer::Private::calculateTotalPhotograms(Model::Scene *scene)
{
	Model::Layers layers = scene->layers();
	
	int total = 0;

	foreach(Model::Layer *layer, layers.values())
	{
		if( layer )
		{
			total = qMax(total, layer->frames().count());
		}
	}
	
	return total;
}

Renderer::Renderer() : d(new Private)
{
	d->photogram = new Drawing::Photogram;
	d->photogram->setBackgroundBrush(Qt::white);
	d->photogram->setSceneRect(QRectF(QPointF(0,0), QSizeF( 500, 400 ) )); // FIXME: this isn't real size
}

Renderer::~Renderer()
{
	delete d;
}

Drawing::Photogram *Renderer::photogram() const
{
	return d->photogram;
}

void Renderer::setScene(Model::Scene *scene)
{
	d->photogram->setCurrentScene(scene);
	
	reset();
	
	d->totalPhotograms = d->calculateTotalPhotograms(scene);
}

bool Renderer::nextPhotogram()
{
	if( d->totalPhotograms < 0 ) return false;
	
	d->currentPhotogram++;
	
	if( d->currentPhotogram == d->totalPhotograms )
		return false;
	
	d->photogram->drawPhotogram(d->currentPhotogram);
	return true;
}

void Renderer::render(QPainter *painter)
{
	d->photogram->render(painter, QRect(0, 0, painter->device()->width(), painter->device()->height()), d->photogram->sceneRect().toRect(), Qt::IgnoreAspectRatio );
}

void Renderer::setCurrentPhotogram(int photogram)
{
	if( photogram > d->totalPhotograms ) return;
	
	d->currentPhotogram = photogram;
	d->photogram->drawPhotogram(d->currentPhotogram);
}

void Renderer::reset()
{
	d->photogram->drawPhotogram(0); // ###: Why whithout this don't work?
	d->currentPhotogram = -1;
}

int Renderer::currentPhotogram() const
{
	return d->currentPhotogram;
}

int Renderer::totalPhotograms() const
{
	return d->totalPhotograms;
}

}
}

