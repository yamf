TEMPLATE = lib
DEFINES += _BUILD_YAMF_ 

CONFIG += dll \
warn_on
SOURCES += renderer.cpp \
exporter.cpp \
area.cpp
HEADERS += renderer.h \
exporter.h \
area.h
INCLUDEPATH += ..

include(../../config.pri)


link_with(dlibswf) {
HEADERS += swfexporter.h
SOURCES += swfexporter.cpp
}

link_with(dlibffmpeg) {
HEADERS += movieexporter.h
SOURCES += movieexporter.cpp
}

TARGET = yamf_render
LIBS += -L$$DESTDIR -lyamf_common -lyamf_item -lyamf_model -lyamf_drawing

INSTALLS += target headers
target.path = /lib/

headers.path = /include/yamf/render
headers.files = *.h

