/***************************************************************************
 *   Copyright (C) 2007 David Cuadrado                                     *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/

 

#ifndef YAMF_EFFECTFILTER_H
#define YAMF_EFFECTFILTER_H

#include <yamf/item/proxy.h>
#include <yamf/common/abstractserializable.h>
#include <yamf/common/yamf_exports.h>

class QToolBar;

namespace YAMF {
namespace Effect {

/**
 * @ingroup effect
 * @author David Cuadrado <krawek@gmail.com>
*/
class YAMF_EXPORT Filter : public QObject, public Item::Proxy, public YAMF::Common::AbstractSerializable
{
	Q_OBJECT;
	
	public:
		enum { Type = UserType + 593 };
		
		Filter();
		Filter(QGraphicsItem *target);
		~Filter();
		
		void setTarget(QGraphicsItem * target);
		
		void paint(QPainter *painter, const QStyleOptionGraphicsItem *opt, QWidget *w);
		virtual QImage apply(QImage &itemImage) = 0;
		virtual QRectF boundingRect() const;
		
		virtual QString properties() const;
		
		void setProperty(const QString &key, const QString &value);
		QString property(const QString &key, const QString &def = QString()) const;
		
		void loadProperties(const QString &properties);
		
		int type() const;
		
		virtual QString name() const = 0;
		
		void fromXml(const QString &xml);
		QDomElement toXml(QDomDocument &doc) const;
		
	private:
		struct Private;
		Private *const d;
};

}

}

#endif
