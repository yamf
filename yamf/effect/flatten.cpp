/***************************************************************************
 *   Copyright (C) 2007 David Cuadrado                                     *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/

#include "flatten.h"
#include <dgraphics/effects.h>

namespace YAMF {
namespace Effect {

struct Flatten::Private
{
	QString prop1;
	QString prop2;
};

Flatten::Flatten(QObject *parent)
 : Filter(), d(new Private)
{
	d->prop1 = "color_a";
	d->prop2 = "color_b";
	
	setProperty(d->prop1, "#000000");
	setProperty(d->prop2, "#FFFFFF");
}


Flatten::~Flatten()
{
	delete d;
}

QImage Flatten::apply(QImage &itemImage)
{
	return DGraphics::Effects::flatten(itemImage, ca(), cb());
}

QString Flatten::name() const
{
	return "flatten";
}


QColor Flatten::ca() const
{
	return QColor(property(d->prop1, "#000000"));
}

QColor Flatten::cb() const
{
	return QColor(property(d->prop2, "#FFFFFF"));
}

QString Flatten::properties() const
{
	return QString("<properties>")+
			"<property name=\""+d->prop1+"\""+QString(" type=\"color\" text=\"Color a\" value=\"%1\" />").arg(ca().name())+
			"<property name=\""+d->prop2+"\""+QString(" type=\"color\" text=\"Color b\" value=\"%1\" />").arg(cb().name())+
			QString("</properties>");
}



}

}
