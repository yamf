/***************************************************************************
 *   Copyright (C) 2007 David Cuadrado                                     *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/

#include "emboss.h"

#include <dgraphics/effects.h>

namespace YAMF {
namespace Effect {

struct Emboss::Private
{
};

Emboss::Emboss()
 : YAMF::Effect::Filter(), d(new Private)
{
	setProperty("radius", "0.0");
	setProperty("sigma", "1.0");
}


Emboss::~Emboss()
{
	delete d;
}


QImage Emboss::apply(QImage &itemImage)
{
	return DGraphics::Effects::emboss(itemImage, radius(), sigma());
}

QString Emboss::name() const
{
	return "flatten";
}


float Emboss::radius() const
{
	return property("radius", "0.0").toFloat();
}

float Emboss::sigma() const
{
	return property("sigma", "1.0").toFloat();
}

QString Emboss::properties() const
{
	return QString("<properties>")+
			"<property name=\"radius\""+QString(" type=\"real\" step=\"0.1\" text=\"Radius\" value=\"%1\" />").arg(radius())+
			"<property name=\"sigma\""+QString(" type=\"real\" step=\"0.2\" text=\"Sigma\" value=\"%1\" />").arg(sigma())+
			QString("</properties>");
}


}

}
