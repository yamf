/***************************************************************************
 *   Copyright (C) 2007 David Cuadrado                                     *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/

#include "swirl.h"

#include <QXmlStreamReader>

#include <dgraphics/effects.h>

namespace YAMF {

namespace Effect {

Swirl::Swirl()
 : YAMF::Effect::Filter()
{
	setProperty("radius", "60.0");
}


Swirl::~Swirl()
{
}

float Swirl::radius() const
{
	return property("radius", "60.0" ).toFloat();
}

QImage Swirl::apply(QImage &itemImage)
{
	return DGraphics::Effects::swirl(itemImage, radius());
}

QString Swirl::name() const
{
	return "swirl";
}

QString Swirl::properties() const
{
	return QString("<properties>")+
			"<property name=\""+tr("radius")+"\""+QString(" type=\"real\" value=\"%1\" min=\"-1000\" max=\"1000\" step=\"10\" />").arg(radius())+
			QString("</properties>");
}

}

}
