/***************************************************************************
 *   Copyright (C) 2007 David Cuadrado                                     *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/

#include "wave.h"

#include <dgraphics/effects.h>

namespace YAMF {

namespace Effect {

Wave::Wave()
 : YAMF::Effect::Filter()
{
}


Wave::~Wave()
{
}

float Wave::amplitude() const
{
	return property("amplitude", "25.0").toFloat();
}

float Wave::frequency() const
{
	return property("frequency", "150.0").toFloat();
}

int Wave::background() const
{
	return property("background", "0").toInt();
}


QImage Wave::apply(QImage &itemImage)
{
	return DGraphics::Effects::wave(itemImage, amplitude(), frequency(), background() );
}

QString Wave::name() const
{
	return "wave";
}

QString Wave::properties() const
{
	return QString("<properties>")+
			"<property name=\""+QString("amplitude")+"\""+QString(" type=\"real\" value=\"%1\" min=\"0\" max=\"1000\" />").arg(amplitude())+
			"<property name=\""+QString("frequency")+"\""+QString(" type=\"real\" value=\"%1\" min=\"0\" max=\"1000\" />").arg(frequency())+
			"<property name=\""+QString("background")+"\""+QString(" type=\"integer\" value=\"%1\" min=\"0\" max=\"255\" step=\"10\" />").arg(background())+
			QString("</properties>");
}


}

}
