/***************************************************************************
 *   Copyright (C) 2007 David Cuadrado                                     *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/

#include "factory.h"

#include "swirl.h"
#include "blur.h"
#include "wave.h"
#include "flatten.h"
#include "edge.h"
#include "sharpen.h"
#include "emboss.h"

namespace YAMF {
namespace Effect {


Filter *Factory::create(const QString &name)
{
	if( name == "blur" )
	{
		return new Blur();
	}
	else
	if( name == "swirl" )
	{
		return new Swirl();
	}
	else
	if( name == "wave" )
	{
		return new Wave();
	}
	else
	if( name == "flatten" )
	{
		return new Flatten();
	}
	else
	if( name == "edge" )
	{
		return new Flatten();
	}
	else
	if( name == "sharpen" )
	{
		return new Sharpen();
	}
	else
	if( name == "emboss" )
	{
		return new Emboss();
	}
	
	else
	{
		qFatal(qPrintable(QObject::tr("Can't find filter: %1").arg(name)));
	}
	
	return 0;
}

}

}


