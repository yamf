/***************************************************************************
 *   Copyright (C) 2007 David Cuadrado                                     *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/

#include "filter.h"
#include <QImage>
#include <QToolBar>
#include <QPainter>
#include <QStyleOptionGraphicsItem>
#include <QTextStream>
#include <QHash>
#include <QXmlStreamReader>

#include <dcore/debug.h>

#include "factory.h"
#include "yamf/item/builder.h"


namespace YAMF {
namespace Effect {

struct Filter::Private
{
	Private() {}
	QSizeF defaultSize;
	
	QHash<QString, QString> properties;
};

Filter::Filter() : Item::Proxy(), d(new Private)
{
}

Filter::Filter(QGraphicsItem *target)
	: Item::Proxy(target), d(new Private)
{
}


Filter::~Filter()
{
	delete d;
}

void Filter::setTarget(QGraphicsItem * target)
{
	setItem(target);
	d->defaultSize = target->boundingRect().size();
	
	update();
}

void Filter::paint(QPainter *painter, const QStyleOptionGraphicsItem *opt, QWidget *w)
{
	QGraphicsItem *target = item();
	
	QRectF br = target->boundingRect(); 
	QImage img(br.size().toSize(), QImage::Format_ARGB32_Premultiplied);
	
	img.fill(qRgba(0, 0, 0, 0));
	
	{
		QStyleOptionGraphicsItem option = *opt;
		option.state = QStyle::State_None;
		
		QPainter p(&img);
		p.translate(-br.topLeft());
		
		target->paint(&p, &option);
		p.end();
	}
	
	QImage filtered = apply(img);
	
	d->defaultSize = filtered.size();
	painter->drawImage(br.topLeft(), filtered);
}

QRectF Filter::boundingRect() const
{
	return QRectF(item()->boundingRect().topLeft(), d->defaultSize);
}

QString Filter::properties() const
{
	return QString();
}

void Filter::setProperty(const QString &key, const QString &value)
{
	d->properties[key] = value;
	update();
}

QString Filter::property(const QString &key, const QString &def) const
{
	if( d->properties.contains(key) )
		return d->properties.value(key);
	return def;
}

void Filter::loadProperties(const QString &properties)
{
	QXmlStreamReader reader(properties);
	
	bool ok = false;
	while(!reader.atEnd() )
	{
		if( reader.readNext() == QXmlStreamReader::StartElement )
		{
			if(reader.name() == "properties" )
			{
				ok = true;
			}
			else if( ok && reader.name() == "property" )
			{
				QString prop = reader.attributes().value("name").toString();
				setProperty(prop, reader.attributes().value("value").toString());
			}
		}
	}
}

int Filter::type() const
{
	return Filter::Type;
}

void Filter::fromXml(const QString &xml)
{
	QDomDocument doc;
	doc.setContent(xml);
	
	QDomElement filter = doc.documentElement();
	
	QDomNode n = filter.firstChild();
	while(!n.isNull())
	{
		QDomElement e = n.toElement();
		if(!e.isNull())
		{
			QString str;
			{
				QTextStream ts(&str);
				
				ts << e;
			}
			
			if( e.tagName() == "properties" )
			{
				loadProperties(str);
			}
			else if( e.tagName() == "filter" )
			{
				Filter *f = Factory::create(e.attribute("name"));
				f->fromXml(str);
				setTarget(f);
			}
			else
			{
				Item::Builder builder;
				setTarget(builder.create(str));
			}
		}
		n = n.nextSibling();
	}
}

QDomElement Filter::toXml(QDomDocument &doc) const
{
	QDomElement e = doc.createElement("filter");
	e.setAttribute("name", name());
	
	
	QDomElement properties = doc.createElement("properties");
	
	QHash<QString, QString>::const_iterator it = d->properties.constBegin();
	while (it != d->properties.constEnd())
	{
		QString key = it.key();
		
		QDomElement e1 = doc.createElement("property");
		
		e1.setAttribute("name", key);
		e1.setAttribute("value", it.value());
		properties.appendChild(e1);
		
		++it;
	}
	
	e.appendChild(properties);
	
	if( Common::AbstractSerializable *s = dynamic_cast<Common::AbstractSerializable *>(item()) )
	{
		e.appendChild(s->toXml(doc));
	}
	
	return e;
}

}

}
