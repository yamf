

DEFINES += _BUILD_YAMF_ 
TEMPLATE = lib

include(../../config.pri)
LIBS += -lyamf_common -lyamf_item

CONFIG += dll warn_on

TARGET = yamf_effect
INSTALLS += target headers

target.path = /lib

headers.files = *.h
headers.path = /include/yamf/effect/

SOURCES += filter.cpp \
blur.cpp \
factory.cpp \
swirl.cpp \
wave.cpp \
flatten.cpp \
edge.cpp \
sharpen.cpp \
emboss.cpp
HEADERS += filter.h \
blur.h \
factory.h \
swirl.h \
wave.h \
flatten.h \
edge.h \
sharpen.h \
emboss.h
