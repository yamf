/***************************************************************************
 *   Copyright (C) 2007 David Cuadrado                                     *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/

#include "blur.h"

#include <QSpinBox>
#include <QToolBar>
#include <QAction>
#include <QHBoxLayout>
#include <QLabel>
#include <QXmlStreamReader>

#include <dgraphics/effects.h>
#include <dcore/debug.h>


namespace YAMF {
namespace Effect {

struct Blur::Private
{
};

Blur::Blur()
 : YAMF::Effect::Filter(), d(new Private)
{
	setProperty("radius", "3");
}

Blur::~Blur()
{
	delete d;
}

QImage Blur::apply(QImage &itemImage)
{
	return DGraphics::Effects::blur(itemImage, radius());
}

QString Blur::name() const
{
	return "blur";
}

int Blur::radius() const
{
	return property("radius", "3").toInt();
}

QString Blur::properties() const
{
	return QString("<properties>")+
			"<property name=\""+ tr("radius")+"\""+QString(" type=\"integer\" value=\"%1\" min=\"0\" max=\"10\" />").arg(radius())+
			QString("</properties>");
}

}

}
