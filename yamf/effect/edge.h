/***************************************************************************
 *   Copyright (C) 2007 David Cuadrado                                     *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/

 

#ifndef YAMF_EFFECTEDGE_H
#define YAMF_EFFECTEDGE_H

#include <yamf/effect/filter.h>

namespace YAMF {
namespace Effect {

/**
 * @author David Cuadrado <krawek@gmail.com>
*/
class YAMF_EXPORT Edge : public YAMF::Effect::Filter
{
	Q_OBJECT;
	public:
		Edge(QObject *parent = 0);
		~Edge();
		
		QImage apply(QImage &itemImage);
		QString name() const;
		
		QString properties() const;
		
	private:
		struct Private;
		Private *const d;
};

}

}

#endif


