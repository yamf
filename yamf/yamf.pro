CONFIG += warn_on \
	  thread \
          qt
QT -= gui
TEMPLATE = subdirs

SUBDIRS += common \
item \
effect \
model \
drawing \
render  \
gui
