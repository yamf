/***************************************************************************
 *   Copyright (C) 2006 by David Cuadrado                                  *
 *   krawek@toonka.com                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "preview.h"

#include <QGraphicsItem>
#include <QGraphicsScene>
#include <QPainter>
#include <QStyleOptionGraphicsItem>

#include "item/group.h"

#include <dcore/debug.h>

namespace YAMF {
namespace Item {

struct Preview::Private
{
	Private() : item(0) {}
	QGraphicsItem *item;
};

/**
 * @~spanish
 * Crea un Previsualizador de items.
 */
Preview::Preview(QWidget *parent) : QWidget(parent), d(new Private)
{
}


/**
 * Destructor
 */
Preview::~Preview()
{
	delete d;
}

/**
 * @~spanish
 * Retorna el tamaño ideal del previsualizador de items.
 */
QSize Preview::sizeHint() const
{
	return QWidget::sizeHint().expandedTo(QSize(100,100));
}


/**
 * @~spanish
 * Asigna el Item a previsualizar.
 */
void Preview::render(QGraphicsItem *item)
{
	d->item = item;
	update();
}

/**
 * @~spanish
 * Dibuja el item, ajustándolo a las dimensiones del previsualizador.
 */
void Preview::paintEvent(QPaintEvent *)
{
	QPainter p(this);
	p.setRenderHint(QPainter::Antialiasing, true);
	
	if ( d->item )
	{
// 		QRectF br = d->item->boundingRect();
// 		p.translate( (rect().width() - br.width())/2, (rect().height() - br.height())/2 );
		
		QGraphicsScene *original = d->item->scene();
		
		QGraphicsScene dummy;
		
		dummy.addItem(d->item);
		dummy.render(&p, rect());
		
		dummy.removeItem(d->item);
		if( YAMF::Item::Group *group = dynamic_cast<YAMF::Item::Group *>(d->item) )
		{
			group->recoverChilds();
		}
		
		if( original )
		{
			original->addItem(d->item);
		}
	}
}

}
}


