/***************************************************************************
 *   Copyright (C) 2006 by David Cuadrado                                  *
 *   krawek@gmail.com                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "line.h"
#include "serializer.h"
#include <dcore/debug.h>

namespace YAMF {
namespace Item {
/**
 * @~spanish
 * Construye una linea con un ítem "padre" @p parent y dentro de la escena @p scene.
 */
Line::Line(QGraphicsItem * parent, QGraphicsScene * scene) : QGraphicsLineItem(parent, scene)
{
}


/**
 * Destructor
 * @return 
 */
Line::~Line()
{
}


/**
 * @~spanish
 * Toma los valores dentro del documento xml @p xml y se las asigna a las propiedades de la linea.
 */
void Line::fromXml(const QString &xml)
{
}

/**
 * @~spanish
 * Crea un elemento del documento xml @p doc con las propiedades actuales de la linea.
 */
QDomElement Line::toXml(QDomDocument &doc) const
{
	QDomElement root = doc.createElement("line");
	
	root.setAttribute("x1", line().x1());
	root.setAttribute("y1", line().y1());
	root.setAttribute("x2", line().x2());
	root.setAttribute("y2", line().y2());
	
	root.appendChild( Serializer::properties( this, doc));
	
	QPen pen = this->pen();
	root.appendChild(Serializer::pen(&pen, doc));

	return root;
}

}
}


