/***************************************************************************
 *   Copyright (C) 2007 by Jorge Cuadrado                                  *
 *   kuadrosxx@gmail.com                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
 
#ifndef YAMF_ITEMPOLYGON_H
#define YAMF_ITEMPOLYGON_H

#include <QAbstractGraphicsShapeItem>

#include <common/abstractserializable.h>
#include <yamf/common/yamf_exports.h>

namespace YAMF {

namespace Item {

/**
 * @ingroup item
 * @~spanish
 * @brief Esta clase implementa un ítem para representar un poligono dentro de un escena.
 * @author Jorge Cuadrado <kuadrosxx@gmail.com>
 */
class YAMF_EXPORT Polygon : public Common::AbstractSerializable, public  QAbstractGraphicsShapeItem
{
	public:
		Polygon(QGraphicsItem * parent = 0, QGraphicsScene * scene = 0);
		Polygon(int corners, QGraphicsItem * parent = 0, QGraphicsScene * scene = 0);
		Polygon(int corners, double start, const QRectF & rect, QGraphicsItem * parent = 0, QGraphicsScene * scene = 0);
		
		~Polygon();
		
		virtual void fromXml(const QString &xml);
		virtual QDomElement toXml(QDomDocument &doc) const;
		
		QRectF boundingRect () const;
		void paint ( QPainter * painter, const QStyleOptionGraphicsItem * option, QWidget * widget = 0 );
		
		void setRect(const QRectF& rect);
		QRectF rect() const;
		
		void setStart(double start);
		double start() const;
		
		void setCorners(int corners);
		int corners() const;
		
		enum { Type = UserType + 4 };
		int type() const { return Type; }
		
		QPainterPath shape () const;
		
	private:
		struct Private;
		Private *const d;
		
};

}

}

#endif
