/***************************************************************************
 *   Copyright (C) 2006 by David Cuadrado                                  *
 *   krawek@gmail.com                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef YAMFITEMSERIALIZER_H
#define YAMFITEMSERIALIZER_H

#include <QDomDocument>
#include <QDomElement>
#include <QXmlAttributes>
#include <QBrush>
#include <QPen>
#include <yamf/item/path.h>

#include <yamf/common/yamf_exports.h>

class QGraphicsItem;
class QXmlStreamReader;
class QXmlStreamWriter;

namespace YAMF {
namespace Item {

/**
 * @ingroup item
 * @~spanish
 * @brief Esta clase se encarga de convertir las propiedades de los items en elementos de un documento xml, ademas permite leer las propiedades en un elemento xml.
 * 
 * @author David Cuadrado <krawek@gmail.com>
 * @todo:
 * - Serialize fonts
 * 
*/
class YAMF_EXPORT Serializer
{
	public:
		Serializer();
		~Serializer();
		
		static QDomElement properties(const QGraphicsItem *item, QDomDocument &doc);
		static void properties(const QGraphicsItem *item, QXmlStreamWriter *writer);
		
		static void loadProperties(QGraphicsItem *item, const QXmlAttributes &atts);
		static void loadProperties(QGraphicsItem *item, const QDomElement &element);
		static void loadProperties(QGraphicsItem *item, const QString &properties);
		static void loadProperties(QGraphicsItem *item, QXmlStreamReader *reader);
		
		static QDomElement gradient(const QGradient *gradient, QDomDocument &doc);
		static void gradient(const QGradient *gradient, QXmlStreamWriter *writer);
		
		static QGradient *createGradient(const QXmlAttributes &atts);
		static QGradient *loadGradient(QXmlStreamReader *reader);
		static QGradient *loadGradient(const QString &doc);
		
		static QDomElement brush(const QBrush *brush, QDomDocument &doc);
		static void brush(const QBrush *brush, QXmlStreamWriter *writer);
		
		static void loadBrush(QBrush &brush, QXmlStreamReader *reader);
		static void loadBrush(QBrush &brush, const QString &doc);
		static void loadBrush(QBrush &brush, const QXmlAttributes &atts);
		static void loadBrush(QBrush &brush, const QDomElement &e);
		
		static QDomElement pen(const QPen *pen, QDomDocument &doc);
		static void pen(const QPen *pen, QXmlStreamWriter *writer);
		
		static void loadPen(QPen &pen, const QXmlAttributes &atts);
		static void loadPen(QPen &pen, QXmlStreamReader *reader);
		static void loadPen(QPen &pen, const QDomElement &e);
		static void loadPen(QPen &pen, const QString &doc);
		
		static QDomElement font(const QFont &font, QDomDocument &doc);
		static void font(const QFont &font, QString *strFont);
		static void font(const QFont &font, QXmlStreamWriter *writer);
		
		static void loadFont(QFont &font, const QDomElement &e);
		static void loadFont(QFont &font, const QXmlAttributes &atts);
		static void loadFont(QFont &font, const QString &properties);
		static void loadFont(QFont &font, QXmlStreamReader *reader);
};
}
}

#endif
