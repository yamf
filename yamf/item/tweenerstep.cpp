/***************************************************************************
 *   Copyright (C) 2007 by David Cuadrado                                  *
 *   krawek@gmail.com                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "tweenerstep.h"
#include <QVector>

#include <dgraphics/pathhelper.h>
#include <dcore/debug.h>

#include "item/serializer.h"

namespace YAMF {
namespace Item {

struct TweenerStep::Private
{
	QPointF position;
	double rotation;
	
	QBrush brush;
	QPen pen;
	QPainterPath path;
	
	struct PairF {
		double x;
		double y;
	} translation, shear, scale;
	
	int flags;
	int n;
};

TweenerStep::TweenerStep(int n)
	: Common::AbstractSerializable(), d(new Private)
{
	d->n = n;
	d->flags = None;
}

TweenerStep::~TweenerStep()
{
	delete d;
}

void TweenerStep::setPosition(const QPointF &pos)
{
	d->position = pos;
	d->flags |= Position;
}

void TweenerStep::setTranslation(double dx, double dy)
{
	d->translation.x = dx;
	d->translation.y = dy;
	d->flags |= Translation;
}

void TweenerStep::setRotation(double angle)
{
	d->rotation = angle;
	d->flags |= Rotation;
}

void TweenerStep::setShear(double sh, double sv)
{
	d->shear.x = sh;
	d->shear.y = sv;
	d->flags |= Shear;
}

void TweenerStep::setScale(double sx, double sy)
{
	d->scale.x = sx;
	d->scale.y = sy;
	d->flags |= Scale;
}

void TweenerStep::setBrush(const QBrush &brush)
{
	d->brush = brush;
	d->flags |= Brush;
}

void TweenerStep::setPen(const QPen &pen)
{
	d->pen = pen;
	d->flags |= Pen;
}

void TweenerStep::setPath(const QPainterPath &path)
{
	d->path = path;
	d->flags |= Path;
}

bool TweenerStep::has(Type type) const
{
	return d->flags & type;
}

int TweenerStep::n() const
{
	return d->n;
}

QPointF TweenerStep::position() const
{
	return d->position;
}

double TweenerStep::horizontalScale() const
{
	return d->scale.x;
}

double TweenerStep::verticalScale() const
{
	return d->scale.y;
}

double TweenerStep::horizontalShear() const
{
	return d->shear.x;
}

double TweenerStep::verticalShear() const
{
	return d->shear.y;
}

double TweenerStep::rotation() const
{
	return d->rotation;
}

double TweenerStep::xTranslation() const
{
	return d->translation.x;
}

double TweenerStep::yTranslation() const
{
	return d->translation.y;
}

QBrush TweenerStep::brush() const
{
	return d->brush;
}

QPen TweenerStep::pen() const
{
	return d->pen;
}

QPainterPath TweenerStep::path() const
{
	return d->path;
}

QDomElement TweenerStep::toXml(QDomDocument& doc) const
{
	QDomElement step = doc.createElement("step");
	step.setAttribute("value", d->n);
	
	if(this->has(TweenerStep::Position) )
	{
		QDomElement e = doc.createElement("position");
		e.setAttribute("x", d->position.x());
		e.setAttribute("y", d->position.y());
		
		step.appendChild(e);
	}
	
	if(this->has(TweenerStep::Scale) )
	{
		QDomElement e = doc.createElement("scale");
		e.setAttribute("sx", d->scale.x);
		e.setAttribute("sy", d->scale.y);
		
		step.appendChild(e);
	}
	
	if(this->has(TweenerStep::Translation) )
	{
		QDomElement e = doc.createElement("translation");
		e.setAttribute("dx", d->translation.x);
		e.setAttribute("dy", d->translation.y);
		
		step.appendChild(e);
	}
	
	if(this->has(TweenerStep::Shear) )
	{
		QDomElement e = doc.createElement("shear");
		e.setAttribute("sh", d->shear.x);
		e.setAttribute("sv", d->shear.y);
		
		step.appendChild(e);
	}
	
	if(this->has(TweenerStep::Rotation) )
	{
		QDomElement e = doc.createElement("rotation");
		e.setAttribute("angle", d->rotation);
		
		step.appendChild(e);
	}
	
	if( this->has(TweenerStep::Brush ) )
	{
		step.appendChild(Item::Serializer::brush(&d->brush, doc));
	}
	
	if( this->has(TweenerStep::Pen ) )
	{
		step.appendChild(Item::Serializer::pen(&d->pen, doc));
	}
	
	if( this->has(TweenerStep::Path ))
	{
		QDomElement element = doc.createElement("path");
		element.setAttribute("d", DGraphics::PathHelper::toString(path()));
		step.appendChild(element);
	}
	
	return step;
}


void TweenerStep::fromXml(const QString& xml)
{
	QDomDocument doc;
	
	if( doc.setContent(xml ) )
	{
		QDomElement root = doc.documentElement();
		
		QDomNode n = root.firstChild();
		
		d->n = root.attribute("value").toInt();
		
		while(!n.isNull())
		{
			QDomElement e = n.toElement();
			if(!e.isNull())
			{
				if( e.tagName() == "rotation" )
				{
					setRotation(e.attribute("angle").toDouble());
				}
				else if( e.tagName() == "shear" )
				{
					setShear(e.attribute("sh").toDouble(), e.attribute("sv").toDouble());
				}
				else if( e.tagName() == "position" )
				{
					setPosition(QPointF(e.attribute("x").toDouble(), e.attribute("y").toDouble()));
				}
				else if( e.tagName() == "scale" )
				{
					setScale(e.attribute("sx").toDouble(), e.attribute("sy").toDouble());
				}
				else if( e.tagName() == "translation" )
				{
					setTranslation(e.attribute("dx").toDouble(), e.attribute("dy").toDouble());
				}
				else if ( e.tagName() == "brush" )
				{
					Item::Serializer::loadBrush(d->brush, e);
					d->flags |= Brush;
				}
				else if ( e.tagName() == "pen" )
				{
					Item::Serializer::loadPen(d->pen, e);
					d->flags |= Pen;
				}
				else if( e.tagName() == "path" )
				{
					d->path = DGraphics::PathHelper::fromString(e.attribute("d"));
					d->flags |= Path;
				}
				
			}
			n = n.nextSibling();
		}
	}
}

QDomDocument TweenerStep::createXml(int frames, const QVector<TweenerStep *> &steps)
{
	QDomDocument doc;
	
	QDomElement root = doc.createElement("tweening");
	root.setAttribute("frames", frames);
	
	foreach(TweenerStep *step, steps)
	{
		root.appendChild(step->toXml(doc));
	}
	
	doc.appendChild(root);
	
	return doc;
}

}
}
