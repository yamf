/***************************************************************************
 *   Copyright (C) 2007 by David Cuadrado                                  *
 *   krawek@gmail.com                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "tweener.h"

#include <QGraphicsItemAnimation>
#include <QGraphicsItem>
#include <QAbstractGraphicsShapeItem>
#include <QHash>
#include <QBrush>
#include <QPen>

#include <dgraphics/itemanimation.h>
#include <dcore/debug.h>

namespace YAMF {
namespace Item {

class Animator : public DGraphics::ItemAnimation
{
	public:
		Animator(Tweener *parent);
		~Animator();
		
		QHash<int, TweenerStep *> steps() const;
		inline TweenerStep *step(int step);
		void build(int frames);
		
#if QT_VERSION < 0x040300
		void holdPosition(qreal step, const QPointF &pos);
	private:
		QMap<qreal, QPointF > positionForStep;
#endif
		
	protected:
		void afterAnimationStep( qreal step );
		
	private:
		Tweener *m_tweener;
		QHash<int, TweenerStep *> m_steps;
};

Animator::Animator(Tweener *parent) : DGraphics::ItemAnimation(parent), m_tweener(parent)
{
}

Animator::~Animator()
{
	qDeleteAll(m_steps);
}

TweenerStep *Animator::step(int step)
{
	TweenerStep *s = m_steps.value(step);
	if( ! s )
	{
		s = new TweenerStep(step);
		m_steps[step] = s;
	}
	
	return s;
}

QHash<int, TweenerStep *> Animator::steps() const
{
	return m_steps;
}

void Animator::build(int frames)
{
	QHash<int, TweenerStep *> steps = this->steps();
	m_steps.clear();
	
	clear();
	foreach(TweenerStep *step, steps)
	{
		m_tweener->addStep(*step);
	}
	
	qDeleteAll(steps);
}

void Animator::afterAnimationStep( qreal step )
{
#if QT_VERSION < 0x040300
	if(item() && positionForStep.contains(step))
	{
		QPointF m = positionForStep[step];
		QPointF a = posAt(step);
		
		if( m != a )
		{
			item()->setPos(m);
		}
	}
	
#endif
	
	DGraphics::ItemAnimation::afterAnimationStep(step);
}

#if QT_VERSION < 0x040300
void Animator::holdPosition(qreal step, const QPointF &pos)
{
	positionForStep[step] = pos;
}
#endif

#define VERIFY_STEP(s) if( s > d->frames || d->frames == 0) { \
                         dWarning() << "Invalid step " << s << " for tweening, maximun step is " << d->frames << "; In " << __FUNCTION__; \
                         return; }

#define STEP(s) s / (double)d->frames

struct Tweener::Private
{
	Private() : frames(0) {}
	
	int frames;
	
	Animator *animator;
};

Tweener::Tweener(int frames, QObject *parent) : QObject(parent), d(new Private)
{
	d->animator = new Animator(this);
	d->frames = frames;
}


Tweener::~Tweener()
{
	delete d;
}

double Tweener::horizontalScaleAt( int step ) const
{
	return d->animator->horizontalScaleAt( STEP(step) );
}

double Tweener::horizontalShearAt( int step ) const
{
	return d->animator->horizontalShearAt(STEP(step));
}

QGraphicsItem *Tweener::item() const
{
	return d->animator->item();
}

QMatrix Tweener::matrixAt( int step ) const
{
	return d->animator->matrixAt(STEP(step));
}

QPointF Tweener::posAt( int step ) const
{
	return d->animator->posAt(STEP(step));
}

double Tweener::rotationAt( int step ) const
{
	return d->animator->rotationAt(STEP(step));
}

void Tweener::setItem( QGraphicsItem * item )
{
	d->animator->setItem(item);
}

void Tweener::setPosAt( int step, const QPointF & point )
{
	VERIFY_STEP(step);
	
	d->animator->setPosAt(STEP(step), point);
	d->animator->step(step)->setPosition(point);
	
#if QT_VERSION < 0x040300
	d->animator->holdPosition(STEP(step), point);
#endif
}

void Tweener::setRotationAt( int step, double angle )
{
	VERIFY_STEP(step);
	d->animator->setRotationAt(STEP(step), angle);
	
	d->animator->step(step)->setRotation(angle);
}

void Tweener::setScaleAt( int step, double sx, double sy )
{
	VERIFY_STEP(step);
	d->animator->setScaleAt(STEP(step), sx, sy);
	
	d->animator->step(step)->setScale(sx, sy);
}

void Tweener::setShearAt( int step, double sh, double sv )
{
	VERIFY_STEP(step);
	d->animator->setShearAt(STEP(step), sh, sv);
	
	d->animator->step(step)->setScale(sh, sv);
}

void Tweener::setTranslationAt( int step, double dx, double dy )
{
	VERIFY_STEP(step);
	d->animator->setTranslationAt(STEP(step), dx, dy);
	
	d->animator->step(step)->setTranslation(dx, dy);
}

void Tweener::setBrushAt(int step, const QBrush &color)
{
	VERIFY_STEP(step);
	d->animator->setBrushAt(STEP(step), color);
	d->animator->step(step)->setBrush(color);
}

void Tweener::setPenAt(int step, const QPen &color)
{
	VERIFY_STEP(step);
	d->animator->setPenAt(STEP(step), color);
	d->animator->step(step)->setPen(color);
}

void Tweener::setPathAt(int step, const QPainterPath &path)
{
	VERIFY_STEP(step);
	d->animator->setPathAt(STEP(step), path);
	d->animator->step(step)->setPath(path);
}

double Tweener::verticalScaleAt( int step ) const
{
	return d->animator->verticalScaleAt(STEP(step));
}

double Tweener::verticalShearAt( int step ) const
{
	return d->animator->verticalShearAt(STEP(step));
}

double Tweener::xTranslationAt( int step ) const
{
	return d->animator->xTranslationAt(STEP(step));
}

double Tweener::yTranslationAt( int step ) const
{
	return d->animator->yTranslationAt(STEP(step));
}

QBrush Tweener::brushAt(int step) const
{
	return d->animator->brushAt(STEP(step));
}

QPen Tweener::penAt(int step) const
{
	return d->animator->penAt(STEP(step));
}

QPainterPath Tweener::pathAt(int step) const
{
	return d->animator->pathAt(STEP(step));
}

void Tweener::addStep(const TweenerStep &step)
{
	int n = step.n();
	
	VERIFY_STEP(n);
	
	if(step.has(TweenerStep::Position) )
	{
		setPosAt(n, step.position());
	}
	
	if(step.has(TweenerStep::Scale) )
	{
		setScaleAt(n, step.horizontalScale(), step.verticalScale());
	}
	
	if(step.has(TweenerStep::Translation) )
	{
		setTranslationAt(n, step.xTranslation(), step.yTranslation());
	}
	
	if(step.has(TweenerStep::Shear) )
	{
		setScaleAt(n, step.horizontalShear(), step.verticalShear());
	}
	
	if(step.has(TweenerStep::Rotation) )
	{
		setRotationAt(n, step.rotation());
	}
	
	if( step.has(TweenerStep::Brush) )
	{
		setBrushAt(n, step.brush());
	}
	
	if( step.has(TweenerStep::Pen) )
	{
		setPenAt(n, step.pen());
	}
	
	if( step.has(TweenerStep::Path ))
	{
		setPathAt(n, step.path());
	}
	
}


void Tweener::setFrames(int frames)
{
	d->frames = frames;
	
	// Rebuild the animator
	d->animator->build(d->frames);
}

int Tweener::frames() const
{
	return d->frames;
}

void Tweener::setStep( int step )
{
	VERIFY_STEP(step);
	d->animator->setStep(STEP(step));
}

void Tweener::fromXml(const QString &xml)
{
	QDomDocument doc;
	if( doc.setContent(xml) )
	{
		QDomElement root = doc.documentElement();
		
		d->frames = root.attribute("frames").toInt();
		
		QDomNode n = root.firstChild();
		
		while( !n.isNull() )
		{
			QDomElement e = n.toElement();
			
			if(!e.isNull())
			{
				if( e.tagName() == "step" )
				{
					QString stepDoc;
					
					{
						QTextStream ts(&stepDoc);
						ts << n;
					}
					
					TweenerStep *step = new TweenerStep(0);
					step->fromXml(stepDoc);
					
					addStep(*step);
					
					delete step;
				}
			}
			
			n = n.nextSibling();
		}
	}
}

QDomElement Tweener::toXml(QDomDocument &doc) const
{
	QDomElement root = doc.createElement("tweening");
	root.setAttribute("frames", d->frames);
	
	foreach(TweenerStep *step, d->animator->steps() )
	{
		root.appendChild(step->toXml(doc));
	}
	
	return root;
}

}
}

