/***************************************************************************
 *   Copyright (C) 2007 by David Cuadrado                                  *
 *   krawek@gmail.com                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef YAMFITEMTWEENERSTEP_H
#define YAMFITEMTWEENERSTEP_H

#include <QPointF>
#include <QBrush>
#include <QPen>

#include <yamf/common/abstractserializable.h>
#include <yamf/common/yamf_exports.h>

namespace YAMF {
namespace Item {

/**
 * @ingroup item
 * @author David Cuadrado \<krawek@gmail.com\>
*/
class YAMF_EXPORT TweenerStep : public Common::AbstractSerializable
{
	public:
		enum Type {
			None = 0x0,
			Position = 0x01,
			Translation = 0x02,
			Rotation = 0x04,
			Shear = 0x8,
			Scale = 0x10,
			Brush = 0x40,
			Pen = 0x80,
			Path = 0x100
		};
		
		TweenerStep(int n);
		
		~TweenerStep();
		
		void setPosition(const QPointF &pos);
		void setTranslation(double dx, double dy);
		void setRotation(double angle);
		void setShear(double sh, double sv);
		void setScale(double sx, double sy);
		void setBrush(const QBrush &brush);
		void setPen(const QPen &pen);
		void setPath(const QPainterPath &path);
		
		QPointF position() const;
		double horizontalScale() const;
		double verticalScale() const;
		double horizontalShear() const;
		double verticalShear() const;
		double rotation() const;
		double xTranslation() const;
		double yTranslation() const;
		
		QBrush brush() const;
		QPen pen() const;
		QPainterPath path() const;
		
		bool has(Type type) const;
		int n() const;
	
		virtual QDomElement toXml(QDomDocument& doc) const;
		virtual void fromXml(const QString& xml);
		
		static QDomDocument createXml(int frames, const QVector<TweenerStep *> &steps);
		
	private:
		struct Private;
		Private *const d;
};

}
}

#endif
