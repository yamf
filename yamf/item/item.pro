TEMPLATE = lib
DEFINES += _BUILD_YAMF_ 

CONFIG += dll \
warn_on

INCLUDEPATH += ..


SOURCES += builder.cpp \
button.cpp \
ellipse.cpp \
group.cpp \
line.cpp \
path.cpp \
preview.cpp \
pixmap.cpp \
rect.cpp \
text.cpp \
serializer.cpp  \
tweener.cpp \
tweenerstep.cpp \
private/svg2qt.cpp \
converter.cpp \
proxy.cpp \
polygon.cpp 

HEADERS += builder.h \
button.h \
ellipse.h \
group.h \
line.h \
path.h \
preview.h \
pixmap.h \
rect.h \
text.h \
serializer.h  \
tweener.h \
tweenerstep.h \
private/svg2qt.h \
converter.h \
proxy.h \
polygon.h

include(../../config.pri)


contains(QT, "opengl") {
HEADERS += opengl.h
SOURCES += opengl.cpp
}



TARGET = yamf_item

LIBS += -lyamf_common

INSTALLS += target headers
target.path = /lib/

headers.path = /include/yamf/item
headers.files = *.h

