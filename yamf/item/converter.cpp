/***************************************************************************
 *   Copyright (C) 2006 by David Cuadrado                                  *
 *   krawek@gmail.com                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "converter.h"

#include "rect.h"
#include "path.h"
#include "line.h"
#include "ellipse.h"
#include "proxy.h"
#include "group.h"
#include "text.h"

#include <dcore/debug.h>

#include <QAbstractGraphicsShapeItem>
#include <QBrush>
#include <QPen>
#include <QFont>

namespace YAMF {
namespace Item {

/**
 * @~spanish
 * Copia las propiedades del item @p src al item @p dest.
 */
void Converter::copyProperties(QGraphicsItem *src, QGraphicsItem *dest)
{
	dest->setMatrix(src->matrix());
	dest->setPos(src->scenePos());
	dest->setFlags(src->flags() );
	
	dest->setSelected(src->isSelected());
	
	
	// Shapes
	QAbstractGraphicsShapeItem *shape =  dynamic_cast<QAbstractGraphicsShapeItem*>(src);
	QAbstractGraphicsShapeItem *shapeDst = qgraphicsitem_cast<QAbstractGraphicsShapeItem*>(dest);
	
	if ( shape && dest )
	{
		QBrush shapeBrush = shape->brush();
		
		if ( shapeBrush.color().isValid() || shapeBrush.gradient() || 
		!shapeBrush.texture().isNull() )
			shapeDst->setBrush( shape->brush() );
		
		shapeDst->setPen(shape->pen() );
	}
}

/**
 * @~spanish
 * Retorna un la transformacion de @p item a un Item::Path,
 * si no lo puede transoformar retorna 0.
 */
Path *Converter::convertToPath(QGraphicsItem *item)
{
	if(!item) return 0;
	
	Path *path = new Path( item->parentItem(), 0);
	
	QPainterPath ppath;
	
	switch(item->type() )
	{
		case Path::Type:
		{
			ppath = qgraphicsitem_cast<Path *>(item)->path();
		}
		break;
		case Rect::Type:
		{
			ppath.addRect(qgraphicsitem_cast<Rect *>(item)->rect());
		}
		break;
		case Ellipse::Type:
		{
			ppath.addEllipse(qgraphicsitem_cast<Ellipse *>(item)->rect());
		}
		break;
		case Proxy::Type:
		{
// 			QGraphicsItem *data = qgraphicsitem_cast<Proxy*>(item)->item();
// 			qgraphicsitem_cast<Proxy*>(item)->setItem(convertToPath(data));
// 			return item;
			return 0;
		}
		break;
		case Text::Type:
		{
			if(Text *text = qgraphicsitem_cast<Text *>(item))
			{
				path->setBrush(text->defaultTextColor());
				ppath.addText(QPoint(0,0), text->font(),text->toPlainText());
			}
		}
		break;
		case Line::Type:
		{
			QLineF line = qgraphicsitem_cast<Line *>(item)->line();
			ppath.moveTo(line.p1());
			ppath.lineTo(line.p2());
		}
		break;
		case Group::Type:
		{
			dWarning() << "Converter::convertToPath no support groups";
			delete path;
			return 0;
		}
		break;
		default:
		{
			dWarning() << "Converter::convertToPath use default";
			ppath = item->shape(); // TODO
		}
		break;
	}
	
	path->setPath(ppath);
	
	Converter::copyProperties( item, path);
	
	return path;
	
}

/**
 * @~spanish
 * Retorna un la transformacion de @p item a un Item::Ellipse,
 * si no lo puede transoformar retorna 0.
 */
Ellipse *Converter::convertToEllipse(QGraphicsItem *item)
{
	Ellipse *ellipse = new Ellipse(item->parentItem());
	
	switch(item->type() )
	{
		case Path::Type:
		{
			ellipse->setRect(qgraphicsitem_cast<QGraphicsPathItem *>(item)->path().boundingRect());
		}
		break;
		case Ellipse::Type:
		{
			ellipse->setRect(qgraphicsitem_cast<QGraphicsEllipseItem *>(item)->rect());
		}
		break;
		// TODO
	}
	
	Converter::copyProperties( item, ellipse);
	
	return ellipse;
}


/**
 * @~spanish
 * Retorna un la transformacion de @p item a un Item::Rect,
 * si no lo puede transoformar retorna 0.
 */
Rect *Converter::convertToRect(QGraphicsItem *item)
{
	Rect *rect = new Rect(item->parentItem());
	
	switch(item->type() )
	{
		case Path::Type:
		{
			rect->setRect(qgraphicsitem_cast<QGraphicsPathItem *>(item)->path().boundingRect());
		}
		break;
		case Ellipse::Type:
		{
			rect->setRect(qgraphicsitem_cast<QGraphicsEllipseItem *>(item)->rect());
		}
		break;
		// TODO
	}
	
	Converter::copyProperties( item, rect);
	
	return rect;
}

/**
 * @~spanish
 * Retorna un la transformacion de @p item a un Item::Line,
 * si no lo puede transoformar retorna 0.
 */
Line *Converter::convertToLine(QGraphicsItem *item)
{
	Line *line = new Line(item->parentItem());
	switch(item->type() )
	{
		case Path::Type:
		{
			QRectF rect = qgraphicsitem_cast<QGraphicsPathItem *>(item)->path().boundingRect();
			line->setLine(QLineF(rect.topLeft(), rect.bottomRight()));
		}
		break;
		case Ellipse::Type:
		{
			QRectF rect = qgraphicsitem_cast<QGraphicsEllipseItem *>(item)->rect();
			line->setLine(QLineF(rect.topLeft(), rect.bottomRight()));
		}
		break;
		// TODO
	}
	Converter::copyProperties( item, line);
	
	return line;
}


/**
 * @~spanish
 * Retorna un la transformacion de @p item a un item de tipo @p toType,
 * si no lo puede transoformar retorna 0.
 */
QGraphicsItem *Converter::convertTo(QGraphicsItem *item, int toType)
{
	switch(toType)
	{
		case Item::Path::Type: // Path
		{
			return Converter::convertToPath( item );
		}
		break;
		case Item::Rect::Type: // Rect
		{
			Item::Rect *rect = Converter::convertToRect( item );

			return rect;
		}
		break;
		case Item::Ellipse::Type: // Ellipse
		{
			Item::Ellipse *ellipse = Converter::convertToEllipse( item );
			return ellipse;
		}
		break;
		case Item::Proxy::Type:
		{
			return new Item::Proxy( item );
		}
		break;
		case Item::Line::Type:
		{
			return Converter::convertToLine( item );
		}
		break;
		default:
		{
			dWarning() << "unknown item " << toType ;
		}
		break;
	}
	return 0;
}

}
}
