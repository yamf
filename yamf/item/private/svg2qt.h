/***************************************************************************
 *   Copyright (C) 2006 by David Cuadrado                                  *
 *   krawek@gmail.com                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef KTSVG2QT_H
#define KTSVG2QT_H

#include <QString>
#include <QPainterPath>
#include <QPen>
#include <QXmlAttributes>
#include <QBrush>

#include <yamf/common/yamf_exports.h>

namespace YAMF {
namespace Item {
namespace Private {

/**
 * @ingroup item
 * @~spanish
 * Esta clase permite convertir a una cadena de texto objetos de Qt como QPainterPath, QMatrix, QPen y QBrush
 * @author David Cuadrado <krawek@gmail.com>
*/
class YAMF_EXPORT Svg2Qt
{
	private:
		Svg2Qt();
		~Svg2Qt();
		
	public:
		static bool svgpath2qtpath(const QString &data, QPainterPath &path);
		static bool svgmatrix2qtmatrix(const QString &data, QMatrix &matrix);
		
		static bool parsePointF(const QString &pointstr, QPointF &point);
		
		static void parsePen(QPen &pen, const QXmlAttributes &attributes);
		static bool parseBrush(QBrush &brush, const QXmlAttributes &attributes);
		
		static QList<qreal> parseNumberList(QString::const_iterator &itr);
		static QList<int> parseIntList(QString::const_iterator &itr);
		
		static QString pointToString(const QPointF &point);
		static QString matrixToString( const QMatrix &matrix );
		static QString numberListToString( const QList<qreal> &list);
		static QString intListToString(const QList<int> &list);
};


}
}
}

#endif
