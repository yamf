/***************************************************************************
 *   Copyright (C) 2006 by David Cuadrado                                  *
 *   krawek@gmail.com                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "path.h"
#include <QGraphicsSceneDragDropEvent>
#include <QMimeData>
#include <QBrush>

#include <dcore/debug.h>

#include <QPainter>
#include <QPainterPath>

#include <dgraphics/algorithm.h>
#include <dgraphics/pathhelper.h>
#include "serializer.h"

#include <QCursor>

namespace YAMF {
namespace Item {

struct Path::Private {
	Private() : dragOver(false) {}
	bool dragOver;
};

/**
 * @~spanish
 * Construye un grï¿½fico vectorial con un ï¿½tem "padre" @p parent y dentro de la escena @p scene.
 */
Path::Path( QGraphicsItem * parent, QGraphicsScene * scene) : QGraphicsPathItem(parent, scene), d(new Private)
{
	setAcceptDrops(true);
// 	setFlags (QGraphicsItem::ItemIsSelectable | QGraphicsItem::ItemIsMovable );
}


/**
 * Desctructor
 */
Path::~Path()
{
	delete d;
}


/**
 * @~spanish
 * Toma los valores dentro del documento xml @p xml y se las asigna a las propiedades del grï¿½fico vectorial.
 */
void Path::fromXml(const QString &xml)
{
}

/**
 * @~spanish
 * Crea un elemento del documento xml @p doc con las propiedades actuales del grï¿½fico vectorial.
 */
QDomElement Path::toXml(QDomDocument &doc) const
{
	QDomElement root = doc.createElement("path");
	
	QString strPath = DGraphics::PathHelper::toString(path());
	root.setAttribute( "data", strPath);
	
	root.appendChild(Serializer::properties( this, doc));
	
	QBrush brush = this->brush();
	root.appendChild(Serializer::brush(&brush, doc));
	
	QPen pen = this->pen();
	root.appendChild(Serializer::pen(&pen, doc));
	
	
	return root;
}

/**
 * @~spanish
 * Dibuja el ï¿½tem
 */
void Path::paint ( QPainter * painter, const QStyleOptionGraphicsItem * option, QWidget * widget)
{
	QGraphicsPathItem::paint(painter, option,widget );
}




bool Path::contains ( const QPointF & point ) const
{
#if 0
	double thickness = 4;
	QRectF rectS(point-QPointF(thickness/2,thickness/2) , QSizeF(thickness,thickness));
	
	QPolygonF pol = shape().toFillPolygon();
	pol.pop_back();
	pol.pop_front();
	
	foreach(QPointF point, pol)
	{
		if(rectS.contains( point))
		{
			return true;
		}
	}
	
	QPolygonF::iterator it1 = pol.begin();
	QPolygonF::iterator it2 = pol.begin()+1;
	
	while(it2 != pol.end())
	{
		if(KTGraphicalAlgorithm::intersectLine( (*it1), (*it2), rectS ))
		{
			return true;
		}
		++it1;
		++it2;
	}
	
	return false;
#else
	return QGraphicsPathItem::contains (point );
#endif
}

/**
 * @~spanish
 * Funciï¿½n reimplementada de QGraphicsPathItem para proveer posibilidad de colocarle un color de brocha a la grï¿½fico vectorial.
 */
void Path::dragEnterEvent(QGraphicsSceneDragDropEvent *event)
{
	if (event->mimeData()->hasColor() )
	{
		event->setAccepted(true);
		d->dragOver = true;
		update();
	} 
	else
	{
		event->setAccepted(false);
	}
}

/**
 * @~spanish
 * Funciï¿½n reimplementada de QGraphicsPathItem para proveer posibilidad de colocarle un color de brocha a la grï¿½fico vectorial.
 */
void Path::dragLeaveEvent(QGraphicsSceneDragDropEvent *event)
{
	Q_UNUSED(event);
	d->dragOver = false;
	update();
}

/**
 * @~spanish
 * Funciï¿½n reimplementada de QGraphicsPathItem para proveer posibilidad de colocarle un color de brocha a la grï¿½fico vectorial.
 */
void Path::dropEvent(QGraphicsSceneDragDropEvent *event)
{
	d->dragOver = false;
	if (event->mimeData()->hasColor())
	{
		setBrush(QBrush(qVariantValue<QColor>(event->mimeData()->colorData())));
	}
	else if (event->mimeData()->hasImage())
	{
		setBrush(QBrush(qVariantValue<QPixmap>(event->mimeData()->imageData())));
	}
	update();
}


}
}

