/***************************************************************************
 *   Copyright (C) 2007 by David Cuadrado                                  *
 *   krawek@gmail.com                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "proxy.h"
#include <QPainter>
#include <QGraphicsScene>
#include <dcore/debug.h>

namespace YAMF {
namespace Item {

struct Proxy::Private 
{
	Private() : realItem(0) {}
	QGraphicsItem *realItem;
	
	
	void paintChildren(QPainter * painter, const QStyleOptionGraphicsItem * option, QWidget * widget, QGraphicsItem *item);
};

void Proxy::Private::paintChildren(QPainter * painter, const QStyleOptionGraphicsItem * option, QWidget * widget, QGraphicsItem *item)
{
	item->paint(painter, option, widget);
	
	foreach(QGraphicsItem *child, item->children())
	{
		painter->setTransform(child->transform(), 1);
		paintChildren(painter, option, widget, child);
	}
}

/**
 * @~spanish
 * Crea el proxy con un ítem para controlar @p item.
 */
Proxy::Proxy(QGraphicsItem *item) : QGraphicsItem(), d(new Private)
{
	setItem(item);
	setPos(0,0);
}


/**
 * Destructor
 * @return 
 */
Proxy::~Proxy()
{
	delete d;
}

/**
 * @~spanish
 * Pone el item a controlar por el proxy.
 */
void Proxy::setItem(QGraphicsItem *item)
{
	if( d->realItem )
	{
		this->removeSceneEventFilter(d->realItem);
	}
	
	d->realItem = item;
	
	if( d->realItem )
	{
		d->realItem->installSceneEventFilter(this);
		this->setFlags(d->realItem->flags());
	}
}

/**
 * @~spanish
 * retorna el item que tiene el proxy actualmente.
 * @return 
 */
QGraphicsItem *Proxy::item() const
{
	return d->realItem;
}


/**
 * @~spanish
 * Permite el acceso a la función boundingRect() del item.
 */
QRectF Proxy::boundingRect() const
{
	if ( d->realItem )
		return d->realItem->boundingRect();
	
	return QRectF(0,0, 0,0);
}

/**
 * @~spanish
 * Permite el acceso a la función paint() del item.
 */
void Proxy::paint(QPainter * painter, const QStyleOptionGraphicsItem * option, QWidget * widget)
{
	if( d->realItem )
	{
		d->paintChildren(painter, option, widget, d->realItem);
	}
}

/**
 * @~spanish
 * Permite el acceso a la función shape() del item.
 */
QPainterPath Proxy::shape() const
{
	if( d->realItem)
		return d->realItem->shape();
	
	return QGraphicsItem::shape();
}

/**
 * Returns the item type
 * @return 
 */
int Proxy::type() const
{
	return Proxy::Type;
}

/**
 * @~spanish
 * Permite el acceso a la función collidesWithItem() del item.
 */
bool Proxy::collidesWithItem( const QGraphicsItem * other, Qt::ItemSelectionMode mode) const
{
	if( d->realItem)
		return d->realItem->collidesWithItem(other, mode);
	
	return QGraphicsItem::collidesWithItem(other, mode);
}

/**
 * @~spanish
 * Permite el acceso a la función collidesWithPath() del item.
 */
bool Proxy::collidesWithPath( const QPainterPath & path, Qt::ItemSelectionMode mode) const
{
	if( d->realItem)
		return d->realItem->collidesWithPath(path, mode);
	
	return QGraphicsItem::collidesWithPath(path, mode);
}

/**
 * @~spanish
 * Permite el acceso a la función contains() del item.
 */
bool Proxy::contains( const QPointF & point ) const
{
	if( d->realItem)
		return d->realItem->contains(point);
	
	return QGraphicsItem::contains(point);
}

/**
 * @~spanish
 * Permite el acceso a la función isObscuredBy() del item.
 */
bool Proxy::isObscuredBy( const QGraphicsItem * item ) const
{
	if( d->realItem)
		return d->realItem->isObscuredBy(item);
	
	return QGraphicsItem::isObscuredBy(item);
}


/**
 * @~spanish
 * Permite el acceso a la función opaqueArea() del item.
 */
QPainterPath Proxy::opaqueArea () const
{
	if( d->realItem)
		return d->realItem->opaqueArea();
	
	return QGraphicsItem::opaqueArea();
}

}
}

