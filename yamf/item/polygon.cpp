/***************************************************************************
 *   Copyright (C) 2007 by Jorge Cuadrado                                  *
 *   kuadrosxx@gmail.com                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "polygon.h"
#include <QPainter>
#include <dcore/debug.h>
#include "serializer.h"
#include <cmath>

namespace YAMF {

namespace Item {

struct Polygon::Private
{
	Private(int corners) : corners(corners) {};
	
	int corners;
	QRectF rect;
	
	QPainterPath polygon;
	double start;
};

/**
 * Constructor por defecto del poligono, con 3 vertices.
 */
Polygon::Polygon(QGraphicsItem * parent, QGraphicsScene * scene): QAbstractGraphicsShapeItem(parent, scene), d(new Private(3))
{
	d->start = 0.0;
}

/**
 * Construye un poligono con @p corners vertices.
 */
Polygon::Polygon(int corners, QGraphicsItem * parent, QGraphicsScene * scene): QAbstractGraphicsShapeItem(parent, scene), d(new Private(corners))
{
	d->start = 0.0;
}

/**
 * Construye un poligono con @p corners vertices y una area dentro de la escena.
 */
Polygon::Polygon(int corners, double start, const QRectF & rect, QGraphicsItem * parent, QGraphicsScene * scene): QAbstractGraphicsShapeItem(parent, scene), d(new Private(corners))
{
	d->start = start;
	setRect(rect);
}

/**
 * Destructor
 */
Polygon::~Polygon()
{
	delete d;
}


/**
 * Construye el poligono a partir del xml.
 */
void Polygon::fromXml(const QString &xml)
{
	Q_UNUSED(xml);
}

/**
 * @~spanish
 * Crea un elemento del documento xml @p doc con las propiedades actuales del poligono.
 */
QDomElement Polygon::toXml(QDomDocument &doc) const
{
	QDomElement root = doc.createElement("polygon");
	
	root.setAttribute("x", rect().x());
	root.setAttribute("y", rect().y());
	root.setAttribute("width", rect().width());
	root.setAttribute("height", rect().height());
	
	root.setAttribute("corners", d->corners);
	root.setAttribute("start", d->start);
	
	root.appendChild( Serializer::properties( this, doc));
	
	QBrush brush = this->brush();
	root.appendChild(Serializer::brush(&brush, doc));
	
	QPen pen = this->pen();
	root.appendChild(Serializer::pen(&pen, doc));

	return root;	
}

/**
 * @~spanish
 * Retorna el area que ocupa el poligono.
 */
QRectF Polygon::boundingRect() const
{
	return d->rect;
}


/**
 * @~spanish
 * Dibuja el poligono
 */
void Polygon::paint( QPainter * painter, const QStyleOptionGraphicsItem *, QWidget *)
{
	painter->save();
	
	
	painter->setPen(pen());
	painter->setBrush(brush());
	
	painter->drawPath(d->polygon);
	painter->restore();
}


/**
 * @~spanish
 * Cambia el area que ocupa el poligono.
 */
void Polygon::setRect(const QRectF& rect)
{
	d->rect = rect;
	
	QPainterPath ellipse;
	QRectF br = sceneBoundingRect();
	ellipse.addEllipse( br );
	
	d->polygon = QPainterPath();
	
	d->polygon.moveTo(ellipse.pointAtPercent(d->start));
	for(double i = (1.0/d->corners)+d->start; i < 1.0+d->start; i += 1.0/d->corners)
	{
		if(i > 1)
		{
			d->polygon.lineTo(ellipse.pointAtPercent(i-1));
		}
		else
		{
			d->polygon.lineTo(ellipse.pointAtPercent(i));
		}
	}
	
	
	d->polygon.closeSubpath();
	
	update();
}

/**
 * Cambia el punto donde esta el primer vertice del poligono.
 */
void Polygon::setStart(double start)
{
	d->start = start;
	setRect(rect());
}

/**
 * Punto donde esta el primer vertice del poligono.
 */
double Polygon::start() const
{
	return d->start;
}

/**
 * Retorna el area del poligono.
 */
QRectF Polygon::rect() const
{
	return d->rect;
}
	
/**
 * Retorna la figura del poligono.
 */
QPainterPath Polygon::shape () const
{
	return d->polygon;
}

/**
 * Asigna el numero de vertices del poligono.
 */
void Polygon::setCorners(int corners)
{
	d->corners = corners;
	setRect(rect());
}

/**
 * Retorna el numero de vertices del poligono.
 */
int Polygon::corners() const
{
	return d->corners;
}

}

}
