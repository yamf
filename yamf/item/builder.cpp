/***************************************************************************
 *   Copyright (C) 2006 by David Cuadrado                                  *
 *   krawek@gmail.com                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "builder.h"

#include <QGraphicsItem>

#include <dcore/debug.h>

#include "path.h"
#include "pixmap.h"
#include "text.h"
#include "button.h"
#include "rect.h"
#include "ellipse.h"
#include "line.h"
#include "group.h"
#include "polygon.h"

#include <dgraphics/algorithm.h>
#include <dgraphics/pathhelper.h>

#include "serializer.h"

namespace YAMF {
namespace Item {

struct Builder::Private
{
	QGraphicsItem *item;
	QGradient *gradient;
	QString loading;//brush or pen
	
	QStack<Group *> groups;
	QStack<QGraphicsItem *> objects;
	
	bool addToGroup, isLoading;
	
	QString textReaded;
};


/**
 * @~spanish
 * Constructor
 * @return 
 */
Builder::Builder() : DCore::XmlParserBase(), d(new Private)
{
	d->item = 0;
	d->addToGroup = false;
	d->isLoading = false;
}


/**
 * @~spanish
 * Destructor
 * @return 
 */
Builder::~Builder()
{
	delete d;
}

/**
 * @~spanish
 * Retorna verdad si se esta cargando un ï¿½tem compuesto(grupo) o falso de lo contrario.
 * @return 
 */
bool Builder::addingToGroup() const
{
	return d->addToGroup;
}

/**
 * @~spanish
 * Si se esta cargando un ï¿½tem compuesto, esta funciï¿½n agrupara el ultimo item cargado al grupo que se esta cargando.
 */
void Builder::addCurrentToGroup()
{
	d->groups.last()->addToGroup(d->objects.last());
}

/**
 * @~spanish
 * Si se esta cargando un ï¿½tem compuesto, Esta funciï¿½n coloca el ï¿½tem @p item en la pila de items para agrupar en el ï¿½tem que se esta cargando.
 */
void Builder::push(QGraphicsItem *item)
{
	d->objects << item;
}

/**
 * @~spanish
 * Si se esta cargando un ï¿½tem compuesto, Esta funciï¿½n retira de la pila el ï¿½tem que ya fue agrupado al ï¿½tem que se esta cargando.
 */
void Builder::pop()
{
	d->objects.pop();
}

/**
 * @~spanish
 * Crea el ï¿½tem a construir a partir de la raï¿½z del documento.
 */
QGraphicsItem* Builder::createItem(const QString &root)
{
	QGraphicsItem* item = 0;
	if ( root == "path" )
	{
		item = new Path;
	}
	else if ( root == "rect" )
	{
		item = new Rect;
	}
	else if ( root == "ellipse" )
	{
		item = new Ellipse;
	}
	else if ( root == "button" )
	{
		item = new Button;
	}
	else if ( root == "text" )
	{
		item = new Text;
	}
	else if ( root == "line" )
	{
		item = new Line;
	}
	else if ( root == "polygon" )
	{
		item = new Polygon;
	}
	else if ( root == "group" )
	{
		item = new Group;
	}
	
	return item;
}

/**
 * @~spanish
 * Analiza etiquetas de apertura del documento XML, para extraer las propiedades del ï¿½tem.
*/
bool Builder::startTag( const QString& qname, const QXmlAttributes& atts)
{
	D_FUNCINFOX("items") << qname;
	if ( qname == "path" )
	{
		QString data = atts.value("data");
		if( data.isNull() )
		{
			data = atts.value("d");
		}
		
		QPainterPath path = DGraphics::PathHelper::fromString( data );
		
		if ( d->addToGroup )
		{
			QGraphicsItem *item = createItem(qname);
			qgraphicsitem_cast<Path *>(item)->setPath(path);
			
			d->objects.push(item);
		}
		else
		{
			if ( !d->item )
				d->item = createItem( qname );
			
			qgraphicsitem_cast<Path *>(d->item)->setPath(path);
			
			d->objects.push(d->item);
		}
	}
	else if ( qname == "rect" )
	{
		QRectF rect(atts.value("x").toDouble(), atts.value("y").toDouble(), atts.value("width").toDouble(), atts.value("height").toDouble() );
		
		if( d->addToGroup )
		{
			Rect *item = qgraphicsitem_cast<Rect *>(createItem(qname));
			item->setRect(rect);
			
			
			d->objects.push(item);
		}
		else
		{
			if ( !d->item )
				d->item = createItem( qname );
			qgraphicsitem_cast<Rect *>(d->item)->setRect(rect);
			
			d->objects.push(d->item);
		}
	}
	else if ( qname == "ellipse" )
	{
		double rx = atts.value("rx").toDouble();
		double ry = atts.value("ry").toDouble();
		double cx = atts.value("cx").toDouble();
		double cy = atts.value("cy").toDouble();
		
		QRectF rect(QPointF(cx-rx, cy-ry), QSizeF(2 * rx, 2 * ry ));
		
		if( d->addToGroup )
		{
			Ellipse *item = qgraphicsitem_cast<Ellipse *>(createItem(qname));
			item->setRect(rect);
			
			d->objects.push(item);
		}
		else
		{
			if ( !d->item )
				d->item = createItem( qname );
			
			qgraphicsitem_cast<Ellipse *>(d->item)->setRect(rect);
			
			d->objects.push(d->item);
		}
		
	}
	else if ( qname == "button" )
	{
		if ( !d->item )
		{
			d->item = createItem( qname );
			d->objects.push(d->item);
		}
		
		if( d->addToGroup )
		{
// 			d->groups.last()->addToGroup( createItem( qname ) );
		}
	}
	else if ( qname == "text" )
	{
		double width = atts.value("width").toDouble();
		QColor textColor(atts.value("text-color"));
		QString text = atts.value("data");
		
		if( d->addToGroup )
		{
			Text *item = qgraphicsitem_cast<Text *>(createItem( qname ));
			
			item->setTextWidth(width);
			item->setDefaultTextColor(textColor);
			item->setPlainText(text);
			
			d->objects.push(item);
		}
		else
		{
			if ( !d->item )
				d->item = createItem( qname );
			
			Text *item = qgraphicsitem_cast<Text *>(d->item);
			
			item->setTextWidth(width);
			item->setDefaultTextColor(textColor);
			item->setPlainText(text);
			
			d->objects.push(d->item);
		}
		
// 		setReadText(true);
// 		d->textReaded = "";
	}
	else if ( qname == "line" )
	{
		QLineF line(atts.value("x1").toDouble(), atts.value("y1").toDouble(), atts.value("x2").toDouble(), atts.value("y2").toDouble());
		
		if( d->addToGroup )
		{
			Line *item = qgraphicsitem_cast<Line *>(createItem(qname));
			item->setLine(line);
			
			d->objects.push(item);
		}
		else
		{
			if ( !d->item )
				d->item = createItem( qname );
			
			qgraphicsitem_cast<Line *>(d->item)->setLine(line);
			d->objects.push(d->item);
		}
	}
	else if( qname == "polygon")
	{
		QRectF rect(atts.value("x").toDouble(), atts.value("y").toDouble(), atts.value("width").toDouble(), atts.value("height").toDouble() );
		
		int corners = atts.value("corners").toInt();
		double start = atts.value("start").toDouble();
		
		if( d->addToGroup )
		{
			Polygon *item = qgraphicsitem_cast<Polygon *>(createItem(qname));
			
			item->setCorners(corners);
			item->setStart(start);
			item->setRect(rect);
			
			d->objects.push(item);
		}
		else
		{
			if ( !d->item )
				d->item = createItem( qname );
			
			qgraphicsitem_cast<Polygon *>(d->item)->setCorners(corners);
			qgraphicsitem_cast<Polygon *>(d->item)->setStart(start);
			qgraphicsitem_cast<Polygon *>(d->item)->setRect(rect);
			
			d->objects.push(d->item);
		}
	}
	else if ( qname == "group" )
	{
		if( d->addToGroup )
		{
			Group *group = qgraphicsitem_cast<Group *>( createItem(qname) );
			
			d->groups.push(group);
			d->objects.push(group);
		}
		else
		{
			if ( !d->item )
				d->item = createItem( qname );
			d->groups.push(qgraphicsitem_cast<Group *>(d->item));
			d->objects.push(d->item);
		}
		
		d->addToGroup = true;
	}
	
	//////////
	
	if ( qname == "properties" && !d->objects.isEmpty() )
	{
		Serializer::loadProperties( d->objects.last(), atts);
	}
	else if ( qname == "brush" )
	{
		QBrush brush;
		Serializer::loadBrush( brush, atts);
		
		if ( currentTag() == "pen" )
		{
			d->loading = "pen";
			QPen pen = itemPen();
			pen.setBrush(brush);
			setItemPen( pen );
		}
		else
		{
			d->loading = qname;
			setItemBrush( brush );
		}
	}
	else if ( qname == "pen" )
	{
		QPen pen;
		d->loading = qname;
		Serializer::loadPen( pen, atts);
		setItemPen( pen );
	}
	else if ( qname == "font" )
	{
		QFont font;
		
		Serializer::loadFont(font, atts);
		
		if ( Text *text = qgraphicsitem_cast<Text *>(d->objects.last()) )
		{
			text->setFont(font);
			text->setPlainText(text->toPlainText());
		}
	}
	else if(qname == "stop")
	{
		if(d->gradient)
		{
			QColor c(atts.value("color"));
			c.setAlpha(atts.value("alpha").toInt());
			d->gradient->setColorAt ( atts.value("value").toDouble(), c);
		}
	}
	else if(qname == "gradient")
	{
		d->gradient = Serializer::createGradient(atts);
	}
	
	return true;
}

/**
 * @~spanish 
 * Analiza el texto del documento XML, para extraer las propiedades del ï¿½tem.
*/

void Builder::text ( const QString & ch )
{
	d->textReaded += ch;
}

/**
 * @~spanish
 * Analiza etiquetas de cierre del documento XML, para extraer las propiedades del ï¿½tem.
*/
bool Builder::endTag(const QString& qname)
{
	D_FUNCINFOX("items") << qname;
	if ( qname == "path" )
	{
		if(d->addToGroup)
		{
			d->groups.last()->addToGroup(d->objects.last());
		}
		d->objects.pop();
	}
	else if ( qname == "rect" )
	{
		if(d->addToGroup)
		{
			d->groups.last()->addToGroup(d->objects.last());
		}
		d->objects.pop();
	}
	else if ( qname == "ellipse" )
	{
		if(d->addToGroup)
		{
			d->groups.last()->addToGroup(d->objects.last());
		}
		d->objects.pop();
	}
	else if( qname == "line" )
	{
		if(d->addToGroup)
		{
			d->groups.last()->addToGroup(d->objects.last());
		}
		d->objects.pop();
	}
	else if ( qname == "button" )
	{
		if(d->addToGroup)
		{
			d->groups.last()->addToGroup(d->objects.last());
		}
		d->objects.pop();
	}
	else if ( qname == "text" )
	{
		if(d->addToGroup)
		{
			d->groups.last()->addToGroup(d->objects.last());
		}
		d->objects.pop();
	}
	else if ( qname == "group" )
	{
		d->groups.pop();
		d->addToGroup = !d->groups.isEmpty();
		
		if(d->addToGroup)
		{
			d->groups.last()->addToGroup(d->objects.last());
		}
		
		d->objects.pop();
	}
	else if( qname == "gradient")
	{
		if(d->loading == "brush")
		{
			setItemGradient(*d->gradient, true);
		}
		else
		{
			setItemGradient(*d->gradient, false);
		}
	}
	else
	{
		dWarning("items") << "Unhandled: " << qname;
	}
	
	return true;
}



/**
 * @internal
 * @~spanish
 * Pone el estilo de linea al ï¿½tem que se esta construyendo actualmente.
 */
void Builder::setItemPen(const QPen &pen)
{
	if(d->objects.isEmpty() ) return;
	
	if(QGraphicsLineItem *line = qgraphicsitem_cast<QGraphicsLineItem *>(d->objects.last()) )
	{
		line->setPen(pen);
	}
	else if ( QAbstractGraphicsShapeItem *shape = qgraphicsitem_cast<QAbstractGraphicsShapeItem *>(d->objects.last() ) )
	{
		shape->setPen(pen);
	}
}

/**
 * @internal
 * @~spanish
 * Pone la brocha al ï¿½tem que se esta construyendo actualmente.
 */
void Builder::setItemBrush(const QBrush &brush)
{
	if(d->objects.isEmpty() ) return;
	
	if ( QAbstractGraphicsShapeItem *shape = qgraphicsitem_cast<QAbstractGraphicsShapeItem *>(d->objects.last() ) )
	{
		shape->setBrush(brush);
	}

}

/**
 * @internal
 * @~spanish
 * Pone un degradado al relleno o al estilo de linea dependiendo del valor de @p brush, al ï¿½tem que se esta construyendo actualmente.
 */
void  Builder::setItemGradient(const QGradient& gradient, bool brush)
{
	if(d->objects.isEmpty() ) return;
	
	QBrush gBrush(gradient);
	if ( QAbstractGraphicsShapeItem *shape = qgraphicsitem_cast<QAbstractGraphicsShapeItem *>(d->objects.last() ) )
	{
		if(brush)
		{
			gBrush.setMatrix(shape->brush().matrix());
			shape->setBrush( gBrush );
		}
		else
		{
			gBrush.setMatrix(shape->pen().brush().matrix());
			QPen pen = shape->pen();
			pen.setBrush(gBrush);
			shape->setPen(pen);
		}
	}
	else if(QGraphicsLineItem *line = qgraphicsitem_cast<QGraphicsLineItem *>(d->objects.last()) )
	{
		gBrush.setMatrix(line->pen().brush().matrix());
		QPen pen = line->pen();
		pen.setBrush(gBrush);
		line->setPen(pen);
	}
}

/**
 * @internal
 * @~spanish
 * Obtiene el estilo de linea del ï¿½tem que se esta construyendo actualmente.
 */
QPen Builder::itemPen() const
{
	if( ! d->objects.isEmpty() )
	{
		if(QGraphicsLineItem *line = qgraphicsitem_cast<QGraphicsLineItem *>(d->objects.last()) )
		{
			return line->pen();
		}
		else if ( QAbstractGraphicsShapeItem *shape = qgraphicsitem_cast<QAbstractGraphicsShapeItem *>(d->objects.last() ) )
		{
			return shape->pen();
		}
	}
	return QPen(Qt::transparent, 1);
}

/**
 * @internal
 * @~spanish
 * Obtiene el relleno del ï¿½tem que se esta construyendo actualmente.
 */

QBrush Builder::itemBrush() const
{
	if( ! d->objects.isEmpty() )
	{
		if ( QAbstractGraphicsShapeItem *shape = qgraphicsitem_cast<QAbstractGraphicsShapeItem *>(d->objects.last() ) )
		{
			return shape->brush();
		}
	}
	return Qt::transparent;
}

/**
 * @~spanish
 * Retorna verdad si se le asigna exitosamente los datos que contiene el documento @p xml al @p item. o falso de lo contrario.
 */
bool Builder::loadItem(QGraphicsItem *item, const QString &xml)
{
	d->item = item;
	
	d->isLoading = true;
	
	bool ok = parse(xml);
	
	d->isLoading = false;
	return ok;
}

/**
 * @~spanish
 * Pone el ï¿½tem en el cual se van a cargar los datos del documento xml.
 */
void Builder::setItem(QGraphicsItem *item)
{
	d->item = item;
}

/**
 * @~spanish
 * Retorna el ï¿½tem que se esta construyendo actualmente.
 */
QGraphicsItem *Builder::item() const
{
	return d->item;
}


/**
 * @~spanish
 * Retorna un ï¿½tem construido a partir de documento @p xml.
*/
QGraphicsItem *Builder::create(const QString &xml)
{
	if( loadItem(0, xml) )
	{
		return d->item;
	}
	
	return 0;
}


}
}

