/***************************************************************************
 *   Copyright (C) 2006 by Jorge Cuadrado                                  *
 *   kuadrosxx@gmail.com                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "ellipse.h"

#include <QGraphicsSceneDragDropEvent>
#include <QMimeData>
#include <QBrush>
#include <dcore/debug.h>
#include <QPainter>
#include <QPainterPath>

#include <dgraphics/algorithm.h>
#include "serializer.h"

namespace YAMF {
namespace Item {

struct Ellipse::Private {
	Private() : dragOver(false) {}
	bool dragOver;
};

/**
 * @~spanish
 * Construye una elipse con un ï¿½tem "padre" @p parent y dentro de la escena @p scene.
 */
Ellipse::Ellipse(QGraphicsItem * parent, QGraphicsScene * scene): QGraphicsEllipseItem(parent, scene), d(new Private)
{
	setAcceptDrops(true);
}

/**
 * @~spanish
 * Construye una elipse con el espacio rectangular que debe ocupar @p rect, un ï¿½tem "padre" @p parent y dentro de la escena @p scene.
 */
Ellipse::Ellipse ( const QRectF & rect, QGraphicsItem * parent, QGraphicsScene * scene ): QGraphicsEllipseItem(rect, parent, scene), d(new Private)
{
	setAcceptDrops(true);
}


/**
 * Destructor
 * @return 
 */
Ellipse::~Ellipse()
{
	delete d;
}

/**
 * @~spanish
 * Toma los valores dentro del documento xml @p xml y se las asigna a las propiedades al elipse.
 */
void Ellipse::fromXml(const QString &xml)
{
}

/**
 * @~spanish
 * Crea un elemento del documento xml @p doc con las propiedades actuales de la elipse.
 */
QDomElement Ellipse::toXml(QDomDocument &doc) const
{
	QDomElement root = doc.createElement("ellipse");
	
	root.setAttribute("cx", rect().center().x());
	root.setAttribute("cy", rect().center().y());
	root.setAttribute("rx", rect().width()/2);
	root.setAttribute("ry", rect().height()/2);
	
	root.appendChild( Serializer::properties( this, doc));
	
	QBrush brush = this->brush();
	root.appendChild(Serializer::brush(&brush, doc));
	
	QPen pen = this->pen();
	root.appendChild(Serializer::pen(&pen, doc));
	
	return root;
}

bool Ellipse::contains ( const QPointF & point ) const
{
#if 0
	int thickness = pen().width();
	QRectF rectS(point-QPointF(thickness/2,thickness/2) , QSizeF(thickness,thickness));
	
	QPolygonF pol = shape().toFillPolygon ();
	foreach(QPointF point, pol)
	{
		if(rectS.contains( point))
		{
			return true;
		}
	}
	QPolygonF::iterator it1 = pol.begin() ;
	QPolygonF::iterator it2 = pol.begin()+1;
	
	while(it2 != pol.end())
	{
		if(KTGraphicalAlgorithm::intersectLine( (*it1), (*it2), rectS  ))
		{
			return true;
		}
		++it1;
		++it2;
	}
	return false;
#else
	return QGraphicsEllipseItem::contains(point);
#endif
}

/**
 * @~spanish
 * Funcion reimplementada de QGraphicsEllipseItem para proveer posibilidad de colocarle un color de brocha a la elipse.
 */
void Ellipse::dragEnterEvent(QGraphicsSceneDragDropEvent *event)
{
	if (event->mimeData()->hasColor() )
	{
		event->setAccepted(true);
		d->dragOver = true;
		update();
	} 
	else
	{
		event->setAccepted(false);
	}
}


/**
 * @~spanish
 * Funcion reimplementada de QGraphicsEllipseItem para proveer posibilidad de colocarle un color de brocha a la elipse.
 */
void Ellipse::dragLeaveEvent(QGraphicsSceneDragDropEvent *event)
{
	Q_UNUSED(event);
	d->dragOver = false;
	update();
}

/**
 * @~spanish
 * Funcion reimplementada de QGraphicsEllipseItem para proveer posibilidad de colocarle un color de brocha a la elipse.
 */
void Ellipse::dropEvent(QGraphicsSceneDragDropEvent *event)
{
	d->dragOver = false;
	if (event->mimeData()->hasColor())
	{
		setBrush(QBrush(qVariantValue<QColor>(event->mimeData()->colorData())));
	}
	else if (event->mimeData()->hasImage())
	{
		setBrush(QBrush(qVariantValue<QPixmap>(event->mimeData()->imageData())));
	}
	update();
}

}
}

