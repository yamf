/***************************************************************************
 *   Copyright (C) 2006 by David Cuadrado                                  *
 *   krawek@gmail.com                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "text.h"

#include "serializer.h"

#include <QFont>
#include <QFocusEvent>
#include <QTimer>
#include <QPainter>
#include <QGraphicsSceneMouseEvent>

#include <dcore/debug.h>

namespace YAMF {
namespace Item {

struct Text::Private {
	Private() : isEditable(false) {}
	
	GraphicsItemFlags flags;
	bool isEditable;
};

/**
 * @~spanish
 * Construye un texto con un ï¿½tem "padre" @p parent y dentro de la escena @p scene.
 */
Text::Text(QGraphicsItem * parent, QGraphicsScene * scene)
	: QGraphicsTextItem(parent, scene), d(new Private)
{
	d->flags = flags();
	setOpenExternalLinks(true);
	setEditable( false );
}


/**
 * Destructor
 */
Text::~Text()
{
	delete d;
}

/**
 * @~spanish
 * Toma los valores dentro del documento xml @p xml y se las asigna a las propiedades del texto.
 */
void Text::fromXml(const QString &xml)
{
}


/**
 * @~spanish
 * Crea un elemento del documento xml @p doc con las propiedades actuales del texto.
 */
QDomElement Text::toXml(QDomDocument &doc) const
{
	QDomElement root = doc.createElement("text");
	root.setAttribute("width", textWidth() );
	root.setAttribute("text-color", defaultTextColor().name() );
	
	root.setAttribute("data", toPlainText() );
	
	root.appendChild( Serializer::properties( this, doc));
	QFont font = this->font();
	root.appendChild( Serializer::font( font, doc ) );
	
	return root;
}

/**
 * @~spanish 
 * Si @p editable es verdadero permitira editar el texto, de lo contrario no.
 */
void Text::setEditable(bool editable)
{
	d->isEditable = editable;
	
	if ( editable )
	{
		d->flags = flags(); // save flags
		setTextInteractionFlags(Qt::TextEditorInteraction);
		setFlags( QGraphicsItem::ItemIsSelectable | QGraphicsItem::ItemIsFocusable );
		setFocus(Qt::MouseFocusReason);
	}
	else
	{
		setTextInteractionFlags(Qt::NoTextInteraction/*Qt::TextBrowserInteraction*/);
// 	setFlags(  QGraphicsItem::ItemIsSelectable | QGraphicsItem::ItemIsMovable ); // restore flags
	}
	update();
}

void Text::paint( QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget * widget)
{
	if(toPlainText().isEmpty())
	{
		painter->drawRect(boundingRect());
	}
	QGraphicsTextItem::paint(painter, option, widget);
}

/**
 * @~spanish
 * Si el item es editable lo cambia a no editable, y si es no editable lo cambia a editable.
 */
void Text::toggleEditable()
{
	setEditable( !d->isEditable );
}

/**
 * @~spanish
 * Desactiva la edicion del texto cuando pierde el foco.
 */
void Text::focusOutEvent(QFocusEvent * event )
{
	QGraphicsTextItem::focusOutEvent(event);
	if ( textInteractionFlags() & Qt::TextEditorInteraction && d->isEditable )
	{
		QTimer::singleShot( 0, this, SLOT(toggleEditable()));
		emit edited();
	}
}

/**
 * @~spanish
 * Activa la edicion del texto cuando el texto recibe un evento de doble click.
 */
void Text::mouseDoubleClickEvent ( QGraphicsSceneMouseEvent * event )
{
	setEditable( true );
}

void Text::mousePressEvent ( QGraphicsSceneMouseEvent * event )
{
	QGraphicsTextItem::mousePressEvent( event );
// 	event->setAccepted( false );
// 	dfDebug << event->isAccepted();
// 	event->ignore();
}



}
}
