/***************************************************************************
 *   Copyright (C) 2006 by David Cuadrado                                  *
 *   krawek@gmail.com                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef YAMFITEMTEXT_H
#define YAMFITEMTEXT_H

#include <QGraphicsTextItem>
#include <yamf/common/abstractserializable.h>
#include <yamf/common/yamf_exports.h>

namespace YAMF {
namespace Item {

/**
 * @ingroup item
 * @~spanish
 * @brief Esta clase implementa un ï¿½tem para representar un texto dentro de una escena.
 * @author David Cuadrado <krawek@gmail.com>
*/

class YAMF_EXPORT Text : public QGraphicsTextItem, public Common::AbstractSerializable
{
	Q_OBJECT;
	public:
		Text(QGraphicsItem * parent = 0, QGraphicsScene * scene = 0);
		~Text();
		
		virtual void fromXml(const QString &xml);
		virtual QDomElement toXml(QDomDocument &doc) const;
		
		void setEditable(bool editable);
		
		void paint( QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget * widget); //DEBUG
		
	public slots:
		void toggleEditable();
		
	signals:
		void edited();
		
	protected:
		virtual void focusOutEvent(QFocusEvent * event );
		virtual void mouseDoubleClickEvent ( QGraphicsSceneMouseEvent * event );
		virtual void mousePressEvent ( QGraphicsSceneMouseEvent * event );
		
	private:
		struct Private;
		Private *const d;
};

}
}


#endif
