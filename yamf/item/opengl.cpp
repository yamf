/***************************************************************************
 *   Copyright (C) 2007 David Cuadrado                                     *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/

#include "opengl.h"

#include <QGLPixelBuffer>
#include <QPainter>
#include <QGLWidget>

namespace YAMF {

namespace Item {

struct OpenGL::Private
{
	Private() : defaultSize(20, 20)
	{
		widget = new QGLWidget;
		QGLFormat format = QGLFormat::defaultFormat();
		
		format.setSampleBuffers(true); // Antialiasing
		format.setAlphaBufferSize(8);
		
		pbuffer = new QGLPixelBuffer(20, 20, format, widget);
		if (pbuffer->isValid())
		{
			pbuffer->makeCurrent();
		}
	}
	
	void resize(const QSize &size)
	{
		QGLFormat format = pbuffer->format();
		delete pbuffer;
		pbuffer = new QGLPixelBuffer(size, format, widget);
	}
	
	~Private()
	{
		delete pbuffer;
		delete widget;
	}
	
	QGLPixelBuffer *pbuffer;
    QGLWidget *widget;
	QSizeF defaultSize;
};

OpenGL::OpenGL()
 : QGraphicsItem(), d(new Private)
{
}


OpenGL::~OpenGL()
{
	delete d;
}


void OpenGL::paint(QPainter *painter, const QStyleOptionGraphicsItem *option)
{
	if ((!d->widget->isValid() || !d->pbuffer->isValid()))
	{
		return;
	}
	
	d->pbuffer->makeCurrent();
	
	glClearColor(0, 0, 0, 0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	QTransform m = painter->worldTransform();
	QRect deviceRect = m.mapRect(boundingRect()).toRect();
	d->resize(deviceRect.size());
	
	QPainter p(d->pbuffer);
	paintGL(&p, option);
	
	QImage image = d->pbuffer->toImage();
	
	d->defaultSize = image.size();
	
	painter->drawImage(0, 0, image);
}

QRectF OpenGL::boundingRect() const
{
	return QRectF(QPointF(0, 0), d->defaultSize);
}

}

}
