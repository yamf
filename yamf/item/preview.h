/***************************************************************************
 *   Copyright (C) 2006 by David Cuadrado                                  *
 *   krawek@toonka.com                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef YAMF_GUIITEMPREVIEW_H
#define YAMF_GUIITEMPREVIEW_H

#include <QWidget>
#include <yamf/common/yamf_exports.h>

class QGraphicsItem;

namespace YAMF {
namespace Item {


/**
 * @ingroup gui
 * @~english
 * @brief This class render a QGraphicsItem 
 * @author David Cuadrado <krawek@gmail.com>
 * @~spanish
 * @brief Esta clase hace un previsualizado de un QGraphicsItem.
 * @author David Cuadrado <krawek@gmail.com>
*/
class YAMF_EXPORT Preview : public QWidget
{
	Q_OBJECT;
	
	public:
		Preview(QWidget *parent = 0);
		~Preview();
		
		void render(QGraphicsItem *item);
		QSize sizeHint() const;
		
	protected:
		void paintEvent(QPaintEvent *e);
		
	private:
		struct Private;
		Private *const d;
};
}
}

#endif
