/***************************************************************************
 *   Copyright (C) 2006 by David Cuadrado                                  *
 *   krawek@gmail.com                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef YAMFITEMBUILDER_H
#define YAMFITEMBUILDER_H

#include <QStack>
#include <QPen>
#include <QBrush>

#include <dcore/xmlparserbase.h>

#include <yamf/common/yamf_exports.h>

class QGraphicsItem;

namespace YAMF {
namespace Item {

/**
 * @ingroup item
 * @~spanish
 * @brief Clase encargada de construir ítems a partir de un documento xml, el formato de dicho documento esta especificado en la documentación del proyecto @see ModelSerializeSpecification.
 * @author David Cuadrado <krawek@gmail.com>
*/
class YAMF_EXPORT Builder : public DCore::XmlParserBase
{
	public:
		Builder();
		~Builder();
		
	protected:
		virtual bool startTag(const QString& qname, const QXmlAttributes& atts);
		virtual void text( const QString & ch );
		virtual bool endTag(const QString& qname);
		
	public:
		QGraphicsItem *create(const QString &xml);
		bool loadItem(QGraphicsItem *item, const QString &xml);
		
	private:
		void setItemPen(const QPen &pen);
		void setItemBrush(const QBrush &brush);
		void setItemGradient(const QGradient& gradient, bool brush );
		
		QPen itemPen() const;
		QBrush itemBrush() const;
		
		
	protected:
		bool addingToGroup() const;
		
		void addCurrentToGroup();
		void push(QGraphicsItem *item);
		void pop();
		
		void setItem(QGraphicsItem *item);
		QGraphicsItem *item() const;
		
		virtual QGraphicsItem* createItem(const QString &root);
		
	private:
		struct Private;
		Private *const d;
};

}
}

#endif
