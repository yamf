/***************************************************************************
 *   Copyright (C) 2006 by David Cuadrado                                  *
 *   krawek@gmail.com                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "rect.h"
#include <QGraphicsSceneDragDropEvent>

#include <QMimeData>
#include <QBrush>
#include <QGraphicsScene>
#include <dgraphics/algorithm.h>
#include "serializer.h"

namespace YAMF {
namespace Item {


struct Rect::Private {
	Private() : dragOver(false) {}
	bool dragOver;
};

/**
 * @~spanish
 * Construye un rectï¿½ngulo con un ï¿½tem "padre" @p parent y dentro de la escena @p scene.
 */
Rect::Rect(QGraphicsItem * parent, QGraphicsScene * scene ) : QGraphicsRectItem(parent, scene), d(new Private)
{
	setAcceptDrops(true);
}
/**
 * @~spanish
 * Construye un rectï¿½ngulo con el espacio rectangular que debe ocupar @p rect, un ï¿½tem "padre" @p parent y dentro de la escena @p scene.
 */
Rect::Rect( const QRectF& rect, QGraphicsItem * parent , QGraphicsScene * scene )
: QGraphicsRectItem(rect, parent, scene), d(new Private)
{
	
}

/**
 * Destructor
 */
Rect::~Rect()
{
	delete d;
}

/**
 * @~spanish
 * Toma los valores dentro del documento xml @p xml y se las asigna a las propiedades al rectï¿½ngulo.
 */
void Rect::fromXml(const QString &xml)
{
}


/**
 * @~spanish
 * Crea un elemento del documento xml @p doc con las propiedades actuales del rectï¿½ngulo.
 */
QDomElement Rect::toXml(QDomDocument &doc) const
{
	QDomElement root = doc.createElement("rect");
	
	root.setAttribute("x", rect().x());
	root.setAttribute("y", rect().y());
	root.setAttribute("width", rect().width());
	root.setAttribute("height", rect().height());
	
	root.appendChild( Serializer::properties( this, doc));
	
	QBrush brush = this->brush();
	root.appendChild(Serializer::brush(&brush, doc));
	
	QPen pen = this->pen();
	root.appendChild(Serializer::pen(&pen, doc));

	return root;
}


/**
 * @~spanish
 * Funciï¿½n reimplementada de QGraphicsRectItem para proveer posibilidad de colocarle un color de brocha a la rectï¿½ngulo.
 */
void Rect::dragEnterEvent(QGraphicsSceneDragDropEvent *event)
{
	if (event->mimeData()->hasColor() )
	{
		event->setAccepted(true);
		d->dragOver = true;
		update();
	} 
	else
	{
		event->setAccepted(false);
	}
}


/**
 * @~spanish
 * Funciï¿½n reimplementada de QGraphicsRectItem para proveer posibilidad de colocarle un color de brocha a la rectï¿½ngulo.
 */
void Rect::dragLeaveEvent(QGraphicsSceneDragDropEvent *event)
{
	Q_UNUSED(event);
	d->dragOver = false;
	update();
}


/**
 * @~spanish
 * Funciï¿½n reimplementada de QGraphicsRectItem para proveer posibilidad de colocarle un color de brocha a la rectï¿½ngulo.
 */
void Rect::dropEvent(QGraphicsSceneDragDropEvent *event)
{
	d->dragOver = false;
	if (event->mimeData()->hasColor())
	{
		setBrush(QBrush(qVariantValue<QColor>(event->mimeData()->colorData())));
	}
	else if (event->mimeData()->hasImage())
	{
		setBrush(QBrush(qVariantValue<QPixmap>(event->mimeData()->imageData())));
	}
	update();
}

bool Rect::contains( const QPointF & point ) const
{
#if 0
	double thickness = pen().widthF()+2;
	QRectF rectS(point-QPointF(thickness/2,thickness/2) , QSizeF(thickness,thickness));
	
	QPolygonF pol = shape().toFillPolygon ();
	foreach(QPointF point, pol)
	{
		if(rectS.contains( point))
		{
			return true;
		}
	}
	
	QPolygonF::iterator it1 = pol.begin();
	QPolygonF::iterator it2 = pol.begin()+1;
	
	while(it2 != pol.end())
	{
		QRectF rect( (*it1).x(), (*it1).y(), (*it2).x()-(*it1).x(), (*it2).y()-(*it1).y() );
		
// 		if(KTGraphicalAlgorithm::intersectLine( (*it1), (*it2), rectS  ))
		if ( rect.intersects(rectS))
		{
			return true;
		}
		++it1;
		++it2;
	}
	
	return false;
	
#else
	return QGraphicsRectItem::contains(point);
#endif
}

}
}

