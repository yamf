/***************************************************************************
 *   Copyright (C) 2006 by David Cuadrado                                  *
 *   krawek@gmail.com                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "button.h"
#include <QGraphicsScene>
#include <QGraphicsSceneMouseEvent>
#include <QPainter>
#include <QStyleOption>
#include <QStyleOptionButton>
#include <QApplication>

#include "serializer.h"

namespace YAMF {
namespace Item {

/**
 * @~spanish
 * Construye un botón con un ítem "padre" @p parent y dentro de la escena @p scene.
 */
Button::Button(QGraphicsItem * parent, QGraphicsScene * scene)
	: QGraphicsItem(parent, scene)
{
	QGraphicsItem::setCursor(QCursor(Qt::PointingHandCursor ));
	setFlags(QGraphicsItem::ItemIsSelectable | QGraphicsItem::ItemIsMovable  );
	
	m_iconSize = QSize(22,22);
}


/**
 * @~spanish
 * desctructor
 * @return 
 */
Button::~Button()
{
}

/**
 * @~spanish
 * Toma los valores dentro del documento xml @p xml y se las asigna a las propiedades del botón.
 */
void Button::fromXml(const QString &xml)
{
}

/**
 * @~spanish
 * Crea un elemento del documento xml @p doc con las propiedades actuales del boton.
 */
QDomElement Button::toXml(QDomDocument &doc) const
{
	QDomElement root = doc.createElement("button");
	
	root.appendChild( Serializer::properties( this, doc));
	
	return root;
}

 /**
 * @~spanish
 * Dibuja el botón en la escena.
  */
void Button::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *w )
{
	QStyleOptionButton optionButton;
	
	optionButton.text = m_text;
	optionButton.rect = boundingRect().toRect();
	optionButton.icon = m_icon;
	optionButton.iconSize = m_iconSize;
	
	if (option->state & QStyle::State_Sunken)
	{
		optionButton.state = option->state;
	}
	else
	{
		
	}
	QApplication::style()->drawControl( QStyle::CE_PushButton, &optionButton ,painter, w );
}

/**
 * @~spanish
 * Retorna área rectangular que esta ocupando el botón.
 */
QRectF Button::boundingRect() const
{
	double width = 2;
	double height = 2;
	
	QFontMetricsF fm(m_font);
	
	width += fm.width( m_text );
	height += fm.height();
	
	width += m_iconSize.width();
	height += m_iconSize.height();
	
	
	
	return QRectF(0, 0, width, height);
}

/**
 * @~spanish
 * Se ejecuta cuando se presiona click sobre el botón.
 */
void Button::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
	QGraphicsItem::mousePressEvent(event);

	update();
}

/**
 * @~spanish
 * Se ejecuta cuando se libera click sobre el botón.
 */
void Button::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
	QGraphicsItem::mouseReleaseEvent(event);
	update();
}

/**
 * @~spanish
 * Se ejecuta cuando se mueve el puntero sobre el botón.
 */
void Button::mouseMoveEvent ( QGraphicsSceneMouseEvent * event )
{
	QGraphicsItem::mouseMoveEvent(event);
	
	emit clicked();
	
	update();
}

/**
 * @~spanish
 * Asigna un tamaño al icono del botón.
*/
void Button::setIconSize(const QSize &size)
{
	m_iconSize = size;
	update();
}

/**
 * @~spanish
 * Asigna un icono al botón.
*/
void Button::setIcon(const QIcon &icon)
{
	m_icon = icon;
	update();
}

/**
 * @~spanish
 * Asigna un texto al botón.
*/
void Button::setText(const QString &text)
{
	m_text = text;
	update();
}


/**
 * @~spanish
 * Asigna el tipo de letra del texto del botón.
*/
void Button::setFont(const QFont &font)
{
	m_font = font;
	update();
}


}
}

