/***************************************************************************
 *   Copyright (C) 2006 by David Cuadrado                                  *
 *   krawek@gmail.com                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "serializer.h"

#include <QGraphicsItem>
#include <QFont>
#include <QXmlStreamReader>
#include <QXmlStreamWriter>

#include <dcore/debug.h>

#include <private/svg2qt.h>

namespace YAMF {
namespace Item {

Serializer::Serializer()
{
}


Serializer::~Serializer()
{
}

/**
 * @~spanish
 * Convierte las propiedades del @p item a un elemento del documento xml @p doc.
 */
QDomElement Serializer::properties(const QGraphicsItem *item, QDomDocument &doc)
{
	QDomElement properties = doc.createElement("properties");
	
	QString strMatrix = YAMF::Item::Private::Svg2Qt::matrixToString(item->matrix());
	properties.setAttribute( "transform", strMatrix);
		
	properties.setAttribute( "pos", YAMF::Item::Private::Svg2Qt::pointToString(item->pos()) );
	properties.setAttribute("z", item->zValue());
	
	properties.setAttribute( "enabled", item->isEnabled());
// 	properties.setAttribute( "flags", item->flags());
	
	
	return properties;
}

/**
 * @~spanish
 * Grava las propiedades del @p item en un Xml.
 */
void Serializer::properties(const QGraphicsItem *item, QXmlStreamWriter *writer)
{
	writer->writeStartElement("properties");
	
	QString strMatrix = YAMF::Item::Private::Svg2Qt::matrixToString(item->matrix());
	writer->writeAttribute("transform", strMatrix);
	
	writer->writeAttribute( "pos", YAMF::Item::Private::Svg2Qt::pointToString(item->pos()) );
	writer->writeAttribute("z", QString::number(item->zValue()));
	
	writer->writeAttribute( "enabled", item->isEnabled() ? "1" : "0" );
	
// 	writer->writeAttribute( "flags", QString::number(item->flags()) );
	
	writer->writeEndElement();
}


/**
 * @~spanish
 * Asigna las propiedades que estï¿½n el los atributos de un documento xml @p atts al ï¿½tem @p item.
 */
void Serializer::loadProperties(QGraphicsItem *item, const QXmlAttributes &atts)
{
	QMatrix matrix;
	Item::Private::Svg2Qt::svgmatrix2qtmatrix( atts.value("transform"), matrix );
	item->setMatrix(matrix);
	
	QPointF pos;
	Item::Private::Svg2Qt::parsePointF(atts.value("pos"), pos );
	item->setPos( pos );
		
	item->setEnabled(atts.value("enabled") != "0"); // default true
	
// 	item->setFlags( static_cast<QGraphicsItem::GraphicsItemFlags>(atts.value("flags").toInt()) );
	
	item->setZValue( atts.value("z").toDouble() );
}


/**
 * @~spanish
 * Asigna las propiedades que estï¿½n en un elemento de un documento xml @p e al ï¿½tem @p item.
 */
void Serializer::loadProperties(QGraphicsItem *item, const QDomElement &element)
{
	if ( element.tagName() == "properties" )
	{
		QMatrix matrix;
		Item::Private::Svg2Qt::svgmatrix2qtmatrix( element.attribute( "transform"), matrix );
		
		item->setMatrix(matrix);
		
		QPointF pos;
		Item::Private::Svg2Qt::parsePointF(element.attribute("pos"), pos );
		item->setPos( pos );
		
		item->setEnabled(element.attribute("enabled") != "0");
		
// 		item->setFlags( QGraphicsItem::GraphicsItemFlags(element.attribute("flags").toInt()) );
		
		item->setZValue( element.attribute("z").toDouble() );
	}
}

/**
 * @~spanish
 * Asigna las propiedades al item, desde un XML
 * @param item 
 * @param reader 
 */
void Serializer::loadProperties(QGraphicsItem *item, const QString &properties)
{
	QXmlStreamReader reader(properties);
	
	while( !reader.atEnd() )
	{
		if( reader.readNext() == QXmlStreamReader::StartElement )
		{
			if( reader.name() == "properties" )
			{
				loadProperties(item, &reader);
				break;
			}
		}
	}
}

/**
 * @~spanish
 * Asigna las propiedades al item, desde un QXmlStreamReader
 * @param item 
 * @param reader 
 */
void Serializer::loadProperties(QGraphicsItem *item, QXmlStreamReader *reader)
{
	if( reader->name().toString() == "properties" )
	{
		foreach(QXmlStreamAttribute att, reader->attributes() )
		{
			QString name = att.name().toString();
			
			if( name == "transform" )
			{
				QMatrix matrix;
				Item::Private::Svg2Qt::svgmatrix2qtmatrix( att.value().toString(), matrix );
				
				item->setMatrix(matrix);
			}
			else if( name == "pos" )
			{
				QPointF pos;
				Item::Private::Svg2Qt::parsePointF(att.value().toString(), pos );
				
				item->setPos( pos );
			}
			else if( name == "enabled" )
			{
				item->setEnabled(att.value().toString() != "0");
			}
// 			else if( name == "flags" )
// 			{
// 				item->setFlags( QGraphicsItem::GraphicsItemFlags(att.value().toString().toInt()) );
// 			}
			else if( name == "z" )
			{
				item->setZValue( att.value().toString().toDouble() );
			}
		}
	}
}


/**
 * @~spanish
 * Convierte un gradiente @p grandient a un elemento del documento xml @p doc.
 */
QDomElement Serializer::gradient(const QGradient *gradient, QDomDocument &doc)
{
	QDomElement element = doc.createElement("gradient");
	element.setAttribute("type", gradient->type() );
	element.setAttribute("spread", gradient->spread() );
	
	switch(gradient->type() )
	{
		case QGradient::LinearGradient:
		{
			element.setAttribute("start", YAMF::Item::Private::Svg2Qt::pointToString(static_cast<const QLinearGradient *>(gradient)->start()) );
			element.setAttribute("final", YAMF::Item::Private::Svg2Qt::pointToString(static_cast<const QLinearGradient *>(gradient)->finalStop() ));
		}
		break;
		case QGradient::RadialGradient:
		{
			element.setAttribute("center", YAMF::Item::Private::Svg2Qt::pointToString(static_cast<const QRadialGradient *>(gradient)->center()) );

			element.setAttribute("focal", YAMF::Item::Private::Svg2Qt::pointToString(static_cast<const QRadialGradient *>(gradient)->focalPoint()) );

			element.setAttribute("radius", static_cast<const QRadialGradient *>(gradient)->radius() );
		}
		break;
		case QGradient::ConicalGradient:
		{
			element.setAttribute("center", YAMF::Item::Private::Svg2Qt::pointToString(static_cast<const QConicalGradient *>(gradient)->center()) );
			element.setAttribute("angle", static_cast<const QConicalGradient *>(gradient)->angle() );
		}
		break;
		case QGradient::NoGradient:
		{

		}
		break;
	}

	QGradientStops stops = gradient->stops();

	foreach(QGradientStop stop, stops)
	{
		QDomElement stopElement = doc.createElement("stop");
		stopElement.setAttribute("value", stop.first );
		stopElement.setAttribute("color", stop.second.name());
		stopElement.setAttribute("alpha", stop.second.alpha());
		element.appendChild(stopElement);
	}
	
	return element;
}

/**
 * @~spanish
 * Escribe el gradiente utilizando @p writer
 * @param gradient 
 * @param writer 
 */
void Serializer::gradient(const QGradient *gradient, QXmlStreamWriter *writer)
{
	writer->writeStartElement("gradient");
	writer->writeAttribute("type", QString::number(gradient->type()) );
	writer->writeAttribute("spread", QString::number(gradient->spread()) );
	
	switch(gradient->type() )
	{
		case QGradient::LinearGradient:
		{
			writer->writeAttribute("start", YAMF::Item::Private::Svg2Qt::pointToString(static_cast<const QLinearGradient *>(gradient)->start()));

			writer->writeAttribute("final", YAMF::Item::Private::Svg2Qt::pointToString(static_cast<const QLinearGradient *>(gradient)->finalStop() ));
		}
		break;
		case QGradient::RadialGradient:
		{
			writer->writeAttribute("center",  YAMF::Item::Private::Svg2Qt::pointToString(static_cast<const QRadialGradient *>(gradient)->center()) );

			writer->writeAttribute("focal",  YAMF::Item::Private::Svg2Qt::pointToString(static_cast<const QRadialGradient *>(gradient)->focalPoint()) );

			writer->writeAttribute("radius", QString::number(static_cast<const QRadialGradient *>(gradient)->radius()) );
		}
		break;
		case QGradient::ConicalGradient:
		{
			writer->writeAttribute("center",  YAMF::Item::Private::Svg2Qt::pointToString(static_cast<const QConicalGradient *>(gradient)->center()) );

			writer->writeAttribute("angle", QString::number(static_cast<const QConicalGradient *>(gradient)->angle()) );
		}
		break;
		case QGradient::NoGradient:
		{

		}
		break;
	}

	QGradientStops stops = gradient->stops();

	foreach(QGradientStop stop, stops)
	{
		writer->writeStartElement("stop");
		writer->writeAttribute("value", QString::number(stop.first) );
		writer->writeAttribute("color", stop.second.name());
		writer->writeAttribute("alpha", QString::number(stop.second.alpha()));
		writer->writeEndElement();
	}
	
	writer->writeEndElement();
}


/**
 * @~spanish
 * Crea un degradado con los atributos de un elemento del documento xml @p atts.
 * @return 
 */
QGradient *Serializer::createGradient(const QXmlAttributes &atts)
{
	QGradient *result = 0;
	switch(atts.value("type").toInt() )
	{
		case QGradient::LinearGradient:
		{
			QPointF start, final;
			YAMF::Item::Private::Svg2Qt::parsePointF(atts.value("start"), start);
			YAMF::Item::Private::Svg2Qt::parsePointF(atts.value("final"), final);
			
			result = new QLinearGradient( start, final );
		}
		break;
		case QGradient::RadialGradient:
		{
			QPointF center, focal;
			YAMF::Item::Private::Svg2Qt::parsePointF(atts.value("center"), center);
			YAMF::Item::Private::Svg2Qt::parsePointF(atts.value("focal"), focal);
			
			result = new QRadialGradient(center, atts.value("radius").toDouble(), focal);
		}
		break;
		case QGradient::ConicalGradient:
		{
			QPointF center;
			YAMF::Item::Private::Svg2Qt::parsePointF(atts.value("center"), center);
			
			result = new QConicalGradient(center, atts.value("angle").toDouble());
		}
		break;
		case QGradient::NoGradient:
		{
			result = 0;
		}
		break;
	}
	
	if(!result)
	{
		return 0;
	}
	result->setSpread(QGradient::Spread( atts.value("spread").toInt() ) );
	return result;
}

QGradient *Serializer::loadGradient(QXmlStreamReader *reader)
{
	QGradient *result = 0;
	
	Q_ASSERT(QXmlStreamReader::NoToken != reader->tokenType());
	Q_ASSERT(reader->tokenType() != QXmlStreamReader::StartDocument);
	
	if(reader->name().toString() == "gradient")
	{
		switch(reader->attributes().value("type").toString().toInt() )
		{
			case QGradient::LinearGradient:
			{
				QPointF start, final;
				YAMF::Item::Private::Svg2Qt::parsePointF(reader->attributes().value("start").toString(), start);
				YAMF::Item::Private::Svg2Qt::parsePointF(reader->attributes().value("final").toString(), final);
				
				result = new QLinearGradient(start,	final);
			}
			break;
			case QGradient::RadialGradient:
			{
				QPointF center, focal;
				YAMF::Item::Private::Svg2Qt::parsePointF(reader->attributes().value("center").toString(), center);
				YAMF::Item::Private::Svg2Qt::parsePointF(reader->attributes().value("focal").toString(), focal);
				qreal radius = reader->attributes().value("radius").toString().toDouble();
				
				result = new QRadialGradient(center, radius, focal);
			}
			break;
			case QGradient::ConicalGradient:
			{
				QPointF center;
				YAMF::Item::Private::Svg2Qt::parsePointF(reader->attributes().value("center").toString(), center);
				qreal angle = reader->attributes().value("angle").toString().toDouble();
				result = new QConicalGradient(center, angle);
			}
			break;
			default: break;
		}
	}
	
	if(!result)
	{
		return 0;
	}
	
	result->setSpread(QGradient::Spread( reader->attributes().value("spread").toString().toInt() ) );
	
	while(!reader->atEnd())
	{
		if( reader->readNext() == QXmlStreamReader::StartElement )
		{
			if( reader->name() == "stop" )
			{
				double value = 0.0;
				QString colorName;
				int alpha = 0;
				foreach(QXmlStreamAttribute att, reader->attributes())
				{
					if( att.name() == "value" )
					{
						value = att.value().toString().toDouble();
					}
					else if( att.name() == "color" )
					{
						colorName = att.value().toString();
					}
					else if( att.name() == "alpha" )
					{
						alpha = att.value().toString().toInt();
					}
				}
				
				QColor color(colorName);
				color.setAlpha(alpha);
				result->setColorAt(value, color);
			}
		}
		else if(reader->tokenType() == QXmlStreamReader::EndElement )
		{
			if( reader->name() == "gradient" )
			{
				break;
			}
		}
	}
	
	return result;
}

/**
 * @~spanish
 * Crea un gradient con un documento xml @p doc.
 */
QGradient *Serializer::loadGradient(const QString &doc)
{
	QXmlStreamReader reader(doc);
	
	while(!reader.atEnd() )
	{
		if( reader.readNext() == QXmlStreamReader::StartElement )
		{
			if( reader.name() == "gradient" )
			{
				return loadGradient(&reader);
			}
		}
	}
	
	return 0;
}

/**
 * @~spanish
 * Crea una brocha con un documento xml @p doc.
 */
QDomElement Serializer::brush(const QBrush *brush, QDomDocument &doc)
{
	QDomElement brushElement = doc.createElement("brush");
	
	brushElement.setAttribute( "style", brush->style());
	
	
	if ( brush->gradient() )
	{
		brushElement.appendChild( gradient( brush->gradient() , doc));
	}
	else if(brush->color().isValid())
	{
		brushElement.setAttribute( "color", brush->color().name() );
		brushElement.setAttribute( "alpha", brush->color().alpha());
	}
	
	QString strMatrix = YAMF::Item::Private::Svg2Qt::matrixToString(brush->matrix());
	
	brushElement.setAttribute( "transform", strMatrix);
	return brushElement;
}


/**
 * @~spanish
 * Guarda los valores de una brocha usando @p writer
 */
void Serializer::brush(const QBrush *brush, QXmlStreamWriter *writer)
{
	
	writer->writeStartElement("brush");
	writer->writeAttribute( "style", QString::number(brush->style()));
	
	QString strMatrix = YAMF::Item::Private::Svg2Qt::matrixToString(brush->matrix());
	writer->writeAttribute( "transform", strMatrix);
	
	if ( brush->gradient() )
	{
		Serializer::gradient( brush->gradient() , writer);
	}
	else if(brush->color().isValid())
	{
		writer->writeAttribute( "color", brush->color().name() );
		writer->writeAttribute( "alpha", QString::number(brush->color().alpha()) );
	}
	
	writer->writeEndElement();
}

/**
 * Carga la brocha utilizando @p reader
 * @param brush 
 * @param reader 
 */
void Serializer::loadBrush(QBrush &brush, QXmlStreamReader *reader)
{
	if( reader->name().toString() == "brush" )
	{
		Qt::BrushStyle style = static_cast<Qt::BrushStyle>(reader->attributes().value("style").toString().toInt());
		
		QMatrix matrix;
		Item::Private::Svg2Qt::svgmatrix2qtmatrix( reader->attributes().value("transform").toString(), matrix );
		brush.setMatrix(matrix);
		
		QString color = reader->attributes().value("color").toString();
		
		if(!color.isEmpty())
		{
			QColor c(color);
			c.setAlpha(reader->attributes().value("alpha").toString().toInt());
			brush.setColor(c);
			
			brush.setStyle(style);
		}
		else
		{
			if( reader->readNext() == QXmlStreamReader::StartElement )
			{
				if( reader->name() == "gradient" )
				{
					QGradient *gradient = loadGradient(reader);
					
					QBrush nbrush(*gradient);
					
					nbrush.setMatrix(brush.matrix());
					
					brush = nbrush;
					
					delete gradient;
				}
			}
		}
	}
}

/**
 * Carga una brush utilizando el documento xml @p doc
 * @param brush 
 * @param doc 
 */
void Serializer::loadBrush(QBrush &brush, const QString &doc)
{
	QXmlStreamReader reader(doc);
	
	while( !reader.atEnd() )
	{
		if( reader.readNext() == QXmlStreamReader::StartElement )
		{
			if( reader.name() == "brush" )
			{
				Serializer::loadBrush(brush, &reader);
				break;
			}
		}
	}
}


/**
 * @~spanish
 * Asigna los valores de los atributos @p atts a la brocha @p brush.
 */
void Serializer::loadBrush(QBrush &brush, const QXmlAttributes &atts)
{
	brush.setStyle(Qt::BrushStyle(atts.value("style").toInt()) );
	
	
	if(!atts.value("color").isEmpty())
	{
		QColor color(atts.value("color"));
		color.setAlpha(atts.value("alpha").toInt());
		brush.setColor(color);
	}
	
	QMatrix matrix;
	Item::Private::Svg2Qt::svgmatrix2qtmatrix( atts.value("transform"), matrix );
	brush.setMatrix(matrix);
}

/**
 * @~spanish
 * Asigna los valores de un elemento de un documento xml @p e a la brocha @p brush.
 */
void Serializer::loadBrush(QBrush &brush, const QDomElement &e)
{
	Q_ASSERT(e.tagName() == "brush");
	Qt::BrushStyle style = static_cast<Qt::BrushStyle>(e.attribute("style").toInt());
	
	QMatrix matrix;
	Item::Private::Svg2Qt::svgmatrix2qtmatrix( e.attribute("transform"), matrix );
	brush.setMatrix(matrix);
	
	if(!e.attribute("color").isEmpty())
	{
		brush.setColor(QColor(e.attribute("color")));
		brush.setStyle(style);
	}
	else
	{
		QDomElement brushElement = e.firstChild().toElement();
		if( brushElement.tagName() == "gradient" )
		{
			QString str;
			{
				QTextStream ts(&str);
				ts << brushElement;
			}
			
			QGradient *gradient = loadGradient(str);
			if( gradient )
			{
				QBrush newBrush(*gradient);
				delete gradient;
				
				newBrush.setMatrix(brush.matrix());
				brush = newBrush;
			}
		}
	}
}


/**
 * @~spanish
 * Crea una estilo de linea con un documento xml @p doc.
 */
QDomElement Serializer::pen(const QPen *pen, QDomDocument &doc)
{
	QDomElement penElement = doc.createElement("pen");
	
	penElement.setAttribute( "style", pen->style());
	penElement.setAttribute( "color", pen->color().name() );
	penElement.setAttribute( "alpha", pen->color().alpha() );
	penElement.setAttribute( "cap", pen->capStyle() );
	penElement.setAttribute( "join", pen->joinStyle() );
	penElement.setAttribute( "width", pen->width() );
	penElement.setAttribute( "ml", pen->miterLimit() );
	
	QBrush brush = pen->brush();
	penElement.appendChild(Serializer::brush(&brush, doc));
	
	return penElement;
}

/**
 * Serializa @p pen utilizando @p writer
 * @param pen 
 * @param writer 
 */
void Serializer::pen(const QPen *pen, QXmlStreamWriter *writer)
{
	writer->writeStartElement("pen");
	
	writer->writeAttribute( "style", QString::number(pen->style()));
	writer->writeAttribute( "color", pen->color().name() );
	writer->writeAttribute( "alpha", QString::number(pen->color().alpha()) );
	writer->writeAttribute( "cap", QString::number(pen->capStyle()) );
	writer->writeAttribute( "join", QString::number(pen->joinStyle()) );
	writer->writeAttribute( "width", QString::number(pen->width()) );
	writer->writeAttribute( "ml", QString::number(pen->miterLimit()) );
	
	QBrush brush = pen->brush();
	Serializer::brush(&brush, writer);
	
	writer->writeEndElement();
}

/**
 * @~spanish
 * Asigna los valores de los atributos @p atts a la estilo de linea @p pen.
 */
void Serializer::loadPen(QPen &pen, const QXmlAttributes &atts)
{
	pen.setCapStyle( Qt::PenCapStyle(atts.value("cap").toInt() ));
	pen.setStyle( Qt::PenStyle(atts.value("style").toInt() ));
	pen.setJoinStyle( Qt::PenJoinStyle(atts.value("join").toInt() ));
	pen.setWidth( atts.value("width").toInt() );
	pen.setMiterLimit( atts.value("ml").toInt() );
	
	if(!atts.value("color").isEmpty())
	{
		QColor color(atts.value("color") );
		color.setAlpha(atts.value("alpha").toInt() );
	}
}

/**
 * @~spanish
 * Asigna los valores desde un documento XML utilizando @p reader al estilo de linea @p pen.
 */
void Serializer::loadPen(QPen &pen, QXmlStreamReader *reader)
{
	pen.setCapStyle( Qt::PenCapStyle(reader->attributes().value("cap").toString().toInt() ));
	pen.setStyle( Qt::PenStyle(reader->attributes().value("style").toString().toInt() ));
	pen.setJoinStyle( Qt::PenJoinStyle(reader->attributes().value("join").toString().toInt() ));
	pen.setWidth( reader->attributes().value("width").toString().toInt() );
	pen.setMiterLimit( reader->attributes().value("ml").toString().toInt() );
	
	QString c = reader->attributes().value("color").toString();
	if(!c.isEmpty())
	{
		QColor color(c);
		color.setAlpha(reader->attributes().value("alpha").toString().toInt() );
		
		pen.setColor(color);
	}
	
	if( reader->readNext() == QXmlStreamReader::StartElement )
	{
		if( reader->name() == "brush" )
		{
			QBrush brush;
			loadBrush(brush, reader);
			pen.setBrush(brush);
		}
	}
}

void Serializer::loadPen(QPen &pen, const QDomElement &e)
{
	pen.setCapStyle( Qt::PenCapStyle(e.attribute("cap").toInt() ));
	pen.setStyle( Qt::PenStyle(e.attribute("style").toInt() ));
	pen.setJoinStyle( Qt::PenJoinStyle(e.attribute("join").toInt() ));
	pen.setWidth( e.attribute("width").toInt() );
	pen.setMiterLimit( e.attribute("ml").toInt() );
	
	QString c = e.attribute("color");
	if(!c.isEmpty())
	{
		QColor color(c);
		color.setAlpha(e.attribute("alpha").toInt() );
		
		pen.setColor(color);
	}
	
	QDomElement brushElement = e.firstChild().toElement();
	if( brushElement.tagName() == "brush" )
	{
		QBrush brush;
		loadBrush(brush, brushElement);
		
		pen.setBrush(brush);
	}
}

/**
 * @~spanish
 * Carga un pen utilizando el documento XML @p doc
 * @param pen 
 * @param doc 
 */
void Serializer::loadPen(QPen &pen, const QString &doc)
{
	QXmlStreamReader reader(doc);
	
	while(!reader.atEnd())
	{
		if( reader.readNext() == QXmlStreamReader::StartElement )
		{
			if( reader.name() == "pen" )
			{
				loadPen(pen, &reader);
				break;
			}
		}
	}
}

/**
 * @~spanish
 * Convierte un tipo de letra @p font a un elemento del documento xml @p doc.
 */
QDomElement Serializer::font(const QFont &font, QDomDocument &doc)
{
	QDomElement fontElement = doc.createElement("font");
	
	fontElement.setAttribute( "family", font.family());
	fontElement.setAttribute( "point-size", font.pointSize());
	fontElement.setAttribute( "weight", font.weight());
	fontElement.setAttribute( "italic", font.italic());
	fontElement.setAttribute( "bold", font.bold());
	fontElement.setAttribute( "style", font.style());
	
	fontElement.setAttribute( "underline", font.underline());
	fontElement.setAttribute( "overline", font.overline());
	
	return fontElement;
}

/**
 * Serializa la letra @p _font
 * @param _font 
 * @param strFont 
 */
void Serializer::font(const QFont &_font, QString *strFont)
{
	QXmlStreamWriter writer(strFont);
	font(_font, &writer);
}

/**
 * Serializa la letra usando @p writer
 * @param _font 
 * @param writer 
 */
void Serializer::font(const QFont &_font, QXmlStreamWriter *writer)
{
	writer->writeStartElement("font");
	
	writer->writeAttribute( "family", _font.family());
	writer->writeAttribute( "point-size", QString::number(_font.pointSize()));
	writer->writeAttribute( "weight", QString::number(_font.weight()));
	writer->writeAttribute( "italic", QString::number(_font.italic()));
	writer->writeAttribute( "bold", QString::number(_font.bold()));
	writer->writeAttribute( "style", QString::number(_font.style()));
	
	writer->writeAttribute( "underline", QString::number(_font.underline()));
	writer->writeAttribute( "overline", QString::number(_font.overline()));
	
	writer->writeEndElement();
}

/**
 * @~spanish
 * Asigna los valores de un elemento de un documento xml @p e al tipo de letra @p font.
 */
void Serializer::loadFont(QFont &font, const QDomElement &e)
{
	font = QFont(e.attribute( "family" ), e.attribute( "point-size", "-1").toInt(), e.attribute( "weight", "-1").toInt(), e.attribute( "italic", "0").toInt());
	
	font.setBold( e.attribute( "bold", "0").toInt( ) );
	font.setStyle( QFont::Style(e.attribute( "style").toInt()) );
	
	font.setUnderline( e.attribute( "underline", "0").toInt() );
	font.setOverline( e.attribute( "overline", "0").toInt() );
	
}

/**
 * @~spanish
 * Asigna los valores de los atributos @p atts al tipo de letra @p font.
 */
void Serializer::loadFont(QFont &font, const QXmlAttributes &atts)
{
	
	font = QFont(atts.value( "family" ), atts.value( "point-size").toInt(), atts.value( "weight").toInt(), atts.value( "italic").toInt());
	
	font.setBold( atts.value( "bold").toInt( ) );
	font.setStyle( QFont::Style(atts.value( "style").toInt()) );
	
	font.setUnderline( atts.value( "underline").toInt() );
	font.setOverline( atts.value( "overline").toInt() );
}

/**
 * Carga la letra utilizando el documento xml @p properties
 * @param font 
 * @param properties 
 */
void Serializer::loadFont(QFont &font, const QString &properties)
{
	QXmlStreamReader reader(properties);
	
	while( !reader.atEnd() )
	{
		if( reader.readNext() == QXmlStreamReader::StartElement )
		{
			if( reader.name() == "font" )
			{
				loadFont(font, &reader);
				break;
			}
		}
	}
	
	if( reader.hasError() )
		dfDebug << reader.errorString();
}

/**
 * Carga la letra utilizando el reader
 * @param font 
 * @param reader 
 */
void Serializer::loadFont(QFont &font, QXmlStreamReader *reader)
{
	Q_ASSERT(reader->name().toString() == "font");
	
	{
		foreach(QXmlStreamAttribute att, reader->attributes())
		{
			QString name = att.name().toString();
			
			if(name == "family")
			{
				font.setFamily(att.value().toString());
			}
			else if(name == "point-size")
			{
				font.setPointSize(att.value().toString().toInt());
			}
			else if(name == "weight")
			{
				font.setWeight(att.value().toString().toInt());
			}
			else if(name == "italic")
			{
				font.setItalic(att.value().toString().toInt());
			}
			else if(name == "bold")
			{
				font.setBold(att.value().toString().toInt());
			}
			else if(name == "style")
			{
				font.setStyle(QFont::Style(att.value().toString().toInt()));
			}
			else if(name == "undeline")
			{
				font.setUnderline(att.value().toString().toInt());
			}
			else if(name == "overline")
			{
				font.setOverline(att.value().toString().toInt());
			}
		}
	}
}

}
}
