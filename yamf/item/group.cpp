/***************************************************************************
 *   Copyright (C) 2006 by Jorge Cuadrado                                  *
 *   kuadrosxx@gmail.com                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "group.h"
#include <dcore/debug.h>

#include "serializer.h"

namespace YAMF {
namespace Item {

struct Group::Private
{
	QList<QGraphicsItem *> childs;
};

/**
 * @~spanish
 * Construye un grupo con un ítem "padre" @p parent y dentro de la escena @p scene.
 */
Group::Group(QGraphicsItem *parent , QGraphicsScene *scene) : QGraphicsItemGroup(parent, scene), d(new Private)
{
	setFlags(QGraphicsItem::ItemIsMovable | QGraphicsItem::ItemIsSelectable | QGraphicsItem::ItemIsFocusable );
}


/**
 * Destructor
 */
Group::~Group()
{
	delete d;
}

/**
 * 
 */
QVariant Group::itemChange ( GraphicsItemChange change, const QVariant & value )
{
	if ( change == QGraphicsItem::ItemChildRemovedChange )
	{
// 		d->childs.removeAll( qvariant_cast<QGraphicsItem *>(value) );
	}
	else if ( change == QGraphicsItem::ItemChildAddedChange )
	{
		if(!d->childs.contains(qvariant_cast<QGraphicsItem *>(value)))
		{
			d->childs << qvariant_cast<QGraphicsItem *>(value);
		}
	}
	
	return QGraphicsItemGroup::itemChange(change, value);
}


/**
 * 
 */
void Group::recoverChilds()
{
	foreach(QGraphicsItem *item, d->childs )
	{
		if ( Group *child = qgraphicsitem_cast<Group *>(item) )
		{
			child->recoverChilds();
		}
		
		if ( item->parentItem() != this )
		{
			item->setParentItem(this);
		}
	}
}

/**
 * 
 */
QList<QGraphicsItem *> Group::childs()
{
	return d->childs;
}

/**
 * 
 */
void Group::fromXml(const QString &)
{
}


/**
 * 
 */
QDomElement Group::toXml(QDomDocument &doc) const
{
	QDomElement root = doc.createElement("group");
	
	root.appendChild( Serializer::properties( this, doc));
	
	foreach(QGraphicsItem *item, children())
	{
		root.appendChild(dynamic_cast<Common::AbstractSerializable *>(item)->toXml( doc ));
	}
	
	return root;
}

}
}

