#ifndef XMLPARSER_H
#define XMLPARSER_H

#include <QHash>
#include <yamf/common/yamf_exports.h>

namespace YAMF {
namespace Common {


class YAMF_EXPORT XmlAttributes : public QHash<QString, QString>
{
	public:
		XmlAttributes();
		~XmlAttributes();
		
		QString value(const QString &name, const QString &defValue = QString()) const;
		int integer(const QString &name, int defValue = 0) const;
		double real(const QString &name, double defValue = 0) const;
		bool boolean(const QString &name, bool defValue = false) const;
};

/**
 * @author Jorge Cuadrado <kuadrosxx@gmail.com>
*/
class YAMF_EXPORT XmlParser
{
	public:
		XmlParser();
		virtual ~XmlParser();
		
	public:
		bool parse(const QByteArray &data);
		bool parse(const QString &data);
		bool parse(QIODevice *file);
		
	protected:
		virtual bool startTag(const QString &tag, const XmlAttributes &atts) = 0;
		virtual bool endTag(const QString &tag) = 0;
		virtual bool characters(const QString &text);
		virtual bool cdata(const QString &text);
		virtual void startDocument();
		virtual void endDocument();
		
		virtual bool error(const QString &errorString);
		
	private:
		struct Private;
		Private *const d;
};

}
}

#endif


