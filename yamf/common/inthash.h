/***************************************************************************
 *   Copyright (C) 2007 by David Cuadrado                                  *
 *   krawek@gmail.com                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef KTINTHASH_H
#define KTINTHASH_H

#include <QHash>

#include <yamf/common/yamf_exports.h>

#include <yamf/common/inthashimp.h>

namespace YAMF {
namespace Common {

/**
 * @ingroup common
 * @author David Cuadrado <krawek@gmail.com>
*/
template<typename T>
class YAMF_EXPORT IntHash
{
	public:
		IntHash();
		IntHash(const IntHash<T> &other);
		~IntHash();
		
		void removeVisual(int pos);
		void removeAll(int pos);
		void moveVisual(int from, int to);
		bool containsVisual(int pos) const;
		
		int visualIndex(T val) const;
		int logicalIndex(T val) const;
		
		T visualValue(int pos) const;
		T takeVisual(int pos) const;
		
		int clones(T value) const;
		
		void clear(bool alsoDelete = false);
		
		int count() const;
		int visualCount() const;
		
		bool contains(int pos) const;
		
		void insert(int visualIndex, T value);
		void add(T value);
		
		void remove(T value);
		
		T value(int index) const;
		T operator[](int index) const;
		QList<T> visualValues() const;
		QList<T> values() const;
		
		QList<int> visualIndices() const;
		QList<int> logicalIndices() const;
		
		bool isEmpty() const;
		
	public:
		IntHash &operator=(const IntHash &other);
		
	private:
		struct Private;
		Private *const d;
};

#include "inthash.cpp"

}
}

#endif

