
#include "xmlparser.h"

#include <QXmlStreamAttributes>
#include <QXmlStreamReader>
#include <QtDebug>

namespace YAMF {
namespace Common {

XmlAttributes::XmlAttributes()
{
}

XmlAttributes::~XmlAttributes()
{
}

QString XmlAttributes::value(const QString &name, const QString &defValue) const
{
	if( QHash<QString, QString>::contains(name) )
		return QHash<QString, QString>::value(name);
	
	return defValue;
}

int XmlAttributes::integer(const QString &name, int defValue) const
{
	if( contains(name) )
	{
		bool ok = false;
		int ret = value(name).toInt(&ok);
		if( ok )
			return ret;
	}
	
	return defValue;
}

double XmlAttributes::real(const QString &name, double defValue ) const
{
	if( contains(name) )
	{
		bool ok = false;
		double ret = value(name).toDouble(&ok);
		if( ok )
			return ret;
	}
	
	return defValue;
}

bool XmlAttributes::boolean(const QString &name, bool defValue ) const
{
	if( contains(name) )
	{
		return value(name) == "1" || value(name) == "true";
	}
	
	return defValue;
}

struct XmlParser::Private
{
	Private(XmlParser *q) : q(q), reader(new QXmlStreamReader) {};
	
	bool parse();
	
	XmlParser *q;
	QXmlStreamReader *reader;
};

bool XmlParser::Private::parse()
{
	while (!reader->atEnd())
	{
		switch(reader->readNext())
		{
			case QXmlStreamReader::StartElement:
			{
				XmlAttributes attributes;
				foreach(QXmlStreamAttribute att, reader->attributes())
				{
					attributes[att.name().toString()] = att.value().toString();
				}
				
				if(!q->startTag( reader->qualifiedName().toString(), attributes))
				{
					break;
				}
			}
			break;
			case QXmlStreamReader::EndElement:
			{
				if(!q->endTag(reader->qualifiedName().toString()))
				{
					break;
				}
			}
			break;
			case QXmlStreamReader::Characters:
			{
				if(reader->isCDATA())
				{
					if(!q->cdata(reader->text().toString()))
					{
						break;
					}
				}
				else
				{
					if(!q->characters(reader->text().toString()))
					{
						break;
					}
				}
			}
			break;
			case QXmlStreamReader::StartDocument:
			{
				q->startDocument();
			}
			break;
			case QXmlStreamReader::EndDocument:
			{
				q->endDocument();
			}
			break;
			default:
			{
// 				qDebug() << reader->tokenType() << " " << reader->text().toString();
			}
			break;
		}
		
		if(reader->hasError())
		{
			if( ! q->error(reader->errorString()) )
			{
				return false;
			}
		}
	}

	return true;
}


XmlParser::XmlParser() : d( new Private(this) )
{
}


XmlParser::~XmlParser()
{
	delete d->reader;
	delete d;
}


bool XmlParser::parse(const QByteArray &data)
{
	d->reader->clear();
	d->reader->addData(data);
	return d->parse();
}

bool XmlParser::parse(const QString &data)
{
	d->reader->clear();
	d->reader->addData(data);
	return d->parse();
}

bool XmlParser::parse(QIODevice *device)
{
	d->reader->clear();
	d->reader->setDevice(device);
	return d->parse();
}

bool XmlParser::characters(const QString &)
{
	return true;
}

bool XmlParser::cdata(const QString &)
{
	return true;
}

void XmlParser::startDocument()
{
}

void XmlParser::endDocument()
{
}

bool XmlParser::error(const QString &errorString)
{
	qDebug() << "Error: " << errorString;
	
	return false;
}

}
}
