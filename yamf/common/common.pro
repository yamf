TEMPLATE = lib

DEFINES += _BUILD_YAMF_ 

CONFIG += dll \
warn_on
HEADERS += yamf_exports.h \
abstractserializable.h \
inthash.h \
player.h \
yamf.h \
inthashimp.h \
xmlparser.h \
configbarbuilder.h
include($$PWD/../../config.pri)

TARGET = yamf_common

SOURCES += player.cpp \
yamf.cpp \
abstractserializable.cpp  \
inthashimp.cpp \
xmlparser.cpp \
configbarbuilder.cpp
INCLUDEPATH += ..

INSTALLS += target headers
target.path = /lib/

headers.path = /include/yamf/common
headers.files = *.h inthash.cpp

