/***************************************************************************
 *   Copyright (C) 2006 by David Cuadrado                                  *
 *   krawek@gmail.com                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef ABSTRACTSERIALIZABLE_H
#define ABSTRACTSERIALIZABLE_H

#include <QString>

#include <QDomDocument>
#include <QDomElement>
#include <yamf/common/yamf_exports.h>

namespace YAMF {
namespace Common {

/**
 * @ingroup common
 * @~spanish
 * @brief Clase abstracta para clases que necesiten ser guardadas, como los frames, scenas, items, etc.
 * @author David Cuadrado <krawek@gmail.com>
*/
class YAMF_EXPORT AbstractSerializable
{
	protected:
		AbstractSerializable();
		
	public:
		virtual ~AbstractSerializable();
		
		/**
		 * @~spanish
		 * Construye el objeto serealizable desde su representación xml.
		 */
		virtual void fromXml(const QString &xml) = 0;
		/**
		 * @~spanish
		 * Convierte el objeto en su representación xml.
		 */
		virtual QDomElement toXml(QDomDocument &doc) const = 0;
		
// 		void setBasePath(const QString &path);
// 		QString basePath() const;
		
// 	private:
// 		struct Private;
// 		Private *const d;
};

}
}

#endif
