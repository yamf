/***************************************************************************
 *   Copyright (C) 2007 David Cuadrado                                     *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/

#include "player.h"

#include <dcore/globaldeleter.h>

static DCore::GlobalDeleter<YAMF::Common::Player> deleter;

namespace YAMF {
namespace Common {

struct Player::Private
{
	Private() : self(0)
	{
	}
	
	~Private()
	{
	}
	
	Player *self;
};

Player::Private *const Player::d = new Player::Private;

Player::Player()
{
}


Player::~Player()
{
	delete d;
}

Player *Player::self()
{
	if( ! d->self )
	{
		d->self = new Player;
		d->self->loadEngine("gstreamer");
		
		deleter.setObject(d->self);
	}
	
	return d->self;
}

}
}

