/***************************************************************************
 *   Copyright (C) 2007 David Cuadrado                                     *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/

#ifndef INTHASH_CPP
#define INTHASH_CPP

template<typename T>
struct IntHash<T>::Private
{
	Private() : counter(0) {}
	
// 	QList<int> visualIndices;
	QList<YAMFPrivate::VisualIndex> visualIndices;
	QHash<int, T> logicalIndices;
	
	int counter;
	
// 	YAMFPrivate::VisualIndex vindex;
};

template<typename T>
IntHash<T>::IntHash() : d(new Private)
{
}

template<typename T>
IntHash<T>::IntHash(const IntHash<T> &other) : d(new Private)
{
	d->visualIndices = other.d->visualIndices;
	d->logicalIndices = other.d->logicalIndices;
	d->counter = other.d->counter;
}

template<typename T>
IntHash<T>::~IntHash()
{
	delete d;
}

template<typename T>
void IntHash<T>::removeVisual(int pos)
{
 	if( containsVisual(pos) )
	{
		int counter = 0;
		QList<YAMFPrivate::VisualIndex>::iterator it = d->visualIndices.begin();
		
		while(it != d->visualIndices.end())
		{
			if(pos >= counter)
			{
				int logical = (*it).logicalAt(pos - counter);
				if(logical > -1)
				{
					(*it).remove( logical );
					
					if((*it).count() < 1)
					{
						d->visualIndices.erase(it);
						d->logicalIndices.remove(logical);
						return;
					}
				}
			}
			counter += (*it).count();
			++it;
		}
	}
}

template<typename T>
bool IntHash<T>::contains(int pos) const
{
	return d->logicalIndices.contains(pos);
}

template<typename T>
void IntHash<T>::removeAll(int pos)
{
	d->logicalIndices.remove(pos);
	QList<YAMFPrivate::VisualIndex>::iterator it = d->visualIndices.begin();
	
	while(it != d->visualIndices.end())
	{
		if((*it).contains(pos))
		{
			d->visualIndices.erase(it);
		}
		++it;
	}
}

template<typename T>
void IntHash<T>::moveVisual(int from, int to)
{
	if( containsVisual(from) && containsVisual(to) )
	{
		int fromPos = -1;
		int toPos = -1;
		int counter = 0;
		int visualPos = 0;
		QList<YAMFPrivate::VisualIndex>::iterator it = d->visualIndices.begin();
		while(it != d->visualIndices.end())
		{
			if(from >= counter)
			{
				int logical = (*it).logicalAt(from - counter);
				if(logical > -1)
				{
					fromPos = visualPos;
				}
			}
			if(to >= counter)
			{
				int logical = (*it).logicalAt(to - counter);
				if(logical > -1)
				{
					toPos = visualPos;
				}
			}
			if(toPos > -1 && fromPos > -1)
				break;
			
			counter += (*it).count();
			visualPos++;
			++it;
		}
		
		
		if(fromPos != toPos)
		{
			YAMFPrivate::VisualIndex val = d->visualIndices.takeAt(fromPos);
			d->visualIndices.insert(toPos, val);
		}
	}
}

template<typename T>
bool IntHash<T>::containsVisual(int pos) const
{
	return pos >= 0 && pos < visualCount();
}

template<typename T>
IntHash<T> &IntHash<T>::operator=(const IntHash<T> &other)
{
	d->visualIndices = other.d->visualIndices;
	d->logicalIndices = other.d->logicalIndices;
	d->counter = other.d->counter;
	
	return *this;
}

template<typename T>
int IntHash<T>::logicalIndex(T val) const
{
	int key = d->logicalIndices.key(val);
	
	if(d->logicalIndices.contains(key))
	{
		if(d->logicalIndices.value(key) == val)
		{
			return key;
		}
	}
	return -1;
}

template<typename T>
int IntHash<T>::visualIndex(T val) const
{
	int counter = 0;
	foreach(YAMFPrivate::VisualIndex vindex, d->visualIndices)
	{
		int visual = vindex.firstPosition(logicalIndex(val));
		if(visual > -1)
		{
			return counter+visual;
		}
		counter += vindex.count();
	}
	
	return -1;
}

template<typename T>
T IntHash<T>::visualValue(int pos) const
{
	if( containsVisual(pos) )
	{
		int counter = 0;
		foreach(YAMFPrivate::VisualIndex vindex,  d->visualIndices)
		{
			int logical = vindex.logicalAt( pos - counter);
			if(logical > -1)
			{
				return d->logicalIndices.value(logical);
			}
			counter += vindex.count();
		}
		
	}
	
	return d->logicalIndices.value(-1);
}

template<typename T>
T IntHash<T>::takeVisual(int pos) const
{
	if( containsVisual(pos) )
	{
		int logicalIndex = d->visualIndices.takeAt(pos);
		return d->logicalIndices.take(logicalIndex);
	}
	
	return d->logicalIndices.take(-1);
}

template<typename T>
int IntHash<T>::clones(T value) const
{
	int index = logicalIndex(value);
	if( index > -1 )
	{
		foreach(YAMFPrivate::VisualIndex vindex,  d->visualIndices)
		{
			if( vindex.contains(index) )
			{
				return vindex.clones(index);
			}
		}
	}
	
	return -1;
}

template<typename T>
void IntHash<T>::clear(bool alsoDelete)
{
	if( alsoDelete )
		qDeleteAll(d->logicalIndices);
	d->logicalIndices.clear();
	d->visualIndices.clear();
	
	d->counter = 0;
}

template<typename T>
int IntHash<T>::count() const
{
	return d->logicalIndices.count();
}

template<typename T>
int IntHash<T>::visualCount() const
{
	int count = 0;
	foreach(YAMFPrivate::VisualIndex vindex, d->visualIndices)
	{
		count += vindex.count();
	}
	return count;
}

template<typename T>
void IntHash<T>::insert(int visualIndex, T value)
{
	if(visualIndex == visualCount())
	{
		add(value);
		return;
	}
	
	int logical = logicalIndex(value);
	if( !d->logicalIndices.contains(logical) )
	{
		d->logicalIndices[d->counter] = value;
		
		YAMFPrivate::VisualIndex newVindex;
		newVindex.add(d->counter, 0);
		
		int counter = 0;
		int pos = 0;
		
		QList<YAMFPrivate::VisualIndex>::iterator it = d->visualIndices.begin();
		
		while(it != d->visualIndices.end())
		{
			counter += (*it).count();
			if(counter > visualIndex)
			{
				d->visualIndices.insert(pos, newVindex);
				break;
			}
			else if(counter == visualIndex)
			{
				d->visualIndices.insert(pos+1, newVindex);
				break;
			}
			pos++;
			++it;
		}
		d->counter++;
	}
	else
	{
		int counter = 0;
		
		QList<YAMFPrivate::VisualIndex>::iterator it = d->visualIndices.begin();
		
		while(it != d->visualIndices.end())
		{
			int logic = (*it).logicalAt(visualIndex - counter);
			
			if(logic > -1)
			{
				if(logic == logical)
				{
					(*it).insert(logic, 1);
				}
				else
				{
					(*it).add(logical, 0 );
				}
				break;
			}
			counter += (*it).count();
			++it;
		}
	}
}

template<typename T>
void IntHash<T>::add(T value)
{
	d->logicalIndices[d->counter] = value;
	
	YAMFPrivate::VisualIndex vindex;
	vindex.add(d->counter, 0);
	d->visualIndices << vindex;
	
// 	d->visualIndices << d->counter;
	
	d->counter++;
}

template<typename T>
void IntHash<T>::remove(T value)
{
	removeVisual(visualIndex(value));
}

template<typename T>
T IntHash<T>::value(int index) const
{
	return d->logicalIndices.value(index);
}

template<typename T>
T IntHash<T>::operator[](int index) const
{
	return d->logicalIndices.value(index);
}

template<typename T>
QList<T> IntHash<T>::visualValues() const
{
	QList<T> visualValues;
	foreach(YAMFPrivate::VisualIndex vindex, d->visualIndices)
	{
		foreach(int pos, vindex.logicalIndices())
		{
			visualValues << d->logicalIndices.value(pos);
		}
	}
	return visualValues;
}

template<typename T>
QList<T> IntHash<T>::values() const
{
	return d->logicalIndices.values();
}


template<typename T>
QList<int> IntHash<T>::visualIndices() const
{
	QList<int> visualIndices;
	int counter = 0;
	foreach(YAMFPrivate::VisualIndex vindex, d->visualIndices)
	{
		int count = vindex.count();
		
		while(count != 0)
		{
			visualIndices << counter;
			counter++;
			--count;
		}
	}
	return visualIndices;
}

template<typename T>
QList<int> IntHash<T>::logicalIndices() const
{
	return d->logicalIndices.keys();
}

template<typename T>
bool IntHash<T>::isEmpty() const
{
	return d->logicalIndices.isEmpty();
}


#endif


