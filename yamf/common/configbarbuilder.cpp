/***************************************************************************
 *   Copyright (C) 2007 David Cuadrado                                     *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/

#include "configbarbuilder.h"

#include <QXmlStreamReader>
#include <QHash>
#include <QDoubleSpinBox>
#include <QComboBox>
#include <QCheckBox>
#include <QToolBar>
#include <QLineEdit>
#include <QLabel>
#include <QHBoxLayout>

#include <QSignalMapper>

#include <dcore/debug.h>
#include <dgui/colorbutton.h>

namespace YAMF {
namespace Common {

struct ConfigBarBuilder::Private
{
	~Private()
	{
	}
	
	class Key : public QObject {
		public:
			Key() : widget(0) {}
			~Key() { delete widget->parentWidget(); }
			
			QWidget *widget;
			QString property;
			QString type;
	};
	
	QHash<QToolBar *, QList<Key *> > toolBars;
	QSignalMapper mapper;
};

ConfigBarBuilder::ConfigBarBuilder(QObject *parent) : QObject(parent), d(new Private)
{
	connect(&d->mapper, SIGNAL(mapped( QObject* )), this, SLOT(changed(QObject *)));
}


ConfigBarBuilder::~ConfigBarBuilder()
{
	delete d;
}

void ConfigBarBuilder::build(const QString &propertiesDoc, QToolBar *toolBar)
{
	if( !toolBar )
	{
		toolBar = new QToolBar;
	}
	else
	{
		toolBar->clear();
	}
	
	if( !d->toolBars.contains(toolBar) )
	{
		d->toolBars[toolBar] = QList<Private::Key *>();
	}
	else
	{
		foreach(Private::Key *key, d->toolBars[toolBar])
		{
			d->mapper.removeMappings(key->widget);
			delete key;
		}
		d->toolBars[toolBar].clear();
	}
	
	QXmlStreamReader reader(propertiesDoc);
	
	while(!reader.atEnd())
	{
		switch(reader.readNext())
		{
			case QXmlStreamReader::StartElement:
			{
				if( reader.name() == "property" )
				{
					QString type = reader.attributes().value("type").toString();
					QString value = reader.attributes().value("value").toString();
					QString property = reader.attributes().value("name").toString();
					
					QString text= reader.attributes().value("text").toString();
					if( text.isEmpty() )
					{
						text = property;
					}
					
					if( type == "integer" )
					{
						QSpinBox *spinBox = new QSpinBox;
						
						spinBox->setValue(value.toInt());
						
						{
							bool ok = false;
							int v = reader.attributes().value("min").toString().toInt(&ok);
							
							if( ok )
							{
								spinBox->setMinimum(v);
							}
						}
						{
							bool ok = false;
							int v = reader.attributes().value("max").toString().toInt(&ok);
							
							if( ok )
							{
								spinBox->setMaximum(v);
							}
						}
						{
							bool ok = false;
							int v = reader.attributes().value("step").toString().toInt(&ok);
							
							if( ok )
							{
								spinBox->setSingleStep(v);
							}
						}
						
						
						QWidget *w = new QWidget;
						QHBoxLayout *layout = new QHBoxLayout(w);
						
						layout->setSpacing(2);
						layout->setMargin(0);
						
						layout->addWidget(new QLabel(text));
						layout->addWidget(spinBox);
						
						toolBar->addWidget(w);
						
						Private::Key *key = new Private::Key;
						key->widget = spinBox;
						key->property = property;
						key->type = type;
						
						d->toolBars[toolBar] << key;
						
						connect(spinBox, SIGNAL(valueChanged(int)), &d->mapper, SLOT(map()));
						d->mapper.setMapping(spinBox, key);
					}
					else if( type == "real" )
					{
						QDoubleSpinBox *spinBox = new QDoubleSpinBox;
						spinBox->setValue(value.toDouble());
						
						{
							bool ok = false;
							double v = reader.attributes().value("min").toString().toDouble(&ok);
							
							if( ok )
							{
								spinBox->setMinimum(v);
							}
						}
						{
							bool ok = false;
							double  v = reader.attributes().value("max").toString().toDouble(&ok);
							
							if( ok )
							{
								spinBox->setMaximum(v);
							}
						}
						{
							bool ok = false;
							double v = reader.attributes().value("step").toString().toDouble(&ok);
							
							if( ok )
							{
								spinBox->setSingleStep(v);
							}
						}
						
						QWidget *w = new QWidget;
						QHBoxLayout *layout = new QHBoxLayout(w);
						
						layout->setSpacing(0);
						layout->setMargin(0);
						
						layout->addWidget(new QLabel(text));
						layout->addWidget(spinBox);
						
						toolBar->addWidget(w);
						
						Private::Key *key = new Private::Key;
						key->widget = spinBox;
						key->property = property;
						key->type = type;
						
						d->toolBars[toolBar] << key;
						
						connect(spinBox, SIGNAL(valueChanged(double)), &d->mapper, SLOT(map()));
						d->mapper.setMapping(spinBox, key);
					}
					else if( type == "string" )
					{
						QLineEdit *line = new QLineEdit;
						line->setText(value);
						
						QWidget *w = new QWidget;
						QHBoxLayout *layout = new QHBoxLayout(w);
						
						layout->setSpacing(0);
						layout->setMargin(0);
						
						layout->addWidget(new QLabel(text));
						layout->addWidget(line);
						
						toolBar->addWidget(w);
						
						Private::Key *key = new Private::Key;
						key->widget = line;
						key->property = property;
						key->type = type;
						
						d->toolBars[toolBar] << key;
						
						connect(line, SIGNAL(editingFinished()), &d->mapper, SLOT(map()));
						d->mapper.setMapping(line, key);
					}
					else if( type == "boolean" )
					{
						QCheckBox *checkBox = new QCheckBox;
						
						if( value == "true" || value == "yes" )
						{
							checkBox->setChecked(true);
						}
						
						QWidget *w = new QWidget;
						QHBoxLayout *layout = new QHBoxLayout(w);
						
						layout->setSpacing(0);
						layout->setMargin(0);
						
						layout->addWidget(new QLabel(text));
						layout->addWidget(checkBox);
						
						toolBar->addWidget(w);
						
						Private::Key *key = new Private::Key;
						key->widget = checkBox;
						key->property = property;
						key->type = type;
						
						d->toolBars[toolBar] << key;
						
						connect(checkBox, SIGNAL(toggled(bool)), &d->mapper, SLOT(map()));
						d->mapper.setMapping(checkBox, key);
					}
					else if( type == "color" )
					{
						QColor c(value);
						DGui::ColorButton *button = new DGui::ColorButton(c);
						
						QWidget *w = new QWidget;
						QHBoxLayout *layout = new QHBoxLayout(w);
						
						layout->setSpacing(0);
						layout->setMargin(0);
						
						layout->addWidget(new QLabel(text));
						layout->addWidget(button);
						
						toolBar->addWidget(w);
						
						Private::Key *key = new Private::Key;
						key->widget = button;
						key->property = property;
						key->type = type;
						
						d->toolBars[toolBar] << key;
						
						connect(button, SIGNAL(clicked()), &d->mapper, SLOT(map()));
						d->mapper.setMapping(button, key);
					}
				}
			}
			break;
			default:
			{
			}
			break;
		}
		
		if( reader.hasError() )
		{
			dfDebug << reader.errorString();
		}
	}
	
	if( reader.hasError())
	{
		dError() << reader.errorString();
	}
}


void ConfigBarBuilder::changed(QObject *keyObj)
{
	Private::Key *key = dynamic_cast<Private::Key *>(keyObj);
	
	if( key )
	{
		if( key->type == "integer" )
		{
			emit propertyChanged(key->property, static_cast<QSpinBox *>(key->widget)->value() );
		}
		else if( key->type == "real" )
		{
			emit propertyChanged(key->property, static_cast<QDoubleSpinBox *>(key->widget)->value());
		}
		else if( key->type == "string" )
		{
			emit propertyChanged(key->property, static_cast<QLineEdit *>(key->widget)->text());
		}
		else if( key->type == "boolean" )
		{
			emit propertyChanged(key->property, static_cast<QCheckBox *>(key->widget)->isChecked());
		}
		else if( key->type == "color" )
		{
			emit propertyChanged(key->property, static_cast<DGui::ColorButton *>(key->widget)->color());
		}
	}
}

}
}



