/***************************************************************************
 *   Copyright (C) 2007 David Cuadrado                                     *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/

#include "inthashimp.h"

#include <QList>

#include <dcore/debug.h>

namespace YAMFPrivate {

struct VisualIndex::Private {
	struct Position {
		int logical;
		int clones;
		
		Position(): logical(-1), clones(-1)	{};
		
		Position(const  Position & copy )
		{
			logical = copy.logical;
			clones = copy.clones;
		};
		
	};
	
	QList<Position> logicalIndices;
};

VisualIndex::VisualIndex() : d(new Private)
{
// 	d->logicalIndices = QList<Private::Position>();
}

VisualIndex::VisualIndex(const  VisualIndex & copy ) : d(new Private)
{
	d->logicalIndices = copy.d->logicalIndices;
}

VisualIndex::~VisualIndex()
{
	delete d;
}


void VisualIndex::add(int logical, int clones)
{
	Private::Position pos;
	pos.logical = logical;
	pos.clones = clones;
	
	d->logicalIndices << pos;
}

void VisualIndex::insert(int logical, int clones)
{
	QList<VisualIndex::Private::Position >::iterator it = d->logicalIndices.begin();
	
	while(it != d->logicalIndices.end())
	{
		if( (*it).logical == logical )
		{
			(*it).clones += clones;
			break;
		}
		++it;
	}
}

void VisualIndex::remove(int logical)
{
	QList<VisualIndex::Private::Position >::iterator it = d->logicalIndices.begin();
	
	while(it != d->logicalIndices.end())
	{
		if( (*it).logical == logical )
		{
			(*it).clones = (*it).clones-1;
			if((*it).clones < 0)
			{
				d->logicalIndices.erase(it);
				break;
			}
		}
		++it;
	}
}

bool VisualIndex::contains(int logical) const
{
	QList<Private::Position>::iterator it = d->logicalIndices.begin();

	while(it != d->logicalIndices.end())
	{
		if( (*it).logical == logical )
		{
			return true;
		}
		++it;
	}
	
	return false;
}

int VisualIndex::firstPosition(int logical) const
{
	int index = 0;
	foreach(Private::Position pos, d->logicalIndices)
	{
		if( pos.logical == logical )
		{
			return index;
		}
		
		++index;
	}
	
	return -1;
}

QList<int> VisualIndex::logicalIndices() const
{
	QList<int> logicals;
	foreach(Private::Position pos, d->logicalIndices)
	{
		for(int i = 0; i < pos.clones+1; i++)
		{
			logicals << pos.logical;
		}
	}
	return logicals;
}

int VisualIndex::logicalAt(int vindex) const
{
	int counter = 0;
	foreach(Private::Position pos, d->logicalIndices)
	{
		counter += pos.clones +1;
		if(vindex < counter)
		{
			return pos.logical;
		}
	}
	return -1;
}

int VisualIndex::count() const
{
	int c = 0;
	foreach(Private::Position pos, d->logicalIndices)
	{
		c += 1+pos.clones;
	}
	
	return c;
}

int VisualIndex::clones(int lindex) const
{
	foreach(Private::Position pos, d->logicalIndices)
	{
		if( pos.logical == lindex)
		{
			return pos.clones;
		}
	}
	
	return -1;
}

VisualIndex &VisualIndex::operator=(const VisualIndex &other)
{
	d->logicalIndices = other.d->logicalIndices;
	
	return *this;
}



}
