/***************************************************************************
 *   Copyright (C) 2006 by David Cuadrado                                  *
 *   krawek@gmail.com                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef YAMF_DRAWINGABSTRACTTOOL_H
#define YAMF_DRAWINGABSTRACTTOOL_H

#include <QObject>
#include <QMap>
#include <yamf/common/yamf_exports.h>

class QGraphicsView;
class QKeyEvent;
class QMenu;
class QToolBar;

class QGraphicsSceneMouseEvent;

namespace DGui {
	class Action;
}

namespace YAMF {
namespace Drawing {

class BrushManager;
class PaintArea;
class Photogram;

/**
 * @ingroup drawing
 * @~spanish
 * @brief Es una clase base para todas las herramientas de dibujo, selección, vista y relleno.
 * @author David Cuadrado \<krawek@gmail.com\>
*/
class YAMF_EXPORT AbstractTool : public QObject
{
	Q_OBJECT;
	
	public:
		enum Type
		{
			None = 0,
			Brush,
			Fill,
			Selection,
			View,
			Tweener
		};
		
		AbstractTool(QObject * parent = 0);
		~AbstractTool();
		
		void setPaintArea(PaintArea *const d);
		PaintArea *paintArea() const;
		
		/**
		 * 
		 * @return 
		 */
		virtual QString id() const = 0;
		/**
		 * 
		 * @return 
		 */
		virtual int type() const = 0;
		
		virtual void init(Photogram *photogram);
		virtual void updatePhotogram(Photogram *photogram);
		
		/**
		 * 
		 * @param input 
		 */
		virtual void press(const QGraphicsSceneMouseEvent *input) = 0;
		virtual void doubleClick(const QGraphicsSceneMouseEvent *input);
		/**
		 * 
		 * @param input 
		 */
		virtual void move(const QGraphicsSceneMouseEvent *input) = 0;
		/**
		 * 
		 * @param input 
		 */
		virtual void release(const QGraphicsSceneMouseEvent *input) = 0;
		
		virtual void photogramChanged(Photogram *const photogram);
		
		/**
		 * 
		 * @return 
		 */
		virtual DGui::Action *action() const = 0;
		
		/**
		 * 
		 */
		virtual void aboutToChangeTool() = 0;
		virtual void aboutToChangePhotogram(Photogram *const photogram);
		
		virtual void keyPressEvent(QKeyEvent *event);
		
		virtual QMenu *menu() const;
		
		virtual void setupConfigBar(QToolBar *configBar);
		
	private:
		struct Private;
		Private *const d;
};

}
}

#endif

