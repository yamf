/***************************************************************************
 *   Copyright (C) 2007 David Cuadrado                                     *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/

#include "view.h"

#include <QGridLayout>
#include <QLabel>

#include <dcore/debug.h>

#include "private/ruler.h"
#include "private/paintareapanel.h"
#include "paintarea.h"
#include <model/command/manager.h>

#include <drawing/brushmanager.h>


namespace YAMF {
namespace Drawing {

struct View::Private
{
	Private() : paintArea(0)
	{
	}
	
	QGridLayout *layout;
	
	Drawing::Private::Ruler *hruler;
	Drawing::Private::Ruler *vruler;
	
	Drawing::PaintArea *paintArea;
	Drawing::Private::PaintAreaPanel *panel;
};

/**
 * @~spanish
 * Construye la interfaz relacionandola con el proyecto @p project y un widget padre @p parent.
 */
View::View(Model::Project *const project, QWidget *parent)
 : QFrame(parent), d(new Private)
{
	d->layout = new QGridLayout(this);
	d->layout->setSpacing(0);
	
	d->hruler = new Drawing::Private::Ruler(Qt::Horizontal);
	d->layout->addWidget(d->hruler,0,1);
	
	d->vruler = new Drawing::Private::Ruler(Qt::Vertical);
	d->layout->addWidget(d->vruler,1,0);
	
	d->paintArea = new PaintArea(project, this);
	d->layout->addWidget(d->paintArea, 1,1);
	
	d->panel = new YAMF::Drawing::Private::PaintAreaPanel(d->paintArea);
	d->layout->addWidget(d->panel, 2,1);
	
	this->setFrameStyle(QFrame::Panel | QFrame::Raised);
	this->setLineWidth(2);
	
	connect(d->paintArea, SIGNAL(cursorMoved(const QPointF&)), this, SLOT(onCursorMoved(const QPointF &)));
	connect(d->paintArea, SIGNAL(originChanged(const QPointF&)), d->vruler, SLOT(setZeroAt(const QPointF &)));
	connect(d->paintArea, SIGNAL(originChanged(const QPointF&)), d->hruler, SLOT(setZeroAt(const QPointF &)));
	
	connect(d->paintArea, SIGNAL(scaled(double)), this, SLOT(onScaled(double)));
}


/**
 * Destructor
 */
View::~View()
{
	delete d;
}

/**
 * @~spanish
 * Retorna el area de dibujo.
 */
PaintArea *View::paintArea() const
{
	return d->paintArea;
}

/**
 * @~spanish
 * Asigna la brocha de relleno.
 */
void View::setBrush(const QBrush &brush)
{
	d->panel->setCurrentBrush(brush);
}

/**
 * Selecciona el marco @p frameIndex de la capa @p layerIndex en la escena @p sceneIndex,
 */
void View::setCurrentFrame(int sceneIndex, int layerIndex, int frameIndex)
{
	d->paintArea->setCurrentFrame(sceneIndex, layerIndex, frameIndex);
}


/**
 * @~spanish
 * Selecciona el layer de la escena @p sceneIndex
 * @param sceneIndex 
 * @param layerIndex 
 */
void View::setCurrentLayer(int sceneIndex, int layerIndex)
{
	setCurrentFrame(sceneIndex, layerIndex, 0);
}

/**
 * @~spanish
 * Selecciona la escena @p sceneIndex
 * @param sceneIndex 
 */
void View::setCurrentScene(int sceneIndex)
{
	setCurrentFrame(sceneIndex, 0, 0);
}


/**
 * @~spanish
 * Permite acceder a la funcion addToLibrary() del area de dibujo.
 */
void View::addToLibrary()
{
	d->paintArea->addToLibrary();
}

/**
 * @~spanish
 * Permite acceder a la funcion addToLibrary() del area de dibujo.
 */
void View::addSymbol(const QString &symbol)
{
	d->paintArea->addSymbol(symbol);
}
/**
 * @internal
 * @~spanish
 * Actualiza la posición de los punteros en las reglas vertical y horizontal.
 * 
 */
void View::onCursorMoved(const QPointF &pos)
{
	d->hruler->movePointer(pos);
	d->vruler->movePointer(pos);
}

/**
 * @internal
 * @~spanish
 * Informa a las reglas de la escalacion del area de dibujo.
 */
void View::onScaled(double factor)
{
	d->hruler->scale(factor);
	d->vruler->scale(factor);
}

}

}

