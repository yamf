/***************************************************************************
 *   Copyright (C) 2006 by David Cuadrado                                  *
 *   krawek@gmail.com                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "photogram.h"

#include <QGraphicsItem>
#include <QGraphicsView>
#include <QStyleOptionGraphicsItem>
#include <QGraphicsSceneMouseEvent>
#include <QKeyEvent>
#include <QSet>

#include <model/scene.h>
#include <model/layer.h>
#include <model/frame.h>
#include <model/object.h>

#include "item/group.h"


#include <drawing/abstracttool.h>
#include <drawing/brushmanager.h>
#include <drawing/paintarea.h>

#include "item/tweener.h"

#include <dcore/debug.h>

#include <private/guide.h>

namespace YAMF {
namespace Drawing {

struct Photogram::Private
{
	Private() : tool(0), scene(0), position(0), isDrawing(false) {}
	
	AbstractTool *tool;
	Model::Scene *scene;
	struct OnionSkin
	{
		int previous;
		int next;
		QHash<QGraphicsItem *, double> opacityMap;
	} onionSkin;
	
	int position;
	
	bool isDrawing;
	BrushManager *brushManager;
	PaintArea *paintArea;
	
	QList<YAMF::Drawing::Private::Guide *> guides;
	QSet<QGraphicsItem *> alwaysVisible;
};

/**
 * @~spanish
 * Construye un fotograma.
 */
Photogram::Photogram(PaintArea *parent) : QGraphicsScene(parent), d(new Private)
{
	d->paintArea = parent;
	
	setItemIndexMethod(QGraphicsScene::NoIndex);
	
	d->onionSkin.next = 0;
	d->onionSkin.previous = 0;
	d->isDrawing = false;
	
	setBackgroundBrush(Qt::gray);
	
	d->brushManager = new BrushManager(this);
}


/**
 * Destructor
 */
Photogram::~Photogram()
{
	D_END;
	
	clearFocus();
	clearSelection();
	
	foreach( QGraphicsView *view, this->views() )
	{
		view->setScene(0);
	}
	
	foreach(QGraphicsItem *item, items())
	{
		removeItem(item);
	}
	
	delete d->tool;
	delete d;
}

/**
 * @~spanish
 * Asigna @p pos como la posición del fotograma.
 */
void Photogram::setCurrent(int pos)
{
	d->position = pos;
}

/**
 * @~spanish
 * Dibuja el fotograma actual.
 */
void Photogram::drawCurrentPhotogram()
{
	if( ! d->isDrawing )
	{
		drawPhotogram( d->position );
	}
}



/**
 * @~spanish
 * Función reimplementada para proveer la funcionalidad de papel cebolla.
 */
void Photogram::drawItems(QPainter *painter, int numItems, QGraphicsItem *items[], const QStyleOptionGraphicsItem options[], QWidget *widget)
{
	for (int i = 0; i < numItems; ++i)
	{
		QGraphicsItem *item = items[i];
		painter->save();
		painter->setMatrix(item->sceneMatrix(), true);
		
		if ( d->onionSkin.opacityMap.contains(item) )
		{
			painter->setOpacity( d->onionSkin.opacityMap[item] );
		}
		
		item->paint(painter, &options[i], widget);
		
		painter->restore();
	}
}


/**
 * @~spanish
 * Dibuja el fotograma de la posición @p photogram.
 */
void Photogram::drawPhotogram(int photogram)
{
	if ( photogram < 0 || !d->scene ) return;
	
	
	if( d->tool )
		d->tool->aboutToChangePhotogram(this);
	
	clean();
	
	bool valid = false;
	
	foreach(Model::Layer *layer, d->scene->layers().values())
	{
		if ( layer->isVisible() )
		{
			if( d->onionSkin.previous > 0 )
			{
				double opacityFactor = 0.5 / (double)qMin(layer->frames().count(),d->onionSkin.previous);
				
				double opacity = 0.6;
				
				for(int frameIndex = photogram-1; frameIndex > photogram-d->onionSkin.previous-1; frameIndex-- )
				{
					Model::Frame * frame = layer->frame(frameIndex);
					if(frame)
					{
						addFrame( frame, opacity );
					}
					opacity -= opacityFactor;
				}
			}
			
			if ( d->onionSkin.next > 0 )
			{
				double opacityFactor = 0.5 / (double)qMin(layer->frames().count(), d->onionSkin.next);
				double opacity = 0.6;
				
				for(int frameIndex = photogram+1; frameIndex < photogram+d->onionSkin.next+1; frameIndex++ )
				{
					Model::Frame * frame = layer->frame(frameIndex);
					if(frame)
					{
						addFrame( frame, opacity );
					}
					opacity -= opacityFactor;
				}
			}
			
			Model::Frame *frame = layer->frame( photogram );
			
			if( frame )
			{
				valid = true;
				addFrame(frame);
			}
		}
	}
	
	if( valid )
	{
		foreach(QGraphicsItem *item, d->alwaysVisible)
		{
			if( item->scene() != this )
				addItem(item);
		}
		
		foreach(Model::Object *obj, d->scene->alwaysVisible() )
		{
			if( obj->frame()->isVisible() )
			{
				QGraphicsItem *item = obj->item();
				
				if( item->scene() != this )
					addItem(item);
			}
		}
		
		foreach(Model::Object *object, d->scene->tweeningObjects())
		{
			if(object->frame()->layer()->isVisible() )
			{
				int origin = object->frame()->visualIndex();
				
				if( Item::Tweener *tweener = object->tweener() )
				{
					if( photogram == 0)
					{
						tweener->setStep(0);
					}
					else if( origin < photogram && photogram <= origin+tweener->frames() )
					{
						tweener->setStep(0);
						int step = photogram - origin;
						
						tweener->setStep(step);
						
						addGraphicObject(object);
					}
				}
			}
		}
		
		update();
	}
}

/**
 * @~spanish
 * Añade el frame al fotograma,si este es visible.
 */
void Photogram::addFrame(Model::Frame *frame, double opacity )
{
	if ( frame->isVisible() )
	{
		foreach(Model::Object *object, frame->graphics().values() )
		{
			addGraphicObject(object, opacity);
		}
	}
}

/**
 * @~spanish
 * Añade el objeto grafico @p object al fotograma, con un factor de opacidad @p opacity.
 */
void Photogram::addGraphicObject(Model::Object *object, double opacity)
{
	QGraphicsItem *item = object->item();
	
	if( item )
	{
		d->onionSkin.opacityMap.insert(item, opacity);
		
		if ( Item::Group *group = qgraphicsitem_cast<Item::Group *>(item) )
		{
			group->recoverChilds();
		}
		
		if( ! qgraphicsitem_cast<Item::Group *>(item->parentItem()))
		{
			if( item->scene() != this )
				addItem(item);
		}
	}
}

/**
 * @~spanish
 * Limpia el fotograma.
 */
void Photogram::clean()
{
	d->onionSkin.opacityMap.clear();
	
	foreach(QGraphicsItem *item, items() )
	{
		if ( item->scene() == this )
		{
			removeItem(item);
		}
	}
	
	foreach(YAMF::Drawing::Private::Guide *guide, d->guides )
	{
		addItem(guide);
	}
}

/**
 * @~spanish
 * Asigna los papel cebolla hacia adelante del fotograma.
 */
void Photogram::setNextOnionSkinCount(int n)
{
	d->onionSkin.next = n;
	drawCurrentPhotogram();
}

/**
 * @~spanish
 * Asigna los papel cebolla hacia atras del fotograma.
 */
void Photogram::setPreviousOnionSkinCount(int n)
{
	d->onionSkin.previous = n;
	
	drawCurrentPhotogram();
}


void Photogram::setAlwaysVisible(QGraphicsItem *item, bool e)
{
	if(e )
	{
		d->alwaysVisible << item;
	}
	else
	{
		d->alwaysVisible.remove(item);
	}
}

/**
 * @~spanish
 * Asigna la scena actual del fotograma.
 */
void Photogram::setCurrentScene(Model::Scene *scene)
{
	qDeleteAll(d->guides);
	d->guides.clear();
	
	clean();
	d->scene = scene;
	
	drawCurrentPhotogram();
	
	if( d->tool )
	{
		d->tool->photogramChanged(this);
	}
}

/**
 * @~spanish
 * Asigna la visibilidad del layer que esta en la posición @p layerIndex a @p visible.
 */
void Photogram::setLayerVisible(int layerIndex, bool visible)
{
	if( !d->scene ) return;
	
	if( Model::Layer *layer = d->scene->layer(layerIndex) )
	{
		layer->setVisible(visible);
	}
}

/**
 * @~spanish
 * Retorna la escena del fotograma.
 */
Model::Scene *Photogram::scene() const
{
	return d->scene;
}

/**
 * @~spanish
 * Asigna @p tool como la herramienta actual.
 */
void Photogram::setTool(AbstractTool *tool)
{
	drawCurrentPhotogram();
	if(d->tool)
	{
		if(d->tool->type() == AbstractTool::Selection )
		{
			foreach(YAMF::Drawing::Private::Guide *guide, d->guides)
			{
				guide->setFlag( QGraphicsItem::ItemIsMovable, false );
				guide->setEnabledSyncCursor(true);
			}
		}
		d->tool->aboutToChangeTool();
	}
	
	d->tool = tool;
 	if(tool->type() == AbstractTool::Selection )
 	{
 		foreach(YAMF::Drawing::Private::Guide *guide, d->guides)
 		{
 			guide->setFlag( QGraphicsItem::ItemIsMovable, true );
 			guide->setEnabledSyncCursor(false);
 		}
 	}
	d->tool->init(this);
	
}

/**
 * @~spanish
 * Retorna la herramienta actual.
 */
AbstractTool *Photogram::currentTool() const
{
	return d->tool;
}


/**
 * @~spanish
 * Función reimplementada para informar a la herramienta actual del evento de presionado de un boton del mouse.
 */
void Photogram::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
	QGraphicsScene::mousePressEvent(event);
	
	d->isDrawing = false;

	if( event->buttons() == Qt::LeftButton &&  (event->modifiers () == (Qt::ShiftModifier | Qt::ControlModifier)))
	{
	}
	else if (d->tool )
	{
// 		if( d->tool->type() == AbstractTool::Brush && event->isAccepted() ) return;
		
		if( d->position > -1 )
		{
			if ( event->buttons() == Qt::LeftButton )
			{
				d->isDrawing = true;
				d->tool->press(event);
			}
		}
	}
}

/**
 * @~spanish
 * Función reimplementada para informar a la herramienta actual del evento de movimiento del mouse.
 */
void Photogram::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
	QGraphicsScene::mouseMoveEvent(event);
	mouseMoved(event);
}
/**
 * @~spanish
 * Función que informa a la herramienta actual del movimiento del mouse.
 */
void Photogram::mouseMoved(QGraphicsSceneMouseEvent *event)
{
// 	d->inputInformation->updateFromMouseEvent( event );
	
	if (d->tool /*&& d->isDrawing*/ )
	{
		d->tool->move(event);
	}
}

/**
 * @~spanish
 * Función reimplementada para informar a la herramienta actual del evento de liberación de un boton del mouse.
 */
void Photogram::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
	QGraphicsScene::mouseReleaseEvent(event);
	mouseReleased(event);
}

/**
 * @~spanish
 * Función que informa a la herramienta actual de liberación de un boton del mouse.
 */
void Photogram::mouseReleased(QGraphicsSceneMouseEvent *event)
{
	if ( d->tool )
	{
		if( d->isDrawing )
		{
			d->tool->release(event);
		}
		
		d->isDrawing = false;
		drawCurrentPhotogram();
	}
}

/**
 * @~spanish
 * Función reimplementada para informar a la herramienta actual del evento de doble click.
 */
void Photogram::mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event)
{
	QGraphicsScene::mouseDoubleClickEvent(event);
	
// 	d->inputInformation->updateFromMouseEvent( event );
	
	if(d->tool)
	{
		d->tool->doubleClick( event);
	}
}
/**
 * 
 * @~spanish
 * Función reimplementada para informar a la herramienta actual del evento de preción de una tecla.
 */
void Photogram::keyPressEvent(QKeyEvent *event)
{
	if ( d->tool )
	{
		d->tool->keyPressEvent(event);
		
		if ( event->isAccepted() )
		{
			return;
		}
	}
	
	QGraphicsScene::keyPressEvent(event);
}


/**
 * @~spanish
 * Función reimplementada para la creación de lineas guias.
 */
void Photogram::dragEnterEvent ( QGraphicsSceneDragDropEvent * event )
{
	if (event->mimeData()->hasFormat("yamf-ruler"))
	{
		event->acceptProposedAction();
		YAMF::Drawing::Private::Guide *guide = 0;
		if(event->mimeData()->data("yamf-ruler") == "verticalLine")
		{
			guide = new YAMF::Drawing::Private::Guide(Qt::Vertical, this);
			guide->setPos(event->scenePos());
		}
		else
		{
			guide = new YAMF::Drawing::Private::Guide(Qt::Horizontal, this);
			guide->setPos(event->scenePos());
		}
		
		if(guide)
		{
			d->guides << guide;
		}
	}
}

/**
 * @~spanish
 * Función reimplementada para la creación de lineas guias.
 */
void  Photogram::dragLeaveEvent ( QGraphicsSceneDragDropEvent * event)
{
	if (event->mimeData()->hasFormat("yamf-ruler"))
	{
		removeItem(d->guides.last());
		delete d->guides.takeLast();
	}
}

/**
 * @~spanish
 * Función reimplementada para la creación de lineas guias.
 */
void Photogram::dragMoveEvent ( QGraphicsSceneDragDropEvent * event )
{
	if (event->mimeData()->hasFormat("yamf-ruler"))
	{
		if(!d->guides.isEmpty())
		{
			d->guides.last()->setPos(event->scenePos());
		}
	}
}

/**
 * @~spanish
 * Función reimplementada para la creación de lineas guias.
 */
void Photogram::dropEvent ( QGraphicsSceneDragDropEvent * event )
{
	Q_UNUSED(event);
	if(d->tool)
	{
		if(d->tool->type() == AbstractTool::Selection )
		{
			d->guides.last()->setEnabledSyncCursor(false);
			d->guides.last()->setFlag( QGraphicsItem::ItemIsMovable, true );
		}
	}
}


bool Photogram::event(QEvent *e)
{
	return QGraphicsScene::event(e);
}

/**
 * @~spanish
 * Retorna verdadero si se esta dibujando.
 */
bool Photogram::isDrawing() const
{
	return d->isDrawing;
}


/**
 * @~spanish
 * Retorna el manejador de brochas del photograma.
 */
BrushManager *Photogram::brushManager() const
{
	return d->brushManager;
}

/**
 * @~spanish
 * Evita la selección de elementos que estan en el papel cebolla.
 */
void Photogram::aboutToMousePress()
{
	QHash<QGraphicsItem *, double>::iterator it = d->onionSkin.opacityMap.begin();

	while(it != d->onionSkin.opacityMap.end() )
	{
		if( it.value() != 1.0 )
		{
			it.key()->setAcceptedMouseButtons(Qt::NoButton);
			it.key()->setFlag(QGraphicsItem::ItemIsSelectable, false);
		}
		else
		{
			it.key()->setAcceptedMouseButtons( Qt::LeftButton | Qt::RightButton | Qt::MidButton | Qt::XButton1 | Qt::XButton2 );
		}

		++it;
	}
}

}
}


