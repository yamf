/***************************************************************************
 *   Copyright (C) 2005 by Jorge Cuadrado                                  *
 *   kuadrosxx@gmail.com                                                   *
 *   Copyright (C) 2006 by David Cuadrado                                  *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "paintarea.h"

#include "private/paintarearotator.h"

#include <QGraphicsScene>
#include <QMouseEvent>
#include <QGraphicsSceneMouseEvent>
#include <QGraphicsRectItem>
#include <QPolygon>
#include <QApplication>
#include <QTimer>
#include <QStyleOptionGraphicsItem>
#include <QClipboard>
#include <QUndoStack>
#include <QMenu>
#include <QToolBar>

#include "photogram.h"
#include <dgraphics/algorithm.h>

#include <dcore/config.h>
#include <dcore/debug.h>
#include <dgui/application.h>
#include <dgui/osd.h>
#include <dgui/commandhistory.h>
#include <dgui/action.h>
#include <dgui/actionmanager.h>
#include <dgui/iconloader.h>

#include <cmath>

#include <model/scene.h>
#include <model/layer.h>
#include <model/frame.h>
#include <model/object.h>
#include <model/library.h>

#include <item/group.h>
#include <item/serializer.h>
#include "yamf/item/tweenerstep.h"
#include "yamf/item/tweener.h"

#include "model/command/base.h"
#include "model/command/groupitem.h"
#include "model/command/ungroupitem.h"
#include "model/command/modifyitem.h"
#include "model/command/modifysymbol.h"
#include "model/command/sendtobackitem.h"
#include "model/command/bringtofrontitem.h"
#include "model/command/sendbackwardsitem.h"
#include "model/command/bringforwardsitem.h"
#include "model/command/manager.h"
#include "yamf/model/command/additemfilter.h"
#include "model/command/addtweening.h"

#include "yamf/model/libraryitem.h"
#include "yamf/model/audiolayer.h"

#include "yamf/effect/factory.h"
#include "yamf/effect/filter.h"

#include "item/builder.h"

#include <drawing/abstracttool.h>
#include "private/addsymboldialog.h"
#include "private/effecthandler.h"

#ifdef QT_OPENGL_LIB

#include <QGLWidget>
#include <QGLFramebufferObject>

class GLDevice : public QGLWidget
{
	public:
		GLDevice() : QGLWidget()
		{
			makeCurrent();
		}
		~GLDevice() {};
		
	protected:
		void initializeGL()
		{
			glDisable(GL_DEPTH_TEST);
		}
};
#endif

namespace YAMF {
namespace Drawing {

struct YAMF::Drawing::PaintArea::Private
{
	Private(YAMF::Drawing::PaintArea *paintArea, Model::Project *const project) : paintArea(paintArea), canvasColor(Qt::white), gridSeparation(10), drawGrid(false), drawBorder(false), project(project), lastScaleFactor(1.0)
	{
	}
	
	DGui::CommandHistory *history;
	
	YAMF::Drawing::PaintArea *paintArea;
	
	QRectF drawingRect;
	QColor canvasColor;
	double gridSeparation;
	
	bool drawGrid;
	bool drawBorder;
	double angle;
	
	YAMF::Drawing::Private::PaintAreaRotator *rotator;
	Photogram *photogram;
	
	struct FramePosition {
		int layer;
		int frame;
	} framePosition;
	
	
	struct ItemProperties {
		QGraphicsItem *item;
		QString properties;
	};
	
	QList<ItemProperties> properties;
	
	QPointF origin;
	
	DGui::ActionManager actionManager;
	
	Model::Project *const project;
	
	double lastScaleFactor;
	
	QToolBar *toolsConfigBar;
	QToolBar *effectsConfigBar;
	EffectHandler  *effectHandler;
	
};

/**
 * @~spanish
 * Construye un area de trabajo relacionada con el proyecto @p project.
*/
PaintArea::PaintArea(Model::Project *const project, QWidget * parent) : QGraphicsView(parent), d(new Private(this, project) )
{
	d->history = new DGui::CommandHistory( project->commandManager()->stack(), this);
	
	d->photogram = new Photogram(this);
	d->angle = 0;
	
	d->rotator = new YAMF::Drawing::Private::PaintAreaRotator(this, this);
	
	d->drawingRect = QRectF(QPointF(0,0), QSizeF( 500, 400 ) ); // FIXME: configurable
	
	d->photogram->setSceneRect(d->drawingRect);
	setScene(d->photogram);
	
	centerDrawingArea();
	
	setUseOpenGL( false );
	setInteractive ( true );
	
	restoreSettings();
	
	setCurrentFrame(-1, -1, -1);
	
	d->origin = mapToScene(QPoint(0, 0));
	
	createActions();
	checkActions();
	
	d->toolsConfigBar = new QToolBar(tr("Configure tool"), this);
	
	d->effectsConfigBar = new QToolBar(tr("Configure effects"), this);
	d->effectHandler = new EffectHandler(this);
}

/**
 * @~spanish
 * Guarda en la configuracion el estado de las propiedades del area de dibujo.
 */
void PaintArea::saveSettings()
{
	DCore::Config *config = dApp->config( "PaintArea" );
	
	config->setValue("render_hints", int(renderHints()));
	config->setValue("show_grid", d->drawGrid);
	config->setValue("show_border", d->drawBorder);
	
	config->setValue("canvas_color", d->canvasColor);
	config->setValue("grid_separation", d->gridSeparation);
	
#ifdef QT_OPENGL_LIB
	if( dynamic_cast<QGLWidget *>(viewport()) )
	{
		config->setValue("opengl", true);
	}
	else
	{
		config->setValue("opengl", false);
	}
#endif
	
	config->endGroup();
}

/**
 * @~spanish
 * Recupera de la configuracion el estado de las propiedades del area de dibujo.
 */
void PaintArea::restoreSettings()
{
	DCore::Config *config = dApp->config( "PaintArea" );
	
	int renderHints = config->value("render_hints", int(this->renderHints())).toInt();
	setRenderHints(QPainter::RenderHints(renderHints));
	
	bool showGrid = config->value("show_grid", false).toBool();
	setDrawGrid(showGrid);
	
	bool showBorder = config->value("show_border", false).toBool();
	setDrawBorder(showBorder);
	
	setCanvasColor(qvariant_cast<QColor>(config->value("canvas_color", Qt::white)));
	setGridSeparation(config->value("grid_separation", 10).toInt());
	
#ifdef QT_OPENGL_LIB
	setUseOpenGL(config->value("opengl", false).toBool());
#endif
	
	
	config->endGroup();
}

/**
 * Destructor
 */
PaintArea::~PaintArea()
{
	disconnect(d->project->commandManager(), SIGNAL(historyChanged()), this, SLOT(onHistoryChanged()));
	
	delete d->history;
	saveSettings();
	
	d->toolsConfigBar->clear();
	d->effectsConfigBar->clear();
	
	
	delete d->photogram;
	delete d;
}

/**
 * @~spanish
 * Si @p v es verdadero, el item siembre serï¿½ mostrado en el area de dibujo
 * @param item 
 * @param v 
 */
void PaintArea::setAlwaysVisible(QGraphicsItem *item, bool v)
{
	d->photogram->setAlwaysVisible(item, v);
}

/**
 * @~spanish
 * Pone el color del canvas.
 * 
 * @param color 
 */
void PaintArea::setCanvasColor(const QColor &color)
{
	d->canvasColor = color;
	viewport()->update();
}

/**
 * @~spanish
 * Retorna el color del canvas
 * @return 
 */
QColor PaintArea::canvasColor() const
{
	return d->canvasColor;
}

/**
 * @~spanish
 * 
 * Pone la separacion entre cuadros de la cuadricula del area de dibujo
 * 
 * @param v 
 */
void PaintArea::setGridSeparation(double v)
{
	if( v >= 5 )
	{
		d->gridSeparation = v;
		viewport()->update();
	}
}

/**
 * @~spanish
 * Retorna la separacon de cuadros de la cuadricula de dibujo.
 * @return 
 */
double PaintArea::gridSeparation() const
{
	return d->gridSeparation;
}


/**
 * @~spanish
 * Crea las acciones relacionadas con el area de dibujo.
 */
void PaintArea::createActions()
{
	addAction( d->history->undoAction() );
	addAction( d->history->redoAction() );
	
	connect(d->project->commandManager(), SIGNAL(historyChanged()), this, SLOT(onHistoryChanged()));
	
	QAction *remove = new QAction(DGui::IconLoader::self()->load("edit-delete.svg"), tr("Delete"), this);
	remove->setShortcut(QKeySequence::Delete);
	connect(remove, SIGNAL(triggered()), this, SLOT(removeSelectedObjects()));
	addAction(remove);
	
	QAction *selectAll = new QAction(DGui::IconLoader::self()->load("select-all.svg"), tr("Select all"), this);
	selectAll->setShortcut(QKeySequence::SelectAll);
	connect(selectAll, SIGNAL(triggered()), this, SLOT(selectAllObjects()));
	addAction(selectAll);
	
	QAction *copy = new QAction( DGui::IconLoader::self()->load("edit-copy.svg"), tr("Copy"), this);
	copy->setShortcut(QKeySequence::Copy);
	connect(copy, SIGNAL(triggered()), this, SLOT(copySelectedObjects()));
	addAction(copy);
	
	QAction *paste = new QAction(DGui::IconLoader::self()->load("edit-paste.svg"), tr("Paste"), this);
	paste->setShortcut(QKeySequence::Paste);
	connect(paste, SIGNAL(triggered()), this, SLOT(pasteObjects()));
	addAction(paste);
	
	QAction *cut = new QAction(DGui::IconLoader::self()->load("edit-cut.svg"), tr("Cut"), this);
	cut->setShortcut(QKeySequence::Cut);
	connect(cut, SIGNAL(triggered()), this, SLOT(cutSelectedObjects()));
	addAction(cut);
	
	
	d->actionManager.add(selectAll, "selectall", "edit");
	d->actionManager.addSeparator("edit");
	d->actionManager.add(cut, "cut", "edit");
	d->actionManager.add(copy, "copy", "edit");
	d->actionManager.add(paste, "paste", "edit");
	d->actionManager.addSeparator("edit");
	d->actionManager.add(remove, "remove", "edit");
	
	/////////
	
	QAction *group = new QAction(DGui::IconLoader::self()->load("object-group.svg"), tr("Group"), this);
	group->setShortcut(Qt::CTRL + Qt::Key_G);
	connect(group, SIGNAL(triggered()), this, SLOT(groupSelectedObjects()));
	d->actionManager.add(group, "group", "object");
	addAction(group);
	
	QAction *ungroup = new QAction(DGui::IconLoader::self()->load("object-ungroup.svg"), tr("Ungroup"), this);
	ungroup->setShortcut(Qt::CTRL + Qt::SHIFT + Qt::Key_G);
	connect(ungroup, SIGNAL(triggered()), this, SLOT(ungroupSelectedObject()));
	d->actionManager.add(ungroup, "ungroup", "object");
	addAction(ungroup);
	
	QAction *sendToBack = new QAction(DGui::IconLoader::self()->load("object-send-to-back.svg"), tr("Send to back"), this);
	sendToBack->setShortcut(Qt::Key_Home);
	connect(sendToBack, SIGNAL(triggered()), this, SLOT(sendToBackSelectedObjects()));
	d->actionManager.add(sendToBack, "sendtoback", "object-order");
	addAction(sendToBack);
	
	QAction *bringToFront = new QAction(DGui::IconLoader::self()->load("object-bring-to-front.svg"), tr("Bring to front"), this);
	bringToFront->setShortcut(Qt::Key_End);
	connect(bringToFront, SIGNAL(triggered()), this, SLOT(bringToFrontSelectedObjects()));
	d->actionManager.add(bringToFront, "bringtofront", "object-order");
	addAction(bringToFront);
	
	QAction *sendBackwards = new QAction(DGui::IconLoader::self()->load("object-send-backwards.svg"), tr("Send backwards"), this);
	sendBackwards->setShortcut(Qt::Key_PageDown);
	connect(sendBackwards, SIGNAL(triggered()), this, SLOT(sendBackwardsSelectedObjects()));
	d->actionManager.add(sendBackwards, "sendbackwards", "object-order");
	addAction(sendBackwards);
	
	QAction *bringForwards = new QAction(DGui::IconLoader::self()->load("object-bring-forwards.svg"), tr("Bring forwards"), this);
	bringForwards->setShortcut(Qt::Key_PageUp);
	connect(bringForwards, SIGNAL(triggered()), this, SLOT(bringForwardsSelectedObjects()));
	d->actionManager.add(bringForwards, "bringforwards", "object-order");
	addAction(bringForwards);
	
	// Library
	
	QAction *addToLibrary = new QAction(DGui::IconLoader::self()->load("object-bring-forwards.svg"), tr("Add to library..."), this);
	connect(addToLibrary, SIGNAL(triggered()), this, SLOT(addToLibrary()));
	d->actionManager.add(addToLibrary, "addtolibrary", "library");
	addAction(addToLibrary);
	
	QAction *applySymbol = new QAction(DGui::IconLoader::self()->load(""), tr("Apply to symbol..."), this);
	connect(applySymbol, SIGNAL(triggered()), this, SLOT(applyCurrentSymbol()));
	d->actionManager.add(applySymbol, "applysymbol", "library");
	
}

/**
 * @~spanish
 * Si @p use es verdadero permite que la animaciï¿½n se vea con antialiasing y si es false desactiva el antialiasing.
 */
void PaintArea::setAntialiasing(bool use)
{
#ifdef QT_OPENGL_LIB
	if ( QGLWidget *gl = dynamic_cast<QGLWidget *>(viewport() ) )
	{
		gl->setUpdatesEnabled(false); // works better
		gl->setFormat(QGLFormat(QGL::SampleBuffers | QGL::HasOverlay /*| QGL::DirectRendering | QGL::AccumBuffer | QGL::Rgba */));
		gl->setUpdatesEnabled(true);
	}
#endif
	
	setRenderHint(QPainter::Antialiasing, use);
	setRenderHint(QPainter::TextAntialiasing, use);
}

/**
 * @~spanish
 * Si @p opengl es verdadero activa el uso de openGl para dibujar el fotograma.
 */
void PaintArea::setUseOpenGL(bool opengl)
{
	D_FUNCINFO << opengl;
	
	QCursor cursor(Qt::ArrowCursor);
	if ( viewport() )
	{
		cursor = viewport()->cursor();
	}
	
#ifdef QT_OPENGL_LIB
	if ( opengl )
	{
		setViewport(new GLDevice());
	}
	else
	{
// 		setViewport( new KTImageDevice() );
	}
#else
	Q_UNUSED(opengl);
	dWarning() << tr("OpenGL isn't supported");
#endif
	
	// to restore the cursor.
	if ( viewport() )
	{
		viewport()->setCursor(cursor);
		viewport()->setAcceptDrops(true);
	}
}

/**
 * @~spanish
 * Si @p draw es verdadero activa el dibujo de una grilla guia en el area, y si es falso la desactiva.
 * Por defecto no se dibuja la grilla.
 */
void PaintArea::setDrawGrid(bool draw)
{
	d->drawGrid = draw;
// 	resetCachedContent();
	viewport()->update();
}


/**
 * @~spanish
 * Si @p draw es verdadero, dibuja el borde del area de dibujo
 * @param draw 
 */
void PaintArea::setDrawBorder(bool draw)
{
	d->drawBorder = draw;
	viewport()->update();
}

/**
 * @~spanish
 * Se asigna la herramienta @p tool como la herramienta actual.
 */
void PaintArea::setTool(AbstractTool *tool )
{
	if ( !scene() ) return;
	
	QCursor cursor = tool->action()->cursor();
	if( (!cursor.pixmap().isNull() && cursor.mask() != 0 && cursor.shape() == Qt::BitmapCursor && cursor.bitmap() != 0 ) || cursor.shape() != Qt::BitmapCursor )
		viewport()->setCursor(cursor);
	
	d->toolsConfigBar->clear();
	tool->setupConfigBar(d->toolsConfigBar);
	tool->setPaintArea(this);
	
	d->photogram->setTool(tool);
}

/**
 * @~spanish
 * Retorna verdadero esta activo el dibujo del borde del area de dibujo.
 */
bool PaintArea::drawBorder() const
{
	return d->drawBorder;
}

/**
 * @~spanish
 * Retorna verdadero esta activo el dibujo de la cuadricula.
 */
bool PaintArea::drawGrid() const
{
	return d->drawGrid;
}

void PaintArea::mousePressEvent( QMouseEvent * event )
{
	if( !canPaint() ) return;
	
	QGraphicsItem *item = d->photogram->itemAt( mapToScene(event->pos()) );
	
	
	d->effectsConfigBar->clear();
	if( Model::Frame *currentFrame = this->currentFrame() )
	{
		Model::Object *object = currentFrame->findGraphic(item);
		if( object )
		{
			d->effectHandler->config(object, d->effectsConfigBar);
		}
	}
	
	if( event->buttons() == Qt::RightButton )
	{
		
		QMenu *menu = this->createMenu();
		
		if( item )
		{
			// Add effects
			{
				QMenu *effects = menu->addMenu(tr("Effects"));
				
				QAction *blur = effects->addAction(tr("Blur"));
				blur->setData("blur");
				
				QAction *swirl = effects->addAction(tr("Swirl"));
				swirl->setData("swirl");
				
				QAction *wave = effects->addAction(tr("Wave"));
				wave->setData("wave");
				
				QAction *flatten = effects->addAction(tr("Flatten"));
				flatten->setData("flatten");
				
				QAction *edge = effects->addAction(tr("Edge"));
				edge->setData("edge");
				
				QAction *sharpen = effects->addAction(tr("Sharpen"));
				sharpen->setData("sharpen");
				
				QAction *emboss = effects->addAction(tr("Emboss"));
				emboss->setData("emboss");
				
				connect(effects, SIGNAL(triggered(QAction *)), this, SLOT(applyFilter(QAction *)));
			}
			
			item->setFlag(QGraphicsItem::ItemIsSelectable, true);
			item->setSelected(true);
			
			if( Effect::Filter *filter = dynamic_cast<Effect::Filter *>(item) )
			{
				QAction *action = menu->addAction(tr("Remove filter %1...").arg(filter->name()));
				connect(action, SIGNAL(triggered()), this, SLOT(removeCurrentFilter()));
			}
			
			Model::Object *object = currentScene()->tweeningObject(item);
			
			if( object )
			{
				QAction *action = menu->addAction(tr("Remove tweening..."));
				connect(action, SIGNAL(triggered()), this, SLOT(removeCurrentTweener()));
			}
		}
		
		menu->addSeparator();
		
		if( dynamic_cast<Model::LibraryItem *>(item) )
		{
			menu->addAction(d->actionManager.find("applysymbol", "library"));
		}
		else
		{
			menu->addAction(d->actionManager.find("addtolibrary", "library"));
		}
		
		menu->addSeparator();
		
		checkActions();
		
		menu->exec(event->globalPos());
		
		delete menu;
	}
	else
	{
		d->photogram->aboutToMousePress();
		QGraphicsView::mousePressEvent(event);
	}
	
	foreach(QGraphicsItem *item , d->photogram->selectedItems() )
	{
		if( item->flags() & QGraphicsItem::ItemIsMovable )
		{
			Private::ItemProperties prop;
			prop.item = item;
			
			QDomDocument doc;
			doc.appendChild(Item::Serializer::properties(item, doc));
			
			prop.properties = doc.toString();
			d->properties << prop;
		}
	}
}

void PaintArea::mouseMoveEvent( QMouseEvent * event )
{
	if ( !canPaint()) return;
	
	// Rotate
	if( !d->photogram->isDrawing() && event->buttons() == Qt::LeftButton &&  (event->modifiers () == (Qt::ShiftModifier | Qt::ControlModifier)))
	{
		setUpdatesEnabled(false);
		
		QGraphicsView::DragMode dragMode = this->dragMode();
		setDragMode(QGraphicsView::NoDrag);
		
		QPointF p1 = event->pos();
		QPointF p2 = d->drawingRect.center();
		
		d->rotator->rotateTo( (int)(-(180*DGraphics::Algorithm::angleForPos(p1,p2))/M_PI) );
		
		setDragMode(dragMode);
		
		setUpdatesEnabled(true);
	}
	else
	{
		QGraphicsView::mouseMoveEvent(event);
		
		if( !d->photogram->mouseGrabberItem() && d->photogram->isDrawing() ) // HACK
		{
			QGraphicsSceneMouseEvent mouseEvent(QEvent::GraphicsSceneMouseMove);
			mouseEvent.setWidget(viewport());
			
			mouseEvent.setPos( event->pos() );
			mouseEvent.setScenePos(mapToScene(event->pos()));
			
			mouseEvent.setScreenPos(event->globalPos());
			mouseEvent.setButtons(event->buttons());
			mouseEvent.setButton(event->button());
			mouseEvent.setModifiers(event->modifiers());
			mouseEvent.setAccepted(false);
			d->photogram->mouseMoved(&mouseEvent);
		}
	}
	
	emit cursorMoved( mapToScene( event->pos() ) );
}

void PaintArea::mouseReleaseEvent(QMouseEvent *event)
{
	QGraphicsView::mouseReleaseEvent(event);
	
	if( ! d->photogram->mouseGrabberItem() && d->photogram->isDrawing() ) // HACK
	{
		QGraphicsSceneMouseEvent mouseEvent(QEvent::GraphicsSceneMouseRelease);
		mouseEvent.setWidget(viewport());
		mouseEvent.setPos( event->pos() );
		mouseEvent.setScenePos(mapToScene(event->pos()));
		mouseEvent.setScreenPos(event->globalPos());
		mouseEvent.setButtons(event->buttons());
		mouseEvent.setButton(event->button());
		mouseEvent.setModifiers(event->modifiers());
		mouseEvent.setAccepted(false);
// 		QApplication::sendEvent(d->photogram, &mouseEvent);
		d->photogram->mouseReleased(&mouseEvent);
	}
	
	foreach(Private::ItemProperties prop, d->properties)
	{
		if(currentFrame()->visualIndexOf(prop.item) > -1 /*|| ritem == prop.item*/)
		{
			QDomDocument doc;
			doc.appendChild(Item::Serializer::properties(prop.item, doc));
			
			if( doc.toString() != prop.properties )
			{
				Command::ModifyItem *current = new Command::ModifyItem( prop.item, prop.properties, currentFrame() );
				addCommand( current );
			}
		}
	}
	
	d->properties.clear();
	
	checkActions();
}

void PaintArea::tabletEvent( QTabletEvent * event )
{
	QGraphicsView::tabletEvent(event );
}

/**
 * Funciï¿½n reimplementada para dibujar el fondo del area de dibujo, mostrando el area que va a aparecer en la animaciï¿½n y la grilla si esta activa la opciï¿½n.
 */
void PaintArea::drawBackground(QPainter *painter, const QRectF &rect)
{
	QGraphicsView::drawBackground(painter, rect);
	
	if( ! currentFrame() ) return;
	
	painter->save();
	
	bool hasAntialiasing = painter->renderHints() & QPainter::Antialiasing;
	
	painter->setRenderHint(QPainter::Antialiasing, false);
	
	painter->setPen( QPen(QColor(0,0,0,180), 3) );
	
	painter->fillRect( d->drawingRect, d->canvasColor );
	painter->drawRect( d->drawingRect);
	
	QPointF origin = painter->matrix().map(QPoint(0, 0));
	
	if( origin != d->origin )
	{
		d->origin = origin;
		emit originChanged( d->origin );
	}
	
	painter->setRenderHint(QPainter::Antialiasing, hasAntialiasing);
	
	
	painter->restore();
}

/**
 * @~spanish
 * Funciï¿½n reimplementada para mostrar si el frame actual esta bloqueado.
 */
void PaintArea::drawForeground( QPainter *painter, const QRectF &rect )
{
	if ( Model::Frame *frame = currentFrame() )
	{
		bool hasAntialiasing = painter->renderHints() & QPainter::Antialiasing;
		painter->setRenderHint(QPainter::Antialiasing, false);
		
		if ( d->drawGrid )
		{
			QTransform transform = painter->transform();
			double sx = painter->matrix().m11();
			double sy = painter->matrix().m22();
			
			
			if( sx > 0.9 && sy > 0.9 )
			{
				painter->setTransform(QTransform());
				painter->scale(sx, sy);
				painter->setPen( QPen(QColor(0,0,180, 20), 1) );
				
				
				int width = this->width();
				int height = this->height();
				
				for(double i = 0; i < qMax(width, height); i+= d->gridSeparation )
				{
					painter->drawLine((int)i, 0, (int)i, height );
					painter->drawLine(0, (int)i, width, (int)i );
				}
				
				painter->setTransform(transform);
			}
		}
		
		if( d->drawBorder )
		{
			QPen pen = painter->pen();
			QBrush brush = painter->brush();
			
			painter->setPen(QPen(Qt::red, 4));
			painter->setBrush(Qt::transparent);
			painter->drawRect(d->drawingRect);
			
			painter->setPen(pen);
			painter->setBrush(brush);
		}
		
		if ( frame->isLocked() )
		{
			painter->fillRect(rect, QColor(201,201,201, 200));
			
			painter->setFont(QFont("Arial", 30) );
			QFontMetricsF fm(painter->font());
			QString text = tr("Locked");
			
			painter->drawText(QPointF(d->photogram->sceneRect().topRight().x() - fm.width(text), (d->photogram->sceneRect().topRight().y() + fm.height()) / 2), text);
		}
		
		painter->setRenderHint(QPainter::Antialiasing, hasAntialiasing);
	}
}

/**
 * @~spanish
 * Retorna verdadero si se puede dibujar sobre el area de dibujo y falso si no.
 */
bool PaintArea::canPaint() const
{
	if( d->photogram )
	{
		Model::Frame *frame = currentFrame();
		
		if( frame )
		{
			return !frame->isLocked() && !frame->layer()->isLocked();
		}
	}
	
	return false;
}

/**
 * @~spanish
 * Centra la vista del fotograma.
 */
void PaintArea::centerDrawingArea()
{
	centerOn(d->drawingRect.center());
}

/**
 * @internal
 */
void PaintArea::onHistoryChanged()
{
	Model::Scene *scene = d->photogram->scene();
	if( scene )
	{
		if( scene->logicalIndex() < 0 )
			d->photogram->setCurrentScene(0);
	}
	
	d->photogram->drawCurrentPhotogram();
	viewport()->update();
}

/**
 * @internal
 */
void PaintArea::applyFilter(QAction *act)
{
	QList<QGraphicsItem *> selected = d->photogram->selectedItems();
	
	if( selected.isEmpty() ) return;
	
	QGraphicsItem *item = selected.first();
	QString filter = act->data().toString();
	Model::Frame *frame = currentFrame();
	
	
	dfDebug << "Applying filter: " << filter;
	if( frame && item && !filter.isEmpty())
	{
		Model::Object *object = this->object(item);
		
		if( object )
		{
			YAMF::Effect::Filter *effect = YAMF::Effect::Factory::create(filter);
			
			d->effectHandler->config(object, d->effectsConfigBar);
			
			
			YAMF::Command::AddItemFilter *cmd = new YAMF::Command::AddItemFilter(object, effect);
			addCommand(cmd);
		}
	}
}

void PaintArea::removeCurrentFilter()
{
	if( d->photogram->selectedItems().isEmpty() )
	{
		return;
	}
	
	QGraphicsItem *item = d->photogram->selectedItems().first();
	Model::Object *object = this->object(item);
	
	if( object )
	{
		object->removeFilter();
	}
}

void PaintArea::removeCurrentTweener()
{
	if( d->photogram->selectedItems().isEmpty() )
	{
		return;
	}
	
	QGraphicsItem *item = d->photogram->selectedItems().first();
	Model::Object *object = this->object(item);
	
	if( object )
	{
		if( object->tweener() )
		{
			object->deleteTweener();
		}
	}
}

void PaintArea::wheelEvent(QWheelEvent *event)
{
	if(event->modifiers () == Qt::ControlModifier)
	{
		scaleView(pow(( double)2, - event->delta() / 240.0));
	}
	else
	{
		QGraphicsView::wheelEvent(event);
	}
}

bool PaintArea::viewportEvent(QEvent *e)
{
	bool ret = QGraphicsView::viewportEvent(e);
	
	if( e->type() == QEvent::Show )
	{
		if( d->photogram->items().isEmpty() )
		{
			d->photogram->drawCurrentPhotogram();
		}
	}
	
	return ret;
}

/**
 * @~spanish
 * Escala la vista sobre el fotograma en un factor  @p scaleFactor.
 */
void PaintArea::scaleView(double scaleFactor)
{
	qreal factor = matrix().scale(scaleFactor, scaleFactor).mapRect(QRectF(0, 0, 1, 1)).width();
	if (factor < 0.07 || factor > 100)
		return;
	
	
	d->lastScaleFactor *= scaleFactor;
	scale(scaleFactor, scaleFactor);
	
	emit scaled(d->lastScaleFactor);
}

/**
 * @~spanish
 * Hace aumento sobre el fotograma actual en un porcentaje  @p percent.
 */
void PaintArea::setZoom(int percent)
{
	double factor = (double)percent / 100.0;
	
	if( factor == d->lastScaleFactor ) return;
	
	double restoreFactor = 1.0/d->lastScaleFactor;
	
	scale(restoreFactor, restoreFactor);
	
	d->lastScaleFactor = factor;
	
	scale(factor, factor);
	
	emit scaled(factor);
}

/**
 * @~spanish
 * Rota la vista sobre el fotograma.
 */
void PaintArea::setRotationAngle(int angle)
{
	rotate(angle - d->angle );
	d->angle = angle;
}

/**
 * @~spanish
 * Retorna el manejador de brochas del area de dibujo.
 */
BrushManager *PaintArea::brushManager() const
{
	return d->photogram->brushManager();
}

/**
 * @~spanish
 * Retorna el area en la cual va aparecer en la animaciï¿½n.
 */
QRectF PaintArea::drawingRect() const
{
	return d->drawingRect;
}

/**
 * @~spanish
 * Retorna el fotograma actual.
 */
Photogram *PaintArea::photogram() const
{
	return d->photogram;
}

/**
 * @~spanish
 * Cambia el frame actual.
 */
void PaintArea::setCurrentFrame(int sceneIndex, int layerIndex, int frameIndex)
{
	Model::Scene *scene = d->project->scene(sceneIndex);
	{
		d->photogram->setCurrentScene(scene);
		d->photogram->setCurrent(frameIndex);
		d->framePosition.layer = layerIndex;
		d->framePosition.frame = frameIndex;
	}
	d->photogram->drawCurrentPhotogram();
	viewport()->update();
}

/**
 * @~spanish
 * Retorna el proyecto asociado al area de dibujo.
 */
Model::Project *PaintArea::project() const
{
	return d->project;
}

/**
 * @~spanish
 * Retorna la escena actual.
 */
Model::Scene *PaintArea::currentScene() const
{
	return d->photogram->scene();
}

/**
 * @~spanish
 * Retorna la capa actual.
 */
Model::Layer *PaintArea::currentLayer() const
{
	Model::Scene *scene = d->photogram->scene();
	
	if( scene )
	{
		return scene->layer(d->framePosition.layer);
	}
	
	
	return 0;
}

/**
 * @~spanish
 * Retorna el frame actual.
 */
Model::Frame *PaintArea::currentFrame() const
{
	Model::Layer *layer = currentLayer();
	
	if( layer )
	{
		return layer->frame(d->framePosition.frame);
	}
	
	
	return 0;
}

Model::Object *PaintArea::object(QGraphicsItem *item) const
{
	return currentFrame()->findGraphic(item);
}

/**
 * @~spanish
 * 
 */
void PaintArea::addCommand(Command::Base *command)
{
	d->project->commandManager()->addCommand(command);
}

/**
 * @~spanish
 * Retorna la historia de comandos
 * @return 
 */
DGui::CommandHistory *PaintArea::history() const
{
	return d->history;
}

/**
 * @~spanish
 * Retorna la acción que permite remover los elementos seleccionados en el area.
 * @return 
 */
QAction *PaintArea::removeAction()
{
	return d->actionManager.find("remove", "edit");
}

/**
 * @~spanish
 * Retorna la acción que permite agrupar los elementos seleccionados en el area.
 * @return 
 */
QAction *PaintArea::groupAction()
{
	return d->actionManager.find("group", "object");
}

/**
 * @~spanish
 * Retorna la acción que permite des-agrupar los elementos seleccionados en el area.
 * @return 
 */
QAction *PaintArea::ungroupAction()
{
	return d->actionManager.find("ungroup", "object");
}

/**
 * @~spanish
 * Retorna la acción que permite enviar al fondo los elementos seleccionados en el area.
 * @return 
 */
QAction *PaintArea::sendToBackAction()
{
	return d->actionManager.find( "sendtoback", "object-order");
}

/**
 * @~spanish
 * Retorna la acción que permite enviar al frente los elementos seleccionados en el area.
 * @return 
 */
QAction *PaintArea::bringToFrontAction()
{
	return d->actionManager.find( "bringtofront", "object-order");
}


/**
 * @~spanish
 * Retorna la acción que permite enviar hacia atras una unidad los elementos seleccionados en el area.
 * @return 
 */
QAction *PaintArea::sendBackwardsAction()
{
	return d->actionManager.find( "sendbackwards", "object-order");
}

/**
 * @~spanish
 * Retorna la acción que permite enviar hacia adelante una unidad los elementos seleccionados en el area.
 */
QAction *PaintArea::bringForwardsAction()
{
	return d->actionManager.find( "bringforwards", "object-order");
}

/**
 * @~spanish
 * Retorna la acción que permite copiar los elementos seleccionados en el area.
 */
QAction *PaintArea::copyAction()
{
	return d->actionManager.find( "copy", "edit");
}

/**
 * @~spanish
 * Retorna la acción que permite pegar los elementos que estan en el portapapeles en el area.
 */
QAction *PaintArea::pasteAction()
{
	return d->actionManager.find( "paste", "edit");
}

/**
 * @~spanish
 * Retorna la acción que permite cortar los elementos seleccionados en el area.
 */
QAction *PaintArea::cutAction()
{
	return d->actionManager.find( "cut", "edit");
}

/**
 * @~spanish
 * Retorna la acción que permite seleccionar todos los elementos en el area.
 */
QAction *PaintArea::selectAllAction()
{
	return d->actionManager.find("selectall", "edit");
}

/**
 * @~spanish
 * Crea el menu contextual de el area de dibujo.
 */
QMenu *PaintArea::createMenu()
{
	QMenu *menu = new QMenu(tr("Drawing area"), this);
	
// 	Command::Manager *manager = d->project->commandManager();
	
	menu->addAction(d->history->undoAction()->icon(), d->history->undoAction()->text(), d->history, SLOT(undo()))->setEnabled(d->history->undoAction()->isEnabled());
	menu->addAction(d->history->redoAction()->icon(), d->history->redoAction()->text(), d->history, SLOT(redo()))->setEnabled(d->history->redoAction()->isEnabled());
	
	menu->addSeparator();
	d->actionManager.setupMenu(menu, "edit", false);
	menu->addSeparator();
	
	QMenu *order = new QMenu(tr("Order"));
	d->actionManager.setupMenu(order, "object-order");
	
	menu->addMenu (order);
	menu->addSeparator();
	
	if ( AbstractTool *tool = d->photogram->currentTool() )
	{
		if( QMenu *toolMenu = tool->menu() )
		{
			menu->addSeparator();
			menu->addMenu(toolMenu);
		}
	}
	
	return menu;
}

/**
 * @~spanish
 * Retorna la barra de herramientas que provee la interfaz grafica para la configuraciï¿½n de las herramientas.
 */
QToolBar *PaintArea::toolsConfigBar() const
{
	return d->toolsConfigBar;
}

/**
 * @~spanish
 * Retorna la barra de herramientas que provee la interfaz grafica para la configuraciï¿½n de los efectos.
 */
QToolBar *PaintArea::effectsConfigBar() const
{
	return d->effectsConfigBar;
}

void PaintArea::checkActions()
{
	if(! scene()->selectedItems().isEmpty())
	{
		d->actionManager.setEnabled("object-order", true);
		d->actionManager.find("remove", "edit")->setEnabled(true);
		d->actionManager.find("cut", "edit")->setEnabled(true);
		d->actionManager.find("copy", "edit")->setEnabled(true);
		d->actionManager.find("addtolibrary", "library")->setEnabled(true);
	}
	else
	{
		d->actionManager.setEnabled("object-order", false);
		d->actionManager.find("remove", "edit")->setEnabled(false);
		d->actionManager.find("cut", "edit")->setEnabled(false);
		d->actionManager.find("copy", "edit")->setEnabled(false);
		d->actionManager.find("addtolibrary", "library")->setEnabled(false);
	}
	
	QString xml = QApplication::clipboard()->mimeData()->data("yamf-graphics");
	if ( !xml.isEmpty() )
	{
		d->actionManager.find("paste", "edit")->setEnabled(true);
	}
	else
	{
		d->actionManager.find("paste", "edit")->setEnabled(false);
	}
}

/**
 * @~spanish
 * Remueve los elementos seleccionados.
 */
void PaintArea::removeSelectedObjects()
{
	QList<QGraphicsItem *> items = d->photogram->selectedItems();
	if( items.count() > 0 )
	{
		foreach(QGraphicsItem *item, items)
		{
			Model::Object *object = currentFrame()->findGraphic(item);
			if( object )
			{
				object->frame()->removeItem(item);
			}
		}
		
// 		currentFrame()->removeItems(items);
		this->checkActions();
	}
}


/**
 * @~spanish
 * Agrupa los elementos seleccionados.
 */
void PaintArea::groupSelectedObjects()
{
	QList<QGraphicsItem *> items = d->photogram->selectedItems();
	if( items.count() > 0 )
	{
		addCommand(new Command::GroupItem(items, currentFrame()));
	}
}

/**
 * @~spanish
 * Desagrupa los elementos seleccionados.
 */
void PaintArea::ungroupSelectedObject()
{
	QList<QGraphicsItem *> items = d->photogram->selectedItems();
	QList<Item::Group *> groups;
	
	Model::Frame *frame = currentFrame();
	
	foreach(QGraphicsItem *item, items)
	{
		if( Item::Group *group = qgraphicsitem_cast<Item::Group *>(item) )
		{
			addCommand(new Command::UngroupItem(group, frame));
		}
	}
}

/**
 * @~spanish
 * Envia atras los elementos seleccionados.
 */
void PaintArea::sendToBackSelectedObjects()
{
	QList<QGraphicsItem *> items = photogram()->selectedItems();
	
	DGraphics::Algorithm::sort(items, DGraphics::Algorithm::ZAxis);
	
	Model::Frame *frame = currentFrame();
	foreach(QGraphicsItem *item, items )
	{
		Model::Object *o = frame->findGraphic(item);
		if(o)
		{
			addCommand(new Command::SendToBackItem(o));
		}
	}
}

/**
 * @~spanish
 * Trae adelante los elementos seleccionados.
 */
void PaintArea::bringToFrontSelectedObjects()
{
	QList<QGraphicsItem *> items = photogram()->selectedItems();
	
	DGraphics::Algorithm::sort(items, DGraphics::Algorithm::ZAxis);
	
	QList<QGraphicsItem *>::iterator it = items.end();
	
	Model::Frame *frame = currentFrame();
	
	while(it != items.begin())
	{
		--it;
		Model::Object *o = frame->findGraphic(*it);
		if(o)
		{
			addCommand(new Command::BringToFrontItem(o));
		}
	}
}

/**
 * @~spanish
 * Envia atras una unidad los elementos seleccionados.
 */
void PaintArea::sendBackwardsSelectedObjects()
{
	QList<QGraphicsItem *> items = photogram()->selectedItems();
	
	DGraphics::Algorithm::sort(items, DGraphics::Algorithm::ZAxis);
	
	Model::Frame *frame = currentFrame();
	foreach(QGraphicsItem *item, items )
	{
		Model::Object *o = frame->findGraphic(item);
		if(o)
		{
			addCommand(new Command::SendBackwardsItem(o));
		}
	}
}


/**
 * @~spanish
 * Trae al frente una unidad los elementos seleccionados.
 */
void PaintArea::bringForwardsSelectedObjects()
{
	QList<QGraphicsItem *> items = photogram()->selectedItems();
	
	DGraphics::Algorithm::sort(items, DGraphics::Algorithm::ZAxis);
	
	QList<QGraphicsItem *>::iterator it = items.end();
	
	Model::Frame *frame = currentFrame();
	
	while(it != items.begin())
	{
		--it;
		Model::Object *o = frame->findGraphic(*it);
		if(o)
		{
			addCommand(new Command::BringForwardsItem(o));
		}
	}
}

/**
 * @~spanish
 * Copia los elementos seleccionados.
 */
void PaintArea::copySelectedObjects()
{
	QList<QGraphicsItem *> selecteds = scene()->selectedItems();
	
	QDomDocument orig;
	QDomElement root = orig.createElement("yamf-graphics");
	
	foreach(QGraphicsItem *item, selecteds)
	{
#if COPY_ONLY_ITEM
		if(YAMF::Common::AbstractSerializable *serializable = dynamic_cast<YAMF::Common::AbstractSerializable *>(item) )
		{
			root.appendChild(serializable->toXml(orig));
		}
#else
		Model::Object *graphic = currentFrame()->findGraphic(item);
		
		if( !graphic)
		{
			return;
		}
		
		root.appendChild(graphic->toXml( orig ));
		
#endif
	}
	
	orig.appendChild(root);
	
	QMimeData *mimeData = new QMimeData;
	
	mimeData->setData("yamf-graphics", orig.toString().toLocal8Bit());
	//TODO: image/svg+xml
	QApplication::clipboard()->setMimeData( mimeData );
}

/**
 * @~spanish
 * Pega los elementos en el portapapeles.
 */
void PaintArea::pasteObjects()
{
	if( !currentFrame() ) return;
	
	const QMimeData *mimeData = QApplication::clipboard()->mimeData();
	
	QString xml = mimeData->data("yamf-graphics");
	
	QDomDocument doc;
	if(!doc.setContent(xml))
	{
		return;
	}
	
	// TODO: image/svg+xml
	
	QDomElement docElem = doc.documentElement();
	QDomNode n = docElem.firstChild();
	
	while(!n.isNull())
	{
		QDomElement e = n.toElement();
		if(!e.isNull())
		{
			QString xml; 
			QTextStream ts(&xml);
			ts << e;
			
			Model::Object *object = new Model::Object(currentFrame());
			
#if COPY_ONLY_ITEM
			YAMF::Item::Builder builder;
			object->setItem(builder.create(xml));
#else
			object->fromXml(xml);
#endif
			
			currentFrame()->addObject(object);
		}
		n = n.nextSibling();
	}
	d->photogram->drawCurrentPhotogram();
}

/**
 * @~spanish
 * Corta los elementos seleccionados.
 */
void PaintArea::cutSelectedObjects()
{
	copySelectedObjects();
	removeSelectedObjects();
}

/**
 * @~spanish
 * Selecciona todos los elementos en el area.
 */
void PaintArea::selectAllObjects()
{
	foreach(QGraphicsItem *item, photogram()->items() )
	{
		item->setFlag(QGraphicsItem::ItemIsSelectable);
		item->setSelected(true);
	}
}

/**
 * @~spanish
 * Aï¿½ade lso elementos seleccionados a la biblioteca de objetos.
 */
void PaintArea::addToLibrary()
{
	QList<QGraphicsItem *> selecteds = scene()->selectedItems();
	
	if ( selecteds.isEmpty() )
	{
		DGui::Osd::self()->display(tr("No items selected"), DGui::Osd::Error);
		return;
	}
	
	YAMF::Drawing::Private::AddSymbolDialog addSymbolDialog;
	
	YAMF::Model::Library *library = d->project->library();
	
	QList<QGraphicsItem *> symbols;
	
	foreach(QGraphicsItem *item, selecteds)
	{
		YAMF::Common::AbstractSerializable *serializable = dynamic_cast<YAMF::Common::AbstractSerializable *>(item);
		if(  serializable && dynamic_cast<Model::LibraryItem *>(item) == 0)
		{
			Q_UNUSED(serializable);
			addSymbolDialog.addSymbol(item);
		}
	}
	
	if( addSymbolDialog.exec() == QDialog::Accepted )
	{
		for(int i = 0; i < addSymbolDialog.symbolCount(); i++)
		{
			library->addItemSymbol(addSymbolDialog.symbol(i), addSymbolDialog.symbolName(i));
		}
	}
}

void PaintArea::applyCurrentSymbol()
{
	if( d->photogram->selectedItems().count() > 0 )
	{
		if( Model::LibraryItem *libraryItem = qgraphicsitem_cast<Model::LibraryItem *>( d->photogram->selectedItems().first()) )
		{
			YAMF::Command::ModifySymbol *cmd = new YAMF::Command::ModifySymbol(libraryItem);
			
			addCommand(cmd);
		}
	}
}

/**
 * @~spanish
 * Aï¿½ade un simbolo de la biblioteca identificado con @p id a el area .
 */
void PaintArea::addSymbol(const QString &id)
{
	YAMF::Model::LibraryObject *object = d->project->library()->findObject(id);
	if( object )
	{
		addSymbol(object);
	}
}

/**
 * @~spanish
 * Aï¿½ade un simbolo @p object de la biblioteca a el area .
 */
void PaintArea::addSymbol(YAMF::Model::LibraryObject *object)
{
	YAMF::Model::Frame *frame = currentFrame();
	YAMF::Model::Scene *scene = currentScene();
	
	switch(object->type())
	{
		case YAMF::Model::LibraryObject::Sound:
		{
			Model::LibrarySoundObject *soundObject = dynamic_cast<Model::LibrarySoundObject *>(object);
			if ( scene != 0 )
			{
				YAMF::Model::AudioLayer *layer = scene->createAudioLayer();
				layer->fromSymbol(soundObject);
			}
		}
		break;
		case YAMF::Model::LibraryObject::Svg:
		case YAMF::Model::LibraryObject::Item:
		case YAMF::Model::LibraryObject::Image:
		case YAMF::Model::LibraryObject::Text:
		{
			if( frame != 0 )
			{
				YAMF::Model::LibraryItem *item = new YAMF::Model::LibraryItem(object);
				frame->addItem(item);
			}
		}
		break;
	}
}

}
}

