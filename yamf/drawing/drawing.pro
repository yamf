TEMPLATE = lib
DEFINES += _BUILD_YAMF_ 

CONFIG += warn_on \
dll
SOURCES += paintarea.cpp \
photogram.cpp \
private/paintarearotator.cpp  \
private/guide.cpp \
private/ruler.cpp \
abstracttool.cpp \
toolplugin.cpp \
brushmanager.cpp \
view.cpp \
private/addsymboldialog.cpp \
private/paintareapanel.cpp  \
private/effecthandler.cpp
HEADERS += paintarea.h \
photogram.h \
private/paintarearotator.h \
private/guide.h \
private/ruler.h \
abstracttool.h \
toolplugin.h \
brushmanager.h \
view.h \
private/addsymboldialog.h \
private/paintareapanel.h  \
private/effecthandler.h

include(tools.pri)

INCLUDEPATH += ..

include(../../config.pri)

TARGET = yamf_drawing
LIBS += -L$$DESTDIR -lyamf_common -lyamf_item -lyamf_effect -lyamf_model  

INSTALLS += target headers
target.path = /lib/

headers.path = /include/yamf/drawing
headers.files = *.h

FORMS += paintarea_panel.ui \
addsymboldialog.ui

link_with(dlib)


