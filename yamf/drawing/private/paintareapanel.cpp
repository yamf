/***************************************************************************
 *   Copyright (C) 2007 David Cuadrado                                     *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/

#include "paintareapanel.h"

#include "ui_paintarea_panel.h"

#include "drawing/paintarea.h"
#include "drawing/brushmanager.h"

#include <QMenu>

namespace YAMF {

namespace Drawing {
namespace Private {

class PaintAreaPanel::IconDrawer
{
	public:
	QPixmap paint(Qt::PenStyle style)
	{
		QPixmap icon(128, 16);
		icon.fill(Qt::transparent);
		QPainter painter(&icon);
		
		painter.setPen(QPen(Qt::black, 10, style));
		
		painter.drawLine(-4, 8, 128, 8);
		
		return icon;
	}
	
	QPixmap paint(Qt::PenCapStyle style)
	{
		QPixmap icon(32, 16);
		icon.fill(Qt::transparent);
		
		QPainter painter(&icon);
		
		painter.setPen(QPen(Qt::black, 10, Qt::SolidLine, style));
		
		painter.drawPoint(16,8);
		return icon;
	}
	
	QPixmap paint(Qt::PenJoinStyle style)
	{
		QPixmap icon(32, 16);
		icon.fill(Qt::transparent);
		
		QPainterPath path;
		path.moveTo(5, 16);
		path.lineTo(16, 5);
		path.lineTo(27, 16);
		
		QPainter painter(&icon);
		
		painter.setPen(QPen(Qt::black, 10, Qt::SolidLine, Qt::FlatCap, style));
		painter.drawPath(path);
		
		painter.drawPoint(16,8);
		return icon;
	}
};

PaintAreaPanel::PaintAreaPanel(YAMF::Drawing::PaintArea *paintArea, QWidget *parent)
 : QFrame(parent), m_paintArea(paintArea)
{
	builder = new Ui::PaintAreaPanel;
	
	this->setFrameStyle(QFrame::Panel | QFrame::Raised);
	this->setLineWidth(2);
	
	builder->setupUi(this);
	
#if 0
	builder->antialiasing->setChecked( m_paintArea->renderHints() & QPainter::Antialiasing);
	connect(builder->antialiasing, SIGNAL(toggled(bool)), m_paintArea, SLOT(setAntialiasing(bool)));
	
#ifndef QT_OPENGL_LIB
	builder->openGL->setEnabled(false);
#else
	builder->openGL->setChecked( false );
	connect(builder->openGL, SIGNAL(toggled(bool)), m_paintArea, SLOT(setUseOpenGL(bool)));
#endif
#endif
	
	for(int zoom = 25; zoom < 100; zoom += 25 )
	{
		builder->zoom->addItem(QString::number(zoom));
	}
	
	for(int zoom = 100; zoom < 400; zoom += 100 )
	{
		builder->zoom->addItem(QString::number(zoom));
	}
	
	builder->zoom->setCurrentIndex(builder->zoom->findText("100"));
	
	connect(builder->zoom, SIGNAL(activated(int)), this, SLOT(onZoomSelected(int)));
	
	for(int angle = 0; angle <= 360; angle+=30)
	{
		builder->rotation->addItem(QString::number(angle));
	}
	connect( builder->rotation, SIGNAL(activated(int)), this, SLOT(onRotationSelected(int)));
	connect(m_paintArea->brushManager(), SIGNAL(penChanged(const QPen &)), this, SLOT(setPen(const QPen &)));
	connect(m_paintArea->brushManager(), SIGNAL(brushChanged(const QBrush &)), this, SLOT(setBrush(const QBrush &)));
	
	
	{
		YAMF::Drawing::BrushManager *brushManager = m_paintArea->brushManager();
		IconDrawer iconDrawer;
		
		QPen currentPen = brushManager->pen();
		
		QMenu *style = new QMenu(tr("Pen style"));
		
		style->addAction(iconDrawer.paint(Qt::NoPen), tr("No pen"))->setData(Qt::NoPen);
		style->addAction(iconDrawer.paint(Qt::SolidLine), tr("Solid"))->setData( Qt::SolidLine);
		style->addAction(iconDrawer.paint(Qt::DashLine), tr("Dash"))->setData( Qt::DashLine );
		style->addAction(iconDrawer.paint(Qt::DotLine), tr("Dot"))->setData( Qt::DotLine );
		style->addAction(iconDrawer.paint(Qt::DashDotLine), tr("Dash dot"))->setData( Qt::DashDotLine);
		style->addAction(iconDrawer.paint(Qt::DashDotDotLine), tr("Dash dot dot"))->setData( Qt::DashDotDotLine);
		
		QMenu *capStyle = new QMenu(tr("Pen cap style"));
		capStyle->addAction( iconDrawer.paint(Qt::FlatCap), tr("Flat"))->setData(Qt::FlatCap);
		capStyle->addAction(iconDrawer.paint(Qt::SquareCap), tr("Square"))->setData(Qt::SquareCap);
		capStyle->addAction(iconDrawer.paint(Qt::RoundCap), tr("Round"))->setData(Qt::RoundCap);
		
		QMenu *joinStyle = new QMenu(tr("Pen join style"));
		joinStyle->addAction(iconDrawer.paint(Qt::MiterJoin), tr("Miter"))->setData(Qt::MiterJoin);
		joinStyle->addAction(iconDrawer.paint(Qt::BevelJoin), tr("Bevel"))->setData(Qt::BevelJoin);
		joinStyle->addAction(iconDrawer.paint(Qt::RoundJoin), tr("Round"))->setData(Qt::RoundJoin);
		
		builder->style->setMenu(style);
		builder->cap->setMenu(capStyle);
		builder->join->setMenu(joinStyle);
		
		builder->color->setBackground(brushManager->brush());
		builder->color->setForeground(brushManager->penBrush());
		
		setPen(currentPen);
		
		connect(style, SIGNAL(triggered(QAction *)), this, SLOT(onStyleChanged(QAction *)));
		connect(capStyle, SIGNAL(triggered(QAction *)), this, SLOT(onCapStyleChanged(QAction *)));
		connect(joinStyle, SIGNAL(triggered(QAction *)), this, SLOT(onJoinStyleChanged(QAction *)));
		connect(builder->penWidth, SIGNAL(valueChanged( int )), this, SLOT(onSetThickness(int)));
		
		connect(builder->color, SIGNAL(foregroundChanged(const QBrush &)), this, SLOT(onForegroundChanged(const QBrush &)));
		connect(builder->color, SIGNAL(backgroundChanged(const QBrush &)), this, SLOT(onBackgroundChanged(const QBrush &)));
	}
}


PaintAreaPanel::~PaintAreaPanel()
{
	delete builder;
}

void PaintAreaPanel::setCurrentBrush(const QBrush &brush)
{
	if( builder->color->current() == DGui::DualColorButton::Background )
	{
		m_paintArea->brushManager()->setBrush(brush);
	}
	else
	{
		QPen pen = m_paintArea->brushManager()->pen();
		pen.setBrush(brush);
		m_paintArea->brushManager()->setPen(pen);
	}
}

void PaintAreaPanel::setBrush(const QBrush &brush)
{
	builder->color->blockSignals(true);
	builder->color->setBackground(brush);
	builder->color->blockSignals(false);
}

void PaintAreaPanel::setPen(const QPen &pen)
{
	builder->color->blockSignals(true);
	builder->color->setForeground(pen.brush());
	builder->color->blockSignals(false);
	
	builder->penWidth->blockSignals(true);
	builder->penWidth->setValue(pen.width());
	builder->penWidth->blockSignals(false);
	
	foreach(QAction *action, builder->style->menu()->actions() )
	{
		if( action->data() == pen.style() )
		{
			builder->style->setDefaultAction(action);
			break;
		}
	}
	
	foreach(QAction *action, builder->join->menu()->actions() )
	{
		if( action->data() == pen.joinStyle() )
		{
			builder->join->setDefaultAction(action);
			break;
		}
	}
	
	foreach(QAction *action, builder->cap->menu()->actions() )
	{
		if( action->data() == pen.capStyle() )
		{
			builder->cap->setDefaultAction(action);
			break;
		}
	}
}

void PaintAreaPanel::onRotationSelected(int index)
{
	int angle = builder->rotation->itemText(index).toInt();
	m_paintArea->setRotationAngle(angle);
}

void PaintAreaPanel::onZoomSelected(int index)
{
	int percent = builder->zoom->itemText(index).toInt();
	
	m_paintArea->setZoom(percent);
}

void PaintAreaPanel::onBackgroundChanged(const QBrush &bg)
{
	m_paintArea->brushManager()->setBrush(bg);
}

void PaintAreaPanel::onForegroundChanged(const QBrush &fg)
{
	YAMF::Drawing::BrushManager *manager = m_paintArea->brushManager();
	QPen pen = manager->pen();
	pen.setBrush(fg);
	
	manager->setPen(pen);
}

void PaintAreaPanel::onSetThickness(int tickness)
{
	YAMF::Drawing::BrushManager *manager = m_paintArea->brushManager();
	QPen pen = manager->pen();
	pen.setWidth(tickness);
	
	manager->setPen(pen);
}

void PaintAreaPanel::onStyleChanged(QAction *action)
{
	builder->style->setDefaultAction(action);
	
	YAMF::Drawing::BrushManager *manager = m_paintArea->brushManager();
	QPen pen = manager->pen();
	pen.setStyle( Qt::PenStyle(action->data().toInt()) );
	manager->setPen(pen);
}

void PaintAreaPanel::onCapStyleChanged( QAction *action )
{
	builder->cap->setDefaultAction(action);
	
	YAMF::Drawing::BrushManager *manager = m_paintArea->brushManager();
	QPen pen = manager->pen();
	pen.setCapStyle( Qt::PenCapStyle(action->data().toInt()));
	manager->setPen(pen);
}

void PaintAreaPanel::onJoinStyleChanged( QAction *action )
{
	builder->join->setDefaultAction(action);
	
	YAMF::Drawing::BrushManager *manager = m_paintArea->brushManager();
	QPen pen = manager->pen();
	pen.setJoinStyle( Qt::PenJoinStyle(action->data().toInt()));
	manager->setPen(pen);
}


}

}

}
