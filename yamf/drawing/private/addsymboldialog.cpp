/***************************************************************************
 *   Copyright (C) 2007 David Cuadrado                                     *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/


#include "addsymboldialog.h"

#include "ui_addsymboldialog.h"

#include <dcore/debug.h>

#include <QGraphicsItem>

#include "yamf/item/preview.h"

namespace YAMF {
namespace Drawing {
namespace Private {

AddSymbolDialog::AddSymbolDialog(QWidget *parent)
 : QDialog(parent)
{
	builder = new Ui::AddSymbolDialog;
	builder->setupUi(this);
	
	connect(builder->toolBox, SIGNAL(currentChanged(int)), this, SLOT(onSymbolChanged(int)));
	connect(builder->symbolName, SIGNAL(textEdited(const QString &)), this, SLOT(setSymbolName(const QString &)));
	connect(builder->buttonBox, SIGNAL(accepted()), this, SLOT(checkNames()));
}


AddSymbolDialog::~AddSymbolDialog()
{
	delete builder;
}

void AddSymbolDialog::addSymbol(QGraphicsItem *symbol)
{
	YAMF::Item::Preview *preview = new YAMF::Item::Preview;
	preview->render( symbol );
	
	QWidget *container = new QWidget;
	QVBoxLayout *layout = new QVBoxLayout(container);
	layout->addWidget(preview);
	
	int index = builder->toolBox->addItem(container, tr("Symbol %1").arg(builder->toolBox->count()+1));
	
	m_symbols.insert(index, symbol);
}

QGraphicsItem *AddSymbolDialog::symbol(int pos) const
{
	return m_symbols.value(pos);
}

QString AddSymbolDialog::symbolName(int pos) const
{
	return m_names.value(pos);
}

int AddSymbolDialog::symbolCount() const
{
	return builder->toolBox->count();
}


void AddSymbolDialog::onSymbolChanged(int index)
{
	builder->symbolName->setText(m_names.value(index));
}

void AddSymbolDialog::setSymbolName(const QString &name)
{
	int currentIndex = builder->toolBox->currentIndex();
	m_names[currentIndex] = name;
}

void AddSymbolDialog::checkNames()
{
	for(int i = 0; i < builder->toolBox->count(); i++)
	{
		if ( m_names.value(i).isEmpty())
		{
			builder->toolBox->setCurrentIndex (i);
			builder->symbolName->setFocus();
			return;
		}
	}
	
	accept();
}


}

}

}
