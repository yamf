/***************************************************************************
 *   Copyright (C) 2007 by Jorge Cuadrado                                  *
 *   kruadrosxx@gmail.com                                                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
 
#ifndef YAMFDRAWINGPRIVATEGUIDE_H
#define YAMFDRAWINGPRIVATEGUIDE_H

#include <QGraphicsItem>
#include <yamf/common/yamf_exports.h>

namespace YAMF {
namespace Drawing {
namespace Private {
/**
 * @ingroup drawing
 * @author Jorge Cuadrado \<kuadrosxx@gmail.com\>
*/
class YAMF_EXPORT Guide : public QGraphicsItem
{
	public:
		Guide(Qt::Orientation o, QGraphicsScene *scene);
		~Guide();
		
		QRectF boundingRect() const;
		void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0 );
		
		void setEnabledSyncCursor(bool enabled);
		
	protected:
		QVariant itemChange(GraphicsItemChange change, const QVariant & value);
		
// 		void hoverEnterEvent(QGraphicsSceneHoverEvent* event);OLD
		void mouseMoveEvent(QGraphicsSceneMouseEvent* event);
		
		bool sceneEvent(QEvent *e);
		
		
	private:
		struct Private;
		Private * const d;
		
		void syncCursor();
};
}
}
}

#endif
