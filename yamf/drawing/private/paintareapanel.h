/***************************************************************************
 *   Copyright (C) 2007 David Cuadrado                                     *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/


#ifndef YAMF_DRAWING_PRIVATEPAINTAREAPANEL_H
#define YAMF_DRAWING_PRIVATEPAINTAREAPANEL_H

#include <QFrame>

namespace Ui {
	class PaintAreaPanel;
}

namespace YAMF {
namespace Drawing {

class PaintArea;

namespace Private {

/**
 * @ingroup drawing
 * @author David Cuadrado <krawek@gmail.com>
*/
class PaintAreaPanel : public QFrame
{
	Q_OBJECT;
	
	public:
		PaintAreaPanel(YAMF::Drawing::PaintArea *paintArea, QWidget *parent = 0);
		~PaintAreaPanel();
		
		void setCurrentBrush(const QBrush &brush);
		
	public slots:
		void setBrush(const QBrush &brush);
		void setPen(const QPen &pen);
		
	private slots:
		void onRotationSelected(int index);
		void onZoomSelected(int index);
		void onForegroundChanged(const QBrush &fg);
		void onBackgroundChanged(const QBrush &bg);
		
		void onSetThickness(int tickness);
		void onStyleChanged(QAction *action);
		void onCapStyleChanged( QAction *action );
		void onJoinStyleChanged( QAction *action );
		
	private:
		Ui::PaintAreaPanel *builder;
		YAMF::Drawing::PaintArea *m_paintArea;
		
		class IconDrawer;
};

}

}

}

#endif
