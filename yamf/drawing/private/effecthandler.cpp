/***************************************************************************
 *   Copyright (C) 2007 David Cuadrado                                     *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/


#include "effecthandler.h"

#include <QToolBar>

#include "yamf/common/configbarbuilder.h"
#include "yamf/effect/filter.h"
#include "yamf/model/object.h"
#include "yamf/model/frame.h"
#include "yamf/model/scene.h"
#include "yamf/model/project.h"
#include "yamf/model/command/manager.h"


#include "yamf/model/command/modifyfilter.h"

#include <dcore/debug.h>

namespace YAMF {
namespace Drawing {

EffectHandler::EffectHandler(QObject *parent)
 : QObject(parent), m_currentObject(0)
{
	m_builder = new YAMF::Common::ConfigBarBuilder(this);
	
	connect(m_builder, SIGNAL(propertyChanged(const QString &, const QVariant &)), this, SLOT(setProperty(const QString &, const QVariant &)));
}


EffectHandler::~EffectHandler()
{
}

void EffectHandler::config(YAMF::Model::Object *object, QToolBar *toolBar)
{
	if( YAMF::Effect::Filter *filter = dynamic_cast<YAMF::Effect::Filter *>(object->item()) )
	{
		m_currentObject = object;
		m_builder->build(filter->properties(), toolBar);
	}
}

void EffectHandler::setProperty(const QString &key, const QVariant &value)
{
	if( m_currentObject )
	{
		Command::ModifyFilter *cmd = new Command::ModifyFilter(m_currentObject, key, value.toString());
		m_currentObject->frame()->scene()->project()->commandManager()->addCommand(cmd);
	}
}

}
}



