/***************************************************************************
 *   Copyright (C) 2007 David Cuadrado                                     *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/

 

#ifndef YAMF_DRAWINGEFFECTHANDLER_H
#define YAMF_DRAWINGEFFECTHANDLER_H

#include <QObject>

class QToolBar;

namespace YAMF {

namespace Common {
class ConfigBarBuilder;
}

namespace Effect {
	class Filter;
}

namespace Model {
	class Object;
}

namespace Drawing {

/**
	@author David Cuadrado <krawek@gmail.com>
*/
class EffectHandler : public QObject
{
	Q_OBJECT;
	public:
		EffectHandler(QObject *parent = 0);
		~EffectHandler();
		
		void config(YAMF::Model::Object *object, QToolBar *toolBar);
		
	private slots:
		void setProperty(const QString &key, const QVariant &value);
		
	private:
		YAMF::Common::ConfigBarBuilder *m_builder;
		YAMF::Model::Object *m_currentObject;
};

}

}

#endif
