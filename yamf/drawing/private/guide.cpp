/***************************************************************************
 *   Copyright (C) 2007 by David Cuadrado                                  *
 *   kruadrosxx@gmail.com                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "guide.h"

#include <QGraphicsScene>
#include <QPainter>
#include <QCursor>
#include <QGraphicsSceneMouseEvent>
#include <QGraphicsView>
#include <QApplication>

#include <dcore/debug.h>

namespace YAMF {
namespace Drawing {
namespace Private {
	
struct Guide::Private
{
	Private(): view(0) {};
	Qt::Orientation orientation;
	bool enabled;
	QGraphicsView *view;
};

Guide::Guide(Qt::Orientation o,QGraphicsScene *scene): QGraphicsItem(0, scene), d(new Private)
{
	d->orientation = o;
	d->enabled = true;
	setZValue(1000);
// 	setAcceptsHoverEvents(true);
// 	setAcceptedMouseButtons(0);
// 	setFlag(QGraphicsItem::ItemIsFocusable, false);
	setFlag(QGraphicsItem::ItemIgnoresTransformations, true);
}



Guide::~Guide()
{
	delete d;
}

QRectF Guide::boundingRect() const
{
	if(!d->view)
	{
		return QRectF(0,0,1,1);
	}
	
	QPointF init = d->view->mapToScene(QPoint(0,0));
	
	if(d->orientation == Qt::Vertical)
	{
		return QRectF( QPointF(0,init.y()) , QSizeF(5, d->view->height()*2));
	}
	else
	{
		return QRectF(QPointF(init.x(), 0), QSizeF(d->view->width()*2, 5));
	}
}

void Guide::paint( QPainter * painter, const QStyleOptionGraphicsItem * , QWidget *w )
{
// 	if(!d->view)
// 	{
		foreach(QGraphicsView *view, scene()->views()) // get current view
		{
			if(view->viewport() == w)
			{
				d->view = view;
			}
		}
// 	}
	
	painter->setPen(QPen(Qt::black, 1, Qt::DashLine));
	if(d->orientation == Qt::Vertical)
	{
		painter->drawLine((int)boundingRect().center().x(), (int)boundingRect().y(), (int)boundingRect().center().x(), (int)boundingRect().height());
	}
	else
	{
		painter->drawLine( (int)boundingRect().x(), (int)boundingRect().center().y(), (int)boundingRect().width(), (int)boundingRect().center().y());
	}
}

void Guide::setEnabledSyncCursor(bool enabled)
{
	d->enabled = enabled;
}

QVariant Guide::itemChange( GraphicsItemChange change, const QVariant & value )
{
	if(change == ItemPositionChange)
	{
		if(d->orientation == Qt::Vertical)
		{
			return QPointF(value.toPointF().x(), 0);
		}
		else
		{
			return QPointF(0, value.toPointF().y());
		}
	}
	return QGraphicsItem::itemChange(change, value );
}

// void Guide::hoverEnterEvent(QGraphicsSceneHoverEvent * e) OLD
// {
// 	QGraphicsSceneMouseEvent *event = new QGraphicsSceneMouseEvent(QEvent::GraphicsSceneMousePress);
// 	event->setButtons(Qt::LeftButton);
// 	event->setButton(Qt::LeftButton);
// 	
// 	mousePressEvent(event);
// 	
// 	delete event;
// 	
// 	setAcceptsHoverEvents(false);
// 	
// 	syncCursor();
// }

void Guide::mouseMoveEvent(QGraphicsSceneMouseEvent * e)
{
	if(d->enabled)
	{
		syncCursor();
	}
	else
	{
		setPos(e->scenePos());
	}
}

bool Guide::sceneEvent(QEvent *e)
{
// 	switch(e->type())
// 	{OLD
// 		case QEvent::GraphicsSceneMouseMove:
// 		case QEvent::GraphicsSceneHoverEnter:
// 		case QEvent::GraphicsSceneHoverLeave:
// 		case QEvent::GraphicsSceneHoverMove:
// 		{
// 			QGraphicsSceneMouseEvent *event = new QGraphicsSceneMouseEvent(QEvent::GraphicsSceneMousePress);
// 			event->setButtons(Qt::LeftButton);
// 			event->setButton(Qt::LeftButton);
// 			
// 			mousePressEvent(event);
// 			
// 			delete event;
// 		}
// 		break;
// 		default: break;
// 	}
	
	return QGraphicsItem::sceneEvent(e);
}

void Guide::syncCursor()
{
	QPointF globalPos;
	if(scene())
	{
		if(d->view)
		{
			globalPos = d->view->viewport()->mapToGlobal(scenePos().toPoint() + d->view->mapFromScene(QPointF(0,0)));
		}
	}
	
	double distance;
	if(d->orientation == Qt::Vertical)
	{
		distance = globalPos.x()+ 2 - QCursor::pos().x();
	}
	else
	{
		distance = globalPos.y() + 2 - QCursor::pos().y();
	}
	
	if( -QApplication::startDragDistance() < distance && distance < QApplication::startDragDistance() )
	{
		if(d->orientation == Qt::Vertical)
		{
			QCursor::setPos((int)globalPos.x()+2, (int)QCursor::pos().y()) ;
		}
		else
		{
			QCursor::setPos((int)QCursor::pos().x(), (int)globalPos.y()+2);
		}
	}
}

}
}
}

