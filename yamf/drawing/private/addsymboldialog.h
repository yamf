/***************************************************************************
 *   Copyright (C) 2007 David Cuadrado                                     *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/

 

#ifndef YAMF_DRAWING_PRIVATEADDSYMBOLDIALOG_H
#define YAMF_DRAWING_PRIVATEADDSYMBOLDIALOG_H

#include <QDialog>
#include <QHash>

namespace Ui {
	class AddSymbolDialog;
}

class QGraphicsItem;

namespace YAMF {
namespace Drawing {
namespace Private {

/**
 * @ingroup drawing
 * @author David Cuadrado <krawek@gmail.com>
*/
class AddSymbolDialog : public QDialog
{
	Q_OBJECT;
	
	public:
		AddSymbolDialog(QWidget *parent = 0);
		~AddSymbolDialog();
		
		void addSymbol(QGraphicsItem *symbol);
		
		QGraphicsItem *symbol(int pos) const;
		QString symbolName(int pos) const;
		int symbolCount() const;
		
	private slots:
		void onSymbolChanged(int index);
		void setSymbolName(const QString &name);
		void checkNames();
		
	private:
		Ui::AddSymbolDialog *builder;
		
		QHash<int, QGraphicsItem *> m_symbols;
		QHash<int, QString> m_names;
};

}

}

}

#endif
