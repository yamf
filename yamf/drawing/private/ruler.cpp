/***************************************************************************
 *   Copyright (C) 2007 by Jorge Cuadrado                                  *
 *   kuadrosxx@gmail.com                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
 
#include "ruler.h"
#include <QApplication>
#include <QMouseEvent>

namespace YAMF {
namespace Drawing {
namespace Private {

struct Ruler::Private
{
	QPointF oldPos;
	QPoint dragStartPosition;
};

Ruler::Ruler(QWidget *parent) : DGui::Ruler( parent), d(new Private)
{
	this->setDrawPointer(true);
}

Ruler::Ruler(Qt::Orientation orientation, QWidget *parent) : DGui::Ruler(orientation, parent), d(new Private)
{
	this->setDrawPointer(true);
}

Ruler::~Ruler()
{
	delete d;
}


void Ruler::mousePressEvent(QMouseEvent *event)
{
	if (event->button() == Qt::LeftButton)
		d->dragStartPosition = event->pos();
}

void Ruler::mouseMoveEvent(QMouseEvent *event)
{
	DGui::Ruler::mouseMoveEvent(event);
	
	if (!(event->buttons() & Qt::LeftButton))
	{
		return;
	}
	
	if ((event->pos() - d->dragStartPosition).manhattanLength()	< QApplication::startDragDistance())
	{
		return;
	}
	
	QDrag *drag = new QDrag(this);
	QMimeData *mimeData = new QMimeData;
	
	QString data;
	if(orientation() == Qt::Vertical)
	{
		data = "verticalLine";
	}
	else
	{
		data = "horizontalLine";
	}
	
	mimeData->setData("yamf-ruler", data.toAscii () );
	drag->setMimeData(mimeData);

	drag->start();
}

}
}
}

