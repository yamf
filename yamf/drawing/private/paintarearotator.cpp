/***************************************************************************
 *   Copyright (C) 2006 by David Cuadrado                                  *
 *   krawek@gmail.com                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "paintarearotator.h"
#include "paintarea.h"

#include <QTimer>

namespace YAMF {
namespace Drawing {
namespace Private {

struct PaintAreaRotator::Private
{
	int rotationAngle;
	PaintArea *view;
	
	QTimer timer;
};

PaintAreaRotator::PaintAreaRotator(QObject *parent, PaintArea *view) : QObject(parent), d(new Private)
{
	d->view = view;
	connect(&d->timer, SIGNAL(timeout()), this, SLOT(applyRotation()));
}


PaintAreaRotator::~PaintAreaRotator()
{
	delete d;
}


void PaintAreaRotator::rotateTo(int angle)
{
	d->rotationAngle = angle;
	
	if ( !d->timer.isActive() )
	{
		d->timer.start(50);
	}
}


void PaintAreaRotator::applyRotation()
{
	d->view->setRotationAngle(d->rotationAngle);
	d->timer.stop();
}

}
}
}

