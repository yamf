/***************************************************************************
 *   Copyright (C) 2006 by David Cuadrado                                  *
 *   krawek@gmail.com                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "abstracttool.h"

#include <QGraphicsView>

#include "photogram.h"

#include <dcore/debug.h>


namespace YAMF {
namespace Drawing {

struct AbstractTool::Private
{
	Private() : paintArea(0) {}
	PaintArea *paintArea;
};

/**
 * @~spanish
 * Constructor
 */
AbstractTool::AbstractTool(QObject * parent) : QObject(parent), d(new Private)
{
}


/**
 * Destructor
 */
AbstractTool::~AbstractTool()
{
	delete d;
}

void AbstractTool::setPaintArea(PaintArea *const paintArea)
{
	d->paintArea = paintArea;
}

PaintArea *AbstractTool::paintArea() const
{
	return d->paintArea;
}


/**
 * @~spanish
 * Se ejecuta en el momento que se selecciona la herramienta.
 */
void AbstractTool::init(Photogram *photogram)
{
	Q_UNUSED(photogram);
}

/**
 * TODO:
 */
void AbstractTool::updatePhotogram(Photogram *photogram)
{
	Q_UNUSED(photogram);
}

/**
 * @~spanish
 * Se ejecuta en un evento de doble click sobre el area de dibujo.
 */
void AbstractTool::doubleClick(const QGraphicsSceneMouseEvent *)
{
}

/**
 * @~spanish
 * Se ejecuta cuando se esta cambiando de herramienta.
 */
void AbstractTool::aboutToChangePhotogram(Photogram *const photogram)
{
	Q_UNUSED(photogram);
}

/**
 * @~spanish
 * Se ejecuta cuando se cambia de fotograma.
 */
void AbstractTool::photogramChanged(Photogram *const photogram)
{
	Q_UNUSED(photogram);
}

/**
 * @~spanish
 * Se ejecuta cuando se presiona una tecla.
 */
void AbstractTool::keyPressEvent(QKeyEvent *event)
{
	Q_UNUSED(event);
}

/**
 * TODO:
 * 
 */
QMenu *AbstractTool::menu() const
{
	return 0;
}

/**
 * @~spanish
 * Permite configurar el toobar para configurar la herramienta, en la barra de herramientas @p toolbar se espera que se provea una interfaz grafica para la configuracion de la herramienta.
 */
void AbstractTool::setupConfigBar(QToolBar *configBar)
{
	Q_UNUSED(configBar);
}

}
}

