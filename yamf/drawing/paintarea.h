/***************************************************************************
 *   Copyright (C) 2005 by Jorge Cuadrado                                  *
 *   kuadrosxx@gmail.com                                                   *
 *   Copyright (C) 2006 by David Cuadrado                                  *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef YAMF_DRAWINGPAINTAREA_H
#define YAMF_DRAWINGPAINTAREA_H

#include <QGraphicsView>
#include <yamf/common/yamf_exports.h>
#include <yamf/model/command/observer.h>

class QMenu;
class QToolBar;
class QUndoCommand;

namespace DGui {
	class CommandHistory;
}

namespace YAMF {

namespace Model {
	class Project;
	class Scene;
	class Layer;
	class Frame;
	class LibraryObject;
	class Object;
}

namespace Drawing {

// class Project;
class Photogram;
class AbstractTool;
class BrushManager;

namespace Private {
	class PaintAreaRotator;
}
	
/**
 * @ingroup drawing
 * @~spanish
 * @brief Esta clase provee de un area de dibujo:
 *  el area de dibujo permite:
 *  - Cambiar la forma de visualizar el actual fotograma.
 *  - Editar el contenido de la animación (agrupar y desagrupar elementos, eliminar elementos, copiar, cortar, pegar, cambiar la profundidad de los graficos, etc).
 *  - Modificar el contenido de la animación mediante herramientas Drawing::AbstractTool.
 * 
 * @author Jorge Cuadrado \<kuadrosxx@gmail.com\> - David Cuadrado \<krawek@gmail.com\>
*/
class YAMF_EXPORT PaintArea : public QGraphicsView
{
	Q_OBJECT;
	public:
		PaintArea(Model::Project *const project, QWidget * parent = 0);
		~PaintArea();
		
		void setAlwaysVisible(QGraphicsItem *item, bool v);
		void setCanvasColor(const QColor &color);
		QColor canvasColor() const;
		
		void setGridSeparation(double v);
		double gridSeparation() const;
		
	public slots:
		void setAntialiasing(bool use);
		void setUseOpenGL(bool opengl);
		void setDrawGrid(bool draw);
		void setDrawBorder(bool draw);
		
	public:
		void setTool(AbstractTool *tool);
		
		bool drawGrid() const;
		bool drawBorder() const;
		
		void scaleView(double scaleFactor);
		void setZoom(int percent);
		
		void setRotationAngle(int angle);
		
		BrushManager *brushManager() const;
		
		QRectF drawingRect() const;
		Photogram *photogram() const;
		
		void setCurrentFrame(int sceneIndex, int layerIndex, int frameIndex);
		
		Model::Project *project() const;
		Model::Scene *currentScene() const;
		Model::Layer *currentLayer() const;
		Model::Frame *currentFrame() const;
		Model::Object *object(QGraphicsItem *item) const;
		
		void addCommand(Command::Base *command);
		DGui::CommandHistory *history() const;
		
		QAction *removeAction();
		QAction *groupAction();
		QAction *ungroupAction();
		QAction *sendToBackAction();
		QAction *bringToFrontAction();
		QAction *sendBackwardsAction();
		QAction *bringForwardsAction();
		
		QAction *copyAction();
		QAction *pasteAction();
		QAction *cutAction();
		
		
		QAction *selectAllAction();
		virtual QMenu *createMenu();
		QToolBar *toolsConfigBar() const;
		QToolBar *effectsConfigBar() const;
		
		virtual void checkActions();
		
	public slots:
		void removeSelectedObjects();
		void groupSelectedObjects();
		void ungroupSelectedObject();
		
		void sendToBackSelectedObjects();
		void bringToFrontSelectedObjects();
		void sendBackwardsSelectedObjects();
		void bringForwardsSelectedObjects();
		
		void copySelectedObjects();
		void pasteObjects();
		void cutSelectedObjects();
		
		void selectAllObjects();
		
		void addToLibrary();
		void applyCurrentSymbol();
		void addSymbol(const QString &id);
		void addSymbol(YAMF::Model::LibraryObject *object);
		
	public:
		virtual void saveSettings();
		virtual void restoreSettings();
		
	private:
		void createActions();
		
	protected:
		virtual void mousePressEvent( QMouseEvent * event  );
		virtual void mouseMoveEvent( QMouseEvent * event );
		virtual void mouseReleaseEvent(QMouseEvent *event);
		virtual void tabletEvent( QTabletEvent * event );
		virtual void wheelEvent( QWheelEvent *event );
		
		virtual bool viewportEvent(QEvent *e);
		
		
	signals:
		void cursorMoved(const QPointF &pos);
		void originChanged(const QPointF &zero);
		void scaled(double factor);
		
	public slots:
		void centerDrawingArea();
		
	private slots:
		void onHistoryChanged();
		void applyFilter(QAction *act);
		void removeCurrentFilter();
		void removeCurrentTweener();
		
	protected:
		virtual void drawBackground(QPainter *painter, const QRectF &rect);
		virtual void drawForeground( QPainter *painter, const QRectF &rect );
		
		virtual bool canPaint() const;
		
	private:
		struct Private;
		Private *const d;
};

}
}

#endif
