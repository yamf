/***************************************************************************
 *   Copyright (C) 2005 by David Cuadrado                                  *
 *   krawek@gmail.com                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef ATOOLINTERFACE_H
#define ATOOLINTERFACE_H

#include <QStringList>
#include <QRect>
#include <QPoint>
#include <QPainter>
#include <QBrush>
#include <QPen>
#include <QPainterPath>
#include <QImage>
#include <QHash>
#include <QCursor>
#include <QMouseEvent>

#include <dgui/action.h>

#include "qplugin.h" // Q_EXPORT_PLUGIN

#include <common/yamf_exports.h>


namespace YAMF {
namespace Drawing {

/**
 * @author David Cuadrado <krawek@gmail.com>
*/

class YAMF_EXPORT ToolInterface
{
	public:
		virtual ~ToolInterface() {};
};

}
}

Q_DECLARE_INTERFACE( YAMF::Drawing::ToolInterface, "com.yamf.ifaces.ToolInterface/0.1" );


#endif


