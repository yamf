/***************************************************************************
 *   Copyright (C) 2007 David Cuadrado                                     *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/

 

#ifndef DRAWINGTOOLPLUGIN_H
#define DRAWINGTOOLPLUGIN_H

#include <yamf/drawing/abstracttool.h>
#include <yamf/drawing/toolinterface.h>

#include <yamf/common/yamf_exports.h>

namespace YAMF {
namespace Drawing {

/**
 * @ingroup drawing
 * @~spanish
 * @brief Interfaz que permite la creacion de plugins para las herramientas.
 * @author David Cuadrado <krawek@gmail.com>
*/
class YAMF_EXPORT ToolPlugin : public AbstractTool, public ToolInterface
{
	Q_OBJECT;
	Q_INTERFACES(YAMF::Drawing::ToolInterface);
	
	public:
		ToolPlugin(QObject *parent = 0);
		~ToolPlugin();
};

}
}

#endif
