/***************************************************************************
 *   Copyright (C) 2007 Jorge Cuadrado                                     *
 *   kuadrosxx@gmail.com                                                   *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/

#include "text.h"


#include <QPointF>
#include <QKeySequence>
#include <QKeyEvent>
#include <QMatrix>
#include <QGraphicsView>

#include <QGraphicsSceneMouseEvent>
#include <QGraphicsLineItem>
#include <QToolBar>

#include <QHBoxLayout>
#include <QLabel>

#include <dgraphics/algorithm.h>
#include <dcore/algorithm.h>
#include <dcore/debug.h>
#include <dgui/action.h>
#include <dgui/iconloader.h>

#include <drawing/photogram.h>
#include <drawing/brushmanager.h>
#include <drawing/paintarea.h>

#include <model/frame.h>
#include <model/command/changetext.h>
#include <model/command/changetextwidth.h>
#include <model/command/changetextfont.h>

#include <item/text.h>

#include <dgui/fontchooser.h>


namespace YAMF {

namespace Drawing {

namespace Tool {

struct Text::Private
{
	YAMF::Item::Text *text;
	QGraphicsLineItem *line;
	
	QPointF position;
	DGui::Action *action;
	DGui::FontChooser *font;
	
	bool editWidth;
	bool added;
	QString oldText;
	double oldWidth;
	
	PaintArea *paintArea;
};

/**
 * @ï¿½spanish
 * Construye la herramienta para insertar texto a la animaciï¿½n.
 */
Text::Text(QObject *parent) : AbstractTool(parent), d(new Private)
{
	d->action = new DGui::Action( DGui::IconLoader::self()->load("draw-text.svg"), tr("Text"), this);
	
	d->added = false;
	
	d->oldWidth = 0;
	d->font = new DGui::FontChooser;
	connect(d->font, SIGNAL(fontChanged()), this, SLOT(onChangeFont()));
	
	d->text = 0;
	
	d->editWidth = false;
}


/**
 * Destructor
 */
Text::~Text()
{
	delete d;
}

/**
 * @~spanish
 * Inicializa la herramienta, evita que los items dentro de el fotograma se puedan mover o seleccionar.
 */
void Text::init(Photogram *photogram)
{
	foreach(QGraphicsView * view, photogram->views())
	{
		view->setDragMode ( QGraphicsView::NoDrag );
		
		if ( QGraphicsScene *photogram = qobject_cast<QGraphicsScene *>(view->scene()) )
		{
			foreach(QGraphicsItem *item, photogram->items() )
			{
				item->setFlag(QGraphicsItem::ItemIsSelectable, false);
				item->setFlag(QGraphicsItem::ItemIsMovable, false);
				
				
				if(YAMF::Item::Text *text = static_cast< YAMF::Item::Text * >(item))
				{
					text->setFlags( QGraphicsItem::ItemIsSelectable | QGraphicsItem::ItemIsFocusable );
				}
			}
		}
	}
}

/**
 * @~spanish
 * Retorna el id de la herramienta.
 */
QString Text::id() const
{
	return tr("Text");
}

/**
 * @~spanish
 * Función sobrecargada para implementar la inserción de texto
 */
void Text::press(const QGraphicsSceneMouseEvent *input)
{
	PaintArea *paintArea = this->paintArea();
	if(input->buttons() == Qt::LeftButton)
	{
// 	QList<QGraphicsItem * > items = paintArea->scene()->selectedItems();
		QList<QGraphicsItem * > items = paintArea->items(paintArea->mapFromScene(input->scenePos()));
		
		foreach(QGraphicsItem *item, items)
		{
			if(item)
			{
				if(YAMF::Item::Text *text = static_cast< YAMF::Item::Text * >(item))
				{
					if(d->text)
					{
						d->text->setEditable(false);
					}
					d->text = text;
					d->text->setEditable(true);
					d->position = d->text->scenePos();
					d->text->setFocus();
					d->editWidth = true;
					d->oldWidth = d->text->textWidth();
					d->added = (paintArea->currentFrame()->visualIndexOf(item) != -1);
					return;
				}
			}
		}
		
		if(d->text)
		{
			d->text->setEditable(false);
			paintArea->setAlwaysVisible(d->text, false);
		}
		
		d->text = new Item::Text();
		
		d->added = false;
		connect(d->text, SIGNAL(edited()), this, SLOT( onEdited() ));
		d->oldText = "";
		d->editWidth = false;
		d->text->setFlags( QGraphicsItem::ItemIsSelectable | QGraphicsItem::ItemIsFocusable );
		
		d->position = input->scenePos();
		d->text->setPos(d->position);
		d->text->setFont(d->font->currentFont());
		
		d->text->setDefaultTextColor(paintArea->brushManager()->penBrush().color());
		
		d->line = new QGraphicsLineItem;
		d->line->setPen(QPen(Qt::red, 2));
		paintArea->photogram()->addItem(d->line);
	}
}

/**
 * @~spanish
 * Función sobrecargada para implementar la inserción de texto.
 */
void Text::move(const QGraphicsSceneMouseEvent *input)
{
	if(input->buttons() == Qt::LeftButton)
	{
		if(d->text)
		{
			if(d->editWidth)
			{
				double width = std::abs(d->position.x() - input->scenePos().x());
				d->text->setTextWidth( width );
			}
			else
			{
				d->line->setLine(QLineF(d->position, QPointF(d->line->mapFromScene(input->scenePos()).x(), d->position.y())));
			}
		}
	}
}

/**
 * @~spanish
 * Función sobrecargada para implementar la inserción de texto
 */
void Text::release(const QGraphicsSceneMouseEvent *input)
{
	PaintArea *paintArea = this->paintArea();
	
	if(!d->editWidth)
	{
		if(d->text)
		{
			double width = std::abs(d->position.x() - input->scenePos().x());
			d->text->setTextWidth( width );
			paintArea->photogram()->removeItem(d->line);
			
			paintArea->scene()->addItem(d->text);
			if(d->position.x() > input->scenePos().x())
			{
				d->text->setPos(d->text->scenePos() - QPointF(d->text->textWidth(), 0));
			}
			
			paintArea->setAlwaysVisible(d->text, true);
			d->text->setEditable(true);
			d->editWidth = true;
		}
	}
	else
	{
		if(d->added)
		{
			paintArea->addCommand(new Command::ChangeTextWidth(paintArea->currentFrame(), d->text, d->oldWidth));
			d->oldWidth = d->text->textWidth();
		}
	}
}

/**
 * @~spanish
 * Retorna la acción que representa la herramienta.
 */
DGui::Action *Text::action() const
{
	return d->action;
}

/**
 * @~spanish
 * Retorna que la herramienta es de tipo Brush.
 */
int Text::type() const
{
	return AbstractTool::Brush;
}

void Text::aboutToChangeTool()
{
	Q_CHECK_PTR(d->text);
	if(d->text)
	{
		if(!d->text->toPlainText().isEmpty())
		{
			d->text->setEditable(false);
			onEdited();
		}
		else
		{
			paintArea()->setAlwaysVisible(d->text, false);
		}
	}
}


void Text::aboutToChangePhotogram(Photogram *photogram)
{
}


/**
 * @~spanish
 * Asigna la interfaz gráfica para configurar el tipo de letra del texto.
 */
void Text::setupConfigBar(QToolBar *configBar)
{
	configBar->addWidget(d->font)->setVisible(true);
}

/**
 * @~spanish
 * Ignora el evento de presionado de teclas para poder escribir el esto.
 */
void Text::keyPressEvent(QKeyEvent *event)
{
	if(d->text)
	{
		if(!d->text->hasFocus())
		{
			d->text->setFocus();
			d->text->setEditable(true);
		}
	}
	event->ignore();
}

/**
 * @internal
 * @~spanish
 * Cambia el tipo de letra que se seleccione en el configurador.
 */
void Text::onChangeFont()
{
	if(d->text)
	{
		QFont oldFont = d->text->font();
		
		d->text->setFont(d->font->currentFont());
		
		d->text->setEditable(true);
		d->text->setFocus();
		d->text->setSelected(true);
		
		if(d->added && oldFont != d->text->font() && paintArea()->currentFrame()->visualIndexOf(d->text) != -1)
		{
			paintArea()->addCommand(new Command::ChangeTextFont( paintArea()->currentFrame(), d->text, oldFont));
		}
	}
}

void Text::onEdited()
{
	if(d->text)
	{
		if(!d->text->toPlainText().isEmpty() && !d->added && d->text->isVisible())
		{
			paintArea()->setAlwaysVisible(d->text, false);
			paintArea()->currentFrame()->addItem(d->text);
			d->text->setEditable(false);
			d->added = true;
		}
		else if(d->added)
		{
			paintArea()->addCommand(new Command::ChangeText( paintArea()->currentFrame(),  d->text, d->oldText));
		}
		else
		{
			d->text->setEditable(false);
		}
		d->oldText = d->text->toPlainText();
	}
}


}

}

}

