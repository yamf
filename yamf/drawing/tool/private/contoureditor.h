/***************************************************************************
 *   Copyright (C) 2007 Jorge Cuadrado                                     *
 *   kuadrosxx@gmail.com                                                   *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/

#ifndef YAMF_DRAWING_TOOLCONTOUREDITOR_H
#define YAMF_DRAWING_TOOLCONTOUREDITOR_H

#include <dgraphics/patheditor.h>

namespace YAMF {

namespace Item {
	class Proxy;
}

namespace Drawing {
namespace Tool {
namespace Private {

/**
 * @ingroup tools
 * @author Jorge Cuadrado <kuadrosxx@gmail.com>
*/
class ContourEditor : public DGraphics::PathEditor
{
	Q_OBJECT;
	public:
		ContourEditor(QGraphicsPathItem * parent, QGraphicsScene *scene);
		~ContourEditor();
		
		void save();
		void restore();
		QPainterPath oldPath();
		
		void setProxyItem( Item::Proxy * proxy );
		Item::Proxy * proxyItem();
		
	protected slots:
		void onItemChanged();
		void syncNodes(const QPainterPath &path);
		
		
	private:
		struct Private;
		Private *const d;
		
};

}
}
}
}

#endif
