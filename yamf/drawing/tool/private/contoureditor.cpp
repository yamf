/***************************************************************************
 *   Copyright (C) 2007 Jorge Cuadrado                                     *
 *   kuadrosxx@gmail.com                                                   *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/

#include "contoureditor.h"

#include <QPainterPath>
#include <QGraphicsPathItem>
#include <QGraphicsScene>
#include "item/proxy.h"

namespace YAMF {
namespace Drawing {
namespace Tool {
namespace Private {

struct ContourEditor::Private 
{
	QPainterPath path;
	Item::Proxy *proxy;
};

ContourEditor::ContourEditor(QGraphicsPathItem *parent, QGraphicsScene *scene)
 : DGraphics::PathEditor(parent, scene), d(new Private)
{
	d->proxy = 0;
	save();
	
	connect(this, SIGNAL(itemChanged()), this, SLOT(onItemChanged()));
}


ContourEditor::~ContourEditor()
{
	delete d;
}


void ContourEditor::save()
{
	d->path = item()->path();
	setModified(false);
}

void ContourEditor::restore()
{
	item()->setPath(d->path);
}

QPainterPath ContourEditor::oldPath()
{
	return d->path;
}

void ContourEditor::setProxyItem( Item::Proxy * proxy )
{
	d->proxy = proxy;
	if(proxy)
		setMatrix(proxy->sceneMatrix());
}

Item::Proxy * ContourEditor::proxyItem()
{
	return d->proxy;
}

void ContourEditor::onItemChanged()
{
	if(d->proxy)
	{
		d->proxy->update();
	}
}

void ContourEditor::syncNodes(const QPainterPath &path)
{
	if(d->proxy)
	{
		QPainterPath newPath = d->proxy->sceneMatrix().map(path);
		DGraphics::PathEditor::syncNodes(newPath);
	}
	else
	{
		DGraphics::PathEditor::syncNodes(path);
	}
}




}
}
}
}
