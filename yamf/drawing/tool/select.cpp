/***************************************************************************
 *   Copyright (C) 2007 Jorge Cuadrado                                     *
 *   kuadrosxx@gmail.com                                                   *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/

#include "select.h"

#include <QGraphicsSceneMouseEvent>
#include <QGraphicsItem>
#include <QList>

#include <dcore/debug.h>

#include <dgui/action.h>
#include <dgui/iconloader.h>

#include <dgraphics/itemtransformer.h>

#include <model/frame.h>

#include <drawing/photogram.h>
#include <drawing/paintarea.h>
#include <drawing/brushmanager.h>

#include "commonalgorithms.h"


namespace YAMF {
namespace Drawing {
namespace Tool {

struct Select::Private
{
	Private() : transformer(0) {}
	DGui::Action *action;
	QList<DGraphics::ItemTransformer *> transformers;
	
	DGraphics::ItemTransformer* transformer;
	
	void createTransfromers(QList<QGraphicsItem*> selecteds, PaintArea *paintArea);
};

void Select::Private::createTransfromers( QList<QGraphicsItem*> items, PaintArea *paintArea )
{
	CommonAlgorithms::createTransfromers(transformers, items, paintArea);
	
	/*QGraphicsScene *scene = paintArea->scene();
	
	QList<DGraphics::ItemTransformer *>::iterator it = transformers.begin();
	QList<DGraphics::ItemTransformer *>::iterator itEnd = transformers.end();
	while(it != itEnd)
	{
		int itemIndex = scene->selectedItems().indexOf((*it)->item() );
		
		if(itemIndex != -1 )
		{
			selecteds.removeAt(itemIndex);
		}
		else
		{
			DGraphics::ItemTransformer *transformer = transformers.takeAt(transformers.indexOf((*it)));
			foreach(QGraphicsItem *node, transformer->nodes())
			{
				paintArea->setAlwaysVisible(node, false);
			}
			delete transformer;
		}
		++it;
	}
	
	foreach(QGraphicsItem *item, selecteds)
	{
		if( paintArea->currentFrame()->logicalIndexOf(item) > -1 )
		{
			DGraphics::ItemTransformer *transformer = new DGraphics::ItemTransformer(item, scene);
			
			foreach(QGraphicsItem *node, transformer->nodes())
			{
				paintArea->setAlwaysVisible(node, true);
			}
			
			transformers << transformer;
		}
	}*/
}


/**
 * @~spanish
 * Construye la herramienta para transformar un objeto grafico.
 */
Select::Select(QObject *parent): Drawing::AbstractTool(parent), d(new Private)
{
	d->action = new DGui::Action( DGui::IconLoader::self()->load("tool-pointer.svg"), tr("Select"), this);
}


/**
 * Destructor
 */
Select::~Select()
{
	delete d;
}

/**
 * @~spanish
 * Inicializa la herramienta, permite que los items dentro de el fotograma se puedan seleccionar y mover.
 */
void Select::init(Photogram *photogram)
{
	foreach(QGraphicsView * view, photogram->views())
	{
		foreach(QGraphicsItem *item, view->scene()->items())
		{
			item->setFlags(QGraphicsItem::ItemIsSelectable);
			item->setFlag(QGraphicsItem::ItemIsMovable, true);
		}
	}
}

/**
 * @~spanish
 * Retorna el id de la herramienta.
 */
QString Select::id() const
{
	return "select";
}

/**
 * @~spanish
 * Función sobrecargada para implementar la transformación de los  objetos graficos.
 */
void Select::press(const QGraphicsSceneMouseEvent *input)
{
	PaintArea *paintArea = this->paintArea();
	
	paintArea->setDragMode (QGraphicsView::RubberBandDrag);
	if(d->transformer)
	{
		d->transformer = 0;
	}
	
	QGraphicsScene *scene = paintArea->scene();
	
	foreach(QGraphicsItem *item, scene->items())
	{
		item->setFlag(QGraphicsItem::ItemIsMovable, true);
		item->setFlag(QGraphicsItem::ItemIsSelectable, true);
	}
	
	if( input->modifiers() != Qt::ControlModifier )
	{
		foreach(DGraphics::ItemTransformer *transformer, d->transformers)
		{
			if(scene->mouseGrabberItem() == transformer->item())
			{
				switch(transformer->operation())
				{
					case DGraphics::ItemTransformer::Scale:
					{
						transformer->setOperation(DGraphics::ItemTransformer::Rotate);
					}
					break;
					case DGraphics::ItemTransformer::Rotate:
					{
						transformer->setOperation(DGraphics::ItemTransformer::Scale);
					}
					break;
				}
				
				d->transformer = transformer;
				break;
			}
		}
	}
	
}

/**
 * @~spanish
 * Función sobrecargada para implementar la transformación de los  objetos graficos.
 */
void Select::move(const QGraphicsSceneMouseEvent *input)
{
	if(d->transformer)
	{
		switch(d->transformer->operation())
		{
			case DGraphics::ItemTransformer::Scale:
			{
				d->transformer->setOperation(DGraphics::ItemTransformer::Rotate);
			}
			break;
			case DGraphics::ItemTransformer::Rotate:
			{
				d->transformer->setOperation(DGraphics::ItemTransformer::Scale);
			}
			break;
		}
		
		d->transformer = 0;
	}
}

/**
 * @~spanish
 * Función sobrecargada para implementar la transformación de los  objetos graficos.
 */
void Select::release(const QGraphicsSceneMouseEvent *input)
{
	PaintArea *paintArea = this->paintArea();
	QGraphicsScene *scene = paintArea->scene();
	
	if(scene->selectedItems().count() > 0)
	{
		QList<QGraphicsItem *> selecteds = scene->selectedItems();
		d->createTransfromers(selecteds, paintArea);
	}
	else
	{
		foreach(DGraphics::ItemTransformer *transformer, d->transformers)
		{
			foreach(QGraphicsItem *node, transformer->nodes())
			{
				paintArea->setAlwaysVisible(node, false);
			}
			delete transformer;
		}
		d->transformers.clear();
	}
}

/**
 * @~spanish
 * Función reimplemenatada para desaparecer los nodos cuando se cambia de fotograma.
 */
void Select::photogramChanged(Photogram *const photogram)
{
	foreach(DGraphics::ItemTransformer *transformer, d->transformers)
	{
		foreach(QGraphicsItem *node, transformer->nodes())
		{
			paintArea()->setAlwaysVisible(node, false);
		}
		delete transformer;
	}
	d->transformers.clear();
	
}

/**
 * @~spanish
 * Retorna la acción que representa la herramienta.
 */
DGui::Action *Select::action() const
{
	return d->action;
}

/**
 * @~spanish
 * Retorna que la herramienta es de tipo Selection.
 */
int Select::type() const
{
	return AbstractTool::Selection;
}

/**
 * @~spanish
 * Elimina los nodos cuando se cambia de herramienta.
 */
void Select::aboutToChangeTool()
{
	foreach(DGraphics::ItemTransformer *transformer, d->transformers)
	{
		foreach(QGraphicsItem *node, transformer->nodes())
		{
			paintArea()->setAlwaysVisible(node, false);
		}
		delete transformer;
	}
	d->transformers.clear();
}


}
}
}
