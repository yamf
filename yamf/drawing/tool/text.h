/***************************************************************************
 *   Copyright (C) 2007 Jorge Cuadrado                                     *
 *   kuadrosxx@gmail.com                                                   *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/

#ifndef YAMF_DRAWING_TOOLTEXT_H
#define YAMF_DRAWING_TOOLTEXT_H

#include <yamf/drawing/abstracttool.h>
#include <yamf/common/yamf_exports.h>

class QKeyEvent;

class QFont;

namespace YAMF {

namespace Drawing {

namespace Tool {

/**
 * @ingroup tools
 * @brief Esta clase provee de una herramienta para insertar texto a la animaciï¿½n.
 * @author Jorge Cuadrado <kuadrosxx@gmail.com>
*/
class YAMF_EXPORT Text : public Drawing::AbstractTool
{
	Q_OBJECT
	public:
		Text( QObject *parent = 0 );
		virtual ~Text();
		
		virtual void init(Photogram *photogram);
		
		virtual QString id() const;
		virtual void press(const QGraphicsSceneMouseEvent *input);
		virtual void move(const QGraphicsSceneMouseEvent *input);
		virtual void release(const QGraphicsSceneMouseEvent *input);
		
		virtual DGui::Action *action() const;
		
		int type() const;
		
		virtual void aboutToChangeTool();
		
		virtual void aboutToChangePhotogram(Photogram *photogram);
		
		void setupConfigBar(QToolBar *configBar);
		
		virtual void keyPressEvent(QKeyEvent *event);
		
	private slots:
		void onChangeFont();
		void onEdited();
		
	private:
		struct Private;
		Private *const d;
};

}

}

}

#endif
