/**************************************************************************
 *   Copyright (C) 2005 by David Cuadrado                                  *
 *   krawek@toonka.com                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "itween.h"

#include <QGraphicsView>
#include <QGraphicsSceneMouseEvent>
#include <QSet>
#include <QHBoxLayout>
#include <QRadioButton>
#include <QToolBar>
#include <QButtonGroup>
#include <QXmlStreamWriter>

#include <dcore/global.h>
#include <dcore/debug.h>

#include <dgui/iconloader.h>
#include <dgui/action.h>

#include <dgraphics/pathhelper.h>
#include <dgraphics/algorithm.h>

#include "yamf/model/object.h"
#include "yamf/model/frame.h"
#include "yamf/model/scene.h"

#include "yamf/drawing/paintarea.h"
#include "yamf/drawing/brushmanager.h"
#include "yamf/drawing/photogram.h"
#include "yamf/item/path.h"
#include "yamf/item/proxy.h"
#include "yamf/item/line.h"
#include "yamf/item/converter.h"
#include "yamf/item/tweener.h"
#include "yamf/item/serializer.h"

#include "yamf/model/command/addtweening.h"
#include "private/contoureditor.h"

#include <dgraphics/itemtransformer.h>

#include "commonalgorithms.h"

#define DEBUG 0

namespace YAMF {
namespace Drawing {
class BrushManager;

namespace Tool {

struct ITween::Private {
	Private() : target(0), nodeEditor(0), transformer(0), pathItem(0), paintArea(0) {}
	enum Type {
		PathMotion = 0x01,
		Fill,
		Transform,
		NodeMotion
	};
	
	DGui::Action * action;
	QWidget *configWidget;
	QButtonGroup group;
	
	Model::Object *target;
	
	QPointF currentPos;
	QPointF absItemPos;
	
	
	Drawing::Tool::Private::ContourEditor *nodeEditor;
	DGraphics::ItemTransformer* transformer;
	
	QGraphicsPathItem *pathItem;
	QPainterPath path;
	QPointF firstPos;
	QPointF lastPos;
	
	Drawing::PaintArea *paintArea;
	
	Item::Tweener *createObjectTweener(int frames = 1);
	void saveChanges();
	void releaseObject();
};

Item::Tweener *ITween::Private::createObjectTweener(int frames)
{
	Item::Tweener *tweener = target->tweener();
	
	if( tweener == 0 )
	{
		tweener = new Item::Tweener(1, target);
		
		tweener->setPosAt(0, target->item()->scenePos());
// 		tweener->setRotationAt(0, 0);
// 		tweener->setScaleAt(0, 1.0, 1.0);
		
// 		if( item()->type() != QAbstractGraphicsShapeItem::Type ) return;
		if( QAbstractGraphicsShapeItem *shape = dynamic_cast<QAbstractGraphicsShapeItem *>(target->item()) )
		{
			tweener->setBrushAt(0, shape->brush());
			tweener->setPenAt(0, shape->pen());
		}
		
		if( QGraphicsPathItem *path = dynamic_cast<QGraphicsPathItem*>(target->item()) )
		{
			tweener->setPathAt(0, path->path());
		}
		
		target->setTweener(tweener);
	}
	
	frames = qMax(frames, tweener->frames());
	tweener->setFrames(frames);
	
	return tweener;
}

void ITween::Private::saveChanges()
{
	if( target )
	{
		int objectPos = target->frame()->visualIndex();
		int framePos = paintArea->currentFrame()->visualIndex();
		int step = qAbs(framePos - objectPos);
		
		int frames = step;
		if( target->tweener() )
		{
			frames = qMax(step, target->tweener()->frames());
		}
		
		Item::Tweener *tweener = createObjectTweener(frames);
		
		switch(group.checkedId())
		{
			case Private::Transform:
			{
				if( currentPos != target->item()->pos() ) // The object was changed
				{
					tweener->setPosAt(step, target->item()->pos());
				}
				
				if( transformer )
				{
					if( transformer->isModified() )
					{
						switch(transformer->operation() )
						{
							case DGraphics::ItemTransformer::Scale:
							{
								tweener->setScaleAt(step, transformer->currentVerticalScale(), transformer->currentHorizontalScale() );
								
								tweener->setPosAt(step, transformer->currentPosition() );
							}
							break;
							case DGraphics::ItemTransformer::Rotate:
							{
								QPointF pos = transformer->currentPosition();
								tweener->setRotationAt(step, transformer->currentRotation());
								tweener->setPosAt(step,  pos );
							}
							break;
						}
						
	// 					transformer->setItem(target->item()); // To reset
					}
				}
			}
			break;
			case Private::PathMotion:
			{
				if( path.elementCount() < 3 ) break;
				
				QRectF br = target->item()->sceneBoundingRect();
				QPointF origin = absItemPos;
				
				QTransform transform;
				transform.translate(-origin.x(), -origin.y());
				
				double factor = 1.0/step;
				
				QPainterPath npath = transform.map(path);
				
				int n = 0;
				for(double percent = 0; percent < 1; percent += factor )
				{
					QPointF pos = npath.pointAtPercent(percent);
					tweener->setPosAt(n, pos);
					
					n++;
				}
				
#if DEBUG
				paintArea->currentFrame()->addItem(pathItem);
#endif
			}
			break;
			case Private::NodeMotion:
			{
				if( QGraphicsPathItem *item = dynamic_cast<QGraphicsPathItem *>(target->item()) )
				{
					tweener->setPathAt(step, item->path());
				}
			}
			break;
			case Private::Fill:
			{
				if( QAbstractGraphicsShapeItem *item = dynamic_cast<QAbstractGraphicsShapeItem *>(target->item()) )
				{
					tweener->setBrushAt(step, item->brush());
					tweener->setPenAt(step, item->pen());
				}
			}
			break;
			default:
			{
			}
			break;
		}
		
		paintArea->addCommand(new YAMF::Command::AddTweening(target, tweener));
		tweener->setStep(step);
	}
}

void ITween::Private::releaseObject()
{
	if( target )
	{
		if( transformer )
		{
			transformer->setVisible(false);
			foreach(QGraphicsItem *node, transformer->nodes())
			{
				paintArea->setAlwaysVisible(node, false);
			}
		}
		if(pathItem)
		{
			paintArea->setAlwaysVisible(pathItem, false);
			delete pathItem;
			pathItem = 0;
		}
		
		{
			if( nodeEditor )
			{
				foreach(QGraphicsItem *node, nodeEditor->nodes())
				{
					paintArea->setAlwaysVisible(node, false);
				}
				
				delete nodeEditor;
				nodeEditor = 0;
			}
		}
		
		target->scene()->setAlwaysVisible(target, false);
		if( target->tweener() )
		{
			target->tweener()->setFrames(target->tweener()->frames());
		}
		target = 0;
	}
}

ITween::ITween(QObject *parent) : AbstractTool(parent), d(new Private)
{
	d->action = new DGui::Action( DGui::IconLoader::self()->load("itween.svg"), tr("iTween"), this);
	
// 	d->action->setShortcut( QKeySequence(tr("Ctrl+W")) );
	
	d->configWidget = new QWidget;
	QHBoxLayout *layout = new QHBoxLayout(d->configWidget);
	layout->setMargin(0);
	layout->setSpacing(2);
	
	
	QRadioButton *transform = new QRadioButton(tr("Transform"));
	transform->setChecked(true);
	layout->addWidget(transform);
	
	QRadioButton *pathMotion = new QRadioButton(tr("Path motion"));
	layout->addWidget(pathMotion);
	
	QRadioButton *fill = new QRadioButton(tr("Fill"));
	layout->addWidget(fill);
	
	QRadioButton *nodeMotion = new QRadioButton(tr("Node motion"));
	layout->addWidget(nodeMotion);
	
	d->group.addButton(pathMotion, Private::PathMotion);
	d->group.addButton(fill, Private::Fill);
	d->group.addButton(transform, Private::Transform);
	d->group.addButton(nodeMotion, Private::NodeMotion);
	
	connect(&d->group, SIGNAL(buttonClicked(int)), this, SLOT(onChangeTool(int)));
}

ITween::~ITween()
{
	if( d->nodeEditor )
	{
		foreach(QGraphicsItem *node, d->nodeEditor->nodes())
		{
			paintArea()->setAlwaysVisible(node, false);
		}
		delete d->nodeEditor;
	}
	
	if( d->transformer )
	{
		foreach(QGraphicsItem *node, d->transformer->nodes())
		{
			paintArea()->setAlwaysVisible(node, false);
		}
		delete d->transformer;
	}
	
	if(d->pathItem)
	{
		paintArea()->setAlwaysVisible(d->pathItem, false);
		delete d->pathItem;
	}
	
	
	delete d;
}

void ITween::init(Photogram *photogram)
{
	foreach(QGraphicsItem *item, photogram->items() )
	{
		item->setFlag(QGraphicsItem::ItemIsSelectable, true);
		item->setFlag(QGraphicsItem::ItemIsFocusable, true);
		item->setFlag(QGraphicsItem::ItemIsMovable, true);
	}
}

QString ITween::id() const
{
	return "itween";
}

void ITween::press(const QGraphicsSceneMouseEvent *input)
{
	PaintArea *paintArea = this->paintArea(); // FIXME
	
	d->firstPos = input->scenePos();
	d->lastPos = input->scenePos();
	
	d->paintArea = paintArea;
	Photogram *photogram = paintArea->photogram();
	
	if( d->target )
	{
		if( !d->target->item()->isSelected() )
		{
			d->saveChanges();
			d->releaseObject();
		}
	}
	
	QList<QGraphicsItem *> selected = photogram->selectedItems();
	if( selected.isEmpty() ) return;
	
	Model::Object *object = paintArea->currentFrame()->graphic(selected.first());
	
	if( object == 0 )
	{
		// If the object has a tweener, it may be not in the current frame.
		object = paintArea->currentScene()->tweeningObject(selected.first());
		
		if( !object )
			return;
	}
	
	if( d->target != object )
	{
		if( d->target )
		{
			d->target->scene()->setAlwaysVisible(d->target, false);
		}
		
		d->target = object;
		d->target->scene()->setAlwaysVisible(d->target, true);
	}
	d->createObjectTweener();
	
	d->currentPos = d->target->item()->pos();
	d->absItemPos = d->target->item()->boundingRect().topLeft();
	
	int objectPos = d->target->frame()->visualIndex();
	int framePos = paintArea->currentFrame()->visualIndex();
	int step = qAbs(framePos - objectPos);
	
	int frames = step;
	if( d->target->tweener() )
	{
		frames = qMax(step, d->target->tweener()->frames());
	}
	
	if(input->buttons() == Qt::LeftButton)
	{
		switch(d->group.checkedId())
		{
			case Private::Transform:
			{
					QList<DGraphics::ItemTransformer *> transformers;
					
					if(d->transformer)
					{
						transformers << d->transformer;
					}
					
					CommonAlgorithms::createTransfromers(transformers, QList<QGraphicsItem*>() << d->target->item(), paintArea);
					
					if(!transformers.isEmpty())
					{
						d->transformer = transformers.first();
					}
#if 0
				else if( input->modifiers() != Qt::ControlModifier )
				{
					if(photogram->mouseGrabberItem() == d->transformer->item())
					{
						switch(d->transformer->operation())
						{
							case DGraphics::ItemTransformer::Scale:
							{
								d->transformer->setOperation(DGraphics::ItemTransformer::Rotate);
							}
							break;
							case DGraphics::ItemTransformer::Rotate:
							{
								d->transformer->setOperation(DGraphics::ItemTransformer::Scale);
							}
							break;
						}
					}
				}
#endif
			}
			break;
			case Private::PathMotion:
			{
				d->path = QPainterPath();
				d->path.moveTo(d->firstPos);
				
				if(d->pathItem)
				{
					paintArea->setAlwaysVisible(d->pathItem, false);
					delete d->pathItem;
					d->pathItem = 0;
				}
				
				if( d->nodeEditor )
				{
					foreach(QGraphicsItem *node, d->nodeEditor->nodes())
					{
						paintArea->setAlwaysVisible(node, false);
					}
					
					delete d->nodeEditor;
					d->nodeEditor = 0;
				}
				
				d->pathItem = new QGraphicsPathItem;
				d->pathItem->setZValue(1000);
				
				{
					QColor c = Qt::lightGray;
					c.setAlpha(120);
					
					QPen pen(c, 1);
					pen.setStyle(Qt::DashDotDotLine);
					d->pathItem->setPen(pen);
				}
				photogram->addItem( d->pathItem );
			}
			break;
			default:
			{
// 				qFatal("Not implemented!");
			}
			break;
		}
	}
}

void ITween::move(const QGraphicsSceneMouseEvent *input)
{
	if( d->target == 0 ) return;
	
	PaintArea *paintArea = this->paintArea();
	if(input->buttons() == Qt::LeftButton)
	{
		if( d->group.checkedId() == Private::PathMotion )
		{
			d->lastPos = paintArea->mapToScene(paintArea->viewport()->mapFromGlobal(QCursor::pos()));;
			d->path.lineTo( d->lastPos );
			
			d->pathItem->setPath(d->path);
			paintArea->brushManager()->map(d->pathItem);
		}
	}
}

void ITween::release(const QGraphicsSceneMouseEvent *input)
{
	if( d->target == 0 ) return;
	
	PaintArea *paintArea = this->paintArea();
	switch(d->group.checkedId())
	{
		case Private::Transform:
		{
			d->saveChanges();
			if(d->transformer)
			{
				d->transformer->show();
			}
			else
			{
				QList<DGraphics::ItemTransformer *> transformers;
				CommonAlgorithms::createTransfromers(transformers, QList<QGraphicsItem*>() << d->target->item(), paintArea);
				
				if(!transformers.isEmpty())
				{
					d->transformer = transformers.first();
				}
			}
			
		}
		break;
		case Private::PathMotion:
		{
			if(!d->path.isEmpty())
			{
				{
					double smoothness = 1.0;
					
					QPolygonF polygon = d->path.toFillPolygon();
					if(!polygon.isEmpty() && polygon.isClosed())
						polygon.pop_back();
					
					d->path = DGraphics::Algorithm::smooth(polygon, smoothness);
					d->pathItem->setPath(d->path);
					
					paintArea->setAlwaysVisible(d->pathItem, true);
					
				}
				
				d->saveChanges();
				if( d->nodeEditor )
				{
					foreach(QGraphicsItem *node, d->nodeEditor->nodes())
					{
						paintArea->setAlwaysVisible(node, false);
					}
					delete d->nodeEditor;
				}
				d->nodeEditor = new Drawing::Tool::Private::ContourEditor(d->pathItem, paintArea->photogram());
				
				d->nodeEditor->show();
				foreach(QGraphicsItem *node, d->nodeEditor->nodes())
				{
					paintArea->setAlwaysVisible(node, true);
				}
			}
		}
		break;
		case Private::NodeMotion:
		{
				QList<Drawing::Tool::Private::ContourEditor *> editors;
				if( d->nodeEditor )
				{
					editors << d->nodeEditor;
				}
				
				CommonAlgorithms::createEditors(editors, QList<QGraphicsItem*>() << d->target->item(), paintArea);
				
				if(!editors.isEmpty())
				{
					d->nodeEditor = editors.first();
					d->nodeEditor->show();
				}
				
				d->saveChanges();
		}
		break;
		case Private::Fill:
		{
			if( QAbstractGraphicsShapeItem *item = dynamic_cast<QAbstractGraphicsShapeItem *>(d->target->item()) )
			{
				paintArea->brushManager()->apply(item);
				d->saveChanges();
			}
			else
			{
				d->releaseObject();
				return;
			}
		}
		break;
	}
}

DGui::Action * ITween::action() const
{
	return d->action;
}

int ITween::type() const
{
	return AbstractTool::Brush;
}

void ITween::aboutToChangeTool()
{
	PaintArea *paintArea = this->paintArea();
	d->saveChanges();
	d->releaseObject();
	
	if(d->pathItem)
	{
		paintArea->setAlwaysVisible(d->pathItem, false);
		delete d->pathItem;
		d->pathItem = 0;
	}
	
	if( d->nodeEditor )
	{
		foreach(QGraphicsItem *node, d->nodeEditor->nodes())
		{
			paintArea->setAlwaysVisible(node, false);
		}
		delete d->nodeEditor;
		d->nodeEditor = 0;
	}
	
	if( d->transformer )
	{
		foreach(QGraphicsItem *node, d->transformer->nodes())
		{
			paintArea->setAlwaysVisible(node, false);
		}
		delete d->transformer;
		d->transformer = 0;
	}
}

void ITween::setupConfigBar(QToolBar *configBar)
{
	configBar->addWidget(d->configWidget)->setVisible(true);
}

void ITween::onChangeTool(int id)
{
	PaintArea *paintArea = this->paintArea();
	switch(id)
	{
// 		case Private::Transform:
// 		{
// 		}
// 		break;
		case Private::NodeMotion:
		{
			if(d->pathItem)
			{
				paintArea->setAlwaysVisible(d->pathItem, false);
				delete d->pathItem;
				d->pathItem = 0;
			}
			
			if( d->nodeEditor )
			{
				foreach(QGraphicsItem *node, d->nodeEditor->nodes())
				{
					paintArea->setAlwaysVisible(node, false);
				}
				
				delete d->nodeEditor;
				d->nodeEditor = 0;
			}
		}
		break;
		default:
		{
		}
		break;
	}
	d->saveChanges();
}

}
}
}
