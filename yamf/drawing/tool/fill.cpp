/**************************************************************************
 *   Copyright (C) 2005 by David Cuadrado                                  *
 *   krawek@toonka.com                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "fill.h"

#include <QGraphicsView>
#include <QGraphicsSceneMouseEvent>
#include <QSet>
#include <QHBoxLayout>
#include <QRadioButton>
#include <QToolBar>
#include <QButtonGroup>
#include <QtDebug>

#include <dcore/global.h>
#include <dcore/debug.h>

#include <dgui/iconloader.h>
#include <dgui/action.h>

#include <dgraphics/pathhelper.h>
#include <dgraphics/algorithm.h>

#include "yamf/drawing/paintarea.h"
#include "yamf/drawing/brushmanager.h"
#include "yamf/drawing/photogram.h"
#include "yamf/item/path.h"
#include "yamf/item/proxy.h"
#include "yamf/item/converter.h"

#include "yamf/model/command/changeitembrush.h"
#include "yamf/model/command/changeitempen.h"

#include "yamf/model/frame.h"

#define SHAPE_FILL (QT_VERSION >= 0x040303)

namespace YAMF {
namespace Drawing {
class BrushManager;

namespace Tool {

struct Fill::Private {
	enum Type {
		Fill,
		ShapeFill,
		OutlineFill
	};
	
	DGui::Action * action;
	QWidget *configWidget;
	QButtonGroup group;
};

Fill::Fill(QObject *parent) : AbstractTool(parent), d(new Private)
{
	d->action = new DGui::Action( DGui::IconLoader::self()->load("fill-tool.svg"), tr("Fill"), this);
	d->action->setShortcut( QKeySequence(tr("Ctrl+B")) );
	
	d->configWidget = new QWidget;
	QHBoxLayout *layout = new QHBoxLayout(d->configWidget);
	layout->setMargin(0);
	layout->setSpacing(2);
	
	QRadioButton *fill = new QRadioButton(tr("Fill"));
	fill->setChecked(true);
	layout->addWidget(fill);
	
#if SHAPE_FILL
	QRadioButton *shapefill = new QRadioButton(tr("Shape fill"));
	layout->addWidget(shapefill);
	d->group.addButton(shapefill, Private::ShapeFill);
#endif
	
	QRadioButton *outlineFill = new QRadioButton(tr("Outline fill"));
	layout->addWidget(outlineFill);
	
	d->group.addButton(fill, Private::Fill);
	d->group.addButton(outlineFill, Private::OutlineFill);
}

Fill::~Fill()
{
	delete d;
}

void Fill::init(Photogram *photogram)
{
	foreach(QGraphicsItem *item, photogram->items() )
	{
		item->setFlag(QGraphicsItem::ItemIsSelectable, true);
		item->setFlag(QGraphicsItem::ItemIsFocusable, true);
	}
}

QString Fill::id() const
{
	return "fill";
}

void Fill::press(const QGraphicsSceneMouseEvent *input)
{
	PaintArea *paintArea = this->paintArea();
	Photogram *photogram = paintArea->photogram();
	
	if(input->buttons() == Qt::LeftButton)
	{
		QGraphicsItem *item = photogram->itemAt(input->scenePos());
		
		if( d->group.checkedId() == Private::ShapeFill )
		{
			if( ! item )
			{
				QList<QGraphicsItem *> items;
				
				// Try to find a item
				for(int s = 10; s < 100; s += 10 )
				{
					items = photogram->items(QRectF(input->scenePos()-QPointF(s/2, s/2), QSizeF(s/2, s/2)));
					
					while( !items.isEmpty() )
					{
						QGraphicsItem *it = items.takeFirst();
						
						if( dynamic_cast<YAMF::Common::AbstractSerializable *>(it) )
						{
							item = it;
							break;
						}
					}
					
					if( item )
						break;
				}
			}
		}
		
		if( item )
		{
			if( paintArea->currentFrame()->logicalIndexOf(item) > -1 )
			{
				while( Item::Proxy *proxy = qgraphicsitem_cast<Item::Proxy *>(item ) )
				{
					item = proxy->item();
				}
				
				if( d->group.checkedId() == Private::ShapeFill )
				{
					qDebug("Shape fill");
					
					Item::Path *pathItem = Item::Converter::convertToPath(item);
					
					QList<QGraphicsItem *> colls = item->collidingItems();
					DGraphics::Algorithm::sort(colls, DGraphics::Algorithm::XAxis);
					
// 					if(0)
					{
						QSet<QGraphicsItem *> visited;
						
						
						QPainterPath res = pathItem->path();
						visited << item;
						while( !colls.isEmpty() )
						{
							QGraphicsItem *join = colls.takeLast();
							
							if( visited.contains(join) )
							{
								continue;
							}
							
							visited << join;
							colls << join->collidingItems();
							
							qDebug() << "Item " << join->boundingRect();
							
							Item::Path *p = Item::Converter::convertToPath(join);
							
							if( p )
							{
								res = res.united(mapPath(p));
							}
							
							delete p;
						}
						
						QList<QPainterPath> subpaths = DGraphics::PathHelper::toSubpaths(res);
						if( subpaths.count() > 1 )
						{
							QPainterPath sp;
							
							foreach(QPainterPath subpath, subpaths)
							{
								if( subpath.contains( input->pos()) )
								{
									res = subpath;
									break;
								}
							}
						}
						
						Item::Path *p = new Item::Path;
						p->setPath(res);
						p->setBrush(Qt::green);
						paintArea->currentFrame()->addItem(p);
						return;
					}
					
					QPainterPath res = mapPath(pathItem);
					
					if( !colls.isEmpty() )
					{
						bool doSubs = false;
						QList<Item::Path *> subs;
						
						foreach(QGraphicsItem *xit, colls)
						{
							if( xit == item ) break;
							
							Item::Path *path = Item::Converter::convertToPath(xit);
							if( path )
							{
								QPointF localPoint = xit->mapFromScene(input->pos());
								dfDebug << localPoint;
								dfDebug << xit->sceneBoundingRect();
								
								dfDebug << xit->sceneBoundingRect().contains(localPoint);
								
								if( path->shape().contains( localPoint ) )
								{
									res = res.intersected(mapPath(path));
								}
								else
								{
									subs << path;
									doSubs = true;
								}
							}
						}
						
						int i = 0;
						foreach(Item::Path *path, subs)
						{
							dfDebug << i++ << " " << DGraphics::PathHelper::toString(mapPath(path) );
							res = res.subtracted( mapPath(path) );
						}
						
						QList<QPainterPath> subpaths = DGraphics::PathHelper::toSubpaths(res);
						dfDebug << DGraphics::PathHelper::toString(res);
						if( subpaths.count() > 1 )
						{
							QPainterPath sp;
							
							foreach(QPainterPath subpath, subpaths)
							{
								sp.intersected(subpath);
							}
							
							res = res.subtracted(sp);
						}
					}
					
					Item::Path *intersection = new Item::Path();
					intersection->setPath(res);
					
					intersection->setZValue(item->zValue()+1);
					
					intersection->setFlags(QGraphicsItem::ItemIsMovable | QGraphicsItem::ItemIsSelectable);
					intersection->setBrush(paintArea->brushManager()->pen().brush());
					
					paintArea->currentFrame()->addItem(intersection);
				}
				else
				{
					if( QAbstractGraphicsShapeItem *shape = qgraphicsitem_cast<QAbstractGraphicsShapeItem *>(item) )
					{
						QBrush brush = paintArea->brushManager()->mapBrush(item->sceneBoundingRect(), paintArea->brushManager()->penBrush());
						
						if( d->group.checkedId() == Private::Fill )
						{
							Command::ChangeItemBrush *cmd = new Command::ChangeItemBrush(paintArea->currentFrame(), photogram->itemAt(input->scenePos()), brush);
							paintArea->addCommand(cmd);
						}
						else if( d->group.checkedId() == Private::OutlineFill)
						{
							QPen pen = shape->pen();
							pen.setBrush(brush);
							
							Command::ChangeItemPen *cmd = new Command::ChangeItemPen(paintArea->currentFrame(), photogram->itemAt(input->scenePos()), pen);
							paintArea->addCommand(cmd);
						}
					}
				}
			}
		}
	}
}

void Fill::move(const QGraphicsSceneMouseEvent *input)
{
	Q_UNUSED(input);
}

void Fill::release(const QGraphicsSceneMouseEvent *input)
{
	Q_UNUSED(input);
}

DGui::Action * Fill::action() const
{
	return d->action;
}

int Fill::type() const
{
	return AbstractTool::Fill;
}

void Fill::aboutToChangeTool()
{
}

void Fill::setupConfigBar(QToolBar *configBar)
{
	configBar->addWidget(d->configWidget)->setVisible(true);
}

QPainterPath Fill::mapPath(const QPainterPath &path, const QPointF &pos)
{
	QMatrix tr1;
	tr1.translate(pos.x(), pos.y());
	
	QPainterPath p1 = tr1.map(path);
// 	p1.closeSubpath();
	
	return p1;
}

QPainterPath Fill::mapPath(const QGraphicsPathItem *item)
{
	return mapPath(item->path(), item->pos());
}


}
}
}
