/***************************************************************************
 *   Copyright (C) 2007 by Jorge Cuadrado                                  *
 *   kuadrosxx@gmail.com                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "polyline.h"

#include <drawing/photogram.h>
#include <drawing/brushmanager.h>
#include <drawing/paintarea.h>

#include <dgraphics/patheditor.h>
#include <dgui/action.h>
#include <dgui/iconloader.h>
#include <dcore/debug.h>

#include "private/contoureditor.h"
#include <model/command/editnodesitem.h>

#include <model/frame.h>
#include <item/path.h>


#include <QPointF>
#include <QIcon>
#include <QKeySequence>
#include <QGraphicsPathItem>
#include <QPainterPath>
#include <QMatrix>
#include <QGraphicsLineItem>
#include <QGraphicsView>

#include <QGraphicsSceneMouseEvent>
#include <QKeyEvent>


namespace YAMF {
namespace Drawing {
namespace Tool {

struct PolyLine::Private 
{
	bool begin, moved;
	QPointF center;
	QPointF right;
	Drawing::Tool::Private::ContourEditor *editor;
	QPainterPath path;
	QPainterPath lastPart;
	
	YAMF::Item::Path *item;
	DGui::Action *action;
	
	QGraphicsLineItem *line1, *line2;
	QGraphicsPathItem *line3;
};

/**
 * @~spanish
 * Construye la herramienta para dibujar trazos rectos y curvilíneos.
 */
PolyLine::PolyLine(QObject *parent): AbstractTool(parent), d(new Private)
{
	d->begin = false;
	
	d->editor = 0;
	d->item = 0;
	d->line1 = new QGraphicsLineItem(0,0,0,0);
	d->line1->setPen ( QPen(Qt::red) );
	d->line2 = new QGraphicsLineItem(0,0,0,0);
	d->line2->setPen ( QPen(Qt::green) );
	
	d->line3 = new QGraphicsPathItem();
	
	d->action = new DGui::Action( DGui::IconLoader::self()->load("draw-path.svg"), tr("Poly line"), this);
	d->action->setShortcut( QKeySequence(tr("")) );
}


/**
 * Destructor
 */
PolyLine::~PolyLine()
{
	delete d;
}

/**
 * @~spanish
 * Inicializa la herramienta, evita que los items dentro de el fotograma se puedan mover o seleccionar.
 */
void PolyLine::init(Photogram *photogram)
{
	endItem();
	//FIXME
	foreach(QGraphicsView *view,  photogram->views() )
	{
		view->setDragMode ( QGraphicsView::NoDrag );
		
		Q_CHECK_PTR(view->scene());
		if ( QGraphicsScene *sscene = qobject_cast<QGraphicsScene *>(view->scene()) )
		{
			foreach(QGraphicsItem *item, sscene->items() )
			{
				item->setFlag(QGraphicsItem::ItemIsSelectable, false);
				item->setFlag(QGraphicsItem::ItemIsMovable, false);
			}
			sscene->addItem( d->line1 );
			sscene->addItem( d->line2 );
		}
	}
}

/**
 * @~spanish
 * Retorna el id de la herramienta.
 */
QString PolyLine::id() const
{
	return tr("PolyLine");
}

/**
 * @~spanish
 * Función sobrecargada para implementar la creación de los trazos rectos y curvilíneos.
 */
void PolyLine::press(const QGraphicsSceneMouseEvent *input)
{
	QGraphicsScene *scene = paintArea()->scene();
	if(scene->items().contains(d->line3))
	{
		scene->removeItem(d->line3);
	}
	
	BrushManager *brushManager = paintArea()->brushManager();
	
	if(d->editor && d->editor->isSelected())
	{
		return;
	}
	
	scene->clearSelection();
	
	if(!d->item)
	{
		d->path = QPainterPath();
		d->path.moveTo(input->scenePos());
		
		d->item = new YAMF::Item::Path();
		d->item->setZValue(1000);
		d->editor = new Drawing::Tool::Private::ContourEditor(d->item, scene);
		connect(d->editor, SIGNAL(nodeClicked()), this, SLOT(nodeChanged()));
		scene->addItem( d->item );
		
		d->begin = true;
		d->moved = false;
	}
	else
	{
		d->begin = false;
		
		if(!d->moved)
		{
			d->path.cubicTo(d->center, d->center, input->scenePos());
			d->editor->save();
			d->item->setPath(d->path);
			paintArea()->brushManager()->map(d->item);
		}
		else
		{
			d->moved = false;
			d->path.cubicTo(d->center, d->right, input->scenePos());
			d->editor->save();
			d->item->setPath(d->path);
			paintArea()->brushManager()->map(d->item);
		}
		
	}
	d->center = input->scenePos();
	
	d->item->setPen( brushManager->pen() );
}

/**
 * @~spanish
 * Función sobrecargada para implementar la creación de los trazos rectos y curvilíneos.
 */
void PolyLine::move(const QGraphicsSceneMouseEvent *input)
{
	PaintArea *paintArea = this->paintArea();
	QGraphicsScene *scene = paintArea->scene();
	if(d->item)
	{
		if(input->buttons() == Qt::NoButton)
		{
			QPainterPath lastPart;
			
			lastPart.moveTo(d->center);
			
			if(!d->moved)
			{
				lastPart.cubicTo(d->center, d->center, input->scenePos());
			}
			else
			{
				lastPart.cubicTo(d->center, d->right, input->scenePos());
			}
			
			d->line3->setPath(lastPart);
			if(!scene->items().contains(d->line3))
			{
				scene->addItem(d->line3);
			}
			
			if(!scene->items().contains(d->line1))
			{
				scene->addItem( d->line1 );
			}
			if(!scene->items().contains(d->line2))
			{
				scene->addItem( d->line2 );
			}
		}
		else if(d->editor && d->editor->isSelected())
		{
			d->path = d->item->path();
		}
		else if(input->buttons() == Qt::LeftButton)
		{
			paintArea->setDragMode(QGraphicsView::NoDrag);
			
			if(d->begin)
			{
				d->moved = true;
				d->right = input->scenePos();
				
				d->line2->setLine(QLineF(d->right, d->center));
				if(!scene->items().contains(d->line2))
				{
					scene->addItem( d->line2 );
				}
			}
			else
			{
				int index = d->path.elementCount()-3;
				
				if( d->path.elementAt(index).type == QPainterPath::CurveToElement )
				{
					if(!d->moved)
					{
						QPointF point = d->path.elementAt(index+1);
						d->path.setElementPositionAt(index, point.x(), point.y() );
					}
					
					QPointF mirror = d->center - ( input->scenePos() - d->center);
					
					d->path.setElementPositionAt(index+1, mirror.x(), mirror.y());
					
					d->line1->setLine(QLineF(mirror, d->center));
					d->line2->setLine(QLineF(d->right, d->center));
					
					if(!scene->items().contains(d->line1))
					{
						scene->addItem( d->line1 );
					}
					if(!scene->items().contains(d->line2))
					{
						scene->addItem( d->line2 );
					}
				}
				
				d->moved = true;
				d->right = input->scenePos();
				d->item->setPath(d->path);
				paintArea->brushManager()->map(d->item);
			}
			
		}
	}
}

/**
 * @~spanish
 * Función sobrecargada para implementar la creación de los trazos rectos y curvilíneos.
 */
void PolyLine::release(const QGraphicsSceneMouseEvent *input)
{
	Q_UNUSED(input);
	PaintArea *paintArea = this->paintArea();
	
// 	d->editor->expandAllNodes ();
	
	if(d->begin)
	{
		paintArea->currentFrame()->addItem(d->item);
	}
	else if(!d->editor->isSelected())
	{
		int position  = paintArea->currentFrame()->visualIndexOf(d->item);
		
		if(position > -1)
		{
			paintArea->addCommand(new Command::EditNodesItem(d->item, d->editor->oldPath(), paintArea->currentFrame()));
			d->editor->save();
		}
	}
}

/**
 * @~spanish
 * Función sobrecargada para implementar la creación de los trazos rectos y curvilíneos.
 * termina de hacer el trazo.
 */
void PolyLine::doubleClick(const QGraphicsSceneMouseEvent *input)
{
	Q_UNUSED(input);
	
	endItem();
	
}

void PolyLine::keyPressEvent(QKeyEvent *event)
{
	if(event->key() == Qt::Key_Escape)
	{
		endItem();
		event->accept();
	}
}

/**
 * @~spanish
 * Retorna la acción que representa la herramienta.
 */
DGui::Action *PolyLine::action() const
{
	return d->action;
}

/**
 * @~spanish
 * Retorna que la herramienta es de tipo Brush.
 */
int PolyLine::type() const
{
	return AbstractTool::Brush;
}

/**
 * @~spanish
 * Función sobrecargada para terminar de crear el ítem, cuando se cambia la herramienta.
 */
void PolyLine::aboutToChangeTool()
{
	endItem();
}

/**
 * Función sobrecargada para actualizar valores cuando hay un cambio en el fotograma.
 */
void PolyLine::photogramChanged(Photogram *const photogram)
{
	Q_UNUSED(photogram);
	if(d->item)
	{
// 		d->path = d->item->path();
// 		d->editor->setItem(d->item);
		if(!d->item->path().isEmpty())
		{
			d->editor->show();
		}
// 		d->editor->expandAllNodes();
		int size = d->item->path().elementCount();
		if(size > 0)
		{
			d->center = d->item->path().elementAt(size -1);
		}
	}
}

/**
 * @internal
 * Termina de crear el trazo.
 */
void PolyLine::endItem()
{
	if(d->item)
	{
		d->path = QPainterPath();
		d->item = 0;
		delete d->editor;
		d->editor = 0;
		
		paintArea()->scene()->removeItem(d->line3);
		paintArea()->scene()->removeItem( d->line1 );
		paintArea()->scene()->removeItem( d->line2 );
		paintArea()->viewport()->update();
	}
}

/**
 * @internal
 * Actualiza los valores cuando se modifican los nodos de trazo.
 */
void PolyLine::nodeChanged()
{
	if(paintArea())
	{
		int position  = paintArea()->currentFrame()->visualIndexOf(d->item);
		
		if(position != -1)
		{
			int size = d->item->path().elementCount();
			if(size > 0)
			{
				d->center = d->item->path().elementAt(size -1);
			}
			d->path = d->item->path();
			paintArea()->addCommand(new Command::EditNodesItem(d->item, d->editor->oldPath(), paintArea()->currentFrame()));
			d->editor->save();
		}
	}
}

}
}
}
