/***************************************************************************
 *   Copyright (C) 2005 by David Cuadrado                                  *
 *   krawek@gmail.com                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "brush.h"

#include <QPointF>
#include <QKeySequence>
#include <QGraphicsPathItem>
#include <QPainterPath>
#include <QMatrix>
#include <QGraphicsLineItem>
#include <QGraphicsView>

#include <QGraphicsSceneMouseEvent>
#include <QToolBar>

#include <QHBoxLayout>
#include <QLabel>

#include <dgraphics/algorithm.h>
#include <dcore/algorithm.h>
#include <dcore/debug.h>
#include <dgui/action.h>
#include <dgui/iconloader.h>

#include <drawing/photogram.h>
#include <drawing/brushmanager.h>
#include <drawing/paintarea.h>


#include <model/frame.h>
#include <item/path.h>

namespace YAMF {
namespace Drawing {
namespace Tool {

struct Brush::Private
{
	Private() : item(0), exactness(0), configWidget(0) {}
	
	QPointF firstPoint;
	QPointF oldPos;
	QPainterPath path;
	
	DGui::Action *action;
	
	YAMF::Item::Path *item;
	
	QDoubleSpinBox *exactness;
	QWidget *configWidget;
};

/**
 * @~spanish
 * Construye la herramienta.
 */
Brush::Brush(QObject *parent) : AbstractTool(parent), d(new Private)
{
	d->action = new DGui::Action( DGui::IconLoader::self()->load("draw-freehand.svg"), tr("Pencil"), this);
	d->action->setShortcut( QKeySequence(tr("Ctrl+B")) );
	
	d->exactness = new QDoubleSpinBox;
	d->exactness->setValue(3.0);
	
	
	d->configWidget = new QWidget;
	QHBoxLayout *layout = new QHBoxLayout(d->configWidget);
	layout->setMargin(0);
	layout->setSpacing(2);
	layout->addWidget(new QLabel(tr("Smoothness")));
	layout->addWidget(d->exactness);
}

/**
 * @~spanish
 * Destructor
 */
Brush::~Brush()
{
	if( d->configWidget )
	{
		d->configWidget->setParent(0);
		delete d->configWidget;
	}
	delete d;
}

/**
 * @~spanish
 * Inicializa la herramienta, evita que los items dentro de el fotograma se puedan mover o seleccionar
 */
void Brush::init(Photogram *photogram)
{
	foreach(QGraphicsView * view, photogram->views())
	{
		view->setDragMode ( QGraphicsView::NoDrag );
		
		if ( QGraphicsScene *photogram = qobject_cast<QGraphicsScene *>(view->scene()) )
		{
			foreach(QGraphicsItem *item, photogram->items() )
			{
				item->setFlag(QGraphicsItem::ItemIsSelectable, false);
				item->setFlag(QGraphicsItem::ItemIsMovable, false);
			}
		}
	}
}

/**
 * @~spanish
 * Retorna el id de la herramienta
 */
QString Brush::id() const
{
	return tr("Pencil");
}

/**
 * @~spanish
 * Funciï¿½n sobrecargada para implementar el trazo de lineas.
 */
void Brush::press(const QGraphicsSceneMouseEvent *input)
{
	PaintArea *paintArea = this->paintArea();
	
	d->firstPoint = input->scenePos();
	d->oldPos = input->scenePos();
	
	d->path = QPainterPath();
	d->path.moveTo(d->firstPoint);
	
	d->item = new Item::Path();
	d->item->setPen(paintArea->brushManager()->pen());
	paintArea->photogram()->addItem( d->item );
	d->item->setZValue(1000);
}

/**
 * @~spanish
 * Funciï¿½n sobrecargada para implementar el trazo de lineas.
 */
void Brush::move(const QGraphicsSceneMouseEvent *input)
{
	PaintArea *paintArea = this->paintArea();
	
	if(input->buttons() == Qt::LeftButton && d->item )
	{
		d->oldPos = paintArea->mapToScene(paintArea->viewport()->mapFromGlobal(QCursor::pos()));
		
		d->path.lineTo( d->oldPos );
		
		d->item->setPath(d->path);
		paintArea->brushManager()->map(d->item);
	}
}

/**
 * @~spanish
 * Funciï¿½n sobrecargada para implementar el trazo de lineas.
 */
void Brush::release(const QGraphicsSceneMouseEvent *input)
{
	PaintArea *paintArea = this->paintArea();
	BrushManager *brushManager = paintArea->brushManager();
	
	
	Item::Path *item = d->item;
	QPainterPath path = item->path();
	d->item = 0;
	d->path = QPainterPath();
	
	double smoothness = 0.1;
	if( d->exactness )
	{
		smoothness = d->exactness->value();
	}
	
	if ( d->firstPoint == input->pos() && path.elementCount() == 1)
	{
		smoothness = 0;
		path.addEllipse(input->pos().x(), input->pos().y(), brushManager->pen().width(), brushManager->pen().width());
	}
	else
	{
		smoothPath( path, smoothness );
	}
	
	d->firstPoint = QPoint(0,0);
	
	item->setPath(path);
	paintArea->brushManager()->apply(item);
	
	
	paintArea->currentFrame()->addItem(item);
}

/**
 * @~spanish
 * @internal
 * Suaviza el trazo hecho por el usuario.
 */
void Brush::smoothPath(QPainterPath &path, double smoothness, int from, int to)
{
	if(smoothness > 0 )
	{
		QPolygonF polygon = path.toFillPolygon();
		if(!polygon.isEmpty() && polygon.isClosed())
			polygon.pop_back();
		
		path = DGraphics::Algorithm::smooth(polygon, smoothness, from, to);
	}
}

/**
 * @~spanish
 * Retorna la acción que representa la herramienta.
 */
DGui::Action * Brush::action() const
{
	return d->action;
}

/**
 * @~spanish
 * Retorna que la herramienta es de tipo Brush.
 */
int Brush::type() const
{
	return AbstractTool::Brush;
}

void Brush::aboutToChangeTool() 
{
}

/**
 * @~spanish
 * Asigna la interfaz gráfica para configurar el grado de suavizado.
 */
void Brush::setupConfigBar(QToolBar *configBar)
{
	configBar->addWidget(d->configWidget)->setVisible(true);
}

}
}
}
