/***************************************************************************
 *   Copyright (C) 2007 Jorge Cuadrado                                     *
 *   kuadrosxx@gmail.com                                                   *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/
 
#include "colorpicker.h"

#include <QPointF>
#include <QKeySequence>
#include <QPainterPath>
#include <QMatrix>
#include <QGraphicsView>
#include <QGraphicsSceneMouseEvent>
#include <QToolBar>
#include <QHBoxLayout>
#include <QSpinBox>
#include <QLabel>

#include <QImage>

#include <QGraphicsItem>
#include <dgraphics/algorithm.h>
#include <dcore/algorithm.h>
#include <dcore/debug.h>
#include <dgui/action.h>
#include <dgui/iconloader.h>

#include <drawing/photogram.h>
#include <drawing/brushmanager.h>
#include <drawing/paintarea.h>

#include <model/frame.h>

namespace YAMF {

namespace Drawing {

namespace Tool {

struct ColorPicker::Private
{
	DGui::Action *action;
	
	QImage image;
};

/**
 * Construye la herramienta.
 */
ColorPicker::ColorPicker(QObject *parent) : Drawing::AbstractTool(parent), d(new Private)
{
	d->action = new DGui::Action( DGui::IconLoader::self()->load("color-picker.svg"), tr("Color Picker"), this);
}


/**
 * Destructor.
 */
ColorPicker::~ColorPicker()
{
	delete d;
}

/**
 * @~spanish
 * Inicializa la herramienta, evita que los items dentro de el fotograma se puedan mover o seleccionar
 */
void ColorPicker::init(Photogram *photogram)
{
	foreach(QGraphicsView * view, photogram->views())
	{
		view->setDragMode ( QGraphicsView::NoDrag );
		
		if ( QGraphicsScene *photogram = qobject_cast<QGraphicsScene *>(view->scene()) )
		{
			foreach(QGraphicsItem *item, photogram->items() )
			{
				item->setFlag(QGraphicsItem::ItemIsSelectable, false);
				item->setFlag(QGraphicsItem::ItemIsMovable, false);
			}
		}
	}
	
	
}

/**
 * @~spanish
 * Retorna el id de la herramienta
 */
QString ColorPicker::id() const
{
	return tr("ColorPicker");
}

/**
 * @~spanish
 * Función sobrecargada para implementar la selección del color.
 */
void ColorPicker::press(const QGraphicsSceneMouseEvent *input)
{
	PaintArea *paintArea = this->paintArea();
	QRectF rect = paintArea->photogram()->sceneRect();
	d->image = QImage( rect.size().toSize(), QImage::Format_ARGB32_Premultiplied );
	d->image.fill(0);
	QPainter painter(&d->image);
	paintArea->photogram()->render(&painter);
	
	if(rect.contains(input->scenePos().toPoint()))
	{
		paintArea->brushManager()->setBrush(QBrush(d->image.pixel(input->scenePos().toPoint())));
	}
	
}

/**
 * @~spanish
 * Función sobrecargada para implementar la selección del color.
 */
void ColorPicker::move(const QGraphicsSceneMouseEvent *input)
{
	PaintArea *paintArea = this->paintArea();
	QRectF rect = paintArea->photogram()->sceneRect();
	if(input->buttons() == Qt::LeftButton)
	{
		if(rect.contains(input->scenePos().toPoint()))
		{
			paintArea->brushManager()->setBrush(QBrush(d->image.pixel(input->scenePos().toPoint())));
		}
	}
}


/**
 * @~spanish
 * Función sobrecargada para implementar la selección del color.
 */
void ColorPicker::release(const QGraphicsSceneMouseEvent *input)
{
}

/**
 * @~spanish
 * Retorna la acción que representa la herramienta.
 */
DGui::Action *ColorPicker::action() const
{
	return d->action;
}

/**
 * @~spanish
 * Retorna que la herramienta es de tipo Brush.
 */
int ColorPicker::type() const
{
	return AbstractTool::Brush;
}

void ColorPicker::aboutToChangeTool()
{
	
}

void ColorPicker::setupConfigBar(QToolBar *configBar)
{
}


}
}
}
