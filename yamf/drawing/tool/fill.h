/***************************************************************************
 *   Copyright (C) 2005 by David Cuadrado                                  *
 *   krawek@toonka.com                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef FillPLUGIN_H
#define FillPLUGIN_H

#include <QObject>
#include <QLabel>

#include <yamf/drawing/abstracttool.h>
#include <yamf/common/yamf_exports.h>

class QGraphicsPathItem;

namespace YAMF {

namespace Drawing {
class BrushManager;
class Photogram;

namespace Tool {

/**
 * @author David Cuadrado <krawek@toonka.com>
*/
class YAMF_EXPORT Fill : public Drawing::AbstractTool
{
	Q_OBJECT;
	
	public:
		Fill(QObject *parent = 0);
		~Fill();
		
		virtual QString id() const;

		void init(Photogram *photogram);
		
		virtual void press(const QGraphicsSceneMouseEvent *input);
		virtual void move(const QGraphicsSceneMouseEvent *input);
		virtual void release(const QGraphicsSceneMouseEvent *input);
		
		virtual DGui::Action *action() const;
		int type() const;
		virtual void aboutToChangeTool();
		void setupConfigBar(QToolBar *configBar);
		
		QPainterPath mapPath(const QPainterPath &path, const QPointF &pos);
		QPainterPath mapPath(const QGraphicsPathItem *item);
		
	private:
		struct Private;
		Private *const d;
};

}
}
}

#endif
