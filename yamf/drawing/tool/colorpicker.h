/***************************************************************************
 *   Copyright (C) 2007 Jorge Cuadrado                                     *
 *   kuadrosxx@gmail.com                                                   *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/
 
#ifndef YAMF_DRAWING_TOOLCOLORPICKER_H
#define YAMF_DRAWING_TOOLCOLORPICKER_H

#include <drawing/abstracttool.h>

namespace YAMF {

namespace Drawing {

namespace Tool {

/**
 * @~spanish
 * @ingroup tools
 * @brief Esta clase provee de una herramienta para seleccionar un color que este dentro del actual fotograma.
 * @author Jorge Cuadrado <kuadrosxx@gmail.com>
*/
class YAMF_EXPORT ColorPicker : public Drawing::AbstractTool
{
	public:
		ColorPicker(QObject *parent = 0);
		virtual ~ColorPicker();
		
		virtual void init(Photogram *photogram);
		
		virtual QString id() const;
		virtual void press(const QGraphicsSceneMouseEvent *input);
		virtual void move(const QGraphicsSceneMouseEvent *input);
		virtual void release(const QGraphicsSceneMouseEvent *input);
		
		virtual DGui::Action *action() const;
		
		int type() const;
		
		virtual void aboutToChangeTool();
		
		void setupConfigBar(QToolBar *configBar);
		
	private:
		struct Private;
		Private *const d;
};

}

}

}

#endif
