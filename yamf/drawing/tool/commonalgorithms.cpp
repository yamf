/***************************************************************************
 *   Copyright (C) 2007 Jorge Cuadrado                                     *
 *   kuadrosxx@gmail.com                                                   *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/

#include "commonalgorithms.h"

#include "private/contoureditor.h"
#include <drawing/photogram.h>
#include <drawing/paintarea.h>

#include <item/path.h>
#include <item/proxy.h>
#include <item/serializer.h>

#include <model/command/convertitem.h>
#include <model/command/editnodesitem.h>
#include <model/frame.h>

#include <dgraphics/itemtransformer.h>

#include <dcore/debug.h>

namespace YAMF {
namespace Drawing {
namespace Tool {

CommonAlgorithms::CommonAlgorithms()
{
}


CommonAlgorithms::~CommonAlgorithms()
{
}

void CommonAlgorithms::createEditors(QList<Drawing::Tool::Private::ContourEditor *> & editors, QList<QGraphicsItem *> items, PaintArea *paintArea)
{
	QList<Drawing::Tool::Private::ContourEditor *>::iterator it = editors.begin();
	QList<Drawing::Tool::Private::ContourEditor *>::iterator itEnd = editors.end();
	
	
	
	while(it != itEnd)
	{
		int parentIndex = items.indexOf((*it)->item());
		if(parentIndex != -1 )
		{
			items.removeAt(parentIndex);
		}
		else
		{
			Drawing::Tool::Private::ContourEditor *toRemove = editors.takeAt(editors.indexOf((*it)));
			
			foreach(QGraphicsItem *node, toRemove->nodes())
			{
				paintArea->setAlwaysVisible(node, false);
			}
			delete toRemove;
		}
		++it;
	}
	
	foreach(QGraphicsItem *item, items)
	{
		if(item)
		{
			int index =  paintArea->currentFrame()->logicalIndexOf(item);
			if(index > -1)
			{
				item->setFlag(QGraphicsItem::ItemIsMovable, false);
				if(Item::Path *path = qgraphicsitem_cast<Item::Path*>(item) )
				{
					editors << new Drawing::Tool::Private::ContourEditor(path, paintArea->scene());
					foreach(QGraphicsItem *node, editors.last()->nodes())
					{
						paintArea->setAlwaysVisible(node, true);
					}
				}
				else
				{
					if(item->type() != QGraphicsItemGroup::Type)
					{
						Command::ConvertItem *convertItem = new Command::ConvertItem(item, Item::Path::Type, paintArea->currentFrame());
						paintArea->addCommand(convertItem);
						
						if(convertItem->isValid())
						{
							if(Item::Path *path = qgraphicsitem_cast<Item::Path*>( paintArea->currentFrame()->item(index) ) )
							{
								editors << new Drawing::Tool::Private::ContourEditor(path, paintArea->photogram());
								
								foreach(QGraphicsItem *node, editors.last()->nodes())
								{
									paintArea->setAlwaysVisible(node, true);
								}
							}
							else if(Item::Proxy *proxy = qgraphicsitem_cast<Item::Proxy*>( paintArea->currentFrame()->item(index) ))
							{
								if(Item::Path *path = qgraphicsitem_cast<Item::Path*>( proxy->item() ) )
								{
									editors << new Drawing::Tool::Private::ContourEditor(path,  paintArea->photogram());
									editors.last()->setProxyItem(proxy);
									foreach(QGraphicsItem *node, editors.last()->nodes())
									{
										paintArea->setAlwaysVisible(node, true);
									}
								}
							}
						}
					}
				}
			}
		}
	}
}

void CommonAlgorithms::createTransfromers(QList<DGraphics::ItemTransformer *> & transformers, QList<QGraphicsItem*> items, PaintArea *paintArea )
{
	QGraphicsScene *scene = paintArea->scene();
	
	QList<DGraphics::ItemTransformer *>::iterator it = transformers.begin();
	QList<DGraphics::ItemTransformer *>::iterator itEnd = transformers.end();
	while(it != itEnd)
	{
		int itemIndex = scene->selectedItems().indexOf((*it)->item() );
		
		if(itemIndex != -1 )
		{
			items.removeAt(itemIndex);
		}
		else
		{
			DGraphics::ItemTransformer *transformer = transformers.takeAt(transformers.indexOf((*it)));
			foreach(QGraphicsItem *node, transformer->nodes())
			{
				paintArea->setAlwaysVisible(node, false);
			}
			delete transformer;
		}
		++it;
	}
	
	foreach(QGraphicsItem *item, items)
	{
		if( paintArea->currentFrame()->logicalIndexOf(item) > -1 )
		{
			DGraphics::ItemTransformer *transformer = new DGraphics::ItemTransformer(item, scene);
			
			foreach(QGraphicsItem *node, transformer->nodes())
			{
				paintArea->setAlwaysVisible(node, true);
			}
			
			transformers << transformer;
		}
	}
}


}
}
}

