/***************************************************************************
 *   Copyright (C) 2007 by Jorge Cuadrado                                  *
 *   kuadrosxx@gmail.com                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "zoom.h"

#include <drawing/photogram.h>
#include <drawing/brushmanager.h>
#include <drawing/paintarea.h>
#include <model/frame.h>

#include <dgui/action.h>
#include <dgui/iconloader.h>
#include <dcore/debug.h>

#include <QPointF>
#include <QIcon>
#include <QKeySequence>
#include <QMatrix>
#include <QGraphicsLineItem>
#include <QGraphicsView>
#include <QGraphicsRectItem>
#include <QGraphicsSceneMouseEvent>


namespace YAMF {

namespace Drawing {

namespace Tool {

struct Zoom::Private 
{
	QGraphicsRectItem *rect;
	DGui::Action *action;
};

/**
 * @~spanish
 * Construye la herramienta.
 */
Zoom::Zoom(QObject *parent) : AbstractTool(parent), d(new Private)
{
	d->action = new DGui::Action( DGui::IconLoader::self()->load("page-magnifier.svg"), tr("Zoom"), this);
	
	d->rect = new QGraphicsRectItem;
	d->rect->setZValue(110);
}


/**
 * Destructor
 */
Zoom::~Zoom()
{
	delete d;
}

/**
 * @~spanish
 * Inicializa la herramienta, evita que los items dentro de el fotograma se puedan mover o seleccionar.
 */
void Zoom::init(Photogram *photogram)
{
	foreach(QGraphicsView * view, photogram->views())
	{
		view->setDragMode ( QGraphicsView::NoDrag );
		
		if ( QGraphicsScene *photogram = qobject_cast<QGraphicsScene *>(view->scene()) )
		{
			foreach(QGraphicsItem *item, photogram->items() )
			{
				item->setFlag(QGraphicsItem::ItemIsSelectable, false);
				item->setFlag(QGraphicsItem::ItemIsMovable, false);
			}
		}
	}
}

/**
 * @~spanish
 * Retorna el id de la herramienta.
 */
QString Zoom::id() const
{
	return tr("Zoom");
}

/**
 * @~spanish
 * Función sobrecargada para habilitar la selección del area para ampliar.
 */
void Zoom::press(const QGraphicsSceneMouseEvent *input)
{
	PaintArea *paintArea = this->paintArea();
	if(!paintArea->scene()->items().contains(d->rect))
	{
		paintArea->scene()->addItem(d->rect);
	}
	
	d->rect->setRect(QRectF(input->scenePos(), QSizeF(0,0)));
	
}

/**
 * @~spanish
 * Función sobrecargada para habilitar la selección del area para ampliar.
 */
void Zoom::move(const QGraphicsSceneMouseEvent *input)
{
	PaintArea *paintArea = this->paintArea();
	if ( input->buttons() == Qt::LeftButton )
	{
		paintArea->setDragMode(QGraphicsView::NoDrag);
		
		QRectF rect = d->rect->rect();
		rect.setBottomLeft(input->scenePos());
		d->rect->setRect(rect);
		
		rect = rect.normalized (); 
		if(rect.height() > 10 && rect.width() > 10 )
		{
			d->rect->setPen(QPen(Qt::black, 2, Qt::DashLine));
		}
		else
		{
			d->rect->setPen(QPen(Qt::red, 2, Qt::DashLine));
		}
	}
}

/**
 * @~spanish
 * Función sobrecargada para habilitar para ampliar el area seleccionada.
 */
void Zoom::release(const QGraphicsSceneMouseEvent *input)
{
	PaintArea *paintArea = this->paintArea();
	QRectF rect = d->rect->rect();
	if ( input->button() == Qt::LeftButton )
	{
		if(rect.normalized().height() > 10 && rect.normalized().width() > 10 )
		{
			paintArea->scene()->removeItem(d->rect);
			paintArea->fitInView( rect, Qt::KeepAspectRatio);
		}
	}
	else if ( input->button() == Qt::RightButton )
	{
		QRectF visibleRect = paintArea->visibleRegion().boundingRect();
		
		paintArea->fitInView( visibleRect.adjusted((rect.width()+50), 0, 0, (rect.height()+50)), Qt::KeepAspectRatio );
	}
}

/**
 * @~spanish
 * Retorna la acción que representa la herramienta.
 */
DGui::Action *Zoom::action() const
{
	return d->action;
}

/**
 * @~spanish
 * Retorna que la herramienta es de tipo View.
 */
int Zoom::type() const
{
	return AbstractTool::View;
}

void Zoom::aboutToChangeTool()
{
}

void Zoom::photogramChanged(Photogram *const photogram)
{
}


}

}

}
