/***************************************************************************
 *   Copyright (C) 2007 Jorge Cuadrado                                     *
 *   kuadrosxx@gmail.com                                                   *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/

#ifndef YAMF_DRAWING_TOOLSELECT_H
#define YAMF_DRAWING_TOOLSELECT_H

#include <yamf/drawing/abstracttool.h>

namespace YAMF {
namespace Drawing {
namespace Tool {

/**
 * @ingroup tools
 * @brief Esta clase provee de una herramienta para transformar un objeto grafico.
 * @author Jorge Cuadrado <kuadrosxx@gmail.com>
*/
class YAMF_EXPORT Select : public Drawing::AbstractTool
{
	Q_OBJECT
	public:
		Select(QObject *parent = 0);
		~Select();
		virtual void init(Photogram *photogram);
		
		virtual QString id() const;
		virtual void press(const QGraphicsSceneMouseEvent *input);
		virtual void move(const QGraphicsSceneMouseEvent *input);
		virtual void release(const QGraphicsSceneMouseEvent *input);
		
		virtual void photogramChanged(Photogram *const photogram);
		
		virtual DGui::Action *action() const;
		
		int type() const;
		
		virtual void aboutToChangeTool();
		
	private:
		struct Private;
		Private *const d;
		
};
}
}
}

#endif
