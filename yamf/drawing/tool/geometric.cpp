/***************************************************************************
 *   Copyright (C) 2007 by Jorge Cuadrado                                  *
 *   kuadrosxx@gmail.com                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
 
#include "geometric.h"

#include <QPointF>
#include <QKeySequence>
#include <QGraphicsPathItem>
#include <QPainterPath>
#include <QMatrix>
#include <QGraphicsLineItem>
#include <QGraphicsView>

#include <QGraphicsSceneMouseEvent>
#include <QAbstractGraphicsShapeItem>
#include <QToolBar>

#include <QHBoxLayout>
#include <QSpinBox>
#include <QLabel>
#include <dgraphics/algorithm.h>
#include <dcore/algorithm.h>
#include <dcore/debug.h>
#include <dgui/action.h>
#include <dgui/iconloader.h>

#include <drawing/photogram.h>
#include <drawing/brushmanager.h>
#include <drawing/paintarea.h>

#include <model/frame.h>
#include <item/rect.h>
#include <item/ellipse.h>
#include <item/line.h>
#include <QActionGroup>
#include <QAction>

namespace YAMF {

namespace Drawing {

namespace Tool {

struct Geometric::Private
{
	QGraphicsItem *item;
	DGui::Action *action, *rect, *ellipse, *line;
	
	QPointF position;
	QActionGroup *group;
	
	enum Shape {
		Rect = 0,
		Ellipse,
		Line
	};
	
	Shape shape;
	
	
};

/**
 * Construye la herramienta para dibujar figuras geometricas.
 */
Geometric::Geometric(QObject *parent): AbstractTool(parent), d(new Private)
{
	d->item = 0;
	d->action = new DGui::Action( DGui::IconLoader::self()->load("draw-geometric.svg"), tr("Geometric"), this);
	
	d->group = new QActionGroup(this);
	
	d->rect = new DGui::Action(DGui::IconLoader::self()->load("draw-rect.svg"), tr("Rect"), d->group);
	d->rect->setCheckable(true);
	d->group->addAction(d->rect);
	d->group->setExclusive( true );
	
	d->rect->setChecked(true);
	
	d->ellipse = new DGui::Action(DGui::IconLoader::self()->load("draw-ellipse.svg"), tr("Ellipse"), d->group);
	d->ellipse->setCheckable(true);
	d->group->addAction(d->ellipse);
	
	d->line = new DGui::Action (DGui::IconLoader::self()->load("draw-line.svg"), tr("Line"), d->group);
	d->line->setCheckable(true);
	d->group->addAction(d->line);

	d->shape = Private::Rect;
	connect(d->group, SIGNAL( triggered(QAction *)), this, SLOT(onChangeAction(QAction *)));
}

/**
 * @~spanish
 * Destructor
 */
Geometric::~Geometric()
{
	delete d;
}

/**
 * @~spanish
 * Inicializa la herramienta, evita que los items dentro de el fotograma se puedan mover o seleccionar.
 */
void Geometric::init(Photogram *photogram)
{
	foreach(QGraphicsView * view, photogram->views())
	{
		view->setDragMode ( QGraphicsView::NoDrag );
		
		if ( QGraphicsScene *photogram = qobject_cast<QGraphicsScene *>(view->scene()) )
		{
			foreach(QGraphicsItem *item, photogram->items() )
			{
				item->setFlag(QGraphicsItem::ItemIsSelectable, false);
				item->setFlag(QGraphicsItem::ItemIsMovable, false);
			}
		}
	}
}

/**
 * @~spanish
 * Retorna el id de la herramienta
 */
QString Geometric::id() const
{
	return tr("Geometric");
}

/**
 * @~spanish
 * Función sobrecargada para implementar la creación de la figura geometrica.
 */
void Geometric::press(const QGraphicsSceneMouseEvent *input)
{
	PaintArea *paintArea = this->paintArea();
	if(input->buttons() == Qt::LeftButton)
	{
		switch(d->shape)
		{
			case Private::Rect:
			{
				d->item = new Item::Rect;
				static_cast<QAbstractGraphicsShapeItem *>(d->item)->setBrush( paintArea->brushManager()->brush() );
				
				static_cast<QAbstractGraphicsShapeItem *>(d->item)->setPen( paintArea->brushManager()->pen() );
			}
			break;
			case Private::Ellipse:
			{
				d->item = new Item::Ellipse;
				static_cast<QAbstractGraphicsShapeItem *>(d->item)->setBrush( paintArea->brushManager()->brush() );
				
				static_cast<QAbstractGraphicsShapeItem *>(d->item)->setPen( paintArea->brushManager()->pen() );
			}
			break;
			case Private::Line:
			{
				d->item = new Item::Line;
				
				static_cast<Item::Line *>(d->item)->setPen( paintArea->brushManager()->pen() );
			}
			break;
		}
		
		d->item->setZValue(1000);
		d->position = input->scenePos();
		paintArea->photogram()->addItem(d->item);
		
	}
}

/**
 * @~spanish
 * Función sobrecargada para implementar la creación de la figura geometrica.
 */
void Geometric::move(const QGraphicsSceneMouseEvent *input)
{
	PaintArea *paintArea = this->paintArea();
	if(input->buttons() == Qt::LeftButton)
	{
		QRectF rect( d->position, d->item->mapFromScene(input->scenePos()));
		
		switch(d->shape)
		{
			case Private::Rect:
			{
				static_cast<Item::Rect *>(d->item)->setRect(rect);
			}
			break;
			case Private::Ellipse:
			{
				static_cast<Item::Ellipse *>(d->item)->setRect(rect);
			}
			break;
			case Private::Line:
			{
				static_cast<Item::Line *>(d->item)->setLine(QLineF( d->position, d->item->mapFromScene(input->scenePos()) ));
			}
			break;
		}
		
		paintArea->brushManager()->map(d->item);
	}
	
}

/**
 * @~spanish
 * Función sobrecargada para implementar la creación de la figura geometrica.
 */
void Geometric::release(const QGraphicsSceneMouseEvent *input)
{
	PaintArea *paintArea = this->paintArea();
	paintArea->currentFrame()->addItem(d->item);
}

/**
 * @~spanish
 * Retorna la acción que representa la herramienta.
 */
DGui::Action *Geometric::action() const
{
	return d->action;
}

/**
 * @~spanish
 * Retorna que la herramienta es de tipo Brush.
 */
int Geometric::type() const
{
	return AbstractTool::Brush;
}

void Geometric::aboutToChangeTool()
{
}

/**
 * @~spanish
 * Asigna la interfaz gráfica para configurar el tipo de figura geometrica que se va a crear.
 */
void Geometric::setupConfigBar(QToolBar *configBar)
{
	configBar->addActions(d->group->actions());
}

/**
 * @internal
 */
void Geometric::onChangeAction( QAction *action)
{
	if(action == d->rect)
	{
		d->shape = Private::Rect;
	}
	else if(action == d->ellipse)
	{
		d->shape = Private::Ellipse;
	}
	else if(action == d->line)
	{
		d->shape = Private::Line;
	}
}

}

}

}
