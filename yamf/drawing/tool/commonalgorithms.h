/***************************************************************************
 *   Copyright (C) 2007 Jorge Cuadrado                                     *
 *   kuadrosxx@gmail.com                                                   *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/

#ifndef YAMF_DRAWING_TOOLCOMMONALGORITHM_H
#define YAMF_DRAWING_TOOLCOMMONALGORITHM_H

#include <QList>
#include <QGraphicsItem>

namespace DGraphics {
	class ItemTransformer;
}

namespace YAMF {
namespace Drawing {
class PaintArea;

namespace Tool {

namespace Private {
	class ContourEditor;
}
/**
 * @author Jorge Cuadrado <kuadrosxx@gmail.com>
*/
class CommonAlgorithms
{
	public:
			CommonAlgorithms();
			~CommonAlgorithms();
			
			static void createEditors(QList<Drawing::Tool::Private::ContourEditor *> & editors, QList<QGraphicsItem *> items, PaintArea *paintArea);
			
			static void createTransfromers(QList<DGraphics::ItemTransformer *> & transformers, QList<QGraphicsItem*> selecteds, PaintArea *paintArea );
};
}
}
}

#endif
