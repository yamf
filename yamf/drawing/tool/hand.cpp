
/***************************************************************************
 *   Copyright (C) 2007 by Jorge Cuadrado                                  *
 *   kuadrosxx@gmail.com                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
 
#include "hand.h"

#include <drawing/photogram.h>
#include <drawing/brushmanager.h>
#include <drawing/paintarea.h>
#include <model/frame.h>

#include <dgui/action.h>
#include <dgui/iconloader.h>
#include <dcore/debug.h>

#include <QPointF>
#include <QIcon>
#include <QKeySequence>
#include <QMatrix>
#include <QGraphicsLineItem>
#include <QGraphicsView>
#include <QGraphicsRectItem>
#include <QGraphicsSceneMouseEvent>

namespace YAMF {

namespace Drawing {

namespace Tool {

struct Hand::Private
{
	DGui::Action *action;
	PaintArea *paintarea;
};

/**
 * @~spanish
 * Construye la herramienta.
 */
Hand::Hand(QObject *parent)
 : Drawing::AbstractTool(parent), d(new Private)
{
	d->action =  new DGui::Action( DGui::IconLoader::self()->load(".svg"), tr("Hand"), this);
	d->paintarea = 0;
}

/**
 * Destructor
 */
Hand::~Hand()
{
	delete d;
}

/**
 * @~spanish
 * Inicializa la herramienta, evita que los items dentro de el fotograma se puedan mover o seleccionar.
 */
void Hand::init(Photogram *photogram)
{
	foreach(QGraphicsView * view, photogram->views())
	{
		view->setDragMode ( QGraphicsView::NoDrag );
		
		if ( QGraphicsScene *photogram = qobject_cast<QGraphicsScene *>(view->scene()) )
		{
			foreach(QGraphicsItem *item, photogram->items() )
			{
				item->setFlag(QGraphicsItem::ItemIsSelectable, false);
				item->setFlag(QGraphicsItem::ItemIsMovable, false);
			}
		}
	}
}

/**
 * @~spanish
 * Retorna el id de la herramienta.
 */
QString Hand::id() const
{
	return tr("Hand");
}

/**
 * @~spanish
 * Función sobrecargada para habilitar la carateristica de la herramieta.
 */
void Hand::press(const QGraphicsSceneMouseEvent *input)
{
	Q_UNUSED(input)
	PaintArea *paintArea = this->paintArea();
	paintArea->setDragMode(QGraphicsView::ScrollHandDrag);
	d->paintarea = paintArea;
}

/**
 * @~spanish
 * Función sobrecargada para habilitar la carateristica de la herramieta.
 */
void Hand::move(const QGraphicsSceneMouseEvent *input)
{
	Q_UNUSED(input)
}

/**
 * @~spanish
 * Función sobrecargada para habilitar la carateristica de la herramieta.
 */
void Hand::release(const QGraphicsSceneMouseEvent *input)
{
	Q_UNUSED(input)
}

/**
 * @~spanish
 * Retorna la acción que representa la herramienta.
 */
DGui::Action *Hand::action() const
{
	return d->action;
}

/**
 * @~spanish
 * Retorna que la herramienta es de tipo View.
 */
int Hand::type() const
{
	return AbstractTool::View;
}

/**
 * @~spanish
 * Función sobrecargada para desactivar la caracteristica de la herramienta.
 */
void Hand::aboutToChangeTool()
{
	if(d->paintarea)
		d->paintarea->setDragMode(QGraphicsView::NoDrag);
}


}

}

}
