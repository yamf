/***************************************************************************
 *   Copyright (C) 2006 Jorge Cuadrado                                     *
 *   kuadrosxx@gmail.com                                                   *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/

#include "contour.h"
#include <dgui/action.h>

#include <drawing/photogram.h>
#include <drawing/paintarea.h>
#include <drawing/brushmanager.h>

#include <item/path.h>


// #include <item/proxy.h>
// #include <item/serializer.h>

// #include <model/command/convertitem.h>
#include <model/command/editnodesitem.h>
#include <model/frame.h>

#include <QGraphicsItem>
#include <QGraphicsSceneMouseEvent>

#include <dgui/iconloader.h>
#include <dcore/debug.h>
#include "private/contoureditor.h"

#include "commonalgorithms.h"

#include <QList>
#include <QKeyEvent>

namespace YAMF {
namespace Drawing {
namespace Tool {

struct Contour::Private
{
	DGui::Action *action;
	QList<Drawing::Tool::Private::ContourEditor *> editors;
	
	void createEditors(QList<QGraphicsItem *> items, PaintArea *paintArea);
	void execEditPath(PaintArea *paintArea);
};

void Contour::Private::execEditPath(PaintArea *paintArea)
{
	Model::Frame *currentFrame = paintArea->currentFrame();
		
	foreach(Drawing::Tool::Private::ContourEditor *editor, editors)
	{
		if(editor->isModified())
		{
			if(currentFrame->logicalIndexOf(editor->item()) > -1)
			{
				QPainterPath path = editor->item()->path();
				paintArea->addCommand(new Command::EditNodesItem(editor->item(), editor->oldPath(), paintArea->currentFrame()));
				
				editor->save();
			}
		}
	}
}

void Contour::Private::createEditors(QList<QGraphicsItem *> items, PaintArea *paintArea)
{
	CommonAlgorithms::createEditors(editors, items, paintArea);
	
	/*
	QList<Drawing::Tool::Private::ContourEditor *>::iterator it = editors.begin();
	QList<Drawing::Tool::Private::ContourEditor *>::iterator itEnd = editors.end();
	
	while(it != itEnd)
	{
		int parentIndex = paintArea->scene()->selectedItems().indexOf((*it)->item() );
		if(parentIndex != -1 )
		{
			items.removeAt(parentIndex);
		}
		else
		{
			Drawing::Tool::Private::ContourEditor *toRemove = editors.takeAt(editors.indexOf((*it)));
			
			foreach(QGraphicsItem *node, toRemove->nodes())
			{
				paintArea->setAlwaysVisible(node, false);
			}
			delete toRemove;
		}
		++it;
	}
	
	foreach(QGraphicsItem *item, items)
	{
		if(item)
		{
			int index =  paintArea->currentFrame()->logicalIndexOf(item);
			if(index > -1)
			{
				item->setFlag(QGraphicsItem::ItemIsMovable, false);
				if(Item::Path *path = qgraphicsitem_cast<Item::Path*>(item) )
				{
					editors << new Drawing::Tool::Private::ContourEditor(path, paintArea->scene());
					foreach(QGraphicsItem *node, editors.last()->nodes())
					{
						paintArea->setAlwaysVisible(node, true);
					}
				}
				else
				{
					if(item->type() != QGraphicsItemGroup::Type)
					{
						Command::ConvertItem *convertItem = new Command::ConvertItem(item, Item::Path::Type, paintArea->currentFrame());
						paintArea->addCommand(convertItem);
						
						if(convertItem->isValid())
						{
							if(Item::Path *path = qgraphicsitem_cast<Item::Path*>( paintArea->currentFrame()->item(index) ) )
							{
								editors << new Drawing::Tool::Private::ContourEditor(path, paintArea->photogram());
								
								foreach(QGraphicsItem *node, editors.last()->nodes())
								{
									paintArea->setAlwaysVisible(node, true);
								}
							}
							else if(Item::Proxy *proxy = qgraphicsitem_cast<Item::Proxy*>( paintArea->currentFrame()->item(index) ))
							{
								if(Item::Path *path = qgraphicsitem_cast<Item::Path*>( proxy->item() ) )
								{
									editors << new Drawing::Tool::Private::ContourEditor(path,  paintArea->photogram());
									editors.last()->setProxyItem(proxy);
									foreach(QGraphicsItem *node, editors.last()->nodes())
									{
										paintArea->setAlwaysVisible(node, true);
									}
								}
							}
						}
					}
				}
			}
		}
	}*/
	
}

/**
 * @~spanish
 * Construye la herramienta para editar los nodos de un grafico.
 */
Contour::Contour(QObject *parent) : Drawing::AbstractTool(parent), d(new Private)
{
	d->action = new DGui::Action( DGui::IconLoader::self()->load("edit-path.svg"), tr("Contour selection"), this);
}


/**
 * Destructor
 */
Contour::~Contour()
{
	delete d;
}

/**
 * @~spanish
 * Inicializa la herramienta, permite que los items dentro de el fotograma se puedan seleccionar o  pero no mover.
 */
void Contour::init(Photogram *photogram)
{
	qDeleteAll(d->editors);
	d->editors.clear();
	
	foreach(QGraphicsView * view, photogram->views())
	{
		foreach(QGraphicsItem *item, view->scene()->items())
		{
			item->setFlags(QGraphicsItem::ItemIsSelectable);
			item->setFlag(QGraphicsItem::ItemIsMovable, false);
		}
	}
}

/**
 * @~spanish
 * Retorna el id de la herramienta.
 */
QString Contour::id() const
{
	return "contour";
}

/**
 * @~spanish
 * Función sobrecargada para implementar la edición de nodos.
 */
void Contour::press(const QGraphicsSceneMouseEvent *input)
{
	Q_UNUSED(input);
	PaintArea *paintArea = this->paintArea();
}

/**
 * @~spanish
 * Función sobrecargada para implementar la edición de nodos.
 */
void Contour::move(const QGraphicsSceneMouseEvent *input)
{
	Q_UNUSED(input);
}

/**
 * @~spanish
 * Función sobrecargada para implementar la edición de nodos.
 */
void Contour::release(const QGraphicsSceneMouseEvent *input)
{
	Q_UNUSED(input);
	PaintArea *paintArea = this->paintArea();
	
	if(paintArea->scene()->selectedItems().count() > 0)
	{
		d->createEditors(paintArea->scene()->selectedItems(), paintArea);
		d->execEditPath(paintArea);
	}
	else
	{
		foreach(Drawing::Tool::Private::ContourEditor *editor, d->editors)
		{
			foreach(QGraphicsItem *node, editor->nodes())
			{
				paintArea->setAlwaysVisible(node, false);
			}
			delete editor;
		}
		d->editors.clear();
	}
}

/**
 * @~spanish
 * Elimina un nodo si se recibe que se presiono la conbinación de teclas Control+supr.
 */
void Contour::keyPressEvent(QKeyEvent *event)
{
	if(event->key() == Qt::Key_Delete & Qt::Key_Control)
	{
		foreach(Drawing::Tool::Private::ContourEditor *editor, d->editors)
		{
			
			foreach(QGraphicsItem *node, editor->nodes())
			{
				paintArea()->setAlwaysVisible(node, false);
			}
			
			editor->removeSelectedNodes();
			
			foreach(QGraphicsItem *node, editor->nodes())
			{
				paintArea()->setAlwaysVisible(node, true);
			}
			d->execEditPath(paintArea());
		}
	}
}

/**
 * @~spanish
 * Función reimplemenatada para eliminar los nodos cuando se cambie de fotograma.
 */
void Contour::photogramChanged(Photogram *const photogram)
{
	foreach(Drawing::Tool::Private::ContourEditor *editor, d->editors)
	{
		foreach(QGraphicsItem *node, editor->nodes())
		{
			paintArea()->setAlwaysVisible(node, false);
		}
		delete editor;
	}
	d->editors.clear();
		
// 	d->createEditors(paintArea());
}

/**
 * @~spanish
 * Retorna la acción que representa la herramienta.
 */
DGui::Action *Contour::action() const
{
	return d->action;
}

/**
 * @~spanish
 * Retorna que la herramienta es de tipo Selection.
 */
int Contour::type() const
{
	return AbstractTool::Selection;
}

/**
 * @~spanish
 * Elimina los nodos cuando se cambia de herramienta.
 */
void Contour::aboutToChangeTool()
{
	foreach(Drawing::Tool::Private::ContourEditor *editor, d->editors)
	{
		foreach(QGraphicsItem *node, editor->nodes())
		{
			paintArea()->setAlwaysVisible(node, false);
		}
		delete editor;
	}
	d->editors.clear();
}


}
}
}
