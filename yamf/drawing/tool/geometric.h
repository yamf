/***************************************************************************
 *   Copyright (C) 2007 by Jorge Cuadrado                                  *
 *   kuadrosxx@gmail.com                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef YAMF_DRAWING_TOOLGEOMETRIC_H
#define YAMF_DRAWING_TOOLGEOMETRIC_H

#include <yamf/drawing/abstracttool.h>
#include <yamf/common/yamf_exports.h>

class QAction;

namespace YAMF {

namespace Drawing {

namespace Tool {

/**
 * @~spanish
 * @ingroup tools
 * @brief Esta clase provee de una herramienta para crear figuras geometricos(linas, rectangulos, elipses).
 * @author Jorge Cuadrado <kuadrosxx@gmail.com>
*/
class YAMF_EXPORT Geometric : public Drawing::AbstractTool
{
	Q_OBJECT;
	public:
		Geometric(QObject *parent = 0);
		virtual ~Geometric();
		
		virtual void init(Photogram *photogram);
		
		virtual QString id() const;
		virtual void press(const QGraphicsSceneMouseEvent *input);
		virtual void move(const QGraphicsSceneMouseEvent *input);
		virtual void release(const QGraphicsSceneMouseEvent *input);
		
		virtual DGui::Action *action() const;
		
		int type() const;
		
		virtual void aboutToChangeTool();
		
		void setupConfigBar(QToolBar *configBar);
		
	private slots:
		void onChangeAction( QAction *action);
		
	private:
		struct Private;
		Private *const d;
};

}

}

}

#endif
