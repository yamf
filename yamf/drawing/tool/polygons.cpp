/***************************************************************************
 *   Copyright (C) 2007 by Jorge Cuadrado                                  *
 *   kuadrosxx@gmail.com                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "polygons.h"

#include <QPointF>
#include <QKeySequence>
#include <QGraphicsPathItem>
#include <QPainterPath>
#include <QMatrix>
#include <QGraphicsLineItem>
#include <QGraphicsView>

#include <QGraphicsSceneMouseEvent>
#include <QToolBar>

#include <QHBoxLayout>
#include <QSpinBox>
#include <QLabel>
#include <dgraphics/algorithm.h>
#include <dcore/algorithm.h>
#include <dcore/debug.h>
#include <dgui/action.h>
#include <dgui/iconloader.h>

#include <drawing/photogram.h>
#include <drawing/brushmanager.h>
#include <drawing/paintarea.h>

#include <model/frame.h>
#include <yamf/item/polygon.h>


namespace YAMF {
namespace Drawing {
namespace Tool {

struct Polygons::Private
{
	YAMF::Item::Polygon *item;
	DGui::Action *action;
	QPointF position;
	QSpinBox *corners;
	QDoubleSpinBox *start;
	QWidget *configWidget;
};

/**
 * @~spanish
 * Construye la herramienta para dibujar poligonos.
 */
Polygons::Polygons(QObject *parent ): AbstractTool(parent), d(new Private)
{
	d->item = 0;
	d->action = new DGui::Action( DGui::IconLoader::self()->load("draw-polygon.svg"), tr("Polygon"), this);
	
	d->corners = new QSpinBox;
	d->corners->setValue(3);
	
	d->start = new QDoubleSpinBox;
	d->start->setValue(0.0);
	d->start->setMaximum(1.0);
	d->start->setSingleStep(0.01);
	
	connect(d->start, SIGNAL(valueChanged(double)), this, SLOT(onChangeStart(double)));
	
	d->configWidget = new QWidget;
	QHBoxLayout *layout = new QHBoxLayout(d->configWidget);
	layout->setMargin(0);
	layout->setSpacing(2);
	
	layout->addWidget(new QLabel(tr("Corners")));
	layout->addWidget(d->corners);
	
	layout->addWidget(new QLabel(tr("Start")));
	layout->addWidget(d->start);
	
}

/**
 * @~spanish
 * Destructor
 */
Polygons::~Polygons()
{
	delete d->configWidget;
	delete d;
}

/**
 * @~spanish
 * Inicializa la herramienta, evita que los items dentro de el fotograma se puedan mover o seleccionar.
 */
void Polygons::init(Photogram *photogram)
{
	foreach(QGraphicsView * view, photogram->views())
	{
		view->setDragMode ( QGraphicsView::NoDrag );
		
		if ( QGraphicsScene *photogram = qobject_cast<QGraphicsScene *>(view->scene()) )
		{
			foreach(QGraphicsItem *item, photogram->items() )
			{
				item->setFlag(QGraphicsItem::ItemIsSelectable, false);
				item->setFlag(QGraphicsItem::ItemIsMovable, false);
			}
		}
	}
}

/**
 * @~spanish
 * Retorna el id de la herramienta
 */
QString Polygons::id() const
{
	return tr("Polygons");
}

/**
 * @~spanish
 * Función sobrecargada para implementar la creación de los poligonos.
 */
void Polygons::press(const QGraphicsSceneMouseEvent *input)
{
	PaintArea *paintArea = this->paintArea();
	if(input->buttons() == Qt::LeftButton)
	{
		d->item = new Item::Polygon(d->corners->value());
		
		d->item->setPen( paintArea->brushManager()->pen() );
		d->item->setBrush( paintArea->brushManager()->brush() );
		d->item->setStart(d->start->value());
		d->position = input->scenePos();
		paintArea->currentFrame()->addItem(d->item);
	}
}

/**
 * @~spanish
 * Función sobrecargada para implementar la creación de los poligonos.
 */
void Polygons::move(const QGraphicsSceneMouseEvent *input)
{
	if(input->buttons() == Qt::LeftButton)
	{
		QRectF rect( d->position, d->item->mapFromScene(input->scenePos()));
		d->item->setRect(rect);
	}
}
/**
 * @~spanish
 * Función sobrecargada para implementar la creación de los poligonos.
 */
void Polygons::release(const QGraphicsSceneMouseEvent *input)
{
}

/**
 * @~spanish
 * Retorna la acción que representa la herramienta.
 */
DGui::Action *Polygons::action() const
{
	return d->action;
}

/**
 * @~spanish
 * Retorna que la herramienta es de tipo Brush.
 */
int Polygons::type() const
{
	return AbstractTool::Brush;
}

void Polygons::aboutToChangeTool()
{
}

/**
 * @~spanish
 * Asigna la interfaz gráfica para configurar el numero de vertices del poligono y donde empieza.
 */
void Polygons::setupConfigBar(QToolBar *configBar)
{
	configBar->addWidget(d->configWidget)->setVisible(true);
}

/**
 * @internal
 */
void Polygons::onChangeStart(double start)
{
	if(d->item)
	{
		d->item->setStart(start);
	}
}

}
}
}

