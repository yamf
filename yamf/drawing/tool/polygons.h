/***************************************************************************
 *   Copyright (C) 2007 by Jorge Cuadrado                                  *
 *   kuadrosxx@gmail.com                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
 
#ifndef POLYGONS_H
#define POLYGONS_H


#include <yamf/drawing/abstracttool.h>
#include <yamf/common/yamf_exports.h>

#include <QPointF>

namespace YAMF {

namespace Item {
	class Polygon; }

namespace Drawing {

class BrushManager;

namespace Tool {

/**
 * @ingroup tools
 * @~spanish
 * @brief Esta clase provee de una herramienta para la creación de poligonos.
 * @author Jorge Cuadrado <kuadrosxx@gmail.com>
*/
class YAMF_EXPORT Polygons: public Drawing::AbstractTool
{
	Q_OBJECT
	public:
		Polygons(QObject *parent = 0);
		virtual ~Polygons();
		
		virtual void init(Photogram *photogram);
		
		virtual QString id() const;
		virtual void press(const QGraphicsSceneMouseEvent *input);
		virtual void move(const QGraphicsSceneMouseEvent *input);
		virtual void release(const QGraphicsSceneMouseEvent *input);
		
		virtual DGui::Action *action() const;
		
		int type() const;
		
		virtual void aboutToChangeTool();
		
		void setupConfigBar(QToolBar *configBar);
		
	private slots:
		void onChangeStart(double start);
		
	private:
		struct Private;
		Private *const d;
		
};
}
}
}

#endif
