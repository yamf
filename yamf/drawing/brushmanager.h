/***************************************************************************
 *   Copyright (C) 2006 by David Cuadrado                                  *
 *   krawek@gmail.com                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef  YAMF_DRAWINGBRUSHMANAGER_H
#define  YAMF_DRAWINGBRUSHMANAGER_H

#include <QObject>
#include <QPen>
#include <QBrush>

#include <yamf/common/yamf_exports.h>

class QGraphicsItem;

namespace YAMF {
namespace Drawing {

/**
 * @ingroup drawing
 * @~spanish
 * @brief Esta clase permite modificar la brocha de relleno, la brocha del contorno y el estilo de linea, con las cuales se crearán los graficos dentro del area de dibujo.
 * @author David Cuadrado <krawek@gmail.com>
*/
class YAMF_EXPORT BrushManager : public QObject
{
	Q_OBJECT;
	public:
		BrushManager(QObject * parent = 0);
		BrushManager(const QPen &pen, const QBrush &brush, QObject * parent = 0);
		~BrushManager();
		
		QPen pen() const;
		QBrush brush() const;
		int penWidth() const;
		QColor penColor() const;
		QBrush penBrush() const;
		
		QColor brushColor() const;
		
		void apply(QGraphicsItem *item);
		void map(QGraphicsItem *item);
		
		QBrush mapBrush(const QRectF &rect, const QBrush &brush);
		
		
	public slots:
		void setPenBrush(const QBrush &brush);
		void setPen(const QPen &pen);
		void setBrush(const QBrush &brush);
		
	signals:
		void penChanged(const QPen &pen);
		void brushChanged(const QBrush &brush);
		
	private:
		struct Private;
		Private *const d;

};

}
}

#endif
