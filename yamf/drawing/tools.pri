
INSTALLS += tools

tools.files += *.h 
tools.path = /include/yamf/drawing/tool

HEADERS += tool/brush.h \
           tool/contour.h \
           tool/private/contoureditor.h \
           tool/select.h \
           tool/polyline.h \
           tool/polygons.h \
           tool/geometric.h \
           tool/text.h \
           tool/colorpicker.h  \
           tool/zoom.h \
           tool/hand.h \
	   tool/fill.h \
	   tool/itween.h \
           tool/commonalgorithms.h
SOURCES += tool/brush.cpp \
           tool/contour.cpp \
           tool/private/contoureditor.cpp \
           tool/select.cpp \
           tool/polyline.cpp \
           tool/polygons.cpp \
           tool/geometric.cpp \
           tool/text.cpp \
           tool/colorpicker.cpp  \
           tool/zoom.cpp \
           tool/hand.cpp \
	   tool/fill.cpp \
	   tool/itween.cpp \
           tool/commonalgorithms.cpp
