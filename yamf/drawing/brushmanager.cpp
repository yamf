/***************************************************************************
 *   Copyright (C) 2006 by David Cuadrado                                  *
 *   krawek@gmail.com                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "brushmanager.h"

#include <dcore/config.h>

#include <QGraphicsLineItem>
#include <QAbstractGraphicsShapeItem>


namespace YAMF {
namespace Drawing {

struct BrushManager::Private
{
	Private() : pen(QPen(Qt::black, 1, Qt::SolidLine, Qt::RoundCap)), brush(Qt::transparent) {}
	QPen pen;
	QBrush brush;
	
	void loadSettings();
	void saveSettings();
};

/**
 * @internal
 */
void BrushManager::Private::loadSettings()
{
	DCore::Config *config = DCore::Config::self();
	config->beginGroup("PaintArea");
	
	brush = qvariant_cast<QBrush>(config->value("last_brush", Qt::transparent));
	pen = qvariant_cast<QPen>(config->value("last_pen", QPen(Qt::black, 2)));
	
	config->endGroup();
}

/**
 * @internal
 */
void BrushManager::Private::saveSettings()
{
	DCore::Config *config = DCore::Config::self();
	config->beginGroup("PaintArea");
	
	config->setValue("last_brush", brush);
	config->setValue("last_pen", pen);
	
	config->endGroup();
}

/**
 * @~spanish
 * Construye el manejador de brochas tomando los colores de la configuracion de la aplicación.
 */
BrushManager::BrushManager(QObject * parent) : QObject(parent), d(new Private)
{
	d->loadSettings();
}

/**
 * @~spanish
 * Construye el manejador de brochas con un estilo de contorno @p pen y una brocha de relleno @p brush.
 */
BrushManager::BrushManager(const QPen &pen, const QBrush &brush, QObject * parent) : QObject(parent), d(new Private)
{
	d->pen = pen;
	d->brush = brush;
}

/**
 * Destructor
 */
BrushManager::~BrushManager()
{
	d->saveSettings();
	delete d;
}

/**
 * @~spanish
 * Asigna un estilo de contorno @p pen.
 */
void BrushManager::setPen(const QPen &pen)
{
	d->pen = pen;
	emit penChanged( pen );
}

/**
 * @~spanish
 * Asigna el color del estilo de contorno @p brush.
 */
void BrushManager::setPenBrush(const QBrush &brush)
{
	d->pen.setBrush(brush);
	emit penChanged( d->pen );
}


/**
 * @~spanish
 * Retorna el estilo de contorno.
 */
QPen BrushManager::pen() const
{
	return d->pen;
}


/**
 * @~spanish
 * Asigna la brocha de relleno @p brush.
 */
void BrushManager::setBrush(const QBrush &brush)
{
	d->brush = brush;
	emit brushChanged( brush );
}

/**
 * @~spanish
 * Retorna la brocha de relleno.
 */
QBrush BrushManager::brush() const
{
	return d->brush;
}


/**
 * @~spanish
 * Retorna el ancho del contorno.
 */
int BrushManager::penWidth() const
{
	return d->pen.width();
}

/**
 * @~spanish
 * Retorna el color del contorno.
 */
QColor BrushManager::penColor() const
{
	return d->pen.color();
}

/**
 * @~spanish
 * Retorna la brocha del contorno.
 */
QBrush BrushManager::penBrush() const
{
	return d->pen.brush();
}

/**
 * @~spanish
 * Retorna la brocha de relleno.
 * @return 
 */
QColor BrushManager::brushColor() const
{
	return d->brush.color();
}

void BrushManager::apply(QGraphicsItem *item)
{
	if(QAbstractGraphicsShapeItem *shape = dynamic_cast<QAbstractGraphicsShapeItem *>(item))
	{
		shape->setBrush(d->brush);
		shape->setPen(d->pen);
	}
	else if( item->type() == QGraphicsLineItem::Type )
	{
		static_cast<QGraphicsLineItem *>(item)->setPen(d->pen);
	}
	
	map(item);
}

void BrushManager::map(QGraphicsItem * item)
{
	if(!item)
	{
		return;
	}
	
	QRectF rect = item->sceneBoundingRect();
	if(QAbstractGraphicsShapeItem *shape = dynamic_cast<QAbstractGraphicsShapeItem *>(item))
	{
		QBrush brush = mapBrush(rect, shape->brush());
		shape->setBrush(brush);
		
		QPen pen = shape->pen();
		brush = pen.brush();
		pen.setBrush(mapBrush(rect, brush) );
		shape->setPen(pen);
	}
	else if( item->type() == QGraphicsLineItem::Type )
	{
		QPen pen = static_cast<QGraphicsLineItem *>(item)->pen();
		QBrush brush = pen.brush();
		pen.setBrush(mapBrush(rect, brush));
		static_cast<QGraphicsLineItem *>(item)->setPen(pen);
	}
}

QBrush BrushManager::mapBrush(const QRectF &rect, const QBrush &brush)
{
	QBrush newBrush = brush;
	
	QMatrix m;
	m.translate(rect.x(), rect.y());
	m.scale( rect.width() / 100.0, rect.height() / 100.0);
	
	newBrush.setMatrix(m);
	
	return newBrush;
}


}
}

