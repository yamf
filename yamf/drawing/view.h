/***************************************************************************
 *   Copyright (C) 2007 David Cuadrado                                     *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/

#ifndef YAMF_DRAWINGVIEW_H
#define YAMF_DRAWINGVIEW_H

#include <QFrame>

#include <yamf/common/yamf_exports.h>

namespace YAMF {

namespace Model {
	class Project;
}

namespace Command {
	class Manager;
}

namespace Drawing {

class PaintArea;

/**
 * @ingroup drawing
 * @~spanish
 * @brief Esta clase integra en una interfaz grafica: el area de dibujo, las reglas horizontal y vertical, además de un panel para la configuración de algunas opciones del area de dibujo.
 * 
 * @author David Cuadrado <krawek@gmail.com>
*/
class YAMF_EXPORT View : public QFrame
{
	Q_OBJECT
	public:
		View(Model::Project *const project, QWidget *parent = 0);
		~View();
		
		PaintArea *paintArea() const;
		
	public slots:
		void setBrush(const QBrush &brush);
		void setCurrentFrame(int sceneIndex, int layerIndex, int frameIndex);
		void setCurrentLayer(int sceneIndex, int layerIndex);
		void setCurrentScene(int sceneIndex);
		
		void addToLibrary();
		void addSymbol(const QString &symbol);
		
	private slots:
		void onCursorMoved(const QPointF &pos);
		void onScaled(double factor);
		
	signals:
		void penChanged( const QPen &pen );
		void brushChanged( const QBrush &brush);
		
	private:
		struct Private;
		Private *const d;
};

}

}

#endif
