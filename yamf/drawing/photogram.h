/***************************************************************************
 *   Copyright (C) 2006 by David Cuadrado                                  *
 *   krawek@gmail.com                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef YAMF_DRAWINGPHOTOGRAM_H
#define YAMF_DRAWINGPHOTOGRAM_H

#include <QGraphicsScene>

#include <yamf/common/yamf_exports.h>

class QMouseEvent;

namespace YAMF {

namespace Model {
	class Scene;
	class Layer;
	class Frame;
	class Object;
}

namespace Drawing {

class Model::Frame;
class Model::Object;
class Model::Scene;

class AbstractTool;
class BrushManager;
class PaintArea;

/**
 * @ingroup drawing
 * @brief Esta clase presenta un fotograma, un fotograma es agrupación de capas en el mismo marco.
 * @author David Cuadrado \<krawek@gmail.com\>
*/
class YAMF_EXPORT Photogram : public QGraphicsScene
{
	Q_OBJECT;
	
	friend class PaintArea;
	public:
		Photogram(PaintArea *parent = 0);
		~Photogram();
		
		void setCurrent(int pos);
		
		void setCurrentScene(Model::Scene *scene);
		
		void setAlwaysVisible(QGraphicsItem *item, bool e);
		
		void drawItems(QPainter *painter, int numItems, QGraphicsItem *items[], const QStyleOptionGraphicsItem options[], QWidget *widget);
		
		void drawPhotogram(int photogram);
		void addFrame(Model::Frame *frame, double opacity = 1.0);
		void addGraphicObject(Model::Object *object, double opacity = 1.0);
		
		void clean();
		
		void setNextOnionSkinCount(int n);
		void setPreviousOnionSkinCount(int n);
		
		void setLayerVisible(int layerIndex, bool visible);
		
		Model::Scene *scene() const;
		
		void setTool(AbstractTool *tool);
		AbstractTool *currentTool() const;
		
		bool isDrawing() const;
		
		BrushManager *brushManager() const;
		virtual void aboutToMousePress();
		
	public slots:
		void drawCurrentPhotogram();
		
	protected:
		void mouseMoved(QGraphicsSceneMouseEvent *event);
		void mouseReleased(QGraphicsSceneMouseEvent *event);
		
		virtual void mousePressEvent(QGraphicsSceneMouseEvent *event);
		virtual void mouseMoveEvent(QGraphicsSceneMouseEvent *event);
		virtual void mouseDoubleClickEvent(QGraphicsSceneMouseEvent *mouseEvent);
		virtual void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);
		virtual void keyPressEvent(QKeyEvent *keyEvent);
		
		virtual void dragLeaveEvent( QGraphicsSceneDragDropEvent * event );
		virtual void dragEnterEvent( QGraphicsSceneDragDropEvent * event );
		virtual void dragMoveEvent( QGraphicsSceneDragDropEvent * event );
		virtual void dropEvent( QGraphicsSceneDragDropEvent * event );
		
		virtual bool event(QEvent *e);
		
		
	private:
		struct Private;
		Private *const d;
};

}
}

#endif
