/***************************************************************************
 *   Copyright (C) 2007 David Cuadrado                                     *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/

#include "projectitemmodel.h"

#include "model/project.h"
#include "model/scene.h"
#include "model/layer.h"
#include "model/frame.h"
#include "model/object.h"

#include "model/command/manager.h"

namespace YAMF {
namespace Gui {

ProjectViewItem::ProjectViewItem(Type type, ProjectViewItem *parent) : m_parent(parent), m_type(type)
{
	if( parent )
		parent->add(this);
}

ProjectViewItem::~ProjectViewItem()
{
	if( m_parent )
		m_parent->remove(this);
	
	foreach(ProjectViewItem *item, m_children)
	{
		item->m_parent = 0;
		delete item;
	}
}


void ProjectViewItem::add(ProjectViewItem *child)
{
	m_children << child;
}

void ProjectViewItem::remove(ProjectViewItem *child)
{
	m_children.removeAll(child);
}

ProjectViewItem *ProjectViewItem::child(int row)
{
	return m_children.value(row);
}

int ProjectViewItem::childCount() const
{
	return m_children.count();
}

int ProjectViewItem::columnCount() const
{
	return 1;
}

int ProjectViewItem::row() const
{
	if (m_parent)
		return m_parent->m_children.lastIndexOf(const_cast<ProjectViewItem*>(this));
	
	return 0;
}

void ProjectViewItem::setParent(ProjectViewItem *item)
{
	if( m_parent )
		m_parent->remove(this);
	m_parent = item;
	if( m_parent )
		m_parent->add(this);
}

ProjectViewItem *ProjectViewItem::parent() const
{
	return m_parent;
}

void ProjectViewItem::setText(const QString &text)
{
	m_text = text;
}

QString ProjectViewItem::text() const
{
	return m_text;
}

ProjectViewItem::Type ProjectViewItem::type() const
{
	return m_type;
}

ProjectItemModel::ProjectItemModel(QObject *parent)
 : QAbstractItemModel(parent), m_project(0)
{
	m_root = new ProjectViewItem(ProjectViewItem::None);
}

ProjectItemModel::ProjectItemModel(Model::Project *project, QObject *parent)
 : QAbstractItemModel(parent), m_project(0)
{
	m_root = new ProjectViewItem(ProjectViewItem::None);
	
	setProject(project);
}

ProjectItemModel::~ProjectItemModel()
{
	delete m_root;
}

QVariant ProjectItemModel::data(const QModelIndex &index, int role) const
{
	if (!index.isValid())
		return QVariant();
	
	if( role != Qt::DisplayRole ) return QVariant();
	
	ProjectViewItem *item = static_cast<ProjectViewItem*>(index.internalPointer());
	
	return item->text();
}

// Qt::ItemFlags ProjectItemModel::flags(const QModelIndex &index) const
// {
// }
// 
// QVariant ProjectItemModel::headerData(int section, Qt::Orientation orientation, int role) const
// {
// }

QModelIndex ProjectItemModel::index(int row, int column, const QModelIndex &parent) const
{
	ProjectViewItem *parentItem;

	if (!parent.isValid())
		parentItem = m_root;
	else
		parentItem = static_cast<ProjectViewItem*>(parent.internalPointer());
	
	ProjectViewItem *childItem = parentItem->child(row);
	
	if (childItem)
		return createIndex(row, column, childItem);
	
	return QModelIndex();
}

QModelIndex ProjectItemModel::parent(const QModelIndex &index) const
{
	if (!index.isValid())
		return QModelIndex();
	
	ProjectViewItem *childItem = reinterpret_cast<ProjectViewItem*>(index.internalPointer());
	
	if( !childItem )
		return QModelIndex();
	
	ProjectViewItem *parentItem = childItem->parent();

	if (parentItem == m_root)
		return QModelIndex();

	return createIndex(parentItem->row(), 0, parentItem);
}

int ProjectItemModel::rowCount(const QModelIndex &parent) const
{
	ProjectViewItem *parentItem = 0;
	
	if (!parent.isValid())
		parentItem = m_root;
	else
		parentItem = static_cast<ProjectViewItem*>(parent.internalPointer());

	return parentItem->childCount();
}

int ProjectItemModel::columnCount(const QModelIndex &parent) const
{
	if (parent.isValid())
		return static_cast<ProjectViewItem*>(parent.internalPointer())->columnCount();
	
	return m_root->columnCount();
}


void ProjectItemModel::setProject(Model::Project *project)
{
	if( m_project == project) return;
	
	if( m_project )
	{
		disconnect(m_project->commandManager(), SIGNAL(historyChanged()), this, SLOT(onProjectChanged()));
	}
	m_project = project;
	
	if( m_project )
	{
		connect(m_project->commandManager(), SIGNAL(historyChanged()), this, SLOT(onProjectChanged()));
	}
	
	delete m_root;
	m_root = new ProjectViewItem(ProjectViewItem::None);
	
	ProjectViewItem *projectItem = new ProjectViewItem(ProjectViewItem::Project, m_root);
	projectItem->setText(tr("Project: %1").arg(m_project->projectName()));
	
	onProjectChanged();
}

Model::Project *ProjectItemModel::project() const
{
	return m_project;
}

void ProjectItemModel::reload()
{
	onProjectChanged();
}

Model::Scene *ProjectItemModel::sceneFromIndex(const QModelIndex &index) const
{
	ProjectViewItem *item = reinterpret_cast<ProjectViewItem *>(index.internalPointer());
	
	return m_scenes.key(item);
}

Model::Layer *ProjectItemModel::layerFromIndex(const QModelIndex &index) const
{
	ProjectViewItem *item = reinterpret_cast<ProjectViewItem *>(index.internalPointer());
	
	return m_layers.key(item);
}

Model::Frame *ProjectItemModel::frameFromIndex(const QModelIndex &index) const
{
	ProjectViewItem *item = reinterpret_cast<ProjectViewItem *>(index.internalPointer());
	
	return m_frames.key(item);
}

Model::Object *ProjectItemModel::objectFromIndex(const QModelIndex &index) const
{
	ProjectViewItem *item = reinterpret_cast<ProjectViewItem *>(index.internalPointer());
	
	return m_objects.key(item);
}

ProjectViewItem *ProjectItemModel::itemFromIndex(const QModelIndex &index) const
{
	return reinterpret_cast<ProjectViewItem *>(index.internalPointer());
}

void ProjectItemModel::onProjectChanged()
{
	if( m_project == 0 ) return;
	
	QHash<Model::Scene *, ProjectViewItem *> scenes;
	QHash<Model::Layer *, ProjectViewItem *> layers;
	QHash<Model::Frame *, ProjectViewItem *> frames;
	QHash<Model::Object *, ProjectViewItem *> objects;
	
	foreach(Model::Scene *scene, m_project->scenes().values() )
	{
		ProjectViewItem *sceneItem = 0;
		
		if( !m_scenes.contains(scene) )
			sceneItem = new ProjectViewItem( ProjectViewItem::Scene, m_root->child(0) );
		else
		{
			sceneItem = m_scenes.take(scene);
		}
		
		sceneItem->setParent(m_root->child(0));
		sceneItem->setText(scene->sceneName());
		scenes[scene] = sceneItem;
		
		foreach(Model::Layer *layer, scene->layers().values() )
		{
			ProjectViewItem *layerItem = 0;
			
			if( !m_layers.contains(layer) )
			{
				layerItem = new ProjectViewItem(ProjectViewItem::Layer, sceneItem);
			}
			else
			{
				layerItem = m_layers.take(layer);
			}
			
			layerItem->setParent(sceneItem);
			layerItem->setText(layer->layerName());
			layers[layer] = layerItem;
			
			foreach(Model::Frame *frame, layer->frames().values() )
			{
				ProjectViewItem *frameItem = 0;
				if( !m_frames.contains(frame) )
				{
					frameItem = new ProjectViewItem(ProjectViewItem::Frame, layerItem);
				}
				else
				{
					frameItem = m_frames.take(frame);
				}
				
				frameItem->setParent(layerItem);
				frameItem->setText(frame->frameName());
				frames[frame] = frameItem;
				
				foreach(Model::Object *object, frame->graphics().values() )
				{
					ProjectViewItem *graphicItem = 0;
					if( !m_objects.contains(object) )
					{
						graphicItem = new ProjectViewItem(ProjectViewItem::Object, frameItem);
					}
					else
					{
						graphicItem = m_objects.take(object);
					}
					
					graphicItem->setParent(frameItem);
					graphicItem->setText(object->objectName());
					objects[object] = graphicItem;
				}
			}
		}
	}
	
	emit layoutAboutToBeChanged();
	
	QList<ProjectViewItem *> toDelete;
	
	foreach(ProjectViewItem *item, m_objects)
	{
		int row = item->row();
		
		createIndex(row, 0, item);
		
		ProjectViewItem *parent = item->parent();
		if( parent )
			parent->remove(item);
		
		toDelete << item;
	}
	
	foreach(ProjectViewItem *item, m_frames)
	{
		int row = item->row();
		
		createIndex(row, 0, item);
		
		ProjectViewItem *parent = item->parent();
		if( parent )
			parent->remove(item);
		
		toDelete << item;
	}
	
	foreach(ProjectViewItem *item, m_layers)
	{
		int row = item->row();
// 		int col = item->childCount();
		
		createIndex(row, 0, item);
		
		ProjectViewItem *parent = item->parent();
		if( parent )
			parent->remove(item);
		
		toDelete << item;
	}
	
	foreach(ProjectViewItem *item, m_scenes)
	{
		int row = item->row();
		int col = item->childCount();
		
		createIndex(row, col, item);
		
		ProjectViewItem *parent = item->parent();
		if( parent )
			parent->remove(item);
		
		toDelete << item;
	}
	
	m_objects = objects;
	m_frames = frames;
	m_layers = layers;
	m_scenes = scenes;
	
	reset();
	emit layoutChanged();
	
	qDeleteAll(toDelete);
}


}

}

