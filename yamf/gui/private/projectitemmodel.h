/***************************************************************************
 *   Copyright (C) 2007 David Cuadrado                                     *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/

#ifndef YAMF_GUIPROJECTITEMMODEL_H
#define YAMF_GUIPROJECTITEMMODEL_H

#include <yamf/common/yamf_exports.h>
#include <QAbstractItemModel>
#include <QHash>

namespace YAMF {

namespace Model {
class Project;
class Scene;
class Layer;
class Frame;
class Object;
}

namespace Gui {

class ProjectViewItem
{
	public:
		enum Type {
			None = 0x0,
			Project,
			Scene,
			Layer,
			Frame,
			Object
		};
		ProjectViewItem(Type type, ProjectViewItem *parent = 0);
		~ProjectViewItem();
		
		void add(ProjectViewItem *child);
		void remove(ProjectViewItem *child);
		
		ProjectViewItem *child(int row);
		int childCount() const;
		int columnCount() const;
		int row() const;
		
		void setParent(ProjectViewItem *item);
		ProjectViewItem *parent() const;
		
		void setText(const QString &text);
		QString text() const;
		
		Type type() const;
		
	private:
		ProjectViewItem *m_parent;
		Type m_type;
		QList<ProjectViewItem *> m_children;
		QString m_text;
};


/**
 * @ingroup gui
 * @author David Cuadrado <krawek@gmail.com>
*/
class YAMF_EXPORT ProjectItemModel : public QAbstractItemModel
{
	Q_OBJECT;
	
	public:
		ProjectItemModel(QObject *parent = 0);
		ProjectItemModel(Model::Project *project, QObject *parent = 0);
		~ProjectItemModel();
		
		QVariant data(const QModelIndex &index, int role) const;
		QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const;
		QModelIndex parent(const QModelIndex &index) const;
		int rowCount(const QModelIndex &parent = QModelIndex()) const;
		int columnCount(const QModelIndex &parent = QModelIndex()) const;
		
		void setProject(Model::Project *project);
		Model::Project *project() const;
		
		void reload();
		
		Model::Scene *sceneFromIndex(const QModelIndex &index) const;
		Model::Layer *layerFromIndex(const QModelIndex &index) const;
		Model::Frame *frameFromIndex(const QModelIndex &index) const;
		Model::Object *objectFromIndex(const QModelIndex &index) const;
		
		ProjectViewItem *itemFromIndex(const QModelIndex &index) const;
		
	private slots:
		void onProjectChanged();
		
	private:
		Model::Project *m_project;
		ProjectViewItem *m_root;
		
		QHash<Model::Scene *, ProjectViewItem *> m_scenes;
		QHash<Model::Layer *, ProjectViewItem *> m_layers;
		QHash<Model::Frame *, ProjectViewItem *> m_frames;
		QHash<Model::Object *, ProjectViewItem *> m_objects;
};

}

}

#endif
