/***************************************************************************
 *   Copyright (C) 2007 David Cuadrado                                     *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/

 

#ifndef YAMF_GUITOOLSWIDGET_H
#define YAMF_GUITOOLSWIDGET_H

#include <dgui/configurabletoolbar.h>
#include <yamf/common/yamf_exports.h>

namespace YAMF {
namespace Drawing { 
class PaintArea;
class AbstractTool;
}
namespace Gui {

/**
 * @ingroup gui
 * @~spanish
 * @brief Esta clase provee de una barra de herramientas especial para agregar herramientas de que tabajando sobre el area de dibujo.
 * @author David Cuadrado <krawek@gmail.com>
 */
class YAMF_EXPORT ToolsWidget : public DGui::ConfigurableToolBar
{
	Q_OBJECT
	public:
		ToolsWidget(Drawing::PaintArea *paintArea, QWidget *parent = 0);
		~ToolsWidget();
		
		void addTool(Drawing::AbstractTool *tool);
	
	private slots:
		void toolFromAction(QAction *act);
		
	private:
		struct Private;
		Private *const d;
};

}

}

#endif
