/***************************************************************************
 *   Copyright (C) 2007 David Cuadrado                                     *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/

 

#include "drawingactions.h"

#include "drawing/paintarea.h"
#include "model/project.h"
#include "model/command/manager.h"

#include <dgui/commandhistory.h>

namespace YAMF {
namespace Gui {

/**
 * @~spanish
 * Crea la barra de herramientas con las acciones que modifican el contenido de un area de dibujo@p paintarea.
 */
DrawingActions::DrawingActions(Drawing::PaintArea *paintarea, QWidget *parent)
 : QToolBar(tr("Drawing actions"), parent)
{
	this->addAction(paintarea->history()->undoAction());
	this->addAction(paintarea->history()->redoAction());
	
	this->addAction(paintarea->cutAction());
	this->addAction(paintarea->copyAction());
	this->addAction(paintarea->pasteAction());
	
	
	this->addAction(paintarea->removeAction());
	this->addAction(paintarea->selectAllAction());
	this->addAction(paintarea->groupAction());
	this->addAction(paintarea->ungroupAction());
	
	this->addAction(paintarea->bringToFrontAction());
	this->addAction(paintarea->sendToBackAction());
	this->addAction(paintarea->sendBackwardsAction());
	this->addAction(paintarea->bringForwardsAction());
}


/**
 * destructor
 */
DrawingActions::~DrawingActions()
{
}


}

}
