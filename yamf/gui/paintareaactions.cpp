/***************************************************************************
 *   Copyright (C) 2007 David Cuadrado                                     *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/

#include "paintareaactions.h"

// DLib
#include <dgui/iconloader.h>


// Yamf
#include <yamf/drawing/paintarea.h>
#include <yamf/drawing/photogram.h>

// Qt
#include <QAction>
#include <QSpinBox>

namespace YAMF {
namespace Gui {

struct PaintAreaActions::Private
{
	Drawing::PaintArea *paintArea;
	
	QSpinBox *previousOnionSkin;
	QSpinBox *nextOnionSkin;
};

/**
 * @~spanish
 * Crea la barra de herramientas con las acciones que afectan el area de dibujo @p paintarea.
 */
PaintAreaActions::PaintAreaActions(Drawing::PaintArea *paintArea, QWidget *parent)
 : QToolBar(tr("Paint area actions"), parent), d(new Private)
{
	d->paintArea = paintArea;
	
	QAction *drawGrid = addAction(DGui::IconLoader::self()->load("show-grid.svg"), tr("Draw grid"));
	drawGrid->setCheckable(true);
	drawGrid->setChecked(paintArea->drawGrid());
	connect(drawGrid, SIGNAL(toggled( bool )), this, SLOT(setDrawGrid(bool)));
	
	QAction *drawBorder = addAction(/*DGui::IconLoader::self()->load(""),*/ tr("Draw border"));
	drawBorder->setCheckable(true);
	drawBorder->setChecked(paintArea->drawBorder());
	connect(drawBorder, SIGNAL(toggled( bool )), this, SLOT(setDrawBorder(bool)));
	
	QAction *antialiasing = addAction(/*DGui::IconLoader::self()->load(""),*/ tr("Antialiasing"));
	antialiasing->setCheckable(true);
	antialiasing->setChecked(paintArea->renderHints() & QPainter::Antialiasing);
	
	connect(antialiasing, SIGNAL(toggled( bool )), this, SLOT(setAntialiasing(bool)));
	
	addSeparator();
	
	d->previousOnionSkin = new QSpinBox;
	d->previousOnionSkin->setPrefix(tr("Previous")+" ");
	d->previousOnionSkin->setSuffix(" "+tr("frames"));
	
	connect(d->previousOnionSkin, SIGNAL(valueChanged( int )), this, SLOT(setPreviousOnionSkin(int)));
	
	addWidget(d->previousOnionSkin);
	
	addSeparator();
	
	d->nextOnionSkin = new QSpinBox;
	d->nextOnionSkin->setPrefix(tr("Next")+" ");
	d->nextOnionSkin->setSuffix(" "+tr("frames"));
	connect(d->nextOnionSkin, SIGNAL(valueChanged( int )), this, SLOT(setNextOnionSkin(int)));
	
	addWidget(d->nextOnionSkin);
}

/**
 * Destructor
 */
PaintAreaActions::~PaintAreaActions()
{
	delete d;
}

/**
 * @internal
 * @~spanish
 * Si @p e es verdadero se dibujarï¿½ el area de dibujo, si es falso entonces no se dibujarï¿½.
 */
void PaintAreaActions::setDrawGrid(bool e)
{
	d->paintArea->setDrawGrid(e);
}

/**
 * @~spanish
 * Dibuja el borde del area de dibujo
 * 
 * @param e 
 */
void PaintAreaActions::setDrawBorder(bool e)
{
	d->paintArea->setDrawBorder(e);
}

/**
 * @~spanish
 * Dibuja usando antialiasing, es proceso de dibujar es mas costoso computacionalmente.
 * @param e 
 */
void PaintAreaActions::setAntialiasing(bool e)
{
	d->paintArea->setAntialiasing(e);
}

/**
 * @internal
 * @~spanish
 * Pone la cantida de papel cebolla previos que tendra el area.
 */
void PaintAreaActions::setPreviousOnionSkin(int v)
{
	d->paintArea->photogram()->setPreviousOnionSkinCount(v);
}

/**
 * @internal
 * @~spanish
 * Pone la cantida de papel cebolla siguientes que tendra el area.
 */
void PaintAreaActions::setNextOnionSkin(int v)
{
	d->paintArea->photogram()->setNextOnionSkinCount(v);
}

}

}
