/***************************************************************************
 *   Copyright (C) 2007 David Cuadrado                                     *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/

#include "projectview.h"
#include "model/project.h"
#include "model/scene.h"
#include "model/layer.h"
#include "model/frame.h"
#include "model/object.h"
#include "model/location.h"

#include "private/projectitemmodel.h"

#include <QHeaderView>
#include <QGraphicsItem>

namespace YAMF {

namespace Gui {


struct ProjectView::Private
{
	ProjectItemModel *model;
};

 /**
 * @~spanish
 * Crea el visualizador del proyecto.
  */
ProjectView::ProjectView(QWidget *parent)
 : QTreeView(parent), d(new Private)
{
	d->model = new ProjectItemModel(this);
	setModel(d->model);
	
	setup();
}

/**
 * @~spanish
 * Crea el visualizador del proyecto, con un proyecto @p project para ser visualizado.
 */
ProjectView::ProjectView(Model::Project *project, QWidget *parent)
 : QTreeView(parent), d(new Private)
{
	d->model = new ProjectItemModel(project, this);
	setModel(d->model);
	
	setup();
}

/**
 * @internal
 */
void ProjectView::setup()
{
	setWindowTitle(tr("Project view"));
	
	
	header()->hide();
	
	connect(d->model, SIGNAL(layoutAboutToBeChanged()), this, SLOT(onLayoutAboutToBeChanged()));
	connect(d->model, SIGNAL(layoutChanged()), this, SLOT(onLayoutChanged()));
	
	connect(this, SIGNAL(pressed(const QModelIndex &)), SLOT(emitItemPressed(const QModelIndex &)));
	connect(this, SIGNAL(clicked(const QModelIndex &)), SLOT(emitItemClicked(const QModelIndex &)));
	connect(this, SIGNAL(doubleClicked(const QModelIndex &)), SLOT(emitItemDoubleClicked(const QModelIndex &)));
	connect(this, SIGNAL(activated(const QModelIndex &)), SLOT(emitItemActivated(const QModelIndex &)));
	connect(this, SIGNAL(entered(const QModelIndex &)), SLOT(emitItemEntered(const QModelIndex &)));
	connect(this, SIGNAL(expanded(const QModelIndex &)), SLOT(emitItemExpanded(const QModelIndex &)));
	connect(this, SIGNAL(collapsed(const QModelIndex &)), SLOT(emitItemCollapsed(const QModelIndex &)));
}

/**
 * @~spanish
 * Destructor
 */
ProjectView::~ProjectView()
{
	delete d;
}

/**
 * @~spanish
 * Asigna un proyecto para visualizar.
 */
void ProjectView::setProject(Model::Project *project)
{
	d->model->setProject(project);
}

/**
 * @~spanish
 * Retorna el proyecto actualmente visualizado.
 */
Model::Project *ProjectView::project() const
{
	return d->model->project();
}

/**
 * @~spanish
 * recarga los valores de la interfaz grafica.
 */
void ProjectView::reload()
{
	d->model->reload();
}

void ProjectView::onLayoutAboutToBeChanged()
{
	setUpdatesEnabled(false);
}

void ProjectView::onLayoutChanged()
{
	expandAll();
	setUpdatesEnabled(true);
}

void ProjectView::emitItemPressed(const QModelIndex &index)
{
	Q_UNUSED(index);
}

void ProjectView::emitItemClicked(const QModelIndex &index)
{
	ProjectViewItem *item = d->model->itemFromIndex(index);
	
	switch(item->type())
	{
		case ProjectViewItem::Scene:
		{
			Model::Location loc = d->model->sceneFromIndex(index)->location();
			emit sceneSelected(loc.value(Model::Location::Scene).toInt());
		}
		break;
		case ProjectViewItem::Layer:
		{
			Model::Location loc = d->model->layerFromIndex(index)->location();
			
			int scene = loc.value(Model::Location::Scene).toInt();
			int layer = loc.value(Model::Location::Layer).toInt();
			
			emit layerSelected(scene, layer);
		}
		break;
		case ProjectViewItem::Frame:
		{
			Model::Location loc = d->model->frameFromIndex(index)->location();
			
			int scene = loc.value(Model::Location::Scene).toInt();
			int layer = loc.value(Model::Location::Layer).toInt();
			int frame = loc.value(Model::Location::Frame).toInt();
			
			emit frameSelected(scene, layer, frame);
		}
		break;
		case ProjectViewItem::Object:
		{
			Model::Object *obj = d->model->objectFromIndex(index);
			
			Model::Location loc = obj->location();
			
			int scene = loc.value(Model::Location::Scene).toInt();
			int layer = loc.value(Model::Location::Layer).toInt();
			int frame = loc.value(Model::Location::Frame).toInt();
			int object = loc.value(Model::Location::Object).toInt();
			
			obj->item()->setFlag(QGraphicsItem::ItemIsSelectable, true);
			obj->item()->setSelected(true);
			
			emit objectSelected(scene, layer, frame, object);
		}
		break;
		default: break;
	}
}

void ProjectView::emitItemDoubleClicked(const QModelIndex &index)
{
	Q_UNUSED(index);
}

void ProjectView::emitItemActivated(const QModelIndex &index)
{
	Q_UNUSED(index);
}

void ProjectView::emitItemEntered(const QModelIndex &index)
{
	Q_UNUSED(index);
}

void ProjectView::emitItemExpanded(const QModelIndex &index)
{
	Q_UNUSED(index);
}

void ProjectView::emitItemCollapsed(const QModelIndex &index)
{
	Q_UNUSED(index);
}


}

}
