/***************************************************************************
 *   Copyright (C) 2007 David Cuadrado                                     *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/

#include "pen.h"

#include <QComboBox>
#include <QPen>
#include <QVBoxLayout>

#include <dgui/linespinbox.h>
#include <dgui/iconloader.h>

namespace YAMF {

namespace Gui {

struct Pen::Private
{
	DGui::LineSpinBox *thickness;
	QComboBox *capStyle;
	QComboBox *joinStyle;
	QComboBox *style;
	QPen pen;
};

/**
 * @~spanish
 * Construye la Interfaz grafica.
 */
Pen::Pen(QWidget *parent)
 : QWidget(parent), d(new Private)
{
	setWindowTitle( tr("Pen"));
	
	QVBoxLayout *layout = new QVBoxLayout(this);
	layout->setSpacing(1);
	layout->setMargin(3);
	
	d->thickness = new DGui::LineSpinBox;
	d->thickness->setMinimum(1);
	d->thickness->setMaximum(100);
	d->thickness->setValue(3);
	d->thickness->setTitle(tr("Thickness") );
	
	connect(d->thickness, SIGNAL(valueChanged( int )), this, SLOT(setThickness(int)));
	
	layout->addWidget( d->thickness );
	
	
	d->style = new QComboBox();
	
	d->style->addItem( tr("No pen"), Qt::NoPen);
	d->style->addItem( tr("Solid"), Qt::SolidLine);
	d->style->addItem( tr("Dash"), Qt::DashLine );
	d->style->addItem( tr("Dot"), Qt::DotLine );
	d->style->addItem( tr("Dash dot"), Qt::DashDotLine);
	d->style->addItem( tr("Dash dot dot"), Qt::DashDotDotLine);
	
	layout->addWidget(d->style);
	
	connect(d->style, SIGNAL(currentIndexChanged( int )), this, SLOT(setStyle(int)) );
	
	d->capStyle = new QComboBox();
	
	d->capStyle->addItem( DGui::IconLoader::self()->load("pen-flatcap.svg"), tr("Flat"), Qt::FlatCap);
	d->capStyle->addItem( DGui::IconLoader::self()->load("pen-squarecap.svg"), tr("Square"), Qt::SquareCap);
	d->capStyle->addItem( DGui::IconLoader::self()->load("pen-roundcap.svg"), tr("Round"), Qt::RoundCap);
	
	layout->addWidget(d->capStyle);
	connect(d->capStyle, SIGNAL(currentIndexChanged( int )), this, SLOT(setCapStyle(int)) );
	
	d->joinStyle = new QComboBox();
	
	d->joinStyle->addItem(DGui::IconLoader::self()->load("pen-miterjoin.svg"), tr("Miter"), Qt::MiterJoin);
	d->joinStyle->addItem(DGui::IconLoader::self()->load("pen-beveljoin.svg"), tr("Bevel"), Qt::BevelJoin);
	d->joinStyle->addItem(DGui::IconLoader::self()->load("pen-roundjoin.svg"), tr("Round"), Qt::RoundJoin);
	
	layout->addWidget( d->joinStyle );
	connect(d->joinStyle, SIGNAL(currentIndexChanged( int )), this, SLOT(setJoinStyle(int)) );
	
	layout->addStretch(2);
	
	setPen(QPen());
}

/**
 * Destructor
 */
Pen::~Pen()
{
	delete d;
}

/**
 * @~spanish
 * Pone un estilo de linea, colocando las propiedades del estilo de linea @p pen en las interfaz grafica.
 */
void Pen::setPen(const QPen &pen)
{
	d->pen = pen;
	
	d->style->setCurrentIndex( d->style->findData( pen.style() ) );
	d->joinStyle->setCurrentIndex(d->joinStyle->findData( pen.joinStyle() ) );
	d->capStyle->setCurrentIndex(d->capStyle->findData( pen.capStyle() ) );
}

/**
 * @~spanish
 * Retorna el estilo de linea actualmente 
 */
QPen Pen::pen() const
{
	return d->pen;
}

/**
 * @~spanish
 * Pone los valores por defecto del estilo de linea.
 */
void Pen::reset()
{
	blockSignals(true);
	d->capStyle->setCurrentIndex( 2 );
	d->joinStyle->setCurrentIndex( 2 );
	setThickness( 3 );
	blockSignals(false);
	d->style->setCurrentIndex( 1 );
	
	d->pen.setColor(QColor() ); // invalid color
}

/**
 * @~spanish
 * Cambia el grosor de la linea.
 */
void Pen::setThickness(int value)
{
	d->pen.setWidth(value);
	emit penChanged( d->pen );
}

/**
 * @~spanish
 * Cambia la continuidad de la linea ver <a href="http://doc.trolltech.com/4.3/qt.html#PenStyle-enum">ver</a>.
 */
void Pen::setStyle(int s)
{
	d->pen.setStyle( Qt::PenStyle(d->style->itemData(s).toInt()) );
	emit penChanged( d->pen );
}

/**
 * @~spanish
 * Cambia el estilo de reunion de la linea
 * <a href="http://doc.trolltech.com/4.3/qt.html#PenJoinStyle-enum">ver</a>
 */
void Pen::setJoinStyle(int s)
{
	d->pen.setJoinStyle(Qt::PenJoinStyle(d->joinStyle->itemData(s).toInt()) );
	emit penChanged( d->pen );
}

/**
 * @~spanish
 * Cambia el estilo del borde de la linea.
 * http://doc.trolltech.com/4.3/qt.html#PenCapStyle-enum
 */
void Pen::setCapStyle(int s )
{
	d->pen.setCapStyle(Qt::PenCapStyle(d->capStyle->itemData(s).toInt()) );
	emit penChanged( d->pen );
}

}

}
