/***************************************************************************
 *   Copyright (C) 2007 David Cuadrado                                     *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/

#include "toolswidget.h"
#include <drawing/paintarea.h>

#include <drawing/abstracttool.h>
#include <dgui/action.h>

#include <QActionGroup>

namespace YAMF {

namespace Gui {

struct ToolsWidget::Private
{
	Drawing::PaintArea *paintArea;
	QActionGroup *group;
};

/**
 * @~spanish
 * Crea la barra de herramientas.
 */
ToolsWidget::ToolsWidget(Drawing::PaintArea *paintArea, QWidget *parent)
 : DGui::ConfigurableToolBar(parent), d( new Private )
{
	d->group = new QActionGroup(this);
	
	d->paintArea = paintArea;
	connect(this, SIGNAL(actionTriggered( QAction*)), this, SLOT(toolFromAction(QAction *)));
	
	d->group->setExclusive(true);
}

/**
 * Destructor.
 */
ToolsWidget::~ToolsWidget()
{
	delete d;
}

/**
 * @~spanish
 * Añade una herramienda al ToolBar.
 * @param tool 
 */
void ToolsWidget::addTool(Drawing::AbstractTool *tool)
{
	QAction *act = d->group->addAction(tool->action());
	act->setCheckable(true);
	addAction(act);
	
}

/**
 * @~spanish
 * Asigna la herramienta seleccionada en el area de dibujo.
 */
void ToolsWidget::toolFromAction(QAction *act)
{
	if( Drawing::AbstractTool *tool = dynamic_cast<Drawing::AbstractTool *>(act->parent()) )
	{
		d->paintArea->setTool(tool);
	}
}

}

}
