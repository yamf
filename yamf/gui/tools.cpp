/***************************************************************************
 *   Copyright (C) 2007 David Cuadrado                                     *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/

#include "tools.h"

#include "drawing/tool/brush.h"
#include "drawing/tool/select.h"
#include "drawing/tool/contour.h"
#include "drawing/tool/polyline.h"
#include "drawing/tool/polygons.h"
#include "drawing/tool/geometric.h"
#include "drawing/tool/text.h"
#include "drawing/tool/colorpicker.h"
#include "drawing/tool/zoom.h"
#include "drawing/tool/hand.h"
#include "drawing/tool/fill.h"
#include "drawing/tool/itween.h"

namespace YAMF {
namespace Gui {

struct Tools::Private
{
};

/**
 * @~spanish
 * Crea el selector de herramientas.
 */
Tools::Tools(Drawing::PaintArea *paintArea, QWidget *parent)
 : ToolsWidget(paintArea, parent), d(new Private)
{
	addTool(new YAMF::Drawing::Tool::Brush(this));
	addTool(new YAMF::Drawing::Tool::Contour(this));
	addTool(new YAMF::Drawing::Tool::Select(this));
	addTool(new YAMF::Drawing::Tool::PolyLine(this));
	addTool(new YAMF::Drawing::Tool::Geometric(this));
	addTool(new YAMF::Drawing::Tool::Polygons(this));
	addTool(new YAMF::Drawing::Tool::Text(this));
	addTool(new YAMF::Drawing::Tool::ColorPicker(this));
	addTool(new YAMF::Drawing::Tool::Zoom(this));
	addTool(new YAMF::Drawing::Tool::Hand(this));
	addTool(new YAMF::Drawing::Tool::Fill(this));
	addTool(new YAMF::Drawing::Tool::ITween(this));
}

/**
 * Destructor
 */
Tools::~Tools()
{
	delete d;
}


}

}
