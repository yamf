/***************************************************************************
 *   Copyright (C) 2007 David Cuadrado                                     *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/

#ifndef YAMF_GUIPEN_H
#define YAMF_GUIPEN_H

#include <QWidget>
#include <yamf/common/yamf_exports.h>

namespace YAMF {
namespace Gui {

/**
 * @ingroup gui
 * @~spanish
 * @brief Esta clase provee de una interfaz grafica para modificar el estilo del linea.
 * @author David Cuadrado <krawek@gmail.com>
 */
class YAMF_EXPORT Pen : public QWidget
{
	Q_OBJECT;
	public:
		Pen(QWidget *parent = 0);
		~Pen();
		
		void setPen(const QPen &pen);
		QPen pen() const;
		
	public slots:
		void reset();
		
	private slots:
		void setThickness(int value);
		void setStyle(int s);
		void setJoinStyle(int s);
		void setCapStyle(int s );
		
	signals:
		void penChanged(const QPen &pen);
		
	private:
		struct Private;
		Private *const d;
};

}

}

#endif
