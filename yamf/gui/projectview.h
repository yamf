/***************************************************************************
 *   Copyright (C) 2007 David Cuadrado                                     *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/

 

#ifndef YAMF_GUIPROJECTVIEW_H
#define YAMF_GUIPROJECTVIEW_H

#include <yamf/common/yamf_exports.h>
#include <QTreeView>

namespace YAMF {

namespace Model {
class Project;
}

namespace Gui {

/**
 * @ingroup gui
 * @~spanish
 * @brief Esta clase prevee de una interfaz grafica para visualizar el proyecto en forma de arbol, permitiendo poder ver como esta confromado el proyecto, sus scenas,  layers, frames y objetos graficos.
 * @author David Cuadrado <krawek@gmail.com>
 */
class YAMF_EXPORT ProjectView : public QTreeView
{
	Q_OBJECT
	public:
		ProjectView(QWidget *parent = 0);
		ProjectView(Model::Project *project, QWidget *parent = 0);
		
	private:
		void setup();
		
	public:
		~ProjectView();
		
		void setProject(Model::Project *project);
		Model::Project *project() const;
		
		virtual void reload();
		
	protected slots:
		void onLayoutAboutToBeChanged();
		void onLayoutChanged();
		
	private slots:
		void emitItemPressed(const QModelIndex &index);
		void emitItemClicked(const QModelIndex &index);
		void emitItemDoubleClicked(const QModelIndex &index);
		void emitItemActivated(const QModelIndex &index);
		void emitItemEntered(const QModelIndex &index);
		void emitItemExpanded(const QModelIndex &index);
		void emitItemCollapsed(const QModelIndex &index);
		
	signals:
		void sceneSelected(int scene);
		void layerSelected(int scene, int layer);
		void frameSelected(int scene, int layer, int frame);
		void objectSelected(int scene, int layer, int frame, int object);
		
	private:
		struct Private;
		Private *const d;
};

}

}

#endif
