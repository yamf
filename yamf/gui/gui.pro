
DEFINES += _BUILD_YAMF_ 
TEMPLATE = lib

CONFIG += dll \
warn_on

include(../../config.pri)
TARGET = yamf_gui

LIBS += -lyamf_drawing -lyamf_model -lyamf_item -lyamf_common

INCLUDEPATH += ..

INSTALLS += target headers
target.path = /lib/

headers.path = /include/yamf/gui
headers.files = *.h

HEADERS += projectview.h private/projectitemmodel.h \
pen.h \
tools.h \
toolswidget.h \
drawingactions.h \
paintareaactions.h 
SOURCES += projectview.cpp private/projectitemmodel.cpp \
pen.cpp \
tools.cpp \
toolswidget.cpp \
drawingactions.cpp \
paintareaactions.cpp 

