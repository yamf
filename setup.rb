
def configure(setup)
  setup.qt >= 4.3
  setup.add_option(:name => "dlib-dir", :type => "path", :optional => true, :description => "Sets the dlib dir")
  setup.find_package(:name => "dlib", :optional => false, :global => true)
  
  setup.add_test(:id => "dlibswf", :name => "SWF Generator", :defines  => ["DLIB_HAVE_SWFGENERATOR"], :optional => true, :headers => ["dgraphics/swfgenerator.h"], :custom => "int main() { DGraphics::SwfGenerator swf(300,300); }"  )
  
  setup.add_test(:id => "dlibffmpeg", :name => "FFMPEG Generator", :defines  => ["DLIB_HAVE_FFMPEGGENERATOR"], :optional => true, :headers => ["dgraphics/ffmpegmoviegenerator.h"], :custom => "int main() { DGraphics::FFMpegMovieGenerator g(DGraphics::MovieGenerator::AVI, 540, 340, 5); }"  )
  
  setup.generate_pkgconfig(:package_name => "yamf", :name => "YAMF", :description => "", :version => "0.1beta", :libdir => nil, :includedir => nil, :libs => "-L${libdir} -lyamf_common  -lyamf_item -lyamf_model -lyamf_drawing -lyamf_render -lyamf_gui -lyamf_effect", :cflags => "-I${includedir}", :requires => ["dlib"] )
  
#   setup.generate_pkgconfig(:package_name => "dlib-core", :name => "DLib Qt library", :description => "A extension for Qt4", :version => "0.1beta", :libdir => nil, :includedir => nil, :libs => "-L${libdir} -ldcore", :cflags => "-I${includedir}")
#   
#   setup.generate_pkgconfig(:package_name => "dlib-gui", :name => "DLib Qt library", :description => "A extension for Qt4", :version => "0.1beta", :libdir => nil, :includedir => nil, :libs => "-L${libdir} -ldcore -ldgui", :cflags => "-I${includedir}")
#   setup.generate_pkgconfig(:package_name => "dlib-media", :name => "DLib Qt library", :description => "A extension for Qt4", :version => "0.1beta", :libdir => nil, :includedir => nil, :libs => "-L${libdir} -ldcore -ldgui -ldmedia", :cflags => "-I${includedir}")
#   setup.generate_pkgconfig(:package_name => "dlib-ideality", :name => "DLib Qt library", :description => "A extension for Qt4", :version => "0.1beta", :libdir => nil, :includedir => nil, :libs => "-L${libdir} -ldcore -ldgui -lideality", :cflags => "-I${includedir}")
#   
#   setup.generate_pkgconfig(:package_name => "dlib-editor", :name => "DLib Qt library", :description => "A extension for Qt4", :version => "0.1beta", :libdir => nil, :includedir => nil, :libs => "-L${libdir} -ldcore -ldgui -leditor", :cflags => "-I${includedir}")
end

def setup_pkgconfig(pkgconfig, args)
  case pkgconfig.id
    when "dlib"
      pkgconfig.add_search_path(args["dlib-dir"]+"/lib/pkgconfig")
  end
end

def setup_test(id, test, args)
end

def setup_config(cfg, args)
  cfg.add_define("__STDC_CONSTANT_MACROS")
  cfg.add_include_path(Dir.getwd)
  cfg.add_qtmodule("xml")
  cfg.add_qtmodule("svg")
  cfg.add_qtmodule("opengl")
end


