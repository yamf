SUBDIRS += yamf \
examples \
tests
TEMPLATE = subdirs 
CONFIG += warn_on \
          qt \
          thread 


INSTALLS += data pc

data.path = /share/yamf/
data.files += data/icons

pc.path = /lib/pkgconfig
pc.files = yamf.pc

