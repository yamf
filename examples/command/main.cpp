
#include <QApplication>
#include "mainwindow.h"

int main(int argc, char** argv)
{
	QApplication app(argc, argv);
	app.setApplicationName("yamf_command_example");
	
	MainWindow mw;
	mw.show();
	
	return app.exec();
}

