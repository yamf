/***************************************************************************
 *   Copyright (C) 2007 David Cuadrado                                     *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/

#include "mainwindow.h"

#include <drawing/paintarea.h>
#include <drawing/photogram.h>

#include <dcore/algorithm.h>
#include <dcore/debug.h>

#include <dgui/iconloader.h>
#include <dgui/commandhistory.h>

#include <model/project.h>
#include <model/scene.h>
#include <model/layer.h>
#include <model/frame.h>
#include <model/object.h>

#include <model/command/manager.h>

#include <item/rect.h>

#include <gui/drawingactions.h>

#include <QDockWidget>
#include <QGridLayout>
#include <QDir>
#include <QToolBar>

#include "commandobserver.h"

MainWindow::MainWindow(QWidget *parent)
 : QMainWindow(parent)
{
	m_project = new YAMF::Model::Project(this);
	m_project->commandManager()->setObserver(new CommandObserver());
	
	YAMF::Model::Scene *scn = m_project->createScene();
	YAMF::Model::Layer *lyr = scn->createLayer();
	/*YAMF::Model::Frame *frm = */lyr->createFrame();
	
	
	m_view = new YAMF::Drawing::View(m_project);
	YAMF::Drawing::PaintArea *paintarea = m_view->paintArea();
	paintarea->setDragMode(QGraphicsView::RubberBandDrag);
	
	paintarea->setCurrentFrame(0, 0, 0);
	
	setCentralWidget(m_view);
	
	YAMF::Gui::DrawingActions *toolbar = new YAMF::Gui::DrawingActions(paintarea);
	toolbar->addAction(tr("Add shape"), this, SLOT(addShape()));
	
	addToolBar( toolbar );
}


MainWindow::~MainWindow()
{
}


void MainWindow::addShape()
{
	if( YAMF::Model::Frame *frame = m_view->paintArea()->currentFrame())
	{
		YAMF::Item::Rect *rect = new YAMF::Item::Rect;
		
		rect->setFlags(QGraphicsItem::ItemIsSelectable | QGraphicsItem::ItemIsMovable);
		rect->setRect(QRect(0,0,DCore::Algorithm::random(200)+10,DCore::Algorithm::random(200)+10));
		
		rect->setPos(DCore::Algorithm::random((int)m_view->paintArea()->drawingRect().width())+10, DCore::Algorithm::random((int)m_view->paintArea()->drawingRect().height())+10);
		
		rect->setBrush(DCore::Algorithm::randomColor());
		
		frame->addItem(rect);
	}
}
