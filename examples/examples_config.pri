
INCLUDEPATH += ../../yamf
LIBS += -L../../lib -lyamf_common -lyamf_model -lyamf_drawing -lyamf_item -lyamf_effect -lyamf_gui

QT += xml

!include(../../config.pri){
    error("run configure first!")
}

