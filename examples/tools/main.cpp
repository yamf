
#include <QApplication>


#include <dgui/application.h>
#include "mainwindow.h"

int main(int argc, char** argv)
{
	DGui::Application app(argc, argv);
	app.setApplicationName("yamf_tools_example");
	
	MainWindow mw;
	mw.show();
	
	return app.exec();
}

