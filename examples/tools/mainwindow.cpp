/***************************************************************************
 *   Copyright (C) 2007 David Cuadrado                                     *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/

#include "mainwindow.h"

#include <common/yamf.h>

#include <drawing/paintarea.h>
#include <drawing/photogram.h>
#include <drawing/brushmanager.h>

#include <dcore/algorithm.h>
#include <dcore/debug.h>

#include <dgui/iconloader.h>

#include <model/project.h>
#include <model/scene.h>
#include <model/layer.h>
#include <model/audiolayer.h>
#include <model/frame.h>
#include <model/object.h>

#include <model/command/manager.h>
#include <model/command/insertscene.h>
#include <model/command/removescene.h>

#include <item/proxy.h>
#include <item/rect.h>

#include <gui/pen.h>
#include <gui/tools.h>

#include <QDockWidget>
#include <QGridLayout>
#include <QDir>
#include <QListWidgetItem>

MainWindow::MainWindow(QWidget *parent)
 : QMainWindow(parent)
{
	YAMF::initialize();
	
	YAMF::Model::Project *project = new YAMF::Model::Project(this);
	YAMF::Model::Scene *scn = project->createScene();
	YAMF::Model::Layer *lyr = scn->createLayer();
	YAMF::Model::Frame *frm = lyr->createFrame();
	
	YAMF::Item::Proxy *proxy = new YAMF::Item::Proxy;
	YAMF::Item::Rect *rect = new YAMF::Item::Rect;
	proxy->setItem( rect );
	
	rect->setFlags(QGraphicsItem::ItemIsSelectable | QGraphicsItem::ItemIsMovable);
	rect->setRect(QRect(10,10, 100,100));
	rect->setBrush(Qt::red);
	
	frm->addItem(proxy);
	
	m_view = new YAMF::Drawing::View(project);
	YAMF::Drawing::PaintArea *paintarea = m_view->paintArea();
	paintarea->brushManager()->setPen(QPen(Qt::red));
	
	paintarea->setCurrentFrame(0, 0, 0);
	
	setCentralWidget(m_view);
	
	addToolBar(Qt::TopToolBarArea, paintarea->toolsConfigBar());
	addToolBar(Qt::TopToolBarArea, paintarea->effectsConfigBar());
	
	addToolBar(Qt::LeftToolBarArea, new YAMF::Gui::Tools(paintarea, this));
	addToolBar(Qt::BottomToolBarArea, new YAMF::Gui::ToolsWidget(paintarea, this));
	
	{
		QDockWidget *dock = new QDockWidget;
		YAMF::Gui::Pen *setupPenWidget = new YAMF::Gui::Pen;
		connect(setupPenWidget, SIGNAL(penChanged(const QPen&)), m_view->paintArea()->brushManager(), SLOT(setPen(const QPen &)));
		
		dock->setWidget(setupPenWidget);
		
		addDockWidget(Qt::LeftDockWidgetArea, dock);
	}
}


MainWindow::~MainWindow()
{
}


