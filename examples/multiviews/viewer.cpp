/***************************************************************************
 *   Copyright (C) 2007 Jorge Cuadrado                                     *
 *   kuadrosxx@gmail.com                                                   *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/
 
#include "viewer.h"

#include <dcore/debug.h>

#include <drawing/paintarea.h>
#include <drawing/photogram.h>

#include <model/scene.h>
#include <model/layer.h>
#include <model/frame.h>
#include <model/object.h>

#include <item/rect.h>

#include "drawing/tool/brush.h"

#include <QSplitter>
#include <QVBoxLayout>

Viewer::Viewer(QWidget *parent) : QWidget(parent)
{
	QVBoxLayout *layout = new QVBoxLayout(this);
	
	QSplitter *container = new QSplitter;
	layout->addWidget(container);
	
	m_project = new YAMF::Model::Project(this);
	
	YAMF::Model::Scene *scn = m_project->createScene();
	YAMF::Model::Layer *lyr = scn->createLayer();
	YAMF::Model::Frame *frm = lyr->createFrame();
	
	
	YAMF::Item::Rect *rect = new YAMF::Item::Rect;
	rect->setRect(QRect(0,0, 50, 50));
	/*YAMF::Model::Object *rectObject = */frm->addItem(rect);
	
	m_view1 = new YAMF::Drawing::View(m_project);
	YAMF::Drawing::PaintArea *paintArea = m_view1->paintArea();
	
	paintArea->setCurrentFrame(0, 0, 0);
	YAMF::Drawing::Tool::Brush *brush = new YAMF::Drawing::Tool::Brush(paintArea);
	paintArea->setTool(brush);
	
	m_view2 = new YAMF::Drawing::View(m_project);
	
// 	m_view2->paintArea()->setCurrentFrame(0, 0, 0);
	m_view2->paintArea()->setScene(paintArea->scene());
	
	YAMF::Drawing::Tool::Brush *brush2 = new YAMF::Drawing::Tool::Brush(paintArea);
	m_view2->paintArea()->setTool(brush2);
	
	container->addWidget(m_view1);
	container->addWidget(m_view2);
}

Viewer::~Viewer()
{
}
