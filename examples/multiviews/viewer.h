/***************************************************************************
 *   Copyright (C) 2007 Jorge Cuadrado                                     *
 *   kuadrosxx@gmail.com                                                   *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/
 
#ifndef VIEWER_H
#define VIEWER_H

#include <QWidget>

#include <model/project.h>
#include <drawing/view.h>

/**
 * @author Jorge Cuadrado <kuadrosxx@gmail.com>
*/
class Viewer : public QWidget
{
	Q_OBJECT
	public:
		Viewer(QWidget *parent = 0);
		~Viewer();
		
	private:
		YAMF::Drawing::View *m_view1;
		YAMF::Drawing::View *m_view2;
		
		YAMF::Model::Project *m_project;
		
};

#endif
