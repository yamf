/***************************************************************************
 *   Copyright (C) 2007 David Cuadrado                                     *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc.,                                                     *
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              *
 ***************************************************************************/

#include "mainwindow.h"

#include <drawing/paintarea.h>
#include <drawing/photogram.h>

#include <dcore/algorithm.h>
#include <dcore/debug.h>

#include <dgui/iconloader.h>

#include <model/project.h>
#include <model/scene.h>
#include <model/layer.h>
#include <model/audiolayer.h>
#include <model/frame.h>
#include <model/object.h>

#include <drawing/tool/brush.h>


#include <model/command/manager.h>
#include <model/command/insertscene.h>
#include <model/command/removescene.h>

#include <QDockWidget>
#include <QGridLayout>
#include <QDir>
#include <QListWidgetItem>

MainWindow::MainWindow(QWidget *parent)
 : QMainWindow(parent)
{
	{
		QDockWidget *explorer = new QDockWidget(tr("Project explorer"), this);
		QWidget *w = new QWidget;
		explorerUi.setupUi(w);
		explorer->setWidget(w);
		
		addDockWidget(Qt::LeftDockWidgetArea, explorer);
	}
	
	{
		QDockWidget *scenes = new QDockWidget(tr("Scenes"), this);
		QWidget *w = new QWidget;
		scenesUi.setupUi(w);
		scenes->setWidget(w);
		
		connect(scenesUi.add,SIGNAL(clicked()), this, SLOT(addScene()));
		scenesUi.add->setIcon(DGui::IconLoader::self()->load("list-add.svg"));
		
		connect(scenesUi.remove,SIGNAL(clicked()), this, SLOT(removeScene()));
		scenesUi.remove->setIcon(DGui::IconLoader::self()->load("list-remove.svg"));
		
		scenesUi.moveUp->setIcon(DGui::IconLoader::self()->load("go-up.svg"));
		
		scenesUi.moveDown->setIcon(DGui::IconLoader::self()->load("go-down.svg"));
		
		addDockWidget(Qt::RightDockWidgetArea, scenes);
	}
	
	m_project = new YAMF::Model::Project(this);
	YAMF::Model::Scene *scn = m_project->createScene();
	YAMF::Model::Layer *lyr = scn->createLayer();
	YAMF::Model::Frame *frm = lyr->createFrame();
	
	m_view = new YAMF::Drawing::View(m_project);
	YAMF::Drawing::PaintArea *paintarea = m_view->paintArea();
	
	paintarea->setCurrentFrame(0, 0, 0);
	
	YAMF::Drawing::Tool::Brush *brush = new YAMF::Drawing::Tool::Brush(paintarea);
	paintarea->setTool(brush);
	
	
	setCentralWidget(m_view);
	
	explorerUi.tree->setProject(m_project);
}


MainWindow::~MainWindow()
{
}


void MainWindow::addScene()
{
	m_project->createScene();
/*	m_view->commandManager()->addCommand(new YAMF::Command::AddScene("scene", m_project));
	
	QListWidgetItem *sceneItem = new QListWidgetItem(scenesUi.list);
	sceneItem->setText(tr("scene"));
	
	D_SHOW_VAR(m_project->scenes().count());*/
}

void MainWindow::removeScene()
{
	/*
	if(!m_project->scenes().isEmpty())
	{
		m_view->commandManager()->addCommand(new YAMF::Command::RemoveScene(m_project->scenes().values().last(), m_project));
		D_SHOW_VAR(m_project->scenes().count());
	}
	*/
}


