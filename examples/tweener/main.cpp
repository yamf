
#include <dgui/application.h>

#include "viewer.h"


int main(int argc, char** argv)
{
	DGui::Application app(argc, argv);
	app.setApplicationName("yamf_tweener_example");
	Viewer viewer;
	viewer.show();
	
	return app.exec();
}

