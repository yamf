
#include "viewer.h"

#include <dcore/debug.h>

#include <drawing/paintarea.h>
#include <drawing/photogram.h>

#include <model/scene.h>
#include <model/layer.h>
#include <model/frame.h>
#include <model/object.h>

#include <item/rect.h>
#include <item/ellipse.h>
#include <item/path.h>

#include <QVBoxLayout>

Viewer::Viewer(QWidget *parent) : QWidget(parent), m_timeline(2000)
{
	QVBoxLayout *layout = new QVBoxLayout(this);
	
	m_project = new YAMF::Model::Project(this);
	
	YAMF::Model::Scene *scn = m_project->createScene();
	YAMF::Model::Layer *lyr = scn->createLayer();
	YAMF::Model::Frame *frm = lyr->createFrame();
	
	m_view = new YAMF::Drawing::View(m_project);
	YAMF::Drawing::PaintArea *paintArea = m_view->paintArea();
	paintArea->setRenderHint(QPainter::Antialiasing);
	
	{
		YAMF::Item::TweenerStep step1(0);
		step1.setPosition(QPoint(400,200));
		{
			QColor c(Qt::red);
			c.setAlpha(255);
			step1.setBrush(c);
		}
		
		step1.setPen(QPen(QBrush(Qt::blue), 1 ));
		
		YAMF::Item::TweenerStep step2(100);
		step2.setPosition(QPoint(100,100));
		step2.setRotation(70);
		{
			QColor c(Qt::green);
			c.setAlpha(80);
			step2.setBrush(c);
		}
		step2.setPen(QPen(QBrush(Qt::green), 5 ));
		
		YAMF::Item::TweenerStep step3(200);
		step3.setPosition(QPoint(300,200));
		step3.setRotation(180);
		step3.setScale(2, 2);
		{
			QColor c(Qt::blue);
			c.setAlpha(255);
			step3.setBrush(c);
		}
		step3.setPen(QPen(QBrush(Qt::red), 10 ));
		
		m_tweener = new YAMF::Item::Tweener(200, this);
		m_tweener->addStep(step1);
		m_tweener->addStep(step2);
		m_tweener->addStep(step3);
	}
	
	YAMF::Item::Ellipse *ellipse = new YAMF::Item::Ellipse;
	ellipse->setRect(QRect(0,0, 50, 70));
	
	YAMF::Model::Object *rectObject = frm->addItem(ellipse);
	rectObject->setTweener(m_tweener);
	
	{
		QPainterPath rect;
		rect.addRect(QRect(0,0, 50, 70));
		
		YAMF::Item::Path *path = new YAMF::Item::Path;
		
		QLinearGradient linearGrad(QPointF(0, 0), QPointF(50, 70));
		linearGrad.setColorAt(0, Qt::red);
		linearGrad.setColorAt(1, Qt::blue);
		path->setBrush(linearGrad);
		
		path->setPath(rect);
		
		YAMF::Item::TweenerStep step1(0);
		step1.setPosition(QPoint(10,10));
		step1.setPath(rect);
		
		YAMF::Item::TweenerStep step2(200);
		step2.setPosition(QPoint(370,300));
		
		rect.setElementPositionAt(1, 50, 70);
		
		rect.setElementPositionAt(0, 0, 70);
		rect.setElementPositionAt(4, 0, 70);
		
		rect.setElementPositionAt(2, 50+40, 0);
		rect.setElementPositionAt(3, 0+40, 0);
		
		
		step2.setPath(rect);
		
		m_pathTweener = new YAMF::Item::Tweener(200, this);
		m_pathTweener->addStep(step1);
		m_pathTweener->addStep(step2);
		
		YAMF::Model::Object *object = frm->addItem(path);
		object->setTweener(m_pathTweener);
	}
	
	paintArea->setCurrentFrame(0, 0, 0);

	layout->addWidget(m_view);
	
	connect(&m_timeline, SIGNAL(frameChanged(int)), this, SLOT(changeStep(int)));
	m_timeline.setLoopCount(0);
	m_timeline.setFrameRange( 0, 200);
	
	int fps = 40;
	m_timeline.setDuration((1000/fps)*200);;
	
	m_timeline.setCurveShape(QTimeLine::SineCurve);
	m_timeline.start ();
}

Viewer::~Viewer()
{
}

void Viewer::changeStep(int step)
{
	m_tweener->setStep(step);
	m_pathTweener->setStep(step);
}


