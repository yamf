
#ifndef VIEWER_H
#define VIEWER_H

#include <QWidget>

#include <model/project.h>
#include <drawing/view.h>
#include <item/tweener.h>

#include <QTimeLine>

/**
 * @author Jorge Cuadrado <kuadrosxx@gmail.com>
*/
class Viewer : public QWidget
{
	Q_OBJECT
	public:
		Viewer(QWidget *parent = 0);
		~Viewer();
		
	public slots:
		void changeStep(int step);
		
	private:
		YAMF::Drawing::View *m_view;
		YAMF::Model::Project *m_project;
		
		YAMF::Item::Tweener *m_tweener;
		YAMF::Item::Tweener *m_pathTweener;
		
		QTimeLine m_timeline;
};

#endif
