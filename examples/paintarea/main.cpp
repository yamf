
#include <QApplication>
#include <QGridLayout>
#include <QDir>

#include <dcore/algorithm.h>
#include <dgui/application.h>

#include <drawing/view.h>
#include <drawing/paintarea.h>
#include <drawing/photogram.h>

#include <model/project.h>
#include <model/scene.h>
#include <model/layer.h>

#include <model/frame.h>
#include <model/object.h>

#include <model/audiolayer.h>

#include <item/button.h>
#include <item/rect.h>
#include <item/polygon.h>

int main(int argc, char** argv)
{
	DGui::Application app(argc, argv);
	app.setApplicationName("yamf_paintarea_example");
	
	YAMF::Model::Project project;
	YAMF::Model::Scene *scn = project.createScene(0);
	YAMF::Model::Layer *lyr = scn->createLayer(0);
	YAMF::Model::Frame *frm = lyr->createFrame(0);
	
	YAMF::Model::AudioLayer *sound = scn->createAudioLayer(0);
	sound->fromFile("laughing.ogg");
	sound->setVolume(20);
	sound->play();
	
	YAMF::Drawing::View *view = new YAMF::Drawing::View(&project);
	YAMF::Drawing::PaintArea *paintarea = view->paintArea();
	paintarea->setCurrentFrame(0, 0, 0);
	
	YAMF::Item::Button *button = new YAMF::Item::Button;
	frm->addItem(button);
	
	YAMF::Item::Polygon *polygon = new YAMF::Item::Polygon(6, 0, QRect(0, 0, 100, 100));
	polygon->setPos(10,10);
	frm->addItem(polygon);
	polygon->setBrush(Qt::green);
	
	polygon = new YAMF::Item::Polygon(5, 0.03, QRect(0, 0, 100, 100));
	polygon->setPos(100,100);
	frm->addItem(polygon);
	
	polygon->setBrush(Qt::red);
	
	/*
	for(int i = 0; i < 50; i++ )
	{
		YAMF::Item::Rect *rect = new YAMF::Item::Rect;
		
		rect->setFlags(QGraphicsItem::ItemIsSelectable | QGraphicsItem::ItemIsMovable);
		rect->setRect(QRect(0,0,DCore::Algorithm::random(200)+10,DCore::Algorithm::random(200)+10));
		
		rect->setPos(DCore::Algorithm::random(paintarea->drawingRect().width())+10, DCore::Algorithm::random(paintarea->drawingRect().height())+10);
		
		rect->setBrush(DCore::Algorithm::randomColor(true));
		
		frm->addItem(rect);
	}
	*/
	paintarea->setDragMode(QGraphicsView::RubberBandDrag);
	
	view->show();
	
	return app.exec();
}

